<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/'],function(){
	Route::get('/adminpanel', 'AdminView@loginPage');
	Route::post('authenticateadmin','UserControl@authenticate');
});
Route::group(['prefix' => '/admin'],function(){
  ################################################################
	/**
	*Manage Dashboard
	*/
  ################################################################
	Route::get('dashboard','AdminView@getDashboard');
	################################################################

  ################################################################
	/**
	*Banners Management
	*/
  ################################################################
	/**
	*Banner
	*/
	Route::get('showetopbanner','AdminView@ManageBannerIndex');
	Route::get('/addnewbanner', 'AdminView@AddNewBannerIndex');

	Route::get('showerightbanner','AdminView@ManageRightBanerIndex');
	Route::get('showefooterbanner','AdminView@ManageFooterBanerIndex');
	Route::get('showcutsombanner','BannerControl@edit');
	Route::get('/deletebanner','BannerControl@destroy');
	Route::post('/savenewbanner','BannerControl@store');
	Route::post('/editnewbanner','BannerControl@update');
  Route::get('/banner_chkvalid','BannerControl@store_Chk_Valid');
	################################################################
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
