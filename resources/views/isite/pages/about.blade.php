@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <section class="page-header">
    <div class="container">
      <div class="row">
        <div class="col">
          <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="active">About Us</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h1>About Us</h1>
        </div>
      </div>
    </div>
  </section>
  <section id="about-us" class="about-us">
		<div class="container">
			<div class="row text-center">
            <div class="col">
               <h3 class="title-head">About Us</h3>
               {!! $getAboutData[0]['desc'] !!}
            </div>
			</div><!--/ Title row end -->

			<div class="row">
               <div class="featured-tab clearfix">
                  <ul class="nav nav-tabs flex-column col-lg-3 col-md-6 float-left" role="tablist">
                     <li class="nav-item">
                        <a class="nav-link animated fadeIn active" href="#tab_a" data-toggle="tab">
                           <span class="tab-icon"><i class="ion-ios-sunny-outline"></i></span>
                           <div class="tab-info"><h3>Our Mission</h3></div>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link animated fadeIn" href="#tab_b" data-toggle="tab">
                           <span class="tab-icon"><i class="ion-ios-eye-outline"></i></span>
                           <div class="tab-info"><h3>Our Vision</h3></div>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link animated fadeIn" href="#tab_c" data-toggle="tab">
                           <span class="tab-icon"><i class="ion-ios-pulse"></i></span>
                           <div class="tab-info"><h3>Our History</h3></div>
                        </a>
                     </li>
                  </ul>
                  <div class="tab-content col-lg-9 col-md-6 float-right">
                     <div class="tab-pane active animated fadeInRight" id="tab_a">
                        <img class="img-fluid pull-right" src="{{asset('isite/images/tab/tab1.png')}}" alt="">
                        <div class="tab-wrapper">
                            {!! $getAboutData[0]['our_mission'] !!}
                        </div>

                     </div>
                     <div class="tab-pane animated fadeInLeft" id="tab_b">
                           <img class="img-fluid pull-left" src="{{asset('isite/images/tab/tab2.png')}}" alt="">
                           {!! $getAboutData[0]['our_vision'] !!}
                     </div>

                     <div class="tab-pane animated fadeInLeft" id="tab_c">
                           {!! $getAboutData[0]['our_history'] !!}
                    </div>
                  </div><!-- tab content -->
               </div><!-- Featured tab end -->
            </div> <!-- COl End -->
			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</section><!-- About us end -->
</body>
</html>
@endsection
