<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Silver City Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('assets/cpanel/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('assets/cpanel/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('assets/cpanel/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('assets/cpanel/dist/css/AdminLTE.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> SilverCity Company Official Website.
          <small class="pull-right">Date: {{ $created_datebill }}</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        From
        <address>
          <strong>SilverCity Company Official Website.</strong><br>
          Office #110,  Makateb Building<br>
          Port Saeed, Deira, Dubai, UAE<br>
          Phone: (971) 4 2367444<br>
          Email: info@silvercity.ae<br>
          Website: www.silvercity.ae<br>
          TRN:100052971700003
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        To
        <address>
          <?php
            $userData = $OrderControlData[0]['user'];
            $user_id = $userData['id'];
          ?>
          <strong>{{ $userData['fullname'] }}</strong><br>
          Address: <?php if(!empty($UserAddressControlData['address'])){ ?>{{$UserAddressControlData['address'] }} <?php } ?><br>
          <?php if(!empty($UserAddressControlData['cityid'])){ ?>{{ $UserAddressControlData['cityid'] }}<?php } ?>,
          <?php if(!empty($CountryControlData['name'])){ ?>{{ $CountryControlData['name'] }} <?php } ?><br>
          Phone: <?php if(!empty($UserAddressControlData['phone'])){ ?>{{ $UserAddressControlData['phone'] }}<?php } ?><br>
          Email: {{ $userData['email'] }}
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Invoice #{{ $OrderControlData[0]['tracknumber'] }}</b><br>
        <br>
        <b>Order ID:</b> {{ $OrderControlData[0]['id'] }}<br>

        <b>Payment Due:</b> {{ $created_datebill  }}<br>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Qty</th>
            <th>Product</th>
            <th>Rate</th>
            <th>Subtotal</th>
          </tr>
          </thead>
          <tbody>
            <?php
              $orderdetail = $OrderControlData[0]['order_detail_m'];
              $TotalAmount = 0;
              $TotalDiscount = 0;
              $shippment = $OrderControlData[0]['shippmentcost'];
            ?>
            @if(count($orderdetail) > 0)
              @for($j=0;$j< count($orderdetail);$j++)
                <?php $ProductMemoryM_id = $orderdetail[$j]['prdmem_id'];
                $ProductMemoryM_var = \silvercity\Http\Controllers\ProductControl::getCustomProductByidProductMemory($ProductMemoryM_id);
                $ProductImageMData = $ProductMemoryM_var['ProductMemoryM_var'][0]['ProductImageMData'];
                $ProductMData = $ProductMemoryM_var['ProductMemoryM_var'][0]['ProductMData'];
                $PhoneMemoryMData = $ProductMemoryM_var['ProductMemoryM_var'][0]['phone__memory_m'];
                $CurrentColor = $ProductMemoryM_var['ProductMemoryM_var'][0]['CurrentColorData'];
                ?>
                @if(LaravelLocalization::getCurrentLocaleName() == "English")
                  <?php $productName_ret = $ProductMData[0]['name'].' '.$PhoneMemoryMData['name'].' '.
                  $CurrentColor['name']; ?>
                @elseif(LaravelLocalization::getCurrentLocaleName() == "Arabic")
                  <?php $productName_ret = $PhoneMemoryMData['name'].' '.$CurrentColor['name'].' '.
                  $ProductMData[1]['name']; ?>
                @endif
                <?php
                $subTotal = $orderdetail[$j]['qty'] * $orderdetail[$j]['rateone'];
                $discountVal = $orderdetail[$j]['discountval'];
                $TotalAmount += $subTotal;
                $TotalDiscount += $discountVal;

                ?>
                <tr>
                  <td>{{ $j+1 }}</td>
                  <td>{{ $orderdetail[$j]['qty'] }}</td>
                  <td>{{ $productName_ret }}</td>

                  <td>{{ $orderdetail[$j]['rateone'] }}</td>
                  <td>{{ $subTotal }}</td>
                </tr>
              @endfor
            @endif
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-6">
        <p class="lead">Payment Methods:</p>
        @if($OrderControlData[0]['paymentmethod'] == 'Credit/Debit-payment')
        {{ $OrderControlData[0]['paymentmethod'] }}
          <img src="{{asset('assets/cpanel/dist/img/credit/visa.png')}}" alt="Visa">
          <img src="{{asset('assets/cpanel/dist/img/credit/mastercard.png')}}" alt="Mastercard">
        @else
        {{ $OrderControlData[0]['paymentmethod'] }}
        @endif
      </div>
      <!-- /.col -->
      <div class="col-xs-6">
        <p class="lead">Amount Due {{ date('d M,y', strtotime($OrderControlData[0]['created_at']))  }}</p>

        <?php
          $discount_Calculation = ($TotalAmount * $TotalDiscount) / 100;
          $diff_Calculation_TA = $TotalAmount - $discount_Calculation;
          $CalcualtionData =  $diff_Calculation_TA + $shippment;

          $TotalVat = ($diff_Calculation_TA * $OrderControlData[0]['ordertax'])/100;
          $vat_Calculation = $CalcualtionData + $TotalVat;
          ?>
        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Subtotal:</th>
              <td>AED {{ $TotalAmount }}</td>
            </tr>
            <tr>
              <th>VAT ({{ $OrderControlData[0]['ordertax'] }} %):</th>
              <td> AED <?php echo $TotalVat; ?></td>
            </tr>
            <tr>
              <th>Shipping:</th>
              <td>AED {{ $shippment }}</td>
            </tr>
            <tr>
              <th>Discount ({{ $TotalDiscount }} %):</th>
              <td> AED
                <?php echo $discount_Calculation; ?>
              </td>
            </tr>
            <tr>
              <th>Balance:</th>
              <td>AED {{ ( $vat_Calculation) }}</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
