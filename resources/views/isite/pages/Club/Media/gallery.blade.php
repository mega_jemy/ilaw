@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner1.jpg)">
		<!-- Subpage title start -->
		<div class="banner-text text-center">
     		<div class="container">
	        	<div class="row">
	        		<div class="col-12">
	        			<div class="banner-heading">
	        				<h1 class="banner-title">Our <span>Gallery</span></h1>
	        				<h2 class="banner-desc">The Most Beautiful Tag Line Will Go Here !</h2>
	        			</div>
			        	<ul class="breadcrumb">
			            <li>Home</li>
			            <li>About Firm</li>
			            <li><a href="#"> Our Gallery</a></li>
		          	</ul>
	        		</div>
	        	</div>
       	</div>
    	</div><!-- Subpage title end -->
	</div><!-- Banner area end -->

  <section id="main-container" class="main-container">

		<div class="container">
			<div class="row text-center">
				<div class="col">
               <h3 class="title-head">Our Photo Gallery</h3>
               <p class="title-description">Law House is a firm dolor sit amet, consectetur adipisicing elit, sed do eius mod tempor incididunt ut labore et dolore magna </p>
            </div>
			</div><!--/ Title row end -->

			<!--Isotope filter start -->
			<div class="row text-center">
            <div class="col-12">
               <div class="isotope-nav" data-isotope-nav="isotope">
                  <ul>
                     <li><a href="#" class="active" data-filter="*">All</a></li>
                     <li><a href="#" data-filter=".office">Office</a></li>

                  </ul>
               </div>
            </div>
			</div><!-- Isotope filter end -->

			<div class="row">
            <div class="col-12">
               <div id="isotope" class="isotope">
                 <div class="col-md-3 col-12 office isotope-item">
                    <div class="isotop-img-conatiner">
                       <a class="gallery-popup" href="{{asset('images/gallery/photo/p1.png')}}">
                          <img class="img-fluid" src="{{asset('images/gallery/photo/p1.png')}}" alt="">
                          <span class="gallery-icon"><i class="ion-plus"></i></span>
                       </a>

                       <div class="isotope-item-info">
                         <h3 class="title-classic">Office</h3>
                         <h4 class="title-desc"></h4>
                       </div>
                    </div>
                 </div><!-- Isotope item 1 end -->

                 <div class="col-md-3 col-12 office isotope-item">
                    <div class="isotop-img-conatiner">
                       <a class="gallery-popup" href="{{asset('images/gallery/photo/p2.png')}}">
                          <img class="img-fluid" src="{{asset('images/gallery/photo/p2.png')}}" alt="">
                          <span class="gallery-icon"><i class="ion-plus"></i></span>
                       </a>

                       <div class="isotope-item-info">
                         <h3 class="title-classic">Office</h3>
                         <h4 class="title-desc"></h4>
                       </div>
                    </div>
                 </div><!-- Isotope item 2 end -->

                 <div class="col-md-3 col-12 office isotope-item">
                    <div class="isotop-img-conatiner">
                       <a class="gallery-popup" href="{{asset('images/gallery/photo/p3.png')}}">
                          <img class="img-fluid" src="{{asset('images/gallery/photo/p3.png')}}" alt="">
                          <span class="gallery-icon"><i class="ion-plus"></i></span>
                       </a>

                       <div class="isotope-item-info">
                         <h3 class="title-classic">Office</h3>
                         <h4 class="title-desc"></h4>
                       </div>
                    </div>
                 </div><!-- Isotope item 3 end -->

                 <div class="col-md-3 col-12 office isotope-item">
                    <div class="isotop-img-conatiner">
                       <a class="gallery-popup" href="{{asset('images/gallery/photo/p4.png')}}">
                          <img class="img-fluid" src="{{asset('images/gallery/photo/p4.png')}}" alt="">
                          <span class="gallery-icon"><i class="ion-plus"></i></span>
                       </a>

                       <div class="isotope-item-info">
                          <h3 class="title-classic">Office</h3>
                          <h4 class="title-desc"></h4>
                       </div>
                    </div>
                 </div><!-- Isotope item 4 end -->

                 <div class="col-md-3 col-12 office isotope-item">
                    <div class="isotop-img-conatiner">
                       <a class="gallery-popup" href="{{asset('images/gallery/photo/p5.jpg')}}">
                          <img class="img-fluid" src="{{asset('images/gallery/photo/p5.jpg')}}" alt="">
                          <span class="gallery-icon"><i class="ion-plus"></i></span>
                       </a>

                       <div class="isotope-item-info">
                         <h3 class="title-classic">Office</h3>
                         <h4 class="title-desc"></h4>
                       </div>
                    </div>
                 </div><!-- Isotope item 5 end -->




               </div><!-- Isotope end -->
            </div>
			</div><!--/ Content row end -->
		</div><!--/ Container end -->


	</section><!-- Main container end -->
  <section class="call-action-all no-padding">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="action-text">
            <h2>Get Immediate Free Consultation !</h2>
            <p class="lead"></p>
          </div>
          <p class="action-btn"><a class="btn btn-primary" href="{{ url('/online_consultacy') }}">Contact Now</a></p>
        </div>
      </div>
    </div><!--/ Container end -->
  </section>
</body>
</html>
@endsection
