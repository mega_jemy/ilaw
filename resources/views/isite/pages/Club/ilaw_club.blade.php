@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <section class="page-header">
    <div class="container">
      <div class="row">
        <div class="col">
          <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/club') }}">Club</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h1>iLAW Club</h1>
        </div>
      </div>
    </div>
  </section>
  <div class="slider-container rev_slider_wrapper">
      <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 500, 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,500]}">
        <ul>
          @if(count($getBannerData) > 0)
            @for($i=0;$i < count($getBannerData);$i++)
            <?php
            $imgpath_st = $getBannerData[$i]['imgpath_st'];
            $imgpath_nd = $getBannerData[$i]['imgpath_nd'];

            $imgpath_st = "images/banner/back/".$imgpath_st;
            $imgpath_nd = "images/banner/single/".$imgpath_nd;
            ?>
            <li data-transition="fade">
              <img src="{{$imgpath_st}}"
                alt=""
                data-bgposition="center center"
                data-bgfit="cover"
                data-bgrepeat="no-repeat"
                class="rev-slidebg">
                @if(!empty($getBannerData[$i]['img_name']))
                <div class="tp-caption"
                  data-x="177"
                  data-y="188"
                  data-start="1000"
                  data-transform_in="x:[-300%];opacity:0;s:500;"><img src="{{asset('isite/img/slides/slide-title-border.png')}}" alt=""></div>

                <div class="tp-caption top-label"
                  data-x="227"
                  data-y="180"
                  data-start="500"
                  data-transform_in="y:[-300%];opacity:0;s:500;">{{$getBannerData[$i]['img_name'] }}</div>

                <div class="tp-caption"
                  data-x="480"
                  data-y="188"
                  data-start="1000"
                  data-transform_in="x:[300%];opacity:0;s:500;"><img src="{{asset('isite/img/slides/slide-title-border.png')}}" alt=""></div>

                <div class="tp-caption main-label"
                  data-x="135"
                  data-y="210"
                  data-start="1500"
                  data-whitespace="nowrap"
                  data-transform_in="y:[100%];s:500;"
                  data-transform_out="opacity:0;s:500;"
                  data-mask_in="x:0px;y:0px;">{{$getBannerData[$i]['img_title'] }}</div>
                @endif







              <div class="tp-caption"
                data-x="['610','610','610','630']"
                data-y="80"
                data-start="3950"
                data-transform_in="y:[300%];opacity:0;s:300;"><img src="{{$imgpath_nd}}" alt=""></div>
            </li>
            @endfor
          @endif
        </ul>
      </div>
    </div>
  <section class="section section-primary">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <h2 class="mb-1">Our <strong>Blog</strong></h2>
          <div class="row">

            @if (count($getBlogData) >= 1)
              @foreach ($getBlogData as $getBlogDatas)
              <div class="col-lg-6">
                <div class="recent-posts mt-4">
                  <article class="post">
                    <div class="date">
                      <span class="day">15</span>
                      <span class="month background-color-secondary">Jan</span>
                    </div>
                    <h4><a class="text-light" href="blog-post.html">{{ $getBlogDatas->name }}</a></h4>
                    <p>{{ $getBlogDatas->title }}</p>
                    <a href="#" class="btn btn-secondary mt-3 mb-2 mb-lg-0">Read More</a>
                  </article>
                </div>
              </div>
              @endforeach
            @endif
          </div>
        </div>
        <div class="col-lg-6">

          <h2 class="mb-1">Our <strong>Stats</strong></h2>

          <div class="content-grid content-grid-dashed mt-4 mb-4">
            <div class="row content-grid-row">
              <div class="content-grid-item col-lg-6 text-center py-4">
                <div class="counters">
                  <div class="counter text-color-light">
                    <strong data-to="30000" data-append="+">0</strong>
                    <label>Happy Clients</label>
                  </div>
                </div>
              </div>
              <div class="content-grid-item col-lg-6 text-center py-4">
                <div class="counters">
                  <div class="counter text-color-light">
                    <strong data-to="15">0</strong>
                    <label>Years in Business</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row content-grid-row">
              <div class="content-grid-item col-lg-6 text-center py-4">
                <div class="counters">
                  <div class="counter text-color-light">
                    <strong data-to="352">0</strong>
                    <label>Cups of Coffee</label>
                  </div>
                </div>
              </div>
              <div class="content-grid-item col-lg-6 text-center py-4">
                <div class="counters">
                  <div class="counter text-color-light">
                    <strong data-to="178">0</strong>
                    <label>High Score</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
  <div class="container">

    <div class="row">
      <div class="col-lg-3 order-2 order-lg-1">
        <aside class="sidebar">

          <h4 class="heading-primary"><strong>Filter</strong> By</h4>

          <ul class="nav nav-list flex-column mb-4 sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
            <li class="nav-item" data-option-value="*"><a class="nav-link active" href="#">Show All</a></li>
            <li class="nav-item" data-option-value=".office"><a class="nav-link" href="#">Office</a></li>
          </ul>

          <hr class="invisible mt-5 mb-2">

          <h4 class="heading-primary">Contact <strong>Us</strong></h4>
          <p>Contact us or give us a call to discover how we can help.</p>

          <div class="form-group col">
            <a href="{{ url('/contact_us') }}" class="btn btn-primary mb-4">Send Message</a>
          </div>
          <!--<ul class="list list-icons list-icons-style-3">
            <li><i class="fas fa-map-marker-alt"></i> <strong>Address-01:</strong> United Arab Emirates – Sharjah – Cornish AL Buhairah – Bel Rashid Tower – Office 1704 BR1</li>
            <li><i class="fas fa-map-marker-alt"></i> <strong>Address-02:</strong> BR2 United Arab Emirates – Ras AL Kaiymah – Julphar Avenue 1st Floor</li>
            <li><i class="fas fa-phone"></i> <strong>Phone:</strong> (+971) 6 556 5566</li>
            <li><i class="fas fa-phone"></i> <strong>Fax:</strong> (+971) 6 55 66 690</li>
            <li><i class="far fa-envelope"></i> <strong>Email:</strong> <a href="mailto:infoline@ilaw.ae">infoline@ilaw.ae</a></li>
            <li><i class="fas fa-phone"></i> <strong>Skype:</strong>ilawuae</li>

          </ul>-->

        </aside>
      </div>
      <div class="col-lg-9 order-1 order-lg-2">

        <div class="sort-destination-loader sort-destination-loader-showing">
          <div class="row portfolio-list sort-destination" data-sort-id="portfolio">
            <!------------------------------------------------------------------------------------------>
            <div class="col-lg-4 isotope-item office">
              <div class="portfolio-item">
                <a href="#">
                  <span class="thumb-info thumb-info-lighten">
                    <span class="thumb-info-wrapper">
                      <img src="{{asset('images/gallery/photo/p1.png')}}" class="img-fluid" alt="">
                      <span class="thumb-info-title">
                        <span class="thumb-info-inner">Name</span>
                        <span class="thumb-info-type">Title</span>
                      </span>
                      <span class="thumb-info-action">
                        <span class="thumb-info-action-icon"><i class="fas fa-link"></i></span>
                      </span>
                    </span>
                  </span>
                </a>
              </div>
            </div>
            <!------------------------------------------------------------------------------------------>
            <div class="col-lg-4 isotope-item office">
              <div class="portfolio-item">
                <a href="#">
                  <span class="thumb-info thumb-info-lighten">
                    <span class="thumb-info-wrapper">
                      <img src="{{asset('images/gallery/photo/p2.png')}}" class="img-fluid" alt="">
                      <span class="thumb-info-title">
                        <span class="thumb-info-inner">Name</span>
                        <span class="thumb-info-type">Title</span>
                      </span>
                      <span class="thumb-info-action">
                        <span class="thumb-info-action-icon"><i class="fas fa-link"></i></span>
                      </span>
                    </span>
                  </span>
                </a>
              </div>
            </div>
            <!------------------------------------------------------------------------------------------>
            <div class="col-lg-4 isotope-item office">
              <div class="portfolio-item">
                <a href="#">
                  <span class="thumb-info thumb-info-lighten">
                    <span class="thumb-info-wrapper">
                      <img src="{{asset('images/gallery/photo/p3.png')}}" class="img-fluid" alt="">
                      <span class="thumb-info-title">
                        <span class="thumb-info-inner">Name</span>
                        <span class="thumb-info-type">Title</span>
                      </span>
                      <span class="thumb-info-action">
                        <span class="thumb-info-action-icon"><i class="fas fa-link"></i></span>
                      </span>
                    </span>
                  </span>
                </a>
              </div>
            </div>
            <!------------------------------------------------------------------------------------------>
            <div class="col-lg-4 isotope-item office">
              <div class="portfolio-item">
                <a href="#">
                  <span class="thumb-info thumb-info-lighten">
                    <span class="thumb-info-wrapper">
                      <img src="{{asset('images/gallery/photo/p4.png')}}" class="img-fluid" alt="">
                      <span class="thumb-info-title">
                        <span class="thumb-info-inner">Name</span>
                        <span class="thumb-info-type">Title</span>
                      </span>
                      <span class="thumb-info-action">
                        <span class="thumb-info-action-icon"><i class="fas fa-link"></i></span>
                      </span>
                    </span>
                  </span>
                </a>
              </div>
            </div>
            <!------------------------------------------------------------------------------------------>


          </div>
        </div>

      </div>
    </div>

  </div>
  <div class="container">
    <div class="row">
      <div class="col">

        <h4 class="heading-primary"><strong>ilaw Club</strong> MemberShip</h4>
      </div>
    </div>
  </div>
  <section class="parallax section section-text-light section-parallax mt-0 mb-0" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="{asset('images/parallax-2.jpg')}}">
    <div class="container">
      <div class="pricing-table princig-table-flat row no-gutters spaced no-borders mt-4 mb-0">
        <div class="col-lg-2 col-sm-6">
          <div class="plan">
            <h3>فئة الاشتراك الطلابي<span>AED 250</span></h3>
            <ul>
              <li>•	الحصول على حساب خاص في الموقع الالكتروني، يتم من خلاله التعريف بالمنتسب. والاطلاع على التحديثات من خلاله.</li>
              <li>•	خصم 25% على رسوم كافة برامج التدريب القانونية والمؤتمرات.</li>
              <li>•	الحصول على النشرة الالكترونية الشهرية.</li>
              <li><a class="btn btn-light" href="#">Sign up</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-2 col-sm-6">
          <div class="plan">
            <h3>رواد الأعمال<span>AED 1500</span></h3>
            <ul>
              <li>•	الحصول على حساب خاص في الموقع الالكتروني، يتم من خلاله التعريف بالمنتسب. والاطلاع على التحديثات من خلاله. </li>
              <li>•	إعلان لمره واحدة في الموقع الالكتروني وحسابات التواصل الالكتروني.</li>
              <li>•	تقديم 5 استشارات قانونية مجانا خلال مدة الانتساب.</li>
              <li>•	خصم 50% على الخدمات القانونية من صياغة عقود ومراجعتها وترجمه قانونية ومتطلبات الترخيص واضافة الأنشطة.</li>
              <li>•	بدل اتعاب محاماة وتمثيل قانوني (تفضيلية)•	خصم 35% على رسوم كافة برامج التدريب القانونية والمؤتمرات.</li>
              <li>•	الحصول على النشرة الالكترونية الشهرية.</li>

              <li><a class="btn btn-light" href="#">Sign up</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 text-center">
          <div class="plan most-popular">
            <h3>الشركات<em class="desc">Most Popular</em><span>AED 5000</span></h3>
            <ul>
              <li>•	الحصول على حساب خاص في الموقع الالكتروني، يتم من خلاله التعريف بالمنتسب. والاطلاع على التحديثات من خلاله. </li>
              <li>•	تقييم القسم القانوني في الشركات وتقديم توصيات التطوير والتحسين مجاناً.</li>
              <li>•	إعلان متكرر لمدة شهر في الموقع الالكتروني وحسابات التواصل الالكتروني.</li>
              <li>•	تقديم 3استشارات قانونية مجانا خلال مدة الانتساب.</li>
              <li>•	خصم 25% على اتعاب الخدمات القانونية الخاصة.</li>
              <li>•	بدل اتعاب محاماة وتمثيل قانوني (تفضيلية)</li>
              <li>•	خصم 35% على رسوم كافة برامج التدريب القانونية والمؤتمرات.</li>
              <li>•	خصم 50% على كافة إصدارات iLAW المطبوعة والمرئية والالكترونية في حال إصدارها.</li>
              <li>•	الحصول على النشرة الالكترونية الشهرية.  </li>
              <li><a class="btn btn-primary" href="#">Get Started Now!</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-2 col-sm-6">
          <div class="plan">
            <h3>القانونين<span>AED </span></h3>
            <ul>
              <li>•	الحصول على حساب خاص في الموقع الالكتروني، يتم من خلاله التعريف بالمنتسب. والاطلاع على التحديثات من خلاله. </li>
              <li>•	خصم 70% على إصدارات iLAW المطبوعة والمرئية والالكترونية في حال إصدارها.</li>
              <li>•	تخصيص مساحة تعريفية في الموقع الالكتروني ومواقع التواصل الالكتروني للتعريف بالمنتسب والمهارات لديه.</li>
              <li>•	الاستفادة من مميزات إضافية في العمولات المقدمة على القضايا المتحصلة من طرف المنتسب تصل لـ (50%) حسب نوع القصية.</li>
              <li>•	خصم 25% على اتعاب الخدمات القانونية الخاصة .</li>

              <li>•	بدل اتعاب محاماة وتمثيل قانوني (تفضيلية)</li>
              <li>•	خصم 50% على رسوم كافة برامج التدريب القانونية والمؤتمرات.</li>
              <li>•	الحصول على النشرة الالكترونية الشهرية.  </li>
              <li>•	فرصة المشاركة في برامج التدريب.</li>
              <li><a class="btn btn-light" href="#">Sign up</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-2 col-sm-6">
          <div class="plan">
            <h3>غير القانونيين <span>AED </span></h3>
            <ul>
              <li>•	الحصول على حساب خاص في الموقع الالكتروني، يتم من خلاله التعريف بالمنتسب. والاطلاع على التحديثات من خلاله. </li>
              <li>•	خصم 30% على إصدارات iLAW المطبوعة والمرئية والالكترونية في حال إصدارها.</li>
              <li>•	تخصيص مساحة تعريفية في الموقع الالكتروني ومواقع التواصل الالكتروني للتعريف بالمنتسب والمهارات لديه.</li>
              <li>•	الاستفادة من مميزات إضافية في العمولات المقدمة على القضايا المتحصلة من طرف المنتسب تصل لـ (50%) حسب نوع القصية.</li>
              <li>•	خصم 25% على اتعاب الخدمات القانونية الخاصة .</li>
              <li>•	بدل اتعاب محاماة وتمثيل قانوني (تفضيلية)</li>
              <li>•	خصم 50% على رسوم كافة برامج التدريب القانونية والمؤتمرات.</li>
              <li>•	الحصول على النشرة الالكترونية الشهرية.  </li>
              <li>•	فرصة المشاركة في برامج التدريب.</li>
              <li><a class="btn btn-light" href="#">Sign up</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>
</html>
@endsection
