@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner3.jpg)">
		<!-- Subpage title start -->
		<div class="banner-text text-center">
     		<div class="container">
	        	<div class="row">
	        		<div class="col-12">
	        			<div class="banner-heading">
	        				<h1 class="banner-title">ilaw <span>Blog</span></h1>
	        				<h2 class="banner-desc"></h2>
	        			</div>
			        	<ul class="breadcrumb">
			            <li>Home</li>
			            <li>ilaw Club</li>
			            <li><a href="#"> ilaw Blog </a></li>
		          	</ul>
	        		</div>
	        	</div>
       	</div>
    	</div><!-- Subpage title end -->
	</div><!-- Banner area end -->
  <section id="main-container" class="main-container">
		<div class="container">
      <?php
        $lang_regional = LaravelLocalization::getCurrentLocale();

      ?>

       @if (count($getBlogData) >= 1)
         @foreach ($getBlogData as $getBlogDatas)


         <div class="row">
   				<div class="col-lg-8 col-md-12">
   					<div class="post">
   						<div class="post-media post-image">
                <?php
                $imgpath = $getBlogDatas->img_path;
                $imgpath = 'images/blog/'.$imgpath;
                 ?>
   							<img src="{{ $imgpath}}" class="img-fluid" alt="">
   						</div>
   						<div class="post-body border-box">
   							<div class="entry-header">
   								<div class="post-date">
   									{{ $getBlogDatas->write_date }}
   								</div>
      							<h2 class="entry-title">
   					 				<a href="#">{{ $getBlogDatas->name }}</a>
   					 			</h2>

   					 			<div class="post-meta">
      								<span class="post-author">
      									Posted By <a href="#">{{ $getBlogDatas->write_name }}</a>
      								</span>

      								<span class="post-cat">
      									Title <a href="#">{{ $getBlogDatas->write_title }}</a>
      								</span>

      								<!--<span class="post-comment">
      									44
                                 <a href="#" class="comments-link">
                                     Comments
                                 </a>
                             </span>-->
      							</div><!--Meta end -->
   							</div><!-- header end -->

   							<div class="entry-content">
   								{!! $getBlogDatas->desc !!}
   							</div>

   							<div class="tags-area clearfix">
   								<!--<div class="post-tags pull-left">
   		   						<span>Tags:</span>
   		   						<a href="#">Law</a>,
   		   						<a href="#">Lawyer</a>,
   		   						<a href="#">Attorney</a>,
   		   						<a href="#">Family Law</a>
   	   						</div>-->
   	   						<div class="share-items pull-right">
   	   							<ul class="post-social-icons unstyled">
   	   								<li class="social-icons-head">Share:</li>
   	   								<li><a href="#"><i class="ion-social-facebook"></i></a></li>
   	   								<li><a href="#"><i class="ion-social-twitter"></i></a></li>
   	   								<li><a href="#"><i class="ion-social-googleplus"></i></a></li>
   	   								<li><a href="#"><i class="ion-social-instagram"></i></a></li>
   	   							</ul>
   	   						</div>
   							</div>
   						</div><!-- post-body end -->
   					</div><!-- post end ******-->

   					<div class="author-box border-box">
   						<div class="author-img pull-left">

   							<img src="" alt="">
   						</div>
   						<div class="author-info">
   							<h3 class="author-name">{{ $getBlogDatas->write_name }}</h3>
   							<p>{{ $getBlogDatas->write_title }}</p>
   							<div class="author-social">
                         	<a href="#"><i class="ion-social-facebook"></i></a>
   	   						<a href="#"><i class="ion-social-twitter"></i></a>
   	   						<a href="#"><i class="ion-social-googleplus"></i></a>
                       </div>
   						</div>
   					</div> <!-- Author box end -->

   					<!-- Post comment start -->
   						<!--<div id="comments" class="border-box comments-area">
   							<h3 class="comments-heading">89 Comments</h3>


   						</div>--><!-- Post comment end -->

   						<div class="comments-form border-box">
   							<h3 class="title-normal">Add a comment</h3>

   							<form role="form">
   								<div class="row">
   									<div class="col-md-6">
   										<div class="form-group">
   											<textarea class="form-control required-field" id="message" placeholder="Your Comment" rows="8" required></textarea>
   										</div>
   									</div><!-- Col 6 end -->
   									<div class="col-md-6">
   										<div class="form-group">
   											<input class="form-control" name="name" id="name" placeholder="Your Name" type="text" required>
   										</div>

   										<div class="form-group">
   											<input class="form-control" name="email" id="email" placeholder="Your Email" type="email" required>
   										</div>

   										<div class="form-group">
   											<input class="form-control" placeholder="Your Website" type="text" required>
   										</div>

   									</div><!-- Col 6 end -->
   								</div><!-- Form row end -->
   								<div class="clearfix">
   									<button class="btn btn-primary" type="submit">Post Comment</button>
   								</div>
   							</form><!-- Form end -->
   						</div><!-- Comments form end -->

   						<nav class="post-navigation clearfix">
                        <div class="post-previous">
                            <a href="#">
                                <i class="ion-ios-arrow-left"></i>
                                <h3 class="title-normal">Prev</h3>
                            </a>
                        </div>
                        <div class="post-next">
                            <a href="#">
                                <i class="ion-ios-arrow-right"></i>
                                <h3 class="title-normal">Next</h3>
                            </a>
                        </div>
                     </nav><!-- Post navigation end -->


   				</div><!--/ Post Content col 9 end -->

   				<div class="col-lg-4 col-md-12">
   					<div class="sidebar sidebar-right">

   						<div class="widget widget-search">
   							<div id="search" class="input-group">
   								<input class="form-control" placeholder="Search" type="search">
   								<span class="input-group-btn">
   									<i class="ion-android-search"></i>
   								</span>
   							</div>
   						</div><!-- Search end -->

   						<div class="widget recent-posts">
   							<h3 class="widget-title">Recent Posts</h3>
   							<ul class="unstyled clearfix">
   		               	<li>
   		                    <div class="posts-thumb pull-left">
   		                    		<a href="#"><img alt="img" src="images/news/news1.jpg"></a>
   		                    </div>
   		                    <div class="post-info">
   		                        <h4 class="entry-title">
   		                        	<a href="#">Find the perfect red lipstick to complement your skin</a>
   		                        </h4>
   		                        <p class="post-meta">
   											<span class="post-date"> January 27, 2017</span>
   										</p>
   		                    </div>
   		                    <div class="clearfix"></div>
   		                  </li><!-- 1st post end-->

   		                  <li>
   		                    <div class="posts-thumb pull-left">
   		                    		<a href="#"><img alt="img" src="images/news/news1.jpg"></a>
   		                    </div>
   		                    <div class="post-info">
   		                        <h4 class="entry-title">
   		                        	<a href="#">This is another lorem ipsum post title of the day</a>
   		                        </h4>
   		                        <p class="post-meta">
   											<span class="post-date"> February 11, 2017</span>
   										</p>
   		                    </div>
   		                    <div class="clearfix"></div>
   		                  </li><!-- 1st post end-->

   		                  <li>
   		                    <div class="posts-thumb pull-left">
   		                    		<a href="#"><img alt="img" src="images/news/news1.jpg"></a>
   		                    </div>
   		                    <div class="post-info">
   		                        <h4 class="entry-title">
   		                        	<a href="#">Law abore et dolore magna aliqua enim ad minim</a>
   		                        </h4>
   		                        <p class="post-meta">
   											<span class="post-date"> February 19, 2017</span>
   										</p>
   		                    </div>
   		                    <div class="clearfix"></div>
   		                  </li><!-- 1st post end-->
   		               </ul>

   						</div><!-- Search end -->


   						<!--<div class="widget">
   							<h3 class="widget-title">Categories</h3>
   							<ul class="arrow nav nav-tabs flex-column">
   		                  <li><a href="#">Lawyers</a></li>
   		                  <li><a href="#">LawFirms</a></li>
   		                  <li><a href="#">Practice Area</a></li>
   		                  <li><a href="#">Court</a></li>
   		                  <li><a href="#">Victim</a></li>
   		              	</ul>
   						</div>--><!-- Categories end -->

   						<!--<div class="widget">
   							<h3 class="widget-title">Archives </h3>
   							<ul class="arrow nav nav-tabs flex-column">
   			              	<li><a href="#">Feburay 2017</a></li>
   		                  <li><a href="#">January 2017</a></li>
   		                  <li><a href="#">December 2017</a></li>
   		                  <li><a href="#">November 2017</a></li>
   		                  <li><a href="#">October 2017</a></li>
   			            </ul>
   						</div>--><!-- Archives end -->

   						<!--<div class="widget widget-tags">
   							<h3 class="widget-title">Tags </h3>
   							<ul class="unstyled clearfix">
   			              	<li><a href="#">Family Law</a></li>
   		                  <li><a href="#">Legal</a></li>
   		                  <li><a href="#">Attorney</a></li>
   		                  <li><a href="#">Sexual Offence</a></li>
   		                  <li><a href="#">Legal Advice</a></li>
   			            </ul>
   						</div>--><!-- Navigation end -->

   					</div><!-- Sidebar end -->
   				</div><!--/ Content col 4 end -->

   			</div><!-- Main Row end -->
         @endforeach
       @endif
       {{ $getBlogData->links() }}
		</div><!-- Container end -->
	</section><!-- Main container end -->
</body>
</html>
@endsection
