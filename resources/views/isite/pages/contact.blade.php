@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <section class="page-header">
    <div class="container">
      <div class="row">
        <div class="col">
          <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="active">Contact us</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h1>iLAW Care</h1>
        </div>
      </div>
    </div>
  </section>
  <section id="main-container" class="main-container">
		<div class="container">
			<div class="row">
        <div class="col-12">
          <!-- Map start here -->
          <div id="map-wrapper" class="no-padding">
            <div class="map" id="map"></div>
          </div><!--/ Map end here -->
        </div>
      </div><!-- Content row  end -->
			<div class="gap-60"></div>
			<div class="row text-center">
				<div class="col">
          <h3 class="title-head">Get In Touch</h3>
          <p class="title-description">"Ibrahim Al Hosani Advocate, Legal Services & Training” offers a wide range of professional and high quality services in all chief fields of law, including </p>
        </div>
			</div><!--/ Title row end -->
			<div class="row">
				<div class="col-lg-8 col-md-12">
					<div class="contact-form-wrapper">
						<h3 class="title-normal">Fill This Form</h3>
						<form id="contact-form" action="{{ url('/storenewenquiries') }}" method="post" role="form">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="inp_idtype" value="0">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<textarea class="form-control required-field" name="pre_details_enq" id="message" placeholder="Your Comment *" rows="8" required></textarea>
									</div>
								</div><!-- Col 6 end -->
								<div class="col-md-6">
									<div class="form-group">
										<input class="form-control" name="pre_name_enq" id="name" placeholder="Your Name *" type="text" required>
									</div>

									<div class="form-group">
										<input class="form-control" name="pre_email_enq" id="email" placeholder="Your Email *" type="email" required>
									</div>

									<div class="form-group">
										<input class="form-control" name="pre_subject_enq" id="subject" placeholder="Your Subject *" type="text" required>
									</div>
								</div><!-- Col 6 end -->
							</div><!-- Form row end -->

							<div class="clearfix">
								<button class="btn btn-primary" type="submit">Submit</button>
							</div>
						</form><!-- Form end -->
					</div><!-- Comments form end -->

				</div><!-- From col end -->

				<div class="col-md-12 col-lg-4">
					<div class="contact-page-info">
						<div class="contact-info-box">
							<i class="ion-ios-location"></i>
							<div class="contact-info-box-content">
								<h4>Address:</h4>
								<p style="color:black;">
                - United Arab Emirates – Sharjah – Cornish AL Buhairah – Bel Rashid Tower – Office 1704 BR1

                </p>
                <p style="color:black;">
                -  BR2 United Arab Emirates – Ras AL Kaiymah – Julphar Avenue 1st Floor
                </p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-email"></i>
							<div class="contact-info-box-content">
								<h4>Email:</h4>
								<p>infoline@ilaw.ae</p>
								<p style="color:black;">Skype:</p><p>ilawuae</p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-android-call"></i>
							<div class="contact-info-box-content">
								<h4>Phone:</h4>
								<p>+(971) 6 556 5566</p>
                <h4>Fax:</h4>
								<p>+(971) 6 55 66 690</p>
							</div>
						</div>

					</div>
				</div><!-- Contact info col end -->


			</div><!--/ Content row end -->
		</div><!--/ Container end -->

	</section><!-- Main container end -->


</body>
</html>
@endsection
