@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <div id="banner-area" class="banner-area" style="background-image:url({{asset('isite/images/banner/banner3.jpg')}})">
    <!-- Subpage title start -->
    <div class="banner-text text-center">
        <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="banner-heading">
                  <h1 class="banner-title">Suggest <span>Training</span></h1>
                  <h2 class="banner-desc"></h2>
                </div>
                <ul class="breadcrumb">
                  <li>Home</li>
                  <li><a href="#"> Suggest Training</a></li>
                </ul>
              </div>
            </div>
        </div>
      </div><!-- Subpage title end -->
  </div><!-- Banner area end -->
  <section id="main-container" class="main-container">
		<div class="container">
			<div class="row text-center">
				<div class="col">
          <h3 class="title-head">Get In Touch</h3>
          <p class="title-description">"Ibrahim Al Hosani Advocate, Legal Services & Training” offers a wide range of professional and high quality services in all chief fields of law, including </p>
        </div>
			</div><!--/ Title row end -->
			<div class="row">
				<div class="col-lg-8 col-md-12">
					<div class="contact-form-wrapper">
						<h3 class="title-normal">Fill This Form</h3>

						<form id="contact-form" action="{{ url('/store_training_course') }}" method="post" role="form">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="inp_idtype" value="0">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input class="form-control" name="inp_qualifiy_name" id="inp_qualifiy_name" placeholder="Qualification *" type="text" required>
									</div>

									<div class="form-group">
										<input class="form-control" name="inp_suggest_name" id="inp_suggest_name" placeholder="Suggest Training Name *" type="text" required>
									</div>

									<div class="form-group">
										<input class="form-control" name="inp_s_price" id="inp_s_price" placeholder="Price *" type="text" required>
									</div>

                  <div class="form-group">
										<input class="form-control" name="inp_n_days" id="inp_n_days" placeholder="# of days *" type="text" required>
									</div>

                  <div class="form-group">
										<textarea class="form-control" name="inp_desc" id="inp_desc" placeholder="Description *"  required></textarea>
									</div>

                  <div class="form-group">
										<input class="form-control" name="inp_other_suggest" id="inp_other_suggest" placeholder="Other Suggest *" type="text" required>
									</div>
								</div><!-- Col 6 end -->
							</div><!-- Form row end -->

							<div class="clearfix">
								<button class="btn btn-primary" type="submit">Submit</button>
							</div>
						</form><!-- Form end -->
					</div><!-- Comments form end -->

				</div><!-- From col end -->

				<div class="col-md-12 col-lg-4">
					<div class="contact-page-info">
						<div class="contact-info-box">
							<i class="ion-ios-location"></i>
							<div class="contact-info-box-content">
								<h4>Address:</h4>
								<p style="color:black;">
                - United Arab Emirates – Sharjah – Cornish AL Buhairah – Bel Rashid Tower – Office 1704 BR1

                </p>
                <p style="color:black;">
                -  BR2 United Arab Emirates – Ras AL Kaiymah – Julphar Avenue 1st Floor
                </p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-email"></i>
							<div class="contact-info-box-content">
								<h4>Email:</h4>
								<p>infoline@ilaw.ae</p>
								<p style="color:black;">Skype:</p><p>ilawuae</p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-android-call"></i>
							<div class="contact-info-box-content">
								<h4>Phone:</h4>
								<p>+(971) 6 556 5566</p>
                <h4>Fax:</h4>
								<p>+(971) 6 55 66 690</p>
							</div>
						</div>

					</div>
				</div><!-- Contact info col end -->


			</div><!--/ Content row end -->
		</div><!--/ Container end -->

	</section><!-- Main container end -->

  <section class="call-action-all no-padding">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="action-text">
						<h2>Get Immediate Free Consultation !</h2>
						<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<p class="action-btn"><a class="btn btn-primary" href="#">Contact Now</a></p>
				</div>
			</div>
		</div><!--/ Container end -->
    </section>
</body>
</html>
@endsection
