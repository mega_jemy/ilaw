@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <section class="page-header">
    <div class="container">
      <div class="row">
        <div class="col">
          <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Training</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h1>Training</h1>
        </div>
      </div>
    </div>
  </section>
  <div class="slider-container rev_slider_wrapper">
      <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 500, 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,500]}">
        <ul>
          @if(count($getBannerData) > 0)
            @for($i=0;$i < count($getBannerData);$i++)
            <?php
            $imgpath_st = $getBannerData[$i]['imgpath_st'];
            $imgpath_nd = $getBannerData[$i]['imgpath_nd'];


            if(LaravelLocalization::getCurrentLocale() == "en")
            {
              $imgpath_st = "images/banner/back/".$imgpath_st;
              $imgpath_nd = "images/banner/single/".$imgpath_nd;
            }
            else {
              $imgpath_st = "../images/banner/back/".$imgpath_st;
              $imgpath_nd = "../images/banner/single/".$imgpath_nd;
            }
            ?>
            ?>
            <li data-transition="fade">
              <img src="{{$imgpath_st}}"
                alt=""
                data-bgposition="center center"
                data-bgfit="cover"
                data-bgrepeat="no-repeat"
                class="rev-slidebg">

              <div class="tp-caption"
                data-x="177"
                data-y="188"
                data-start="1000"
                data-transform_in="x:[-300%];opacity:0;s:500;"><img src="{{asset('isite/img/slides/slide-title-border.png')}}" alt=""></div>
                @if(!empty($getBannerData[$i]['img_name']))
                <div class="tp-caption top-label"
                  data-x="227"
                  data-y="180"
                  data-start="500"
                  data-transform_in="y:[-300%];opacity:0;s:500;">{{$getBannerData[$i]['img_name'] }}</div>

                <div class="tp-caption"
                  data-x="480"
                  data-y="188"
                  data-start="1000"
                  data-transform_in="x:[300%];opacity:0;s:500;"><img src="{{asset('isite/img/slides/slide-title-border.png')}}" alt=""></div>

                <div class="tp-caption main-label"
                  data-x="135"
                  data-y="210"
                  data-start="1500"
                  data-whitespace="nowrap"
                  data-transform_in="y:[100%];s:500;"
                  data-transform_out="opacity:0;s:500;"
                  data-mask_in="x:0px;y:0px;">{{$getBannerData[$i]['img_title'] }}</div>
                @endif







              <div class="tp-caption"
                data-x="['610','610','610','630']"
                data-y="80"
                data-start="3950"
                data-transform_in="y:[300%];opacity:0;s:300;"><img src="{{$imgpath_nd}}" alt=""></div>
            </li>
            @endfor
          @endif
        </ul>
      </div>
    </div>
    <div class="image-gallery sort-destination full-width mb-0">
      <div class="isotope-item">
        <div class="image-gallery-item mb-0">
          <a href="#">
            <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
              <span class="thumb-info-wrapper">
                <img src="{{asset('images/project.jpg')}}" class="img-fluid" alt="">
                <span class="thumb-info-title">
                  <span class="thumb-info-inner">COURSES</span>
                  <span class="thumb-info-type">Lorem ipsum dolor sit amet, con aipis icing elit, sed do eiusmod tempor</span>
                </span>
                <span class="thumb-info-action">
                  <span class="thumb-info-action-icon"><i class="fas fa-link"></i></span>
                </span>
              </span>
            </span>
          </a>
        </div>
      </div>
      <div class="isotope-item">
        <div class="image-gallery-item mb-0">
          <a href="#">
            <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
              <span class="thumb-info-wrapper">
                <img src="{{asset('images/project-2.jpg')}}" class="img-fluid" alt="">
                <span class="thumb-info-title">
                  <span class="thumb-info-inner">UPCOMING EVENTS</span>
                  <span class="thumb-info-type">Lorem ipsum dolor sit amet, con aipis icing elit, sed do eiusmod tempor</span>
                </span>
                <span class="thumb-info-action">
                  <span class="thumb-info-action-icon"><i class="fas fa-link"></i></span>
                </span>
              </span>
            </span>
          </a>
        </div>
      </div>
      <div class="isotope-item">
        <div class="image-gallery-item mb-0">
          <a href="#">
            <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
              <span class="thumb-info-wrapper">
                <img src="{{asset('images/project-4.jpg')}}" class="img-fluid" alt="">
                <span class="thumb-info-title">
                  <span class="thumb-info-inner">APPOINTMENT</span>
                  <span class="thumb-info-type">Lorem ipsum dolor sit amet, con aipis icing elit, sed do eiusmod tempor</span>
                </span>
                <span class="thumb-info-action">
                  <span class="thumb-info-action-icon"><i class="fas fa-link"></i></span>
                </span>
              </span>
            </span>
          </a>
        </div>
      </div>
      <div class="isotope-item">
        <div class="image-gallery-item mb-0">
          <a href="#">
            <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
              <span class="thumb-info-wrapper">
                <img src="{{asset('images/project-5.jpg')}}" class="img-fluid" alt="">
                <span class="thumb-info-title">
                  <span class="thumb-info-inner">SUGGETIONS</span>
                  <span class="thumb-info-type">Lorem ipsum dolor sit amet, con aipis icing elit, sed do eiusmod tempor</span>
                </span>
                <span class="thumb-info-action">
                  <span class="thumb-info-action-icon"><i class="fas fa-link"></i></span>
                </span>
              </span>
            </span>
          </a>
        </div>
      </div>

      <div class="isotope-item">
        <div class="image-gallery-item mb-0">
          <a href="#">
            <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
              <span class="thumb-info-wrapper">
                <img src="{{asset('images/project-5.jpg')}}" class="img-fluid" alt="">
                <span class="thumb-info-title">
                  <span class="thumb-info-inner">REGISTER AS COACH</span>
                  <span class="thumb-info-type">Lorem ipsum dolor sit amet, con aipis icing elit, sed do eiusmod tempor</span>
                </span>
                <span class="thumb-info-action">
                  <span class="thumb-info-action-icon"><i class="fas fa-link"></i></span>
                </span>
              </span>
            </span>
          </a>
        </div>
      </div>

    </div>
    <hr class="tall">
    <div class="container">

      <div class="row text-center mt-5">
        <div class="col">
          {!! Lang::get('training.about_training_title') !!}
          {!! Lang::get('training.about_training_message') !!}



        </div>
      </div>
    </div>
    <hr class="tall">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          {!! Lang::get('training.trainee_course_title') !!}

          <p>Showing {{ count($TrainingCoursesData) }} results.</p>
        </div>
      </div>

      <div class="masonry-loader masonry-loader-showing">
        <div class="row products product-thumb-info-list mt-3" data-plugin-masonry>
          @if(count($TrainingCoursesData) > 0)
            @for($i=0;$i < count($TrainingCoursesData);$i++)
            <?php $url_link = '/training_courses_detail?id='.$TrainingCoursesData[$i]['id'];  ?>
            <div class="col-12 col-sm-6 col-lg-3 product">
              <!--<a href="{{ url($url_link) }}">
                <span class="onsale">Sale!</span>
              </a>-->
              <span class="product-thumb-info">
                <a href="{{ url($url_link) }}" class="add-to-cart-product">
                  <span><i class="fas fa-shopping-cart"></i> Enroll Now</span>
                </a>
                <a href="{{ url($url_link) }}">
                  <span class="product-thumb-info-image">
                    <span class="product-thumb-info-act">
                      <span class="product-thumb-info-act-left"><em>View </em></span>
                      <span class="product-thumb-info-act-right"><em><i class="fas fa-plus"></i> Detail</em></span>
                    </span>
                    <?php
                    if(LaravelLocalization::getCurrentLocale() == "en")
                    {
                      $imgpath = 'images/training/'.$TrainingCoursesData[$i]['img_path'];
                    }
                    else {
                      $imgpath = '../images/training/'.$TrainingCoursesData[$i]['img_path'];
                    }
                    ?>
                    <img alt="{{ $TrainingCoursesData[$i]['title'] }}" class="img-fluid" src="{{$imgpath}}">
                  </span>
                </a>
                <span class="product-thumb-info-content">
                  <a href="{{ url($url_link) }}">
                    <h4>{{ $TrainingCoursesData[$i]['name'] }}</h4>
                    <h5>{{ $TrainingCoursesData[$i]['title'] }}</h5>
                    <span class="price">
                      <del><span class="amount">AED {{ $TrainingCoursesData[$i]['oldprice'] }}</span></del>
                      <ins><span class="amount">AED {{ $TrainingCoursesData[$i]['newprice'] }}</span></ins>
                    </span>
                  </a>
                </span>
              </span>
            </div>
            @endfor
          @endif


        </div>
      </div>

      <!--<div class="row">
        <div class="col">
          <ul class="pagination float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item active"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div>
      </div>-->

    </div>
    <section id="team" class="py-3">
      <div class="container">
        <div class="row mt-5">
          <div class="col">
            {!! Lang::get('training.trainee_title') !!}
          </div>
        </div>
        <div class="row mb-5 pb-2">
          <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <span class="thumb-info thumb-info-hide-wrapper-bg">
              <span class="thumb-info-wrapper">
                <a href="about-me.html">
                  <img src="{{asset('images/training/lectures/1.png')}}" class="img-fluid" alt="">
                  <span class="thumb-info-title">
                    <span class="thumb-info-inner">{{ Lang::get('training.trainee_one_name') }}</span>
                    <span class="thumb-info-type">{{ Lang::get('training.trainee_one_title') }}</span>
                  </span>
                </a>
              </span>
              <span class="thumb-info-caption">
                <span class="thumb-info-caption-text">{{ Lang::get('training.trainee_one_desc') }}</span>
                <!--<span class="thumb-info-social-icons">
                  <a target="_blank" href="http://www.facebook.com/"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
                  <a href="http://www.twitter.com/"><i class="fab fa-twitter"></i><span>Twitter</span></a>
                  <a href="http://www.linkedin.com/"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
                </span>-->
              </span>
            </span>
          </div>
          <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <span class="thumb-info thumb-info-hide-wrapper-bg">
              <span class="thumb-info-wrapper">
                <a href="about-me.html">
                  <img src="{{asset('images/training/lectures/3.png')}}" class="img-fluid" alt="">
                  <span class="thumb-info-title">
                    <span class="thumb-info-inner">{{ Lang::get('training.trainee_two_name') }}</span>
                    <span class="thumb-info-type">{{ Lang::get('training.trainee_two_title') }}</span>
                  </span>
                </a>
              </span>
              <span class="thumb-info-caption">
                <span class="thumb-info-caption-text">{{ Lang::get('training.trainee_two_desc') }}</span>

              </span>
            </span>
          </div>
          <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-sm-0">
            <span class="thumb-info thumb-info-hide-wrapper-bg">
              <span class="thumb-info-wrapper">
                <a href="about-me.html">
                  <img src="{{asset('images/training/lectures/2.png')}}" class="img-fluid" alt="">
                  <span class="thumb-info-title">
                    <span class="thumb-info-inner">{{ Lang::get('training.trainee_three_name') }}</span>
                    <span class="thumb-info-type">{{ Lang::get('training.trainee_three_title') }}</span>
                  </span>
                </a>
              </span>
              <span class="thumb-info-caption">
                <span class="thumb-info-caption-text">{{ Lang::get('training.trainee_three_desc') }}</span>

              </span>
            </span>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <span class="thumb-info thumb-info-hide-wrapper-bg">
              <span class="thumb-info-wrapper">
                <a href="about-me.html">
                  <img src="{{asset('images/training/lectures/4.png')}}" class="img-fluid" alt="">
                  <span class="thumb-info-title">
                    <span class="thumb-info-inner">{{ Lang::get('training.trainee_four_name') }}</span>
                    <span class="thumb-info-type">{{ Lang::get('training.trainee_four_title') }}</span>
                  </span>
                </a>
              </span>
              <span class="thumb-info-caption">
                <span class="thumb-info-caption-text">{{ Lang::get('training.trainee_four_desc') }}</span>

              </span>
            </span>
          </div>
        </div>
      </div>
    </section>
</body>
</html>
@endsection
