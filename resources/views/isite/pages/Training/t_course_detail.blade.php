@extends('isite.layout.master')
@section('content')
<html>
<head>
    <script src="https://eu-gateway.mastercard.com/checkout/version/42/checkout.js"
    data-error="errorCallback"
    data-cancel="cancelCallback"
    data-complete="completeCallback">
    </script>
    <script type="text/javascript">
            function completeCallback(resultIndicator, sessionVersion) {
                /*var OrderID_id = $('#OrderID_id').val();
                var tracknumber = 'SC-'+OrderID_id;
                var route = "https://silvercity.ae/bill-print/"+tracknumber;
                location.replace(route);*/
            }
            function cancelCallback()
            {
              /*var OrderID_id = $('#OrderID_id').val();
              var tracknumber = 'SC-'+OrderID_id;
              var route = "https://silvercity.ae/cancel-order/"+tracknumber;
              location.replace(route);*/
            }
            function errorCallback()
            {
              /*var OrderID_id = $('#OrderID_id').val();
              var tracknumber = 'SC-'+OrderID_id;
              var route = "https://silvercity.ae/error-order/"+tracknumber;
              location.replace(route);*/
            }

        </script>
        <!-- INCLUDE SESSION.JS JAVASCRIPT LIBRARY -->
        <script src="https://eu-gateway.mastercard.com/form/version/42/merchant/7000670/session.js"></script>
</head>
<body>
  <section class="page-header">
    <div class="container">
      <div class="row">
        <div class="col">
          <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Specific Course</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h1>Specific Course</h1>
        </div>
      </div>
    </div>
  </section>
  <div class="container">
    @if(count($TrainingCoursesData) > 0)
    <?php $imgpath = 'images/training/'.$TrainingCoursesData[0]['img_path']; ?>
    <div class="row">
      <div class="col">
        <div class="portfolio-title">
          <div class="row">
            <!--<div class="portfolio-nav-all col-lg-1">
              <a href="#" data-tooltip data-original-title="Back to list"><i class="fas fa-th"></i></a>
            </div>-->
            <div class="col-lg-10 text-center">
              <h2 class="mb-0">{{$TrainingCoursesData[0]['name']}}</h2>
            </div>
            <!--<div class="portfolio-nav col-lg-1">
              <a href="#" class="portfolio-nav-prev" data-tooltip data-original-title="Previous">
                <i class="fas fa-chevron-left"></i></a>
              <a href="#" class="portfolio-nav-next" data-tooltip data-original-title="Next"><i class="fas fa-chevron-right"></i></a>
            </div>-->
          </div>
        </div>

        <hr class="tall">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4">

        <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 3000}">
          <div>
            <span class="img-thumbnail d-block">
              <img alt="" class="img-fluid" src="{{$imgpath}}">
            </span>
          </div>
        </div>

      </div>

      <div class="col-lg-8">

        <div class="portfolio-info">
          <div class="row">
            <div class="col-md-6">
              <h5>Project Description</h5>
            </div>
            <!--<div class="col-md-6 text-left text-lg-right">
              <ul>
                <li>
                  <a href="#" data-tooltip data-original-title="Like"><i class="fas fa-heart"></i></a>
                </li>
                <li>
                  <i class="fas fa-calendar-alt"></i>
                </li>
                <li>
                  <i class="fas fa-tags"></i> <a href="#"></a>, <a href="#"></a>
                </li>
              </ul>
            </div>-->
          </div>
        </div>

        <p class="mt-2">{!! $TrainingCoursesData[0]['desc'] !!}</p>

        <a href="#"  data-toggle="modal" data-target="#myModal"  class="btn btn-primary btn-icon"><i class="fas fa-external-link-alt"></i>Endroll Now</a> <span class="arrow hlb appear-animation" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>



      </div>
    </div>
    </div>
    @endif
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title">Enroll Course</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form id="contact-form" action="{{ url('/storenewenquiries') }}" method="post" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_id" id="lastID" value="0">

            <input type="hidden" name="inp_idtype" value="0">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input class="form-control" name="inp_name" id="inp_name" placeholder="Your Name *" type="text" required>
                  <span style="color:red;" class="help-block span_name">

                  </span>
                </div>

                <div class="form-group">
                  <input class="form-control" name="inp_email" id="inp_email" placeholder="Your Email *" type="email" required>
                  <span style="color:red;" class="help-block span_email">

                  </span>
                </div>

                <div class="form-group">
                  <input class="form-control" name="inp_emp_at" id="inp_emp_at" placeholder="Empolyer at *" type="text" required>
                  <span style="color:red;" class="help-block span_emp_at">

                  </span>
                </div>

                <div class="form-group">
                  <input class="form-control" name="inp_jobtitle" id="inp_jobtitle" placeholder="Job title *" type="text" required>
                  <span style="color:red;" class="help-block span_jobtitle">

                  </span>
                </div>

                <div class="form-group">
                  <input class="form-control" name="inp_phone" id="inp_phone" placeholder="Phone *" type="text" required>
                  <span style="color:red;" class="help-block span_phone">

                  </span>
                </div>

                <div class="form-group">
                  <label id="lbl_wpay">1 AED</label>
                </div>

                <div class="form-group">
                  <select name="inp_crsinfo" id="inp_crsinfo" class="form-control">
                    <option value="0" selected="">-- Where you know about course --</option>
                    <option value="Friends">Friends</option>
                    <option value="Twitter">Twitter</option>
                    <option value="Facebook">Facebook</option>
                    <option value="Instagram">Instagram</option>
                    <option value="Snapchat">Snapchat</option>
                  </select>
                  <span style="color:red;" class="help-block span_crsinfo">

                  </span>
                </div>


              </div><!-- Col 6 end -->
            </div><!-- Form row end -->


          </form><!-- Form end -->
        </div>
        <div class="modal-footer">
          <div class="clearfix">
            <button class="btn btn-primary validationFields" type="submit">Submit</button>

          </div>
          <div class="clearfix">
            <button class="btn btn-default" type="submit" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>

    </div>
  </div>
  <script src="{{asset('js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
  <script src="{{asset('js/isite/payCard.js')}}" type="text/javascript"></script>
</body>
</html>
@endsection
