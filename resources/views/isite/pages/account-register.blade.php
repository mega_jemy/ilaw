@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>

  <section id="main-container" class="main-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12">
					<div class="contact-form-wrapper">
						<h3 class="title-normal">Fill This Form</h3>
            @if(LaravelLocalization::getCurrentLocaleName() == 'English')
              <?php $lnk = LaravelLocalization::getLocalizedURL('en', url('register-new-user'))?>
            @elseif(LaravelLocalization::getCurrentLocaleName() == 'Arabic')
              <?php $lnk = LaravelLocalization::getLocalizedURL('ar', url('register-new-user'))?>
            @endif
            <form class="row" role="form" action="{{ url($lnk) }}" method="POST">
              <div class="col-sm-6">
                <div class="form-group{{ $errors->has('txt_firstname') ? ' has-error' : '' }}">
                  <label for="reg-fn">First Name</label>
                  <input name="txt_firstname" placeholder="First Name"
                  value="{{ old('txt_firstname') }}" class="form-control" type="text" id="reg-fn" required>
                  @if ($errors->has('txt_firstname'))
                      <span class="help-block">
                          <strong>{{ $errors->first('txt_firstname') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group{{ $errors->has('txt_lastname') ? ' has-error' : '' }}">
                  <label for="reg-ln">Last Name</label>
                  <input class="form-control" type="text" id="reg-ln" required placeholder="Last Name"
                  value="{{ old('txt_lastname') }}" name="txt_lastname">
                  @if ($errors->has('txt_lastname'))
                      <span class="help-block">
                          <strong>{{ $errors->first('txt_lastname') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group{{ $errors->has('reg_email') ? ' has-error' : '' }}">
                  <label for="reg-email">E-mail Address</label>
                  <input  name="reg_email" placeholder="Email"
                  value="{{ old('reg_email') }}" class="form-control" type="email" id="reg-email" required>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('reg_email') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group{{ $errors->has('txt_phone') ? ' has-error' : '' }}">
                  <label for="reg-phone">Phone Number</label>
                  <input class="form-control" type="text" id="reg-phone" required name="txt_phone"
                  placeholder="Phone"
                  value="{{ old('txt_phone') }}">
                  @if ($errors->has('txt_phone'))
                      <span class="help-block">
                          <strong>{{ $errors->first('txt_phone') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="reg-pass">Password</label>
                  <input name="password" placeholder="Password"
                  class="form-control" type="password" id="reg-pass" required>
                  @if ($errors->has('password'))
                       <span class="help-block">
                           <strong>{{ $errors->first('password') }}</strong>
                       </span>
                   @endif
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  <label for="reg-pass-confirm">Confirm Password</label>
                  <input name="password_confirmation" placeholder="Confirm Password"
                   class="form-control" type="password" id="reg-pass-confirm" required>
                   @if ($errors->has('password_confirmation'))
                       <span class="help-block">
                           <strong>{{ $errors->first('password_confirmation') }}</strong>
                       </span>
                   @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group{{ $errors->has('opt_countryname') ? ' has-error' : '' }}">
                  <label for="account-country">Gender</label>
                  <select name="opt_gender" class="form-control" id="account-gender">
                    <option value="0" selected="selected">-- Select gender --</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>

                  </select>
                  @if ($errors->has('opt_countryname'))
                      <span class="help-block">
                          <strong>{{ $errors->first('opt_countryname') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group{{ $errors->has('opt_countryname') ? ' has-error' : '' }}">
                  <label for="account-country">Country</label>
                  <select name="opt_countryname" class="form-control" id="account-country">
                    <option value="0" selected="selected">-- Select Country --</option>
                     @if (count($country_var) > 0)
                      @for ($i = 0; $i < count($country_var) ; $i++)
                        <option value="{{ $country_var[$i]->id }}">{{ $country_var[$i]->name }}</option>
                      @endfor

                     @endif
                  </select>
                  @if ($errors->has('opt_countryname'))
                      <span class="help-block">
                          <strong>{{ $errors->first('opt_countryname') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group{{ $errors->has('opt_cityname') ? ' has-error' : '' }}">
                  <label for="reg-city">City</label>
                  <input name="opt_cityname" placeholder="City"
                   value="{{ old('opt_cityname') }}"
                   class="form-control" type="text" id="reg-city" required>
                   @if ($errors->has('opt_cityname'))
                       <span class="help-block">
                           <strong>{{ $errors->first('opt_cityname') }}</strong>
                       </span>
                   @endif
                </div>
              </div>
              <div class="col-12 text-center text-sm-right">
                <button class="btn btn-primary margin-bottom-none" type="submit">Register</button>
                <br>
                if you have an account, login know<a class="navi-link" href="{{url('/login')}}">Login</a>
              </div>
            </form><!-- Form end -->
					</div><!-- Comments form end -->

				</div><!-- From col end -->

				<div class="col-md-12 col-lg-4">
					<div class="contact-page-info">
						<div class="contact-info-box">
							<i class="ion-ios-location"></i>
							<div class="contact-info-box-content">
								<h4>Address:</h4>
								<p style="color:black;">
                - United Arab Emirates – Sharjah – Cornish AL Buhairah – Bel Rashid Tower – Office 1704 BR1

                </p>
                <p style="color:black;">
                -  BR2 United Arab Emirates – Ras AL Kaiymah – Julphar Avenue 1st Floor
                </p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-email"></i>
							<div class="contact-info-box-content">
								<h4>Email:</h4>
								<p>infoline@ilaw.ae</p>
								<p style="color:black;">Skype:</p><p>ilawuae</p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-android-call"></i>
							<div class="contact-info-box-content">
								<h4>Phone:</h4>
								<p>+(971) 6 556 5566</p>
                <h4>Fax:</h4>
								<p>+(971) 6 55 66 690</p>
							</div>
						</div>

					</div>
				</div><!-- Contact info col end -->


			</div><!--/ Content row end -->
		</div><!--/ Container end -->

	</section><!-- Main container end -->


</body>
</html>
@endsection
