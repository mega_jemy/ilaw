@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <section class="page-header">
    <div class="container">
      <div class="row">
        <div class="col">
          <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/firm') }}">Firm</a></li>
            <li class="active">iLAW Care</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h1>iLAW Care</h1>
        </div>
      </div>
    </div>
  </section>
  <div class="container">

    <div class="row">
      <div class="col-lg-3 order-2 order-lg-1">
        <aside class="sidebar">

          <h4 class="heading-primary">Area of Firm</h4>
          <ul class="nav nav-list flex-column mb-5">
            <li class="nav-item"><a href="{{ url('/ilaw_care') }}">iLAW Care</a></li>
            <li class="nav-item"><a href="{{ url('intellectual_property') }}">Intellectual Porperty</a></li>
            <li class="nav-item"><a href="{{ url('/debt_collection') }}">Debt Collection</a></li>
            <li class="nav-item"><a href="{{ url('litigation_dispute') }}">Litigation & Dispute</a></li>
          </ul>
        </aside>
      </div>
      <div class="col-lg-9 order-1 order-lg-2">
        <img class="img-fluid" src="" alt="" />
        <h3 class="title-classic">Fill This Form</h3>



        <div class="row">
          <div class="col">
            <div class="row">
              <form id="contact-form" action="{{ url('/storeilawcare') }}" method="post" role="form">
                <input type="hidden" name="_lang" value="{{ LaravelLocalization::getCurrentLocale() }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="inp_idtype" value="0">
  							<div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Please select from below :</label>
                      <br>
                      <label><input name="inp_rdtype" type="radio" value="">Normal</label>
                      <br>
                      <label><input name="inp_rdtype" type="radio" value="">Disabled</label>

  									</div>
                    <div class="form-group">
                      <select name="opt_countryname" id="optcountryname" class="form-control">
                        <option value="0" selected="selected">-- Select Country --</option>
                         @if (count($country_var) > 0)
                          @for ($i = 0; $i < count($country_var) ; $i++)
                            <option value="{{ $country_var[$i]->id }}">{{ $country_var[$i]->name }}</option>
                          @endfor

                         @endif
                      </select>
                    </div>
  									<div class="form-group">
  										<input class="form-control" name="inp_fullname" id="name" placeholder="Full Name *" type="text" required>
  									</div>

  									<div class="form-group">
  										<input class="form-control" name="inp_email" id="email" placeholder="Email *" type="email" required>
  									</div>

                    <div class="form-group">
  										<input class="form-control" name="inp_phone" id="phone" placeholder="Phone *" type="text" required>
  									</div>

                    <div class="form-group">
  										<input class="form-control" name="inp_job" id="job" placeholder="Job *" type="text" required>
  									</div>

                    <div class="form-group">
                      <input type="text" class="form-control" name="inp_message" id="message" placeholder="Your message *" required />
                    </div>
                    <div class="form-group">
                      <label>Please select Branch :</label>
                      <br>
                      <label><input name="inp_rdbranch" type="radio" value="">Sharjah</label>
                      <br>
                      <label><input name="inp_rdbranch" type="radio" value="">Ras Al-khaimah</label>
  									</div>

  								</div><!-- Col 6 end -->
  							</div><!-- Form row end -->

  							<div class="clearfix">
  								<button class="btn btn-primary" type="submit">Submit</button>
  							</div>
  						</form><!-- Form end -->
							<!--<div class="col-md-6">
								<h4>Things To Consider</h4>
								<p>Haque ipsa quae ab illo inventore veritatis aea sunt explicabo. Nemo enitem quia voluptas sit aspernatur aut odit </p>
								<ul class="unstyled arrow">
									<li>Aut fugit, sed quia consequuntur magni dolores eos quone</li>
									<li>Novoluptatem sequi nesciunt. Neque porrest, qui dolo</li>
									<li>Serem ipsum quia dolor sit amet consectetur adipis</li>
									<li>Bosici velit, sed quia non numquam eiodi tempora incidunt</li>
									<li>Tuit labore et dolore magnam aliq quaerat voluptatem</li>
								</ul>
							</div>--><!-- Col 6 end -->

							<!--<div class="col-md-6">
								<h4>How We Help</h4>
								<p>Haque ipsa quae ab illo inventore veritatis aea sunt explicabo. Nemo enitem quia voluptas sit aspernatur aut odit </p>
								<ul class="unstyled check">
									<li>Aut fugit, sed quia consequuntur magni dolores eos quone</li>
									<li>Novoluptatem sequi nesciunt. Neque porrest, qui dolo</li>
									<li>Serem ipsum quia dolor sit amet consectetur adipis</li>
									<li>Bosici velit, sed quia non numquam eiodi tempora incidunt</li>
									<li>Tuit labore et dolore magnam aliq quaerat voluptatem</li>
								</ul>
							</div>--><!-- Col 6 end -->
						</div><!-- Row end -->
          </div>
        </div>

        <hr class="tall">
      </div>

    </div>

  </div>
</body>
</html>
@endsection
