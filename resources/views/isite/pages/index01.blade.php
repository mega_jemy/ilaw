@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <div class="slider-container rev_slider_wrapper">
      <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 500, 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,500]}">
        <ul>
          @if(count($getBannerData) > 0)
            @for($i=0;$i < count($getBannerData);$i++)
            <?php
            $imgpath_st = $getBannerData[$i]['imgpath_st'];
            $imgpath_nd = $getBannerData[$i]['imgpath_nd'];

            $imgpath_st = "images/banner/back/".$imgpath_st;
            $imgpath_nd = "images/banner/single/".$imgpath_nd;
            ?>
            <li data-transition="fade">
              <img src="{{$imgpath_st}}"
                alt=""
                data-bgposition="center center"
                data-bgfit="cover"
                data-bgrepeat="no-repeat"
                class="rev-slidebg">

              <div class="tp-caption"
                data-x="177"
                data-y="188"
                data-start="1000"
                data-transform_in="x:[-300%];opacity:0;s:500;"><img src="{{asset('isite/img/slides/slide-title-border.png')}}" alt=""></div>

              <div class="tp-caption top-label"
                data-x="227"
                data-y="180"
                data-start="500"
                data-transform_in="y:[-300%];opacity:0;s:500;">{{$getBannerData[$i]['img_name'] }}</div>

              <div class="tp-caption"
                data-x="480"
                data-y="188"
                data-start="1000"
                data-transform_in="x:[300%];opacity:0;s:500;"><img src="{{asset('isite/img/slides/slide-title-border.png')}}" alt=""></div>

              <div class="tp-caption main-label"
                data-x="135"
                data-y="210"
                data-start="1500"
                data-whitespace="nowrap"
                data-transform_in="y:[100%];s:500;"
                data-transform_out="opacity:0;s:500;"
                data-mask_in="x:0px;y:0px;">{{$getBannerData[$i]['img_title'] }}</div>

              <div class="tp-caption bottom-label"
                data-x="['185','185','185','95']"
                data-y="280"
                data-start="2000"
                data-fontsize="['20','20','20','30']"
                data-transform_in="y:[100%];opacity:0;s:500;">Check our services.</div>




              <div class="tp-caption"
                data-x="['610','610','610','630']"
                data-y="80"
                data-start="3950"
                data-transform_in="y:[300%];opacity:0;s:300;"><img src="{{$imgpath_nd}}" alt=""></div>
            </li>
            @endfor
          @endif
        </ul>
      </div>
    </div>
    <div class="home-intro" id="home-intro">
      <div class="container">



      </div>
    </div>

    <div class="container">

      <div class="row text-center">
        <div class="col">
          <h1 class="mb-2 word-rotator-title">
            iLAW is
            <strong class="inverted">
              <span class="word-rotator" data-plugin-options="{'delay': 2000, 'animDelay': 300}">
                <span class="word-rotator-items">
                  <span>incredibly</span>
                  <span>especially</span>
                  <span>extremely</span>
                </span>
              </span>
            </strong>

          </h1>
          <p class="lead">

          </p>
        </div>
      </div>

    </div>

    <div class="container">

      <h2>Our <strong>Services</strong></h2>

      <div class="row align-items-center">
        <div class="col-lg-10">
          <p class="lead">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non pulvinar. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh metus.
          </p>
        </div>
        <div class="col-lg-2">
          <a href="#" class="btn btn-lg btn-primary">Contact Us!</a>
        </div>
      </div>

      <hr>

      <div class="featured-boxes">
        <div class="row">
          <!------------------------------------------------------------------------------------------------------------------->
          <div class="col-lg-3 col-sm-6">
            <div class="featured-box featured-box-primary featured-box-effect-1 mt-0 mt-lg-5">
              <div class="box-content">
                <i class="icon-featured fas fa-user"></i>
                <h4 class="text-uppercase">Loved by Customers</h4>
                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus.</p>
                <p><a href="http://preview.oklerthemes.com/" class="lnk-primary learn-more">Learn More <i class="fas fa-angle-right"></i></a></p>
              </div>
            </div>
          </div>
          <!------------------------------------------------------------------------------------------------------------------->


        </div>
      </div>


      <hr class="tall">

      <div class="row">
        <div class="col">
          <h2>Our <strong>FIELDS</strong></h2>
          <p>The law firm offers a wide range of professional specialized high quality services in all significant fields of law, including</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 mb-4 mb-lg-0">
          <div class="accordion accordion-primary" id="accordion2Primary">
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryOne">
                    Criminal Law
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryOne" class="collapse show">
                <div class="card-body">
                  <p class="mb-0">As the United Arab Emirates are characterized with safe, security, and development, as well laws amendment constantly to be up to date with the era, as it is proactive in issuing and legislating the laws that keep up with this development, including the law of discrimination and hatred, Information Technology law and Wadima law along with the penal law, and we offer legal services in criminal matters and all its branches (penalties - discrimination and hatred – Information Technology - Traffic) through advocacy and pleading before the criminal courts and departments through attending its hearings with the defendants or personal rights plaintiffs' (civil) alike, as well as submitting the necessary services starting from police and prosecutors stations until the completion of the consideration of the cases and reaching the best possible results. The firm is advocated in various kinds of cases and in its all stages and levels and provides financial provisional release on bail or personal requests. Our experience also extends to the civil claim for compensating the client against the damages that has been sustained by him as a result of the crime; obtain a judgment for compensation whether before the criminal or civil courts.
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryTwo">
                    Commerical LAW
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryTwo" class="collapse">
                <div class="card-body">
                  <p class="mb-0">Due to the vitality of commercial activity in the United Arab Emirates and its strategic role in the prosperity of the overall economy of the state, the firm is submitting legal services in various fields including: insurance - Commercial mortgages - sales contracts and the merges - Banking works- Business financing contracts - Equipment rental - commercial contracts - sales and purchase agreements – ownership of commercial assets - incorporation and licensing businesses and joint ventures- bankruptcy and insolvency - the international trade, preparation and the drafting of international trade contracts - commercial agencies and commercial sales - land, sea and air transportation - illegal competition in commercial transactions.
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryThree">
                    Commerical Companies LAW
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryThree" class="collapse">
                <div class="card-body">
                  <p class="mb-0">The United Arab Emirates is attracting a huge number of foreign and regional investments each year, and providing investment facilities for several procedures in establishing all kinds of commercial companies. Recently, the state enacted the modern Commercial Companies Law to cope with the economic development. Accordingly, our firm is providing legal services concerning corporate cases and the preparation of the statement of claims that are related thereto for defending and pleading before the courts to achieve the best possible findings through the perfect knowledge of commercial companies' law and understanding of what is new in this field.
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryFour">
                    Civil LAW
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryFour" class="collapse">
                <div class="card-body">
                  <p class="mb-0">Whereas the civil law has paramount importance in knowing the rights of persons and organize all civil transactions between them, accordingly , our firm is interested in providing the best legal services concerning all different kinds of civil cases (such as compensation claims - guardianship - Contracting – mortgage - sale - donation – contracts…).
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryFive">
                    Labor LAW
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryFive" class="collapse">
                <div class="card-body">
                  <p class="mb-0">The United Arab Emirates is considered to be one of the most labor attractions. It is also one of the most active countries in enacting the laws sponsoring the rights of both employees and employers. Accordingly, our firm is providing the best legal services in relation to labor law through advocacy and the defense for both employers and employees to achieve the best possible findings and give everyone his right.
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimarySex">
                    Real Estate LAW
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimarySex" class="collapse">
                <div class="card-body">
                  <p class="mb-0">The United Arab Emirates is encouraging real estate investment, and the state is classified among the best global attractive destinations for investors in the real estate field, which would make it a forerunner in the legislation of real estate laws that guarantees the rights of the parties to the real estate transactions. Therefore, our firm is offering the best legal services in the real estate field, which includes the representation of real estate companies or individuals in real estate claims and the preparation of statement of claims concerning real estate also advocacy, defending, pleading and preparing all the memoranda that are related to this case to achieve the best possible and satisfactory results for our clients.
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimarySeven">
                    Tenancy LAW
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimarySeven" class="collapse">
                <div class="card-body">
                  <p class="mb-0">The rental transaction is the link between lessor and lessee, and governed by the Tenancy Laws of each Emirate, and our firm has a full knowledge of each Emirate's Tenancy Law within the UAE and we provide the best legal services in this significant field in many ways that includes the representation of the lessor or lessee before committees of resolving rental disputes and prepare a statement of claims for rental disputes and defend the rental parties in order to achieve the best possible and satisfactory results for our clients.
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryEight">
                    Maritime LAW
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryEight" class="collapse">
                <div class="card-body">
                  <p class="mb-0">The United Arab Emirates is considered to be one of the most important countries embracing the largest seaports in the world. and our firm is offering the best legal services in the field of maritime law and maritime trade that are related to all maritime claims (such as maritime lien – disputes of marine insurance – claims for compensation- the responsibility of the shipping carrier – maritime mortgage...).
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryNine" aria-expanded="false">
                    Personal Status LAW
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryNine" class="collapse">
                <div class="card-body">
                  <p class="mb-0">The family is the core of the society and the basis of its renaissance, and since the United Arab Emirates is concerned with the family and organizing the family relations through the enactment and legislation of Personal Status Law, Our firm is offering the best legal services in the field of personal status law through all kinds of personal status cases which may include , for example, (alimonies - custodianship - guardianship - inheritance - the estates and testaments- the visitation right - kinship ...). We are doing our best firstly for settling the conflict between the parties to maintain the family bonding and preventing the dispersion of children and reaching an amicable agreement preserves the rights of all parties while taking all necessary legal action to grant our client the legitimate rights guaranteed by the law.
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
          </div>
        </div>

      </div>

      <!--<div class="row">
        <div class="col">
          <hr class="tall">
        </div>
      </div>-->

    </div>
    <!----------------------------------------------------------------------------------------------------->
    <hr class="mt-0 mb-1 solid">
    <!----------------------------------------------------------------------------------------------------->

    <div class="container">

      <!--<div class="row">
        <div class="col">
          <hr class="tall mt-4">
        </div>
      </div>-->





      <div class="row text-center pt-4">
        <div class="col">
          <h2 class="mb-2 word-rotator-title">
            We're not the only ones
            <strong>
              <span class="word-rotator" data-plugin-options="{'delay': 3500, 'animDelay': 400}">
                <span class="word-rotator-items">
                  <span>excited</span>
                  <span>happy</span>
                </span>
              </span>
            </strong>
            about ilaw Toruism
          </h2>
          <h4 class="heading-primary lead tall">30,000 customers in 100 countries use Porto Template. Meet our customers.</h4>
        </div>
      </div>

      <div class="row text-center">
        <div class="owl-carousel owl-theme" data-plugin-options="{'items': 6, 'autoplay': true, 'autoplayTimeout': 3000}">
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-1.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-2.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-3.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-4.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-5.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-6.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-4.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-2.png')}}" alt="">
          </div>
        </div>
      </div>

    </div>
  <!-- Carousel -->
   <div id="main-slide" class="carousel slide" data-ride="carousel">

		<!-- Indicators -->
		<ol class="carousel-indicators">
		  	<li data-target="#main-slide" data-slide-to="0" class="active"></li>
		   <li data-target="#main-slide" data-slide-to="1"></li>
		   <li data-target="#main-slide" data-slide-to="2"></li>
		</ol><!--/ Indicators end-->

		<!-- Carousel inner -->
		<div class="carousel-inner">
		   <div class="carousel-item active" style="background-image:url({{asset("isite/images/slider/bg1.jpg")}})">
            <div class="slider-content">
               <div class="col-md-12">
              		<div class="slider-text wow slideInLeft" data-wow-delay=".2s">
              			<h2 class="slide-head">iLAW</h2>
                     <h3 class="slide-title">Lorem <span>Lorem</span> &amp;<br/>
                     <span>Lorem</span></h3>
                 	</div>
                  <div class="slider-img wow slideInRight" data-wow-delay=".5s">
                     	<img class="pull-right" src="{{asset('isite/images/slider/img1.png')}}" alt="">
                  </div>
              	</div>
            </div>
		   </div><!--/ Carousel item end -->

		   <div class="carousel-item" style="background-image:url({{asset("isite/images/slider/bg2.jpg")}})">
		    	<div class="slider-content">
               <div class="col-md-12">
              		<div class="slider-text text-right">
              			<h2 class="slide-head">Lorem</h2>
                     <h3 class="slide-title">Lorem<br/><span>Lorem</span> &amp; <span>Lorem</span></h3>

                     <p>
                     	<a href="#" class="slider btn btn-primary">Lorem Lorem Lorem Lorem</a>
                  	</p>
                 	</div>

              	</div>
            </div>
		   </div><!--/ Carousel item end -->

		</div><!-- Carousel inner end-->

		<!-- Controllers -->
		<a class="left carousel-control carousel-control-prev" href="#main-slide" data-slide="prev">
	    	<span><i class="fa fa-angle-left"></i></span>
		</a>
		<a class="right carousel-control carousel-control-next" href="#main-slide" data-slide="next">
	    	<span><i class="fa fa-angle-right"></i></span>
		</a>

	</div><!--/ Carousel end -->

  <section id="features" class="features">
    <div class="container">
      <div class="row features-row">

        <div class="feature-box col-lg-4 col-md-12">
          <span class="feature-icon">
            <i class="ion-social-buffer-outline"></i>
          </span>
          <div class="feature-box-content">
            <h3 class="title-normal">Lorem Lorem Lorem</h3>
            <p>Lorem Lorem Lorem, Lorem Lorem, Lorem Lorem Lorem</p>
          </div>
        </div><!--/ End 1st features -->

        <div class="feature-box two col-lg-4 col-md-12">
          <span class="feature-icon">
            <i class="ion-ios-search"></i>
          </span>
          <div class="feature-box-content">
            <h3 class="title-normal">Lorem Lorem Lorem<</h3>
            <p>Lorem Lorem Lorem, Lorem Lorem, Lorem Lorem Lorem</p>
          </div>
        </div><!--/ End 2nd features -->

        <div class="feature-box three col-lg-4 col-md-12">
          <span class="feature-icon">
            <i class="ion-ios-person-outline"></i>
          </span>
          <div class="feature-box-content">
            <h3 class="title-normal">Lorem Lorem Lorem<</h3>
            <p>Lorem Lorem Lorem, Lorem Lorem, Lorem Lorem Lorem</p>
          </div>
        </div><!--/ End 3rd features -->

      </div><!-- Content row end -->
    </div><!--/ Container end -->
  </section><!--/ Featured end -->



  <section id="practice-area" class="practice-area">
		<div class="container">
			<div class="row text-center">
            <div class="col">
               <h3 class="title-head">Services</h3>
               <p class="title-description">"Ibrahim Al Hosani Advocate, Legal Services & Training” offers a wide range of professional and high quality services in all chief fields of law, including </p>
            </div>
			</div><!--/ Title row end -->

      <div class="row">
				<div class="col-lg-4 col-md-12 mrb-30">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/family.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-people-outline"></i>
								</span>
								<h3 class="practice-title">Legal consultationt</h3>
                <h5 class="practice-title">Providing legal consultations </h5>
								<p class="practice-details">The Firm provides legal consultation for all clients, in order to reach the best results...</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 1st item col end -->

				<div class="col-lg-4 col-md-12 mrb-30">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/land.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-home-outline"></i>
								</span>
								<h3 class="practice-title">Drafting the Contarctst</h3>
                <h5 class="practice-title">Drafting contracts and Documents</h5>
								<p class="practice-details">“Pacta sunt servanda” The contract is the law of the contractors.</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 2nd item col end -->

				<div class="col-lg-4 col-md-12 mrb-30">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/sex.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-woman"></i>
								</span>
								<h3 class="practice-title">Banking Disputes</h3>
                <h5 class="practice-title">Banking Disputes</h5>
								<p class="practice-details">The UAE is witnessing an increase in the number of the banks and theirt...</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 3rd item col end -->

				<div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/finance.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-cash"></i>
								</span>
								<h3 class="practice-title">Disputes of Money</h3>
                <h5 class="practice-title">Disputes of Money Laundry</h5>
								<p class="practice-details">Money laundry or what is known as white collar crimes is one of the most...</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 4th item col end -->

				<div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/criminal.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-social-reddit-outline"></i>
								</span>
								<h3 class="practice-title">Disputes of maritime</h3>
                <h5 class="practice-title">Disputes of maritime and Air...</h5>
								<p class="practice-details">With the development of international trade, the marine sales were developed...</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 5th item col end -->

				<div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Legal Representation</h3>
                <h5 class="practice-title">Legal Representation and Pleading</h5>
								<p class="practice-details">Our Firm represents all clients before all types and grades of courts in the...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Warnings and Notifications of</h3>
                <h5 class="practice-title">Warnings and Notifications of...</h5>
								<p class="practice-details">Our Firm represents all clients before all types and grades of courts in the...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->


        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Arbitration</h3>
                <h5 class="practice-title">Arbitration</h5>
								<p class="practice-details">Arbitration is one of the most effective ways to settle disputes instead of...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Disputes of Multinational</h3>
                <h5 class="practice-title">Disputes of Multinational...</h5>
								<p class="practice-details">We provide our services in the disputes that are related to multinational...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Disputes of International</h3>
                <h5 class="practice-title">Disputes of International Trade...</h5>
								<p class="practice-details">International Trade Contract is considered to be one of the most risky contracts...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Disputes of Innominate...</h3>
                <h5 class="practice-title">Disputes of Innominate Contracts</h5>
								<p class="practice-details">The past few years have witnessed the emergence of Innominate Contracts...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Legal Services for Contracting...</h3>
                <h5 class="practice-title">Legal Services for Contracting..</h5>
								<p class="practice-details">Through our Firm, we offer an integrated package of legal and consulting services...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Intellectual Property Legal Services</h3>
                <h5 class="practice-title">Intellectual Property Legal Services</h5>
								<p class="practice-details">Through our Firm, we offer all legal consultation and services in the field...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Legal Translation</h3>
                <h5 class="practice-title">Legal Translation</h5>
								<p class="practice-details">Through our Firm, we offer all sorts of legal translation based on local...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Training Center</h3>
                <h5 class="practice-title">Training Center</h5>
								<p class="practice-details">Training is one of the main activities of our firm, and the goal of this center...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->


			</div><!--/ Content row end -->

			<div class="general-btn text-center"><a class="btn btn-primary" href="#">View All</a></div>

		</div><!--/ Container end -->
	</section><!-- Practice area end -->

  <section class="call-to-action">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="action-title">Bad Things <br/> Happen</h2>
					<h3 class="action-subtitle">We are here to help</h3>
					<p class="action-btn">
						<a href="#" class="btn btn-primary"><i class="ion-android-call"></i>+(971) 6 556 5566</a>
					</p>
				</div>
			</div>
		</div><!--/ Container end -->
	</section><!-- Action end -->

  <section id="main-container" class="main-container">
		<div class="container">
			<div class="row">
        <div class="col-md-6">
        </div><!--/ Col end -->
			</div><!-- Row 1 end -->
      <div class="gap-30"></div>
      <h3 class="border-title">Feature Box</h3>
			<div class="row">
				<div class="col-lg-6">
					<div class="feature">
						<span class="feature-icon">
							<i class="ion-ios-world-outline"></i>
						</span>
						<h3 class="feature-title">Huge Experience</h3>
            <div class="fb-page" data-href="https://www.facebook.com/ilawuae" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/ilawuae" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ilawuae">Ibrahim Al Hosani Advocate &amp; Legal Services &amp; Training Centre LLC</a></blockquote></div>
						<p class="feature-desc">Dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt. Dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididuntDolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt</p>
					</div>
				</div><!--/ End 1st -->
				<div class="col-lg-6">
					<div class="feature">
						<span class="feature-icon">
							<i class="ion-ios-people-outline"></i>
						</span>
						<h3 class="feature-title">Skilled Attorneys</h3>
            <div class="ch-item">

							<a class="twitter-timeline" data-height="500" href="https://twitter.com/ilawuae">Tweets by ilawuae</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

						</div><!-- end ch-item -->
						<p class="feature-desc">Dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt. Dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididuntDolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt</p>
					</div>
				</div><!--/ End 2nd -->
			</div>



		</div><!-- Container end -->
	</section><!-- Main container end -->



  <section id="image-block" class="image-block no-padding">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-md-12 ts-padding img-block-left">
					<h3 class="title-classic">Why iLaw House</h3>
					<p>We've Ibrahim AlHosni Office of Law Firm since our beginnings, put our approach to resolving all legal issues for our customers and how to deal with them relying on the principle of complementary and negotiation.</p>

					<div class="gap-20"></div>

					<div class="row">
						<div class="col-md-6 col-12">
							<div class="feature">
								<span class="feature-icon">
									<i class="ion-ios-world-outline"></i>
								</span>
								<h3 class="feature-title">Huge Experience</h3>
								<p class="feature-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
							</div>
						</div><!--/ End 1st -->
						<div class="col-md-6 col-12">
							<div class="feature">
								<span class="feature-icon">
									<i class="ion-ios-people-outline"></i>
								</span>
								<h3 class="feature-title">Skilled Attorneys</h3>
								<p class="feature-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
							</div>
						</div><!--/ End 2nd -->
					</div><!--1st Row end -->

					<div class="gap-20"></div>

					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="feature">
								<span class="feature-icon">
									<i class="ion-ios-lightbulb-outline"></i>
								</span>
								<h3 class="feature-title">Amazing Strategy</h3>
								<p class="feature-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
							</div>
						</div><!--/ End 3rd -->
						<div class="col-md-6 col-xs-12">
							<div class="feature">
								<span class="feature-icon">
									<i class="ion-ios-analytics-outline"></i>
								</span>
								<h3 class="feature-title">Low Cost</h3>
								<p class="feature-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
							</div>
						</div><!--/ End 4th -->
					</div><!--1st Row end -->

				</div><!-- Col end -->
				<div class="col-lg-6 col-md-12 ts-padding" style="height:600px;background:url({{asset("isite/images/image-block-bg1.jpg")}}) 50% 50% / cover no-repeat;">
				</div><!-- Col end -->
			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</section><!-- Image block end -->



  <section id="image-block2" class="image-block2 no-padding">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-8 ts-padding img-block-left">
					<h3 class="title-classic">What People Say</h3>

					<div class="gap-40"></div>

						<div id="testimonial-carousel" class="owl-carousel owl-theme testimonial-slide">
				        <div class="item">
					        <div class="testimonial-content">
					            <p class="testimonial-text">
					              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitoa tion ullamco laboris nisi ut aliquip ex ea commodo consequat.
					            </p>
						         <div class="testimonial-thumb">
					            	<img src="{{asset('isite/images/clients/testimonial1.png')}}" alt="testimonial">
					            </div>
					            <h3 class="testimonial-name">Lorem Lorem<span>Customer Relation</span></h3>

					        </div>
				        </div><!--/ Item end -->

				        <div class="item">
					        <div class="testimonial-content">
					            <p class="testimonial-text">
					              Lorem Ipsum as their default model text, and a search for ‘lorem ipsum’ will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes accident.
					            </p>
						         <div class="testimonial-thumb">
					            	<img src="{{asset('isite/images/clients/testimonial2.png')}}" alt="testimonial">
					          	</div>
					          	<h3 class="testimonial-name">Lorem Lorem<span>Sr. Manager</span></h3>
					        </div>
				        </div><!--/ Item end -->
				        <div class="item">
				          	<div class="testimonial-content">
					            <p class="testimonial-text">
					              Lorem Ipsum as their default model text, and a search for ‘lorem ipsum’ will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes accident.
					            </p>
					            <div class="testimonial-thumb">
					            	<img src="{{asset('isite/images/clients/testimonial3.png')}}" alt="testimonial">
					        		</div>
					        		<h3 class="testimonial-name">Lorem Lorem<span>Accountant</span></h3>

				          	</div>
				        </div>
				    </div><!--/ Testimonial carousel end-->

				</div><!-- Col end -->

				<div class="col-lg-4 col-md-12 ts-padding img-block-right" style="height:575px;background:url({{asset("isite/images/image-block-bg2.jpg")}}) 50% 50% / cover no-repeat;">
					<div class="ts_counter">
						<div class="facts mrb-30">
							<span class="facts-icon">
								<i class="ion-social-bitcoin-outline"></i>
							</span>
							<div class="facts-num">
								<span class="sign">AED</span><h3 class="counter">960</h3><span class="amount">M</span>
								<h4 class="facts-title">Saved Till Now</h4>
							</div>
						</div><!-- 1st facts end -->

						<div class="facts mrb-30">
							<span class="facts-icon">
								<i class="ion-ios-people-outline"></i>
							</span>
							<div class="facts-num">
								<h3 class="counter">232</h3><span class="amount">K</span>
								<h4 class="facts-title">Clients</h4>
							</div>
						</div><!-- 2nd facts end -->

						<div class="facts mrb-30">
							<span class="facts-icon">
								<i class="ion-ios-briefcase-outline"></i>
							</span>
							<div class="facts-num">
								<h3 class="counter">731</h3><span class="amount">K</span>
								<h4 class="facts-title">Case Wins</h4>
							</div>
						</div><!-- 3rd facts end -->

						<div class="facts mrb-30">
							<span class="facts-icon">
								<i class="ion-ios-color-filter-outline"></i>
							</span>
							<div class="facts-num">
								<h3 class="counter">82</h3>
								<h4 class="facts-title">Awards</h4>
							</div>
						</div><!-- 4th facts end -->

					</div>
				</div><!-- Col end -->
			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</section><!--/ Image block 2 end -->



  <section id="clients" class="clients">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<h3 class="title-classic">A House of Trust</h3>
					<p class="lead">Lorem ipsum dolor sit amet, consec tetur adipi sicing elit, sed do eiusmod tempor incidi dunt ut labore et dolore magna aliqua romaro tomaiyn dilemaso onendo. </p>
				</div>
				<div class="col-md-6 ml-lg-auto">
					<ul class="row clients-row unstyled">
						<li class="col-lg-4 col-md-4 col-6">
							<span class="client-logo">
								<a href="#"><img src="{{asset('isite/images/clients/client1.png')}}" alt="" /> </a>
							</span>
						</li>
						<li class="col-lg-4 col-md-4 col-6">
							<span class="client-logo">
								<a href="#"><img src="{{asset('isite/images/clients/client2.png')}}" alt="" /> </a>
							</span>
						</li>
						<li class="col-lg-4 col-md-4 col-6">
							<span class="client-logo">
								<a href="#"><img src="{{asset('isite/images/clients/client3.png')}}" alt="" /> </a>
							</span>
						</li>
						<li class="col-lg-4 col-md-4 col-6">
							<span class="client-logo">
								<a href="#"><img src="{{asset('isite/images/clients/client4.png')}}" alt="" /> </a>
							</span>
						</li>
						<li class="col-lg-4 col-md-4 col-6">
							<span class="client-logo">
								<a href="#"><img src="{{asset('isite/images/clients/client5.png')}}" alt="" /> </a>
							</span>
						</li>
						<li class="col-lg-4 col-md-4 col-6">
							<span class="client-logo">
								<a href="#"><img src="{{asset('isite/images/clients/client6.png')}}" alt="" /> </a>
							</span>
						</li>
						<li class="col-lg-4 col-md-4 col-6">
							<span class="client-logo">
								<a href="#"><img src="{{asset('isite/images/clients/client7.png')}}" alt="" /> </a>
							</span>
						</li>
						<li class="col-lg-4 col-md-4 col-6">
							<span class="client-logo">
								<a href="#"><img src="{{asset('isite/images/clients/client8.png')}}" alt="" /> </a>
							</span>
						</li>
						<li class="col-lg-4 col-md-4 col-6">
							<span class="client-logo">
								<a href="#"><img src="{{asset('isite/images/clients/client9.png')}}" alt="" /> </a>
							</span>
						</li>
					</ul>

				</div><!-- Client logo end -->
			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</section><!--/ Clients end -->
</body>
</html>
@endsection
