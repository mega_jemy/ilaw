@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <div class="slider-container rev_slider_wrapper">
      <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 500, 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,500]}">
        <ul>
          @if(count($getBannerData) > 0)
            @for($i=0;$i < count($getBannerData);$i++)
            <?php
            $imgpath_st = $getBannerData[$i]['imgpath_st'];
            $imgpath_nd = $getBannerData[$i]['imgpath_nd'];

            $imgpath_st = "images/banner/back/".$imgpath_st;
            $imgpath_nd = "images/banner/single/".$imgpath_nd;
            ?>
            <li data-transition="fade">
              <img src="{{$imgpath_st}}"
                alt=""
                data-bgposition="center center"
                data-bgfit="cover"
                data-bgrepeat="no-repeat"
                class="rev-slidebg">

                @if(!empty($getBannerData[$i]['img_name']))
                <div class="tp-caption"
                  data-x="177"
                  data-y="188"
                  data-start="1000"
                  data-transform_in="x:[-300%];opacity:0;s:500;"><img src="{{asset('isite/img/slides/slide-title-border.png')}}" alt=""></div>

                <div class="tp-caption top-label"
                  data-x="227"
                  data-y="180"
                  data-start="500"
                  data-transform_in="y:[-300%];opacity:0;s:500;">{{$getBannerData[$i]['img_name'] }}</div>

                <div class="tp-caption"
                  data-x="480"
                  data-y="188"
                  data-start="1000"
                  data-transform_in="x:[300%];opacity:0;s:500;"><img src="{{asset('isite/img/slides/slide-title-border.png')}}" alt=""></div>

                  <div class="tp-caption main-label"
                    data-x="135"
                    data-y="210"
                    data-start="1500"
                    data-whitespace="nowrap"
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    data-mask_in="x:0px;y:0px;">{{$getBannerData[$i]['img_title'] }}</div>
                @endif



              <div class="tp-caption"
                data-x="['610','610','610','630']"
                data-y="80"
                data-start="3950"
                data-transform_in="y:[300%];opacity:0;s:300;"><img src="{{$imgpath_nd}}" alt=""></div>
            </li>
            @endfor
          @endif
        </ul>
      </div>
    </div>
    <div class="home-intro" id="home-intro">
      <div class="container">



      </div>
    </div>

    <!--<div class="container">

      <div class="row text-center">
        <div class="col">
          <h1 class="mb-2 word-rotator-title">
            iLAW is
            <strong class="inverted">
              <span class="word-rotator" data-plugin-options="{'delay': 2000, 'animDelay': 300}">
                <span class="word-rotator-items">
                  <span>incredibly</span>
                  <span>especially</span>
                  <span>extremely</span>
                </span>
              </span>
            </strong>

          </h1>
          <p class="lead">

          </p>
        </div>
      </div>

    </div>-->

    <div class="container">

      {!! Lang::get('index.service_title') !!}

      <div class="row align-items-center">
        <div class="col-lg-10">
          <p class="lead">
            {{ Lang::get('index.office_title') }}
          </p>
        </div>
        <!--<div class="col-lg-2">
          <a href="#" class="btn btn-lg btn-primary">Contact Us!</a>
        </div>-->
      </div>

      <hr>

      <div class="featured-boxes">
        <div class="row">

          @if(count($getServiceData) > 0)

            <?php  $id_lang = $getDataLnag[0]['id'];


            $coll_getCPData = collect($getServiceData)->where('idlang','=',$id_lang)->values()->toArray();
             ?>
             @if(count($coll_getCPData) > 0)
              @for($i=0;$i < count($coll_getCPData);$i++)
              <!------------------------------------------------------------------------------------------------------------------->
              <div class="col-lg-3 col-sm-6">
                <div class="featured-box featured-box-primary featured-box-effect-1 mt-0 mt-lg-5">
                  <div class="box-content">
                    <i class="icon-featured fas fa-user"></i>
                    <h4 class="text-uppercase">{{ $coll_getCPData[$i]['name']}}</h4>
                    <h5 class="text-uppercase">{{ $coll_getCPData[$i]['title']}}</h5>
                    <p>{!! $coll_getCPData[$i]['desc'] !!}</p>
                    <!--<p><a href="#" class="lnk-primary learn-more">Learn More <i class="fas fa-angle-right"></i></a></p>-->
                  </div>
                </div>
              </div>
              <!------------------------------------------------------------------------------------------------------------------->
              @endfor
             @endif
          @endif
          <!------------------------------------------------------------------------------------------------------------------->
        </div>
      </div>


      <hr class="tall">

      <div class="row">
        <div class="col">
          {!! Lang::get('index.fields_title') !!}
          <p>{{ Lang::get('index.office_fields_title') }}</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 mb-4 mb-lg-0">
          <div class="accordion accordion-primary" id="accordion2Primary">
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryOne">
                    {{ Lang::get('index.fields_one_title') }}
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryOne" class="collapse show">
                <div class="card-body">
                  <p class="mb-0">{{ Lang::get('index.fields_one_desc') }}</p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryTwo">
                    {{ Lang::get('index.fields_two_title') }}
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryTwo" class="collapse">
                <div class="card-body">
                  <p class="mb-0">{{ Lang::get('index.fields_two_desc') }}</p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryThree">
                    {{ Lang::get('index.fields_three_title') }}
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryThree" class="collapse">
                <div class="card-body">
                  <p class="mb-0">{{ Lang::get('index.fields_three_desc') }}
                  </p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryFour">
                    {{ Lang::get('index.fields_four_title') }}
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryFour" class="collapse">
                <div class="card-body">
                  <p class="mb-0">{{ Lang::get('index.fields_four_desc') }}</p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryFive">
                    {{ Lang::get('index.fields_five_title') }}
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryFive" class="collapse">
                <div class="card-body">
                  <p class="mb-0">{{ Lang::get('index.fields_five_desc') }}</p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimarySex">
                    {{ Lang::get('index.fields_six_title') }}
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimarySex" class="collapse">
                <div class="card-body">
                  <p class="mb-0">{{ Lang::get('index.fields_six_desc') }}</p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimarySeven">
                    {{ Lang::get('index.fields_seven_title') }}
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimarySeven" class="collapse">
                <div class="card-body">
                  <p class="mb-0">{{ Lang::get('index.fields_seven_desc') }}</p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryEight">
                    {{ Lang::get('index.fields_eight_title') }}
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryEight" class="collapse">
                <div class="card-body">
                  <p class="mb-0">{{ Lang::get('index.fields_eight_desc') }}</p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
            <div class="card card-default">
              <div class="card-header">
                <h4 class="card-title m-0">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryNine" aria-expanded="false">
                    {{ Lang::get('index.fields_nine_title') }}
                  </a>
                </h4>
              </div>
              <div id="collapse2PrimaryNine" class="collapse">
                <div class="card-body">
                  <p class="mb-0">{{ Lang::get('index.fields_nine_desc') }}</p>
                </div>
              </div>
            </div>
            <!----------------------------------------------------------------------------------------------------->
          </div>
        </div>

      </div>

      <!--<div class="row">
        <div class="col">
          <hr class="tall">
        </div>
      </div>-->

    </div>
    <!----------------------------------------------------------------------------------------------------->
    <hr class="mt-0 mb-1 solid">
    <!----------------------------------------------------------------------------------------------------->

    <div class="container">

      <!--<div class="row">
        <div class="col">
          <hr class="tall mt-4">
        </div>
      </div>-->





      <!--<div class="row text-center pt-4">
        <div class="col">
          <h2 class="mb-2 word-rotator-title">
            We're not the only ones
            <strong>
              <span class="word-rotator" data-plugin-options="{'delay': 3500, 'animDelay': 400}">
                <span class="word-rotator-items">
                  <span>excited</span>
                  <span>happy</span>
                </span>
              </span>
            </strong>
            about ilaw Toruism
          </h2>
          <h4 class="heading-primary lead tall">30,000 customers in 100 countries use Porto Template. Meet our customers.</h4>
        </div>
      </div>-->

      <!--<div class="row text-center">
        <div class="owl-carousel owl-theme" data-plugin-options="{'items': 6, 'autoplay': true, 'autoplayTimeout': 3000}">
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-1.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-2.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-3.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-4.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-5.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-6.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-4.png')}}" alt="">
          </div>
          <div>
            <img class="img-fluid" src="{{asset('isite/img/logos/logo-2.png')}}" alt="">
          </div>
        </div>
      </div>-->

    </div>
</body>
</html>
@endsection
