@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <div id="banner-area" class="banner-area" style="background-image:url({{asset('images/banner/banner3.jpg')}}">
		<!-- Subpage title start -->
		<div class="banner-text text-center">
     		<div class="container">
	        	<div class="row">
	        		<div class="col-12">
	        			<div class="banner-heading">
	        				<h1 class="banner-title">Litigation <span>Dispute</span></h1>
	        				<h2 class="banner-desc"></h2>
	        			</div>
			        	<ul class="breadcrumb">
			            <li>Home</li>
			            <li><a href="#"> Litigation Dispute</a></li>
		          	</ul>
	        		</div>
	        	</div>
       	</div>
    	</div><!-- Subpage title end -->
	</div><!-- Banner area end -->
  <section id="main-container" class="main-container">

		<div class="container">
      <?php
        $lang_regional = LaravelLocalization::getCurrentLocale();

      ?>
      <?php  $coll_getLangData = collect($getLangData)->where('regional','=',$lang_regional)->values()->toArray();
      $id_lang = $coll_getLangData[0]['id'];

      $coll_getCPData = collect($getCPData)->where('idlang','=',$id_lang)->values()->toArray();
       ?>
			<div class="row">
				<div class="col-lg-12 col-md-12">
          <?php
          $imgpath = $coll_getCPData[0]['img_path'];
          $imgpath = 'images/bussines_setup/'.$imgpath;
           ?>
					<img class="img-fluid" src="{{$imgpath}}" alt="{{ $coll_getCPData[0]['title'] }}" />

					<h3>{{ $coll_getCPData[0]['name'] }}</h3>
          <h5>{{ $coll_getCPData[0]['title'] }}</h5>
          {!! $coll_getCPData[0]['desc'] !!}

				</div><!-- col 8 end -->


			</div><!-- Single case row end -->




		</div><!--/ Container end -->

	</section><!-- Main container end -->
</body>
</html>
@endsection
