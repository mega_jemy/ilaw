@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <div id="banner-area" class="banner-area" style="background-image:url({{asset('images/ilawcare/care.png')}})">
  </div>
  <section id="main-container" class="main-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12">
					<div class="contact-form-wrapper">
						<h3 class="title-normal">Fill This Form</h3>
						<form id="contact-form" action="{{ url('/storeilawcare') }}" method="post" role="form">
              <input type="hidden" name="_lang" value="{{ LaravelLocalization::getCurrentLocale() }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="inp_idtype" value="0">
							<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Please select from below :</label>
                    <br>
                    <label><input name="inp_rdtype" type="radio" value="">Normal</label>
                    <br>
                    <label><input name="inp_rdtype" type="radio" value="">Disabled</label>

									</div>
                  <div class="form-group">
                    <select name="opt_countryname" id="optcountryname" class="form-control">
                      <option value="0" selected="selected">-- Select Country --</option>
                       @if (count($country_var) > 0)
                        @for ($i = 0; $i < count($country_var) ; $i++)
                          <option value="{{ $country_var[$i]->id }}">{{ $country_var[$i]->name }}</option>
                        @endfor

                       @endif
                    </select>
                  </div>
									<div class="form-group">
										<input class="form-control" name="inp_fullname" id="name" placeholder="Full Name *" type="text" required>
									</div>

									<div class="form-group">
										<input class="form-control" name="inp_email" id="email" placeholder="Email *" type="email" required>
									</div>

                  <div class="form-group">
										<input class="form-control" name="inp_phone" id="phone" placeholder="Phone *" type="text" required>
									</div>

                  <div class="form-group">
										<input class="form-control" name="inp_job" id="job" placeholder="Job *" type="text" required>
									</div>

  								<div class="col-md-12">
  									<div class="form-group">
  										<textarea class="form-control required-field" name="inp_message" id="message" placeholder="Your Comment *" rows="8" required></textarea>
  									</div>
  								</div><!-- Col 6 end -->
                  <div class="form-group">
                    <label>Please select Branch :</label>
                    <br>
                    <label><input name="inp_rdbranch" type="radio" value="">Sharjah</label>
                    <br>
                    <label><input name="inp_rdbranch" type="radio" value="">Ras Al-khaimah</label>
									</div>

								</div><!-- Col 6 end -->
							</div><!-- Form row end -->

							<div class="clearfix">
								<button class="btn btn-primary" type="submit">Submit</button>
							</div>
						</form><!-- Form end -->
					</div><!-- Comments form end -->

				</div><!-- From col end -->

				<div class="col-md-12 col-lg-4">
					<div class="contact-page-info">
						<div class="contact-info-box">
							<i class="ion-ios-location"></i>
							<div class="contact-info-box-content">
								<h4>Address:</h4>
								<p style="color:black;">
                - United Arab Emirates – Sharjah – Cornish AL Buhairah – Bel Rashid Tower – Office 1704 BR1

                </p>
                <p style="color:black;">
                -  BR2 United Arab Emirates – Ras AL Kaiymah – Julphar Avenue 1st Floor
                </p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-email"></i>
							<div class="contact-info-box-content">
								<h4>Email:</h4>
								<p>infoline@ilaw.ae</p>
								<p style="color:black;">Skype:</p><p>ilawuae</p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-android-call"></i>
							<div class="contact-info-box-content">
								<h4>Phone:</h4>
								<p>+(971) 6 556 5566</p>
                <h4>Fax:</h4>
								<p>+(971) 6 55 66 690</p>
							</div>
						</div>

					</div>
				</div><!-- Contact info col end -->


			</div><!--/ Content row end -->
		</div><!--/ Container end -->

	</section><!-- Main container end -->
</body>
</html>
@endsection
