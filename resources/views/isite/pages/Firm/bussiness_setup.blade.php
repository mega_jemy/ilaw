@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <section class="page-header">
    <div class="container">
      <div class="row">
        <div class="col">
          <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/firm') }}">Firm</a></li>
            <li class="active">Bussines Setup</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h1>Bussines Setup</h1>
        </div>
      </div>
    </div>
  </section>
  <div class="container">

    <div class="row">
      <div class="col">
        <div class="blog-posts">
          <article class="post post-large">
            <div class="post-image">
              <?php
                $lang_regional = LaravelLocalization::getCurrentLocale();

              ?>
              <?php  $coll_getLangData = collect($getLangData)->where('regional','=',$lang_regional)->values()->toArray();
              $id_lang = $coll_getLangData[0]['id'];

              $coll_getCPData = collect($getCPData)->where('idlang','=',$id_lang)->values()->toArray();
               ?>
               <?php
               $imgpath = $coll_getCPData[0]['img_path'];
               $imgpath = 'images/bussines_setup/'.$imgpath;
                ?>
              <div class="owl-carousel owl-theme" data-plugin-options="{'items':1}">
                <div>
                  <div class="img-thumbnail d-block">
                    <img class="img-fluid" src="{{$imgpath}}" alt="{{ $coll_getCPData[0]['title'] }}">
                  </div>
                </div>

              </div>
            </div>

            <!--<div class="post-date">
              <span class="day">10</span>
              <span class="month">Jan</span>
            </div>-->

            <div class="post-content">

              <h2><a href="blog-post.html">{{ $coll_getCPData[0]['name'] }}</a></h2>
              {{ $coll_getCPData[0]['title'] }}
              <p>{!! $coll_getCPData[0]['desc'] !!}</p>

              <!--<div class="post-meta">
                <span><i class="fas fa-user"></i> By <a href="#">John Doe</a> </span>
                <span><i class="fas fa-tag"></i> <a href="#">Duis</a>, <a href="#">News</a> </span>
                <span><i class="fas fa-comments"></i> <a href="#">12 Comments</a></span>
                <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="blog-post.html" class="btn btn-xs btn-primary">Read more...</a></span>
              </div>-->

            </div>
          </article>
        </div>
      </div>
    </div>
  </div>




</body>
</html>
@endsection
