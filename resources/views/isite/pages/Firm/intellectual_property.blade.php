@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <section class="page-header">
    <div class="container">
      <div class="row">
        <div class="col">
          <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/firm') }}">Firm</a></li>
            <li class="active">Intellectual Property</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h1>Intellectual Property</h1>
        </div>
      </div>
    </div>
  </section>
  <div class="container">

    <div class="row">
      <div class="col-lg-3 order-2 order-lg-1">
        <aside class="sidebar">

          <h4 class="heading-primary">Area of Firm</h4>
          <ul class="nav nav-list flex-column mb-5">
            <li class="nav-item"><a href="{{ url('/ilaw_care') }}">iLAW Care</a></li>
            <li class="nav-item"><a href="{{ url('intellectual_property') }}">Intellectual Porperty</a></li>
            <li class="nav-item"><a href="{{ url('/debt_collection') }}">Debt Collection</a></li>
            <li class="nav-item"><a href="{{ url('litigation_dispute') }}">Litigation & Dispute</a></li>
          </ul>
        </aside>
      </div>
      <div class="col-lg-9 order-1 order-lg-2">
        <?php
          $lang_regional = LaravelLocalization::getCurrentLocale();

        ?>
        <?php  $coll_getLangData = collect($getLangData)->where('regional','=',$lang_regional)->values()->toArray();
        $id_lang = $coll_getLangData[0]['id'];

        $coll_getCPData = collect($getCPData)->where('idlang','=',$id_lang)->values()->toArray();
         ?>
         <?php
         $imgpath = $coll_getCPData[0]['img_path'];
         $imgpath = '/images/intellectual_property/'.$imgpath;
          ?>
        <img class="img-fluid" src="{{ asset($imgpath)  }}" alt="{{ $coll_getCPData[0]['title'] }}" />

        <h2>{{ $coll_getCPData[0]['name'] }}</h2>

        <div class="row">
          <div class="col">
            <p class="lead">
              {{ $coll_getCPData[0]['title'] }}
            </p>
          </div>
        </div>

        <hr class="tall">

        <div class="row">
          <div class="col">
            {!! $coll_getCPData[0]['desc'] !!}
          </div>
        </div>

      </div>

    </div>

  </div>
  
</body>
</html>
@endsection
