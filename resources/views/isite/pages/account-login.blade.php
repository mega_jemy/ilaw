@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>

  <section id="main-container" class="main-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12">
					<div class="contact-form-wrapper">
						<h3 class="title-normal">Fill This Form</h3>
            <form class="login-box" method="POST" action="{{ url('/login') }}">
                  {{ csrf_field() }}
              <!--<div class="row margin-bottom-1x">
                <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block facebook-btn" href="#"><i class="socicon-facebook"></i>&nbsp;Facebook login</a></div>
                <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block twitter-btn" href="#"><i class="socicon-twitter"></i>&nbsp;Twitter login</a></div>
                <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block google-btn" href="#"><i class="socicon-googleplus"></i>&nbsp;Google+ login</a></div>
              </div>
              <h4 class="margin-bottom-1x">Or using form below</h4>-->
              <div class="form-group input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input class="form-control" type="email" placeholder="Email" name="email" value="{{ old('email') }}" required><span class="input-group-addon"><i class="icon-mail"></i></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input class="form-control" type="password" placeholder="Password" name="password" required><span class="input-group-addon"><i class="icon-lock"></i></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
              <div class="d-flex flex-wrap justify-content-between padding-bottom-1x">
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="remember_me" checked>
                  <label class="custom-control-label" for="remember_me">Remember me</label>
                </div>
                @if(LaravelLocalization::getCurrentLocaleName() == "English")
                  <?php $lnkviewall = LaravelLocalization::getLocalizedURL('en', url('account-password-recovery'))?>
                @elseif(LaravelLocalization::getCurrentLocaleName() == "Arabic")
                  <?php $lnkviewall = LaravelLocalization::getLocalizedURL('ar', url('account-password-recovery')) ?>
                @else
                  <?php $lnkviewall = LaravelLocalization::getLocalizedURL('en', url('account-password-recovery'))?>
                @endif
                <a class="navi-link" href="{{url($lnkviewall)}}">Forgot password?</a>
              </div>
              <div class="text-center text-sm-right">
                <button class="btn btn-primary margin-bottom-none" type="submit">Login</button>
                <br>
                if you don't have account, join us know<a class="navi-link" href="{{url('/register')}}">Register</a>
              </div>
            </form><!-- Form end -->
					</div><!-- Comments form end -->

				</div><!-- From col end -->

				<div class="col-md-12 col-lg-4">
					<div class="contact-page-info">
						<div class="contact-info-box">
							<i class="ion-ios-location"></i>
							<div class="contact-info-box-content">
								<h4>Address:</h4>
								<p style="color:black;">
                - United Arab Emirates – Sharjah – Cornish AL Buhairah – Bel Rashid Tower – Office 1704 BR1

                </p>
                <p style="color:black;">
                -  BR2 United Arab Emirates – Ras AL Kaiymah – Julphar Avenue 1st Floor
                </p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-email"></i>
							<div class="contact-info-box-content">
								<h4>Email:</h4>
								<p>infoline@ilaw.ae</p>
								<p style="color:black;">Skype:</p><p>ilawuae</p>
							</div>
						</div>
						<div class="contact-info-box">
							<i class="ion-android-call"></i>
							<div class="contact-info-box-content">
								<h4>Phone:</h4>
								<p>+(971) 6 556 5566</p>
                <h4>Fax:</h4>
								<p>+(971) 6 55 66 690</p>
							</div>
						</div>

					</div>
				</div><!-- Contact info col end -->


			</div><!--/ Content row end -->
		</div><!--/ Container end -->

	</section><!-- Main container end -->


</body>
</html>
@endsection
