@extends('isite.layout.master')
@section('content')
<html>
<head></head>
<body>
  <section id="practice-area" class="practice-area">
		<div class="container">
			<div class="row text-center">
            <div class="col">
               <h3 class="title-head">Services</h3>
               <p class="title-description">"Ibrahim Al Hosani Advocate, Legal Services & Training” offers a wide range of professional and high quality services in all chief fields of law, including </p>
            </div>
			</div><!--/ Title row end -->

      <div class="row">
				<div class="col-lg-4 col-md-12 mrb-30">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/family.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-people-outline"></i>
								</span>
								<h3 class="practice-title">Legal consultationt</h3>
                <h5 class="practice-title">Providing legal consultations </h5>
								<p class="practice-details">The Firm provides legal consultation for all clients, in order to reach the best results...</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 1st item col end -->

				<div class="col-lg-4 col-md-12 mrb-30">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/land.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-home-outline"></i>
								</span>
								<h3 class="practice-title">Drafting the Contarctst</h3>
                <h5 class="practice-title">Drafting contracts and Documents</h5>
								<p class="practice-details">“Pacta sunt servanda” The contract is the law of the contractors.</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 2nd item col end -->

				<div class="col-lg-4 col-md-12 mrb-30">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/sex.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-woman"></i>
								</span>
								<h3 class="practice-title">Banking Disputes</h3>
                <h5 class="practice-title">Banking Disputes</h5>
								<p class="practice-details">The UAE is witnessing an increase in the number of the banks and theirt...</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 3rd item col end -->

				<div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/finance.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-cash"></i>
								</span>
								<h3 class="practice-title">Disputes of Money</h3>
                <h5 class="practice-title">Disputes of Money Laundry</h5>
								<p class="practice-details">Money laundry or what is known as white collar crimes is one of the most...</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 4th item col end -->

				<div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/criminal.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-social-reddit-outline"></i>
								</span>
								<h3 class="practice-title">Disputes of maritime</h3>
                <h5 class="practice-title">Disputes of maritime and Air...</h5>
								<p class="practice-details">With the development of international trade, the marine sales were developed...</p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 5th item col end -->

				<div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Legal Representation</h3>
                <h5 class="practice-title">Legal Representation and Pleading</h5>
								<p class="practice-details">Our Firm represents all clients before all types and grades of courts in the...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Warnings and Notifications of</h3>
                <h5 class="practice-title">Warnings and Notifications of...</h5>
								<p class="practice-details">Our Firm represents all clients before all types and grades of courts in the...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->


        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Arbitration</h3>
                <h5 class="practice-title">Arbitration</h5>
								<p class="practice-details">Arbitration is one of the most effective ways to settle disputes instead of...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Disputes of Multinational</h3>
                <h5 class="practice-title">Disputes of Multinational...</h5>
								<p class="practice-details">We provide our services in the disputes that are related to multinational...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Disputes of International</h3>
                <h5 class="practice-title">Disputes of International Trade...</h5>
								<p class="practice-details">International Trade Contract is considered to be one of the most risky contracts...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Disputes of Innominate...</h3>
                <h5 class="practice-title">Disputes of Innominate Contracts</h5>
								<p class="practice-details">The past few years have witnessed the emergence of Innominate Contracts...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Legal Services for Contracting...</h3>
                <h5 class="practice-title">Legal Services for Contracting..</h5>
								<p class="practice-details">Through our Firm, we offer an integrated package of legal and consulting services...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Intellectual Property Legal Services</h3>
                <h5 class="practice-title">Intellectual Property Legal Services</h5>
								<p class="practice-details">Through our Firm, we offer all legal consultation and services in the field...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Legal Translation</h3>
                <h5 class="practice-title">Legal Translation</h5>
								<p class="practice-details">Through our Firm, we offer all sorts of legal translation based on local...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->

        <div class="col-lg-4 col-md-12">
					<div class="practice-item">
						<div class="img-thumb">
							<img class="img-fluid" src="{{asset('isite/images/practice/industry.jpg')}}" alt="" />
							<div class="overlay text-center">
								<span class="practice-icon">
									<i class="ion-ios-cog-outline"></i>
								</span>
								<h3 class="practice-title">Training Center</h3>
                <h5 class="practice-title">Training Center</h5>
								<p class="practice-details">Training is one of the main activities of our firm, and the goal of this center...  </p>
								<a class="read-more" href="#">Read More</a>
							</div><!-- Item overlay end -->
						</div><!-- Item img end -->
					</div><!-- Practice item end -->
				</div><!-- 6th item col end -->


			</div><!--/ Content row end -->

			<div class="general-btn text-center"><a class="btn btn-primary" href="#">View All</a></div>

		</div><!--/ Container end -->
	</section><!-- Practice area end -->
</body>
</html>
@endsection
