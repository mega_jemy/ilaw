<div class="container">
  <div class="row">
    <div class="footer-ribbon">
      <span>Get in Touch</span>
    </div>
    <div class="col-lg-3">
      <div class="newsletter">
        <h4>Newsletter</h4>
        <p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>

        <div class="alert alert-success d-none" id="newsletterSuccess">
          <strong>Success!</strong> You've been added to our email list.
        </div>

        <div class="alert alert-danger d-none" id="newsletterError"></div>

        <form id="newsletterForm" action="#">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="input-group">
            <input class="form-control form-control-sm" placeholder="Email Address" name="email" id="newsletter" type="text">
            <span class="input-group-append">
              <button onclick="saveNewsletter()" class="btn btn-light" type="button">Go!</button>
            </span>
          </div>
        </form>
      </div>
    </div>
    <div class="col-lg-3">
      <h4>Latest Tweets</h4>
      <div id="tweet" class="twitter" data-plugin-tweets data-plugin-options="{'username': 'oklerthemes', 'count': 2}">
        <p>Please wait...</p>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="contact-details">
        <h4>Contact Us</h4>
        <ul class="contact">



          <li><p><i class="fas fa-map-marker-alt"></i> <strong>Address-01:</strong> United Arab Emirates – Sharjah – Cornish AL Buhairah – Bel Rashid Tower – Office 1704 BR1</p></li>
          <li><p><i class="fas fa-map-marker-alt"></i> <strong>Address-02:</strong> <strong>Address-02:</strong> BR2 United Arab Emirates – Ras AL Kaiymah – Julphar Avenue 1st Floor</p></li>
          <li><p><i class="fas fa-phone"></i> <strong>Phone:</strong> (+971) 6 556 5566</p></li>
          <li><p><i class="fas fa-phone"></i> <strong>Fax:</strong> (+971) 6 55 66 690</p></li>
          <li><p><i class="far fa-envelope"></i> <strong>Email:</strong> <a href="mailto:infoline@ilaw.ae">infoline@ilaw.ae</a></p></li>
          <li><p><i class="fas fa-phone"></i> <strong>Skype:</strong>ilawuae</p></li>
        </ul>
      </div>
    </div>
    <div class="col-lg-2">
      <h4>Follow Us</h4>
      <ul class="social-icons">
        <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
        <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
        <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="footer-copyright">
  <div class="container">
    <div class="row">
      <div class="col-lg-1">
        <a href="index.html" class="logo">
          <img alt="Porto Website Template" class="img-fluid" src="{{asset('images/logo.png')}}">
        </a>
      </div>
      <div class="col-lg-6">
        <p>© Copyright 2018. All Rights Reserved.</p>
      </div>
      <div class="col-lg-5">
        <nav id="sub-menu">
          <ul>
            <li><a href="{{url('/terms_and_condition')}}">Terms and condition</a></li>
            <li><a href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
            <li><a href="{{ url('/site_map') }}">Sitemap</a></li>
            <li><a href="{{ url('/contact_us') }}">Contact</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>
