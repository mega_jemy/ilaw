<!-- Vendor -->
<script src="{{asset('isite/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('isite/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
<script src="{{asset('isite/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('isite/vendor/jquery-cookie/jquery-cookie.min.js')}}"></script>
<!--<script src="{{asset('isite/master/style-switcher/style.switcher.js')}}" id="styleSwitcherScript" data-base-path="" data-skin-src=""></script>-->
<script src="{{asset('isite/vendor/popper/umd/popper.min.js')}}"></script>
<script src="{{asset('isite/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('isite/vendor/common/common.min.js')}}"></script>
<script src="{{asset('isite/vendor/jquery.validation/jquery.validation.min.js')}}"></script>
<script src="{{asset('isite/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')}}"></script>
<script src="{{asset('isite/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
<script src="{{asset('isite/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
<script src="{{asset('isite/vendor/isotope/jquery.isotope.min.js')}}"></script>
<script src="{{asset('isite/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('isite/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('isite/vendor/vide/vide.min.js')}}"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{asset('isite/js/theme.js')}}"></script>

<!-- Current Page Vendor and Views -->
<script src="{{asset('isite/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('isite/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('isite/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js')}}"></script>
<script src="{{asset('isite/js/views/view.home.js')}}"></script>

<!-- Theme Custom -->
<script src="{{asset('isite/js/custom.js')}}"></script>

<!-- Theme Initialization Files -->
<script src="{{asset('isite/js/theme.init.js')}}"></script>
<!-- footer js -->
<script src="{{asset('isite/js/pages/footer.js')}}"></script>
<!-- Examples -->
<script src="{{asset('isite/js/examples/examples.portfolio.js')}}"></script>
<!-- Examples -->
<script src="{{asset('isite/js/examples/examples.gallery.js')}}"></script>	
