<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 55, 'stickySetTop': '-55px', 'stickyChangeLogo': true}">
  <div class="header-body">
    <div class="header-container container">
      <div class="header-row">
        <div class="header-column">
          <div class="header-row">
            <div class="header-logo">
              <a href="index.html">
                <img alt="iLAW" width="111" height="54" data-sticky-width="82" data-sticky-height="40" data-sticky-top="33" src="{{asset('images/logo.png')}}">
              </a>
            </div>
          </div>
        </div>
        <div class="header-column justify-content-end">
          <div class="header-row pt-3">
            <nav class="header-nav-top">
              <ul class="nav nav-pills">
                <li class="nav-item d-none d-sm-block">
                  <a class="nav-link" href="{{ url('/about_us') }}"><i class="fas fa-angle-right"></i> {{ Lang::get('header.lnk_aboutus') }}</a>
                </li>
                <li class="nav-item d-none d-sm-block">
                  <a class="nav-link" href="{{ url('/contact_us') }}"><i class="fas fa-angle-right"></i> {{ Lang::get('header.lnk_contactus') }}</a>
                </li>
                <li class="nav-item dropdown">
                  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    @if(LaravelLocalization::getCurrentLocaleName() == $properties['name'])
                    <a class="nav-link" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null , [] , true) }}" hreflang="{{ $localeCode }}" role="button" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <!--<img src="img/blank.gif" class="flag flag-us" alt="English" />--> {{ $properties['native'] }}
                      <i class="fas fa-angle-down"></i>
                    </a>
                    @endif

                  @endforeach


                  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    @if(LaravelLocalization::getCurrentLocaleName() != $properties['name'])
                    <div class="dropdown-menu" aria-labelledby="dropdownLanguage">
                      <a class="dropdown-item" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null , [] , true) }}" hreflang="{{ $localeCode }}">
                        <!--<img src="img/blank.gif" class="flag flag-fr" alt="English" />-->{{ $properties['native'] }}
                      </a>
                    </div>
                    @endif

                  @endforeach
                </li>
                <li class="nav-item">
                  <span class="ws-nowrap"><i class="fas fa-phone"></i> (+971) 6 556 5566</span>
                </li>
              </ul>
            </nav>
            <!--<div class="header-search d-none d-md-block">
              <form id="searchForm" action="http://preview.oklerthemes.com/porto/6.2.1/page-search-results.html" method="get">
                <div class="input-group">
                  <input type="text" class="form-control" name="q" id="q" placeholder="Search..." required>
                  <span class="input-group-append">
                    <button class="btn btn-light" type="submit"><i class="fas fa-search"></i></button>
                  </span>
                </div>
              </form>
            </div>-->
          </div>
          <div class="header-row">
            <div class="header-nav">
              <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                @include('isite.layout.navigation')
              </div>
              <ul class="header-social-icons social-icons d-none d-sm-block">
                <li class="social-icons-facebook"><a href="https://www.facebook.com/ilawuae" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                <li class="social-icons-twitter"><a href="https://twitter.com/ilawuae" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
              </ul>
              <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                <i class="fas fa-bars"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
