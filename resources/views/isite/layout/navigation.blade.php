<nav class="collapse">
  <ul class="nav nav-pills" id="mainNav">
    <li>
      <a class="dropdown-item <?php if($activepage == 1) { ?> active <?php } ?>" href="{{ url('/') }}">
        {{ Lang::get('navigation.lnk_home') }}
      </a>
    </li>
    <li class="dropdown">
      <a class="dropdown-item <?php if($activepage == 3 || $activepage == 4 || $activepage == 5 || $activepage == 6 || $activepage == 7) { ?> active <?php } ?>" href="{{ url('/firm') }}">
        {{ Lang::get('navigation.lnk_firm') }}
      </a>
    </li>
    <li class="dropdown">
      <a class="dropdown-item <?php if($activepage == 8 || $activepage == 9 || $activepage == 24 || $activepage == 20) { ?> active <?php } ?>" href="{{ url('/training_courses') }}">
        {{ Lang::get('navigation.lnk_training') }}
      </a>
    </li>
    <li class="dropdown">
      <a class="dropdown-item <?php if($activepage == 10 || $activepage == 11 || $activepage == 12 || $activepage == 13) { ?> active <?php } ?>" href="{{ url('/ilaw_club') }}">
        {{ Lang::get('navigation.lnk_club') }}
      </a>
    </li>
    <li class="dropdown">
      <a class="dropdown-item <?php if($activepage == 16) { ?> active <?php } ?>" href="{{ url('/bussiness_setup') }}">
        {{ Lang::get('navigation.lnk_bussienss_setup') }}
      </a>
    </li>
    <!--<li class="dropdown">
      <a class="dropdown-item <?php if($activepage == 17) { ?> active <?php } ?>" href="{{ url('/ilaw_careers') }}">
        Careers
      </a>
    </li>
    <li>
      <a class="dropdown-item <?php if($activepage == 18) { ?> active <?php } ?>" href="{{ url('/ilaw_feedback') }}">
        FeedBack
      </a>
    </li>-->
  </ul>
</nav>
