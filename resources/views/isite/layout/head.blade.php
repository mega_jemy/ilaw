<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>iLAW Official Website</title>

<meta name="keywords" content="iLAW Official Website" />
<meta name="description" content="iLAW Official Website">
<meta name="author" content="www.ilaw.ae">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<!-- Favicon -->
<!-- Favicon and Apple Icons-->
<link rel="icon" type="image/x-icon" href="{{asset('images/log.png')}}">
<link rel="icon" type="image/png" href="{{asset('images/log.png')}}">
<!--<link rel="shortcut icon" href="{{asset('isite/img/favicon.ico')}}" type="image/x-icon" />
<link rel="apple-touch-icon" href="{{asset('isite/img/apple-touch-icon.png')}}">-->

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

<!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="{{asset('isite/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('isite/vendor/font-awesome/css/fontawesome-all.min.css')}}">
<link rel="stylesheet" href="{{asset('isite/vendor/animate/animate.min.css')}}">
<link rel="stylesheet" href="{{asset('isite/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
<link rel="stylesheet" href="{{asset('isite/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('isite/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('isite/vendor/magnific-popup/magnific-popup.min.css')}}">

<!-- Theme CSS -->
<link rel="stylesheet" href="{{asset('isite/css/theme.css')}}">
<link rel="stylesheet" href="{{asset('isite/css/theme-elements.css')}}">
<link rel="stylesheet" href="{{asset('isite/css/theme-blog.css')}}">
<link rel="stylesheet" href="{{asset('isite/css/theme-shop.css')}}">

<!-- Current Page CSS -->
<link rel="stylesheet" href="{{asset('isite/vendor/rs-plugin/css/settings.css')}}">
<link rel="stylesheet" href="{{asset('isite/vendor/rs-plugin/css/layers.css')}}">
<link rel="stylesheet" href="{{asset('isite/vendor/rs-plugin/css/navigation.css')}}">
<link rel="stylesheet" href="{{asset('isite/vendor/circle-flip-slideshow/css/component.css')}}">

<!-- Demo CSS -->


<!-- Skin CSS -->
<link rel="stylesheet" href="{{asset('isite/css/skins/default.css')}}">
<script src="{{asset('/isite/master/style-switcher/style.switcher.localstorage.js')}}"></script>

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="{{asset('isite/css/custom.css')}}">

<!-- Head Libs -->
<script src="{{asset('isite/vendor/modernizr/modernizr.min.js')}}"></script>

<!-- iziToast-master -->
<script src="{{asset('js/lib/iziToast-master/dist/js/iziToast.min.js')}}" type="text/javascript"></script>
<link rel="stylesheet" media="screen" href="{{asset('js/lib/iziToast-master/dist/css/iziToast.min.css')}}">
