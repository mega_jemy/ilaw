<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}">
<head>
  @include('isite.layout.head')
</head>

<body>

	<div class="body-inner">
    @include('isite.layout.header')

    <!--/ Navigation start -->
    @include('isite.layout.navigation')
	  <!--/ Navigation end -->

    <!-- Main content start -->
    @yield('content')
	  <!--/ Main content end -->

	<!-- Footer start -->
	@include('isite.layout.footer')

  @include('isite.layout.foot')




	</div><!-- Body inner end -->
</body>

<!-- Mirrored from themewinter.com/demo/html/lawbizs/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Jun 2018 08:05:08 GMT -->
</html>
