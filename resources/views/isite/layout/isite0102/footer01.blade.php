<footer id="footer" class="footer">
  <div class="angle-bg"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <img src="{{asset('isite/images/footer-logo1.png')}}" alt="" />
      </div>
      <div class="col-lg-4 col-md-6 ml-lg-auto">
        <div class="footer-top-call">
          <h3><i class="ion-android-call"></i>+(971) 6 556 5566</h3>
        </div>
      </div>
    </div><!-- Row end -->


    <div class="row">
      <div class="col-lg-3 col-md-6 col-12 footer-widget">
        <h3 class="widget-title">About Us</h3>
        <p class="footer-about-desc">
          We are a awward winning Law Agency company. We believe in quality and standard worldwide. It has survived not only five centuries.
        </p>
        <div class="footer-social">
          <a href="#"><i class="ion-social-facebook"></i></a>
          <a href="#"><i class="ion-social-twitter"></i></a>
          <a href="#"><i class="ion-social-instagram"></i></a>
          <a href="#"><i class="ion-social-youtube"></i></a>
        </div>
      </div><!-- Footer widget end -->

      <div class="col-lg-2 col-md-6 col-12 footer-widget">
        <h3 class="widget-title">Contact Us</h3>
        <div class="contact-info-box">
          <i class="ion-ios-location"></i>
          <div class="contact-info-box-content">
            <p>Sharjah - Ras Khima.</p>
          </div>
        </div>
        <div class="contact-info-box">
          <i class="ion-email"></i>
          <div class="contact-info-box-content">
            <p>info@ilaw.ae</p>
          </div>
        </div>
        <div class="contact-info-box">
          <i class="ion-android-call"></i>
          <div class="contact-info-box-content">
            <p>+(971) 6 556 5566</p>
          </div>
        </div>
      </div><!-- Footer widget end -->

      <div class="col-lg-3 col-md-6 col-12 footer-widget">
        <h3 class="widget-title">Useful Links</h3>

        <ul class="unstyled">
          <li><a href="https://www.dc.gov.ae/PublicServices/Home.jsf?lang=en">Dubai Courts</a></li>
          <li><a href="https://www.moi.gov.ae/ar/default.aspx">Ministry of Interior</a></li>
          <li><a href="https://www.dc.gov.ae/PublicServices/Home.jsf?lang=en">Ministry of Justice</a></li>
          <li><a href="{{ url('/club_membership') }}">Club MemberShip</a></li>
          <li><a href="{{ url('/ilaw_care') }}">iLAW Care</a></li>
        </ul>
      </div><!-- Footer widget end -->

      <div class="col-lg-4 col-md-6 col-12 footer-widget newsletter-widget">

        <h3 class="widget-title">Subscribe to our <span>Newsletter</span></h3>

        <form id="newsletter-form" class="newsletter-form">
          <div class="form-group">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input style="color:white;" type="email" name="email" id="newsletter" class="form-control form-control-lg" placeholder="Your Email Here" autocomplete="off">
            <button type="button" onclick="saveNewsletter()" class="btn btn-primary">Subscribe</button>
          </div>
        </form>
      </div><!-- Footer widget end -->

    </div><!--/ Content row end -->

    <div class="row copyright">
      <div class="col-12 col-md-5">
        <div class="copyright-info">
          <span>Copyright © 2018 <a href="http://www.sketch.ae"> Sketch.ae </a> Support. All Rights Reserved.</span>
        </div>
      </div>

      <div class="col-12 col-md-6 ml-md-auto">
        <div class="footer-menu">
          <ul class="nav unstyled ">
            <li><a href="{{ url('/terms_and_condition') }}">Terms of Use</a></li>
            <li><a href="{{ url('/privacy_policy') }}">Privacy Policy</a></li>
            <li><a href="{{ url('/advertise') }}">Advetise</a></li>
            <li><a href="{{ url('/contact') }}">Contact</a></li>
          </ul>
        </div>
      </div>
    </div><!-- Copyright -->

  </div><!--/ Container end -->
</footer><!-- Footer end -->
