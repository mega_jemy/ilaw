<div class="site-navigation navigation">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="site-nav-inner">
                <nav class="navbar navbar-expand-lg">
                   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                      aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon">
                         <i class="fa fa-bars"></i>
                      </span>
                   </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

              <ul class="navbar-nav">
                <li class="nav-item dropdown <?php if($activepage == 1) { ?> active <?php } ?> "><a class="nav-link" href="{{ url('/') }}">Home</a></li>
                <!--<li class="nav-item dropdown <?php /*if($activepage == 2) { */?> active <?php /*}*/ ?>"><a class="nav-link" href="{{ url('/services') }}">Services</a></li>-->
                <li class="nav-item dropdown <?php if($activepage == 3 || $activepage == 4 || $activepage == 5 || $activepage == 6 || $activepage == 7) { ?> active <?php } ?>">
                  <a href="{{ url('/firm') }}" class="nav-link" data-toggle="dropdown">Firm <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <!--<li><a href="{{ url('/online_consultacy') }}">Online Consultacy</a></li>-->
                    <li><a href="{{ url('/ilaw_care') }}">iLAW Care</a></li>
                    <li><a href="{{ url('intellectual_property') }}">Intellectual Porperty</a></li>
                    <li><a href="{{ url('/debt_collection') }}">Debt Collection</a></li>
                    <li><a href="{{ url('litigation_dispute') }}">Litigation & Dispute</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown  <?php if($activepage == 8 || $activepage == 9 || $activepage == 24 || $activepage == 20) { ?> active <?php } ?>">
                  <a href="#" class="nav-link" data-toggle="dropdown">Training Center <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('/training_courses') }}">Courses</a></li>
                    <li><a href="{{ url('/training_conferences') }}">Conferences</a></li>
                    <li><a href="{{ url('/training_suggest') }}">Suggest</a></li>
                    <li><a href="{{ url('/appointment') }}">Appointment</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown  <?php if($activepage == 10 || $activepage == 11 || $activepage == 12 || $activepage == 13) { ?> active <?php } ?>">
                  <a href="#" class="nav-link" data-toggle="dropdown">iLAW Club <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('/club_membership') }}">MemberShip</a></li>
                    <li><a href="{{ url('/club_blog') }}">Blog</a></li>
                    <li class="dropdown-submenu">
                       <a href="#">Media</a>
                         <ul class="dropdown-menu">
                           <li><a href="{{ url('/club_media_gallery') }}">Gallery</a></li>
                           <li><a href="{{ url('/club_media_videos') }}">Videos</a></li>
                         </ul>
                     </li>
                  </ul>
                </li>
                <!--<li class="nav-item dropdown  <?php if($activepage == 5) { ?> active <?php } ?>">
                  <a href="#" class="nav-link" data-toggle="dropdown">iLAW Kids <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Festival</a></li>
                    <li><a href="#">Club</a></li>
                  </ul>
                </li>-->
                <li class="nav-item dropdown  <?php if($activepage == 16) { ?> active <?php } ?>"><a href="{{ url('/bussiness_setup') }}">Bussiness Setup</a></li>
                <li class="nav-item dropdown  <?php if($activepage == 17) { ?> active <?php } ?>"><a class="nav-link" href="{{ url('/ilaw_careers') }}">Careers</a></li>
                <li class="nav-item dropdown  <?php if($activepage == 18) { ?> active <?php } ?>"><a class="nav-link" href="{{ url('/ilaw_feedback') }}">FeedBack</a></li>

                <li class="search nav-item d-none d-lg-inline-block"><button class="fa fa-search"></button></li>
              </ul><!--/ Nav ul end -->
            </div>
          </nav><!--/ Collapse end -->
        </div><!-- Site navbar end -->
      </div><!--/ Cold end -->
    </div><!--/ Row end -->
  </div><!--/ Container end -->

  <div class="site-search search-full-width">
    <div class="container">
      <input type="text" placeholder="type what you want and enter">
      <span class="close">×</span>
    </div>
  </div>
</div>
