<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>iLAW Official Website | Home</title>
<!-- Mobile Specific Metas
================================================== -->
<!-- SEO Meta Tags-->
<meta name="description" content="iLAW Official Website">
<meta name="keywords" content="iLAW Official Website">
<meta name="author" content="iLAW">
<!-- Mobile Specific Meta Tag-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<!-- Favicon and Apple Icons-->
<link rel="icon" type="image/x-icon" href="{{asset('images/log.png')}}">
<link rel="icon" type="image/png" href="{{asset('images/log.png')}}">

<meta http-equiv="X-UA-Compatible" content="IE=edge">


<!-- Site Title -->
<title>iLaw - iDemo</title>


<!-- CSS
================================================== -->

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('isite/css/bootstrap.min.css')}}">
<!-- FontAwesome -->
<link rel="stylesheet" href="{{asset('isite/css/font-awesome.min.css')}}">
<!-- IonIcons -->
<link rel="stylesheet" href="{{asset('isite/css/ionicons.min.css')}}">
<!-- Animation -->
<link rel="stylesheet" href="{{asset('isite/css/animate.css')}}">
<!-- Owl Carousel -->
<link rel="stylesheet" href="{{asset('isite/css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('isite/css/owl.theme-min.css')}}">
<!-- Colorbox -->
<link rel="stylesheet" href="{{asset('isite/css/colorbox.css')}}">
<!-- Template styles-->
<link rel="stylesheet" href="{{asset('isite/css/style.css')}}">
<!-- Responsive styles-->
<link rel="stylesheet" href="{{asset('isite/css/responsive.css')}}">
<!-- fullCalendar -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->

  <script src="{{asset('js/lib/iziToast-master/dist/js/iziToast.min.js')}}" type="text/javascript"></script>
  <link rel="stylesheet" media="screen" href="{{asset('js/lib/iziToast-master/dist/css/iziToast.min.css')}}">
