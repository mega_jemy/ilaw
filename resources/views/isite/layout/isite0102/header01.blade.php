<div id="top-bar" class="top-bar">
  <div class="container">
    <div class="row">
      <div class="col-md-4 d-none d-lg-inline-block">
        <div class="top-bg">
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-12 top-menu ">
        <ul class="unstyled">
          <li><a href="{{ url('/about') }}">About</a></li>
          <li class="active"><a href="{{ url('/contact') }}">Contact</a></li>


        </ul>
      </div><!--/ Top menu end -->

      <div class="col-lg-5 col-md-6 col-12 top-social">
        <ul class="unstyled">
          <li>
            <a title="Facebook" href="#">
              <span class="social-icon"><i class="fa fa-facebook"></i></span>
            </a>
            <a title="Twitter" href="#">
              <span class="social-icon"><i class="fa fa-twitter"></i></span>
            </a>
            <a title="instagram" href="#">
              <span class="social-icon"><i class="fa fa-instagram"></i></span>
            </a>
          </li>
        </ul>
      </div><!--/ Top social end -->

    </div><!--/ Content row end -->
  </div><!--/ Container end -->
</div><!--/ Topbar end -->

<!-- Header start -->
<header id="header" class="header">
  <div class="container">
    <div class="row">
      <div class="logo col-12 col-md-4">
            <div class="logo-area" >
                <a href="{{ url('/') }}">
                   <img class="img-fluid" src="{{asset('isite/images/logo1.png')}}" alt="">
               </a>
             </div>
          </div><!-- logo end -->

        <div class="col-12 col-md-7 ml-auto header-right">
          <ul class="top-info">
            <li>
              @if (Auth::guest())
              <div class="info-box"><a href="{{url('login')}}">
                <span class="info-icon">
                  <i class="fa fa-lock">&nbsp;</i>
                </span>
                <div class="info-box-content">
                <h3 class="info-box-title">login</h3>
                <h4 class="info-box-subtitle"><br></h4></a>
                </div>
              </div>
              @else
              <div class="info-box">
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                             <span class="info-icon">
                               <i class="fa fa-unlock">&nbsp;</i>
                             </span>
                    <div class="info-box-content">
                    <h3 class="info-box-title">logout</h3>
                    <h4 class="info-box-subtitle"><br></h4>
                    </div>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>



              </div>
              @endif

            </li>
          <li>
            <div class="info-box"><span class="info-icon"><i class="fa fa-phone">&nbsp;</i></span>
              <div class="info-box-content">
              <h3 class="info-box-title">Call Us</h3>
              <h4 class="info-box-subtitle">+(971) 6 556 5566</h4>
              </div>
            </div>
          </li>
          <li>
            <div class="info-box"><span class="info-icon"><i class="fa fa-envelope-o">&nbsp;</i></span>
              <div class="info-box-content">
                <h3 class="info-box-title">Email Us</h3>
                <h4 class="info-box-subtitle">info@ilaw.ae</h4>
              </div>
            </div>
          </li>
          <li>
            <div class="consult">
              <a href="{{ url('/online_consultacy') }}"><span>Free Consult</span></a>
            </div>
          </li>
        </ul>
        </div><!-- header right end -->
    </div><!-- Row end -->
  </div><!-- Container end -->
</header><!--/ Header end -->
