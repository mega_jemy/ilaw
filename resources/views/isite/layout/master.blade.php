<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}">

<!-- Mirrored from preview.oklerthemes.com/porto/6.2.1/index-classic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Jul 2018 12:15:57 GMT -->
<head>
	@include('isite.layout.head')


	</head>
	<body class="loading-overlay-showing" data-loading-overlay>
		<div class="loading-overlay">
			<div class="bounce-loader">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>
		<div class="modal fade" id="myModal01" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
						<h4 class="modal-title">Reserve your course Now</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          </div>
          <div class="modal-body">
						<?php
						if(isset($getTrainingCourse))
						{
							?>
							@if (count($getTrainingCourse) >= 1)
	              @foreach ($getTrainingCourse as $getTrainingCourses)
								<div class="row justify-content-center mt-4">
									<div class="col-lg-4 mx-auto">
										<div class="thumb-gallery">
											<div class="owl-carousel owl-theme manual thumb-gallery-detail show-nav-hover" id="thumbGalleryDetail">
												<div>
													<span class="img-thumbnail d-block">
														<?php
						                $imgpath = $getTrainingCourses->img_path;
						                $imgpath = 'images/training/'.$imgpath;
						                 ?>
						   							<img src="{{ $imgpath}}" class="img-fluid" alt="">
														<img alt="Project Image" src="{{asset('images/course_def.jpg')}}" class="img-fluid">
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endforeach
							@endif
							<?php
						}
						?>


            <!--<a href="#"><img src="{{asset('images/course_def.jpg')}}" width="100%" class="img-responsive" alt="" draggable="false"></a>-->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
		<div class="body">
			@include('isite.layout.header')

			<div role="main" class="main">
				@yield('content')
			</div>


			<footer id="footer">
				@include('isite.layout.footer')
			</footer>
		</div>

		@include('isite.layout.foot')
		<?php
		if(isset($getTrainingCourse))
		{
			?>
		<script type="text/javascript">
      $(window).on('load',function(){
          $('#myModal01').modal('show');
      });
  </script>
<?php } ?>
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto/6.2.1/index-classic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Jul 2018 12:15:58 GMT -->
</html>
