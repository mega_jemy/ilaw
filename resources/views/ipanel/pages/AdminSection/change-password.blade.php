@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Main content -->
<section class="content-header">
  <h1>
    Admin Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Change Password</li>
  </ol>
</section>
    <section class="content">
      <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Fill required Fields</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- .box-body -->
              <div class="box-body">
                @if(Session::has('changepasswordaction_result'))

                      <?php $saved_message= []; $saved_message=Session::pull('changepasswordaction_result');?>
                      <?php if($saved_message[0] == 'success')
                      {
                        ?>
                        <div  class="alert alert-success">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      elseif ($saved_message[0] == 'failed') {
                        # code...
                        ?>
                        <div  class="alert alert-danger">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                          <strong>Error! </strong>
                          <?php
                          for($i=0;$i<count($saved_message);$i++)
                          {
                            echo $saved_message[$i].'<br/>';
                          }

                          ?>
                        </div>
                        <?php
                      }
                      ?>

                @endif
                <!-- input states -->
                <form role="form" method="post" action="{{ url('/ipanel/changepassword') }}"  id="frm_employee_register" enctype="multipart/form-data">
                  {{ csrf_field() }}

                  {{-- Old Password --}}
                  <div class="form-group {{ $errors->has('txt_oldpassword') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_colororder">Old Password:</label>
                    <input value="{{ old('txt_oldpassword') }}" required type="text" class="form-control" name="txt_oldpassword" placeholder="Old Password...">
                    @if ($errors->has('txt_oldpassword'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_oldpassword') }}</strong>
                        </span>
                    @endif
                  </div>

                  {{-- New Password --}}
                  <div class="form-group {{ $errors->has('txt_newpassword') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_colororder">New Password:</label>
                    <input value="{{ old('txt_newpassword') }}" required type="text" class="form-control" name="txt_newpassword" placeholder="New Password...">
                    @if ($errors->has('txt_newpassword'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_newpassword') }}</strong>
                        </span>
                    @endif
                  </div>

                  {{-- Confirm Password --}}
                  <div class="form-group {{ $errors->has('txt_confirmpassword') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_colororder">Confirm Password:</label>
                    <input value="{{ old('txt_confirmpassword') }}" required type="text" class="form-control" name="txt_confirmpassword" placeholder="Confirm Password...">
                    @if ($errors->has('txt_confirmpassword'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_confirmpassword') }}</strong>
                        </span>
                    @endif
                  </div>



              <!-- /.box-body -->
              <div class="box-footer with-border">
                <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
