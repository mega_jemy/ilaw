@extends('ipanel.layout.master')
@section('content')
<html>
  <head>
    <title>ASC-AE |  User Roles </title>
  </head>
  <body class="skin-blue sidebar-mini">
    <section class="content-header">
      <h1>
        Admin Management
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ url('/ipanel/dashboard') }}">Manage Admin User</a></li>
        <li class="active"> Account's User Permissions</li>
      </ol>
    </section>
      <!-- Content Wrapper. Contains page content -->

        <!-- Main content -->
        <section class="content">

          <!-- row -->
          <div class="row">
            <div class="col-md-12">
              <span id="span_error_msg" class="error_msg"></span>
              @if(Session::has('setpermission_result'))

                          <?php $saved_message= []; $saved_message=Session::pull('setpermission_result');?>
                          <?php if($saved_message[0] == 'success')
                          {
                            ?>
                            <div  class="alert alert-success">

                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Success! </strong><?php echo $saved_message[1]; ?>
                            </div>
                            <?php
                          }
                          elseif ($saved_message[0] == 'danger') {
                            # code...
                            ?>
                            <div  class="alert alert-danger">

                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Error! </strong><?php echo $saved_message[1]; ?>
                            </div>
                            <?php
                          }
                          ?>

                @endif
              <form action="{{ url('/ipanel/setpermission') }}" method="post" role="form" >
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <input type="hidden" name="_access" value="{{ $_access }}">
              <ul class="sidebar-menu" data-widget="tree">

                <li class="header">MAIN NAVIGATION</li>
                <?php
                  if(!empty($RuleMData))
                  {
                    $MenuData = $RuleMData['MenuData'];
                    $MenuItemData = $RuleMData['MenuItemData'];
                    $RolesData = $RuleMData['RolesData'];
                    $EventsData = $RuleMData['EventsData'];


                    #############################################################################
                    if(!empty($MenuData))
                    {
                      for($i=0;$i<count($MenuData);$i++)
                      {

                        $MenuData_id  = $MenuData[$i]['id'];
                        $MenuData_name = $MenuData[$i]['name'];
                        ?>
                        <li class="active treeview" >
                        <a href="#">
                          <i class="fa fa-files-o"></i> <span>{{ $MenuData_name }}</span>
                          <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                        <?php
                        $custom_menuitem = collect($MenuItemData)->where('menu_id','=',$MenuData_id)->values()->toArray();
                        //var_dump($custom_menuitem);

                        if(!empty($custom_menuitem) && !empty($RolesData) && !empty($EventsData))
                        {

                          ?>
                          <ul class="treeview-menu">
                            <?php
                              for($j=0;$j<count($custom_menuitem);$j++)
                              {
                                $MenuItemData_id = $custom_menuitem[$j]['id'];
                                $MenuItemData_name = $custom_menuitem[$j]['name'];
                                $res_RolesData = collect($RolesData)->where('menuitem_id','=',$MenuItemData_id)->values()->toArray();
                                if(!empty($res_RolesData))
                                {
                                  $linkhead = $res_RolesData[0]['link'];
                                  $checkactive = encrypt($MenuItemData_id);
                                  ?>
                                    <li  @if(decrypt($activepage) == decrypt($checkactive))  class="active treeview" @else class="treeview"  @endif>
                                      <a href="#"><i class="fa fa-circle-o"></i> {{ $MenuItemData_name }}
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                      </a>
                                      <?php
                                        if(!empty($res_RolesData))
                                        {
                                          ?>
                                          <ul class="treeview-menu">
                                          <?php
                                          for($s= 0;$s<count($res_RolesData);$s++)
                                          {
                                            $roles_id = $res_RolesData[$s]['id'];
                                            $events_id = $res_RolesData[$s]['events_id'];
                                            $name = collect($EventsData)->where('id','=',$events_id)->values()->toArray()[0]['name'];
                                            $checkedID = 0;
                                            if(!empty($UserRoleData))
                                            {
                                              $rolesid = $UserRoleData[0]['roles_id'];
                                              $activeid = $UserRoleData[0]['active'];
                                              $split_rolesid = explode(',',$rolesid);
                                              $split_activeid = explode(',',$activeid);

                                              $index_key = array_search($roles_id, $split_rolesid);
                                              //var_dump($index_key);
                                              if ($index_key != "" || $index_key == 0) {
                                                $lampCheck = $split_activeid[$index_key];
                                                $checkedID =$lampCheck;
                                              }
                                              else {
                                                $checkedID = 0;
                                              }
                                            }
                                            else {
                                            }

                                            ?>
                                            <li>
                                              <label style="color:white;">
                                                <input name="checkbox_{{ $roles_id }}"  type="checkbox" class="minimal" @if($checkedID == '1') checked @endif>
                                                {{$name}}
                                              </label>
                                            </li>
                                            <?php
                                          }
                                          ?>
                                          </ul>
                                          <?php
                                        }
                                        ?>
                                    </li>
                                  <?php
                                }
                              }
                              ?>
                          </ul>
                          <?php
                        }
                        ?>
                        </li>
                        <?php
                      }
                    }
                  }
                    ?>
                <!---------------------------------------------------->
              </ul>
              <div class="box-footer with-border">
                <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            </form>
            </div><!-- /.col -->
          </div><!-- /.row -->



        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <script src="{{asset('assets/cpanel/dist/js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
    <!--<script src="{{asset('assets/cpanel/dist/js/pages/userprjs.js')}}" type="text/javascript"></script>-->
  </body>
</html>
@endsection
