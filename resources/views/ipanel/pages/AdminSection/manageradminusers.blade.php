@extends('ipanel.layout.master')
@section('content')<!-- Main content -->
    <section class="content">
      <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Users Data</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <a href="addnewadminuser"><h5 class="box-title">Add Admin User</h5></a>
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('useraction_result'))

                    <?php $saved_message= []; $saved_message=Session::pull('useraction_result');?>
                    <?php if($saved_message[0] == 'success')
                    {
                      ?>
                      <div  class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    elseif ($saved_message[0] == 'failed') {
                      # code...
                      ?>
                      <div  class="alert alert-danger">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    ?>

              @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Status</th>
                  <th>Type</th>
                  <th>Delete</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if (count($UserDataSuperAdmin) >= 1)
                    @for ($i = 0; $i < count($UserDataSuperAdmin) ; $i++)
                      <tr>
                        <td>{{ ($i+1) }}</th>
                        <td hidden="hidden">{{ $UserDataSuperAdmin[$i]->id }}</td>
                        <td>{{ $UserDataSuperAdmin[$i]->fullname }}</td>
                        <td>{{ $UserDataSuperAdmin[$i]->email }}</td>
                        <td>{{ $UserDataSuperAdmin[$i]->username }}</td>
                        <td>{{ $UserDataSuperAdmin[$i]->status }}</td>
                        <td>{{ $UserDataSuperAdmin[$i]->typeacc }}</td>
                        <td>{{ $UserDataSuperAdmin[$i]->softdelete }}</td>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-info">Action</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <?php
                              if($UserDataSuperAdmin[$i]->softdelete != "Delete")
                              {
                                ?>
                                <li>
                                  <a href="showspecificuser?id=<?php echo encrypt($UserDataSuperAdmin[$i]->id) ;?>">
                                    <i class="fa fa-recycle"></i>
                                    <span class="text-info" style="font-family: Tahome;font-weight: bold;">Edit</span>
                                  </a>
                                </li>

                                <li>
                                  <a href="ipanel/Permissions?id=<?php echo encrypt($UserDataSuperAdmin[$i]->id) ;?>">
                                    <i class="fa fa-recycle"></i>
                                    <span class="text-info" style="font-family: Tahome;font-weight: bold;">Permissions</span>
                                  </a>
                                </li>
                              </ul>
                                <?php
                              }
                               ?>
                            </diV>
                        </th>
                      </tr>
                    @endfor
                  @endif

                  @if (count($UserData_admin) >= 1)
                    @for ($i = 0; $i < count($UserData_admin) ; $i++)
                      <tr>
                        <td>{{ ($i+1) }}</th>
                        <td hidden="hidden">{{ $UserData_admin[$i]->id }}</td>
                        <td>{{ $UserData_admin[$i]->fullname }}</td>
                        <td>{{ $UserData_admin[$i]->email }}</td>
                        <td>{{ $UserData_admin[$i]->username }}</td>
                        <td>{{ $UserData_admin[$i]->status }}</td>
                        <td>{{ $UserData_admin[$i]->typeacc }}</td>
                        <td>{{ $UserData_admin[$i]->softdelete }}</td>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-info">Action</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <?php
                              if($UserData_admin[$i]->softdelete != "Delete")
                              {
                                ?>

                                <li>
                                  <a href="showspecificuser?id=<?php echo encrypt($UserData_admin[$i]->id) ;?>">
                                    <i class="fa fa-recycle"></i>
                                    <span class="text-info" style="font-family: Tahome;font-weight: bold;">Edit</span>
                                  </a>
                                </li>

                                <li>
                                  <a href="/deleteuser?id=<?php echo encrypt($UserData_admin[$i]->id) ;?>">
                                    <i class="fa fa-recycle"></i>
                                    <span class="text-danger" style="font-family: Tahome;font-weight: bold;">Delete</span>
                                  </a>
                                </li>

                                <li>
                                  <a href="permissions?_access=<?php echo encrypt($UserData_admin[$i]->id) ;?>">
                                    <i class="fa fa-recycle"></i>
                                    <span class="text-info" style="font-family: Tahome;font-weight: bold;">Permissions</span>
                                  </a>
                                </li>
                              </ul>
                                <?php
                              }
                               ?>
                            </diV>
                        </th>
                      </tr>
                    @endfor
                  @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Status</th>
                  <th>Type</th>
                  <th>Delete</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
