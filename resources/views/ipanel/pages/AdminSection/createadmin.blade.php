@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>Silver City Cpanel| Users</title>
</head>
<body>
  <!-- Main content -->
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Add New User Data</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <!-- form start -->
        {!! Form::open(array('url' => '/ipanel/addnewadminuser','method' => 'post','files' => true)) !!}
          {{ csrf_field() }}
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                {{-- user type account --}}
                <div class="form-group">
                  <div class="form-group{{ $errors->has('opt_typeacc') ? ' has-error' : '' }}">
                    <label>User Type</label>
                    <select  name="opt_typeacc" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">choose user type</option>
                      <option value="1" selected="selected">Admin</option>
                      <option value="2" selected="selected">User</option>
                    </select>
                    @if ($errors->has('opt_typeacc'))
                        <span class="help-block">
                            <strong>{{ $errors->first('opt_typeacc') }}</strong>
                        </span>
                    @endif
                 </div>
                </div><!-- /.form-group -->

                {{-- user Country --}}
                <div class="form-group">
                  <div class="form-group{{ $errors->has('opt_countryname') ? ' has-error' : '' }}">
                    <label>User Country</label>
                    <select id="optcountryname" name="opt_countryname" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">choose user country</option>
                       @if (count($country_var) >= 1)
                        @for ($i = 0; $i < count($country_var) ; $i++)
                          <option value="{{ $country_var[$i]->id }}">{{ $country_var[$i]->name }}</option>
                        @endfor

                       @endif
                    </select>
                    @if ($errors->has('opt_countryname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('opt_countryname') }}</strong>
                        </span>
                    @endif
                  </div>
                </div><!-- /.form-group -->

                {{-- user City --}}
                <div class="form-group">
                  <div class="form-group{{ $errors->has('opt_cityname') ? ' has-error' : '' }}">
                    <label>User Country</label>
                    <select id="optcityname" name="opt_cityname" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">choose user City</option>
                    </select>
                    @if ($errors->has('opt_cityname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('opt_cityname') }}</strong>
                        </span>
                    @endif
                  </div>
                </div><!-- /.form-group -->

                {{-- First Name --}}
                <div class="form-group">
                  <div class="form-group{{ $errors->has('txt_firstname') ? ' has-error' : '' }}">
                    <label for="Inputfirstname">First Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                      </div>
                      <input type="text" class="form-control" placeholder="First Name" name="txt_firstname" value="{{ old('txt_firstname') }}" required autofocus="">
                    </div><!-- /.input group -->
                    @if ($errors->has('txt_firstname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_firstname') }}</strong>
                        </span>
                    @endif
                  </div>
                </div><!-- /.form group -->

                {{-- Last Name --}}
                <div class="form-group{{ $errors->has('txt_lastname') ? ' has-error' : '' }}">
                    <label for="Inputlastname">Last Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                      </div>
                      <input type="text" class="form-control" placeholder="Last Name" name="txt_lastname" value="{{ old('txt_lastname') }}">
                    </div><!-- /.input group -->
                    @if ($errors->has('txt_lastname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_lastname') }}</strong>
                        </span>
                    @endif
                </div><!-- /.form group -->

                {{-- Gender --}}
                <div class="form-group{{ $errors->has('opt_gender') ? ' has-error' : '' }}">
                  <label>Gender</label>
                  <select  name="opt_gender" class="form-control select2" style="width: 100%;">
                    <option value="0" selected="selected">choose gender</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                  </select>
                  @if ($errors->has('opt_gender'))
                      <span class="help-block">
                          <strong>{{ $errors->first('opt_gender') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form-group -->


                {{-- Username --}}
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                  <label for="Inputusername">Username:</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <span class="glyphicon glyphicon-user"></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}">
                  </div><!-- /.input group -->
                  @if ($errors->has('username'))
                      <span class="help-block">
                          <strong>{{ $errors->first('username') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form group -->

                {{-- Phone --}}
                <div class="form-group{{ $errors->has('txt_phone') ? ' has-error' : '' }}">
                  <label for="Inputphone">Phone Number:</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-phone"></i>
                    </div>
                    <input type="text" class="form-control" placeholder="Phone Number" name="txt_phone" value="{{ old('txt_phone') }}">
                  </div><!-- /.input group -->
                  @if ($errors->has('txt_phone'))
                      <span class="help-block">
                          <strong>{{ $errors->first('txt_phone') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form group -->

                {{-- Email --}}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="Inputemail">Email address:</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                    </div>
                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
                  </div><!-- /.input group -->
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form group -->

                {{-- Password --}}
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="Inputpassword">Password:</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-key"></i>
                    </div>
                    <input type="password" class="form-control" placeholder="Password" name="password">
                  </div><!-- /.input group -->
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form group -->

                {{-- Confirm Password --}}
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  <label for="Inputconfirm_password">Confirm Password:</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-key"></i>
                    </div>
                    <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
                  </div><!-- /.input group -->
                  @if ($errors->has('password_confirmation'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form group -->

                <div class="form-group {{ $errors->has('opt_status') ? ' has-error' : '' }}">
                  <label>Status</label>
                  <select name="opt_status" class="form-control select2" style="width: 100%;">
                    <option selected="selected">Active</option>
                    <option>Not-active</option>
                  </select>
                  @if ($errors->has('opt_status'))
                      <span class="help-block">
                          <strong>{{ $errors->first('opt_status') }}</strong>
                      </span>
                  @endif
                </div>

                {{-- Profile Picture --}}
                <div class="form-group{{ $errors->has('user_image') ? ' has-error' : '' }}">
                  <label for="file">Select image to upload</label>
                  <input type="file" id="img_user_fname" name="user_image" accept=".jpg,.png,.gif" class="filestyle" data-input="true">
                  <p class="help-block">Only jpg,png file with maximum size of 1.5 MB is allowed.</p>
                  @if ($errors->has('user_image'))
                      <span class="help-block">
                          <strong>{{ $errors->first('user_image') }}</strong>
                      </span>
                  @endif
                </div>

              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
           <div class="box-footer">
                  <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
            </div>
        {!! Form::close() !!}
      </div><!-- /.box-body -->
      <div class="box-footer">
        NPO user page
      </div><!-- /.box-footer-->
    </div><!-- /.box -->
<script src="{{asset('assets/cpanel/dist/js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/cpanel/dist/js/lib/file_upload/img.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/cpanel/dist/js/pages/createuser.js')}}" type="text/javascript"></script>
</body>
</html>
@endsection
