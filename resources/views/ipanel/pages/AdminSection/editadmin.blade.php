@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>NPO Cpanel| Users</title>
</head>
<body>
  <!-- Main content -->
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Add New User Data</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <!-- form start -->
        {!! Form::open(array('url' => '/ipanel/editnewadminuser','method' => 'post','files' => true)) !!}
          {{ csrf_field() }}
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                {{-- user type account --}}
                <div class="form-group">
                  <div class="form-group{{ $errors->has('opt_typeacc') ? ' has-error' : '' }}">
                    <label>User Type</label>
                    <select  name="opt_typeacc" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">choose user type</option>
                      <option value="1" <?php if($UserControl_var['typeacc'] == "admin") { ?>selected="selected"<?php } ?>>Admin</option>
                      <option value="2" <?php if($UserControl_var['typeacc'] == "user") { ?>selected="selected"<?php } ?>>User</option>
                    </select>
                    @if ($errors->has('opt_typeacc'))
                        <span class="help-block">
                            <strong>{{ $errors->first('opt_typeacc') }}</strong>
                        </span>
                    @endif
                 </div>
                </div><!-- /.form-group -->

                {{-- user Country --}}
                <div class="form-group">
                  <div class="form-group{{ $errors->has('opt_countryname') ? ' has-error' : '' }}">
                    <label>User Country</label>
                    <select id="optcountryname" name="opt_countryname" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">choose user country</option>

                    </select>
                    @if ($errors->has('opt_countryname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('opt_countryname') }}</strong>
                        </span>
                    @endif
                  </div>
                </div><!-- /.form-group -->

                {{-- user City --}}
                <div class="form-group">
                  <div class="form-group{{ $errors->has('opt_cityname') ? ' has-error' : '' }}">
                    <label>User Country</label>
                    <select id="optcityname" name="opt_cityname" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">choose user City</option>
                    </select>
                    @if ($errors->has('opt_cityname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('opt_cityname') }}</strong>
                        </span>
                    @endif
                  </div>
                </div><!-- /.form-group -->

                {{-- Full Name --}}
                <div class="form-group">
                  <div class="form-group{{ $errors->has('txt_fullname') ? ' has-error' : '' }}">
                    <label for="Inputfirstname">Full Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                      </div>
                      <input  type="text" class="form-control" placeholder="First Name" name="txt_fullname" value="{{ $UserControl_var['fullname'] }}" required autofocus="">
                    </div><!-- /.input group -->
                    @if ($errors->has('txt_fullname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_fullname') }}</strong>
                        </span>
                    @endif
                  </div>
                </div><!-- /.form group -->



                {{-- Gender --}}
                <div class="form-group{{ $errors->has('opt_gender') ? ' has-error' : '' }}">
                  <label>Gender</label>
                  <select  name="opt_gender" class="form-control select2" style="width: 100%;">
                    <option value="0" selected="selected">choose gender</option>
                    <option value="1" <?php if($UserControl_var['gender'] == "Male") { ?>selected="selected"<?php } ?>>Male</option>
                    <option value="2" <?php if($UserControl_var['gender'] == "Female") { ?>selected="selected"<?php } ?>>Female</option>
                  </select>
                  @if ($errors->has('opt_gender'))
                      <span class="help-block">
                          <strong>{{ $errors->first('opt_gender') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form-group -->


                {{-- Username --}}
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                  <label for="Inputusername">Username:</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <span class="glyphicon glyphicon-user"></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Username" name="username" value="{{ $UserControl_var['username'] }}">
                  </div><!-- /.input group -->
                  @if ($errors->has('username'))
                      <span class="help-block">
                          <strong>{{ $errors->first('username') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form group -->

                {{-- Phone --}}
                <div class="form-group{{ $errors->has('txt_phone') ? ' has-error' : '' }}">
                  <label for="Inputphone">Phone Number:</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-phone"></i>
                    </div>
                    <input type="text" class="form-control" placeholder="Phone Number" name="txt_phone" value="{{ $UserControl_var['phone'] }}">
                  </div><!-- /.input group -->
                  @if ($errors->has('txt_phone'))
                      <span class="help-block">
                          <strong>{{ $errors->first('txt_phone') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form group -->

                {{-- Email --}}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="Inputemail">Email address:</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                    </div>
                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ $UserControl_var['email'] }}">
                  </div><!-- /.input group -->
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div><!-- /.form group -->

                <div class="form-group {{ $errors->has('opt_status') ? ' has-error' : '' }}">
                  <label>Status</label>
                  <select name="opt_status" class="form-control select2" style="width: 100%;">
                    <option <?php if($UserControl_var['status'] == "Active") { ?>selected="selected"<?php } ?>>Active</option>
                    <option <?php if($UserControl_var['status'] == "Not-active") { ?>selected="selected"<?php } ?>>Not-active</option>
                  </select>
                  @if ($errors->has('opt_status'))
                      <span class="help-block">
                          <strong>{{ $errors->first('opt_status') }}</strong>
                      </span>
                  @endif
                </div>

              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
           <div class="box-footer">
                  <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
            </div>
        {!! Form::close() !!}
      </div><!-- /.box-body -->
      <div class="box-footer">
        NPO user page
      </div><!-- /.box-footer-->
    </div><!-- /.box -->
<script src="{{asset('assets/cpanel/dist/js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/cpanel/dist/js/lib/file_upload/img.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/cpanel/dist/js/pages/createuser.js')}}" type="text/javascript"></script>
</body>
</html>
@endsection
