@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<section class="content-header">
  <h1>
    Newsletter Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Show Newsletter Data</li>
  </ol>
</section>
<!-- Main content -->
    <section class="content">
      <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Newsletter Data</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('newsletteraction_result'))

                    <?php $saved_message= []; $saved_message=Session::pull('newsletteraction_result');?>
                    <?php if($saved_message[0] == 'success')
                    {
                      ?>
                      <div  class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    elseif ($saved_message[0] == 'failed') {
                      # code...
                      ?>
                      <div  class="alert alert-danger">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    ?>

              @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Email</th>
                  <th>Delete</th>
                  <th>Created at</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if (count($NewsletterData) >= 1)
                    @for ($i = 0; $i < count($NewsletterData) ; $i++)
                      <tr>
                        <td>{{ ($i+1) }}</th>
                        <td hidden="hidden">{{ $NewsletterData[$i]->id }}</td>
                        <td>{{ $NewsletterData[$i]->email }}</td>
                        <td>{{ $NewsletterData[$i]->softdelete }}</td>
                        <td>{{ $NewsletterData[$i]->created_at }}</td>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-info">Action</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <?php
                              if($NewsletterData[$i]->softdelete != "Delete")
                              {
                                ?>
                                <li>

                                    <a href="deletenewsletter?id=<?php echo encrypt($NewsletterData[$i]->id) ;?>">
                                      <i class="fa fa-recycle"></i>
                                      <span class="text-danger" style="font-family: Tahome;font-weight: bold;">Delete</span>
                                    </a>
                                </li>
                                <?php
                              }
                              ?>

                            </ul>
                          </div>
                        </th>
                      </tr>
                    @endfor

                  @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Email</th>
                  <th>Delete</th>
                  <th>Created at</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
