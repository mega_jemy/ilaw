@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Banner Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Add new Banner</li>
  </ol>
</section>
<!-- Main content -->
<!-- /.content -->

    <section class="content">
      <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Fill required Fields</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- .box-body -->
              <div class="box-body">
                @if(Session::has('action_result'))

                      <?php $saved_message= []; $saved_message=Session::pull('action_result');?>
                      <?php if($saved_message[0] == 'success')
                      {
                        ?>
                        <div  class="alert alert-success">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      elseif ($saved_message[0] == 'failed') {
                        # code...
                        ?>
                        <div  class="alert alert-danger">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Error! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      ?>

                @endif
                <!-- input states -->
                <form action="{{ url('/ipanel/savenewabout') }}" method="post" role="form" enctype="multipart/form-data" >
                  {{ csrf_field() }}
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Language --}}
                  <div class="form-group {{ $errors->has('opt_lang') ? ' has-error' : '' }}">
                    <label>Language:</label>
                    <select id="opt_langID" name="opt_lang" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Language --</option>
                      @if(count($activeLang) > 0)
                        @for ($i = 0; $i < count($activeLang) ; $i++)
                          <option value="{{ $activeLang[$i]->id }}">{{ $activeLang[$i]->name }} - {{ $activeLang[$i]->regional }}</option>
                        @endfor
                      @endif
                    </select>
                    <span class="help-block " id="span_opt_langID">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Title --}}
                  <div class="form-group {{ $errors->has('inp_desc') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Description:</label>
                    <textarea id="editor1" name="inp_desc" rows="10" cols="80" placeholder="Description of training course">
                      Description of Title
                    </textarea>
                    <span id="span_inp_desc" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Vision --}}
                  <div class="form-group {{ $errors->has('inp_vision') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Our Vision:</label>
                    <textarea id="editor2" name="inp_vision" rows="10" cols="80" placeholder="Description of training course">
                      Description of Vision
                    </textarea>
                    <span id="span_inp_vision" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Mission --}}
                  <div class="form-group {{ $errors->has('inp_mission') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_mission">Our Mission:</label>
                    <textarea id="editor3" name="inp_mission" rows="10" cols="80" placeholder="Description ">
                      Description of Mission
                    </textarea>
                    <span id="span_inp_mission" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- History --}}
                  <div class="form-group {{ $errors->has('inp_history') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_history">out History:</label>
                    <textarea id="editor4" name="inp_history" rows="10" cols="80" placeholder="Description of training course">
                      Description of History
                    </textarea>
                    <span id="span_inp_history" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->



                  <!--<button onclick="validateTblFields()" id="btn_add_input" type="button" class="btn btn-primary">Add thumbnail</button>
                  <table id="example" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>language</th>
                        <th>place</th>
                        <th>status</th>
                        <th>order</th>
                        <th>thumbnail title</th>
                        <th>thumbnail alt</th>
                        <th>thumbnail</th>
                        <th>action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table>-->
              <!-- /.box-body -->
              <div class="box-footer with-border">
                <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
    <script src="{{asset('js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/lib/file_upload/img.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/ipages/banner_page.js')}}" type="text/javascript"></script>
  </body>
  </html>
@endsection
