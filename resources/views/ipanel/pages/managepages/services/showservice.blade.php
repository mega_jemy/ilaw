@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Service Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Show Service Data</li>
  </ol>
</section>
<!-- Main content -->
<!-- /.content -->
    <section class="content">
      <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Services Data</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <a href="{{ url('ipanel/addnewservice') }}"><h5 class="box-title">Add New Service</h5></a>
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('banneraction_result'))

                    <?php $saved_message= []; $saved_message=Session::pull('banneraction_result');?>
                    <?php if($saved_message[0] == 'success')
                    {
                      ?>
                      <div  class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    elseif ($saved_message[0] == 'failed') {
                      # code...
                      ?>
                      <div  class="alert alert-danger">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    ?>

              @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Name</th>
                  <th>Title</th>
                  <th>Status</th>
                  <th>Order</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @if (count($ServiceData) >= 1)
                      @for ($i = 0; $i < count($ServiceData) ; $i++)
                        <tr>
                          <td>{{ ($i+1) }}</th>
                          <td hidden="hidden">{{ $ServiceData[$i]->id }}</td>
                          <td>{{ $ServiceData[$i]->name }}</td>
                          <td>{{ $ServiceData[$i]->title }}</td>
                          <td>{{ $ServiceData[$i]->status }}</td>
                          <td>{{ $ServiceData[$i]->order }}</td>
                          <td>
                            <div class="btn-group">
                              <button type="button" class="btn btn-info">Action</button>
                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <?php
                                if($ServiceData[$i]->softdelete != "Delete")
                                {
                                  ?>
                                  <li>
                                    <a href="show_service?id=<?php echo encrypt($ServiceData[$i]->id) ;?>">
                                      <i class="fa fa-edit"></i>
                                      <span class="text-success" style="font-family: Tahome;font-weight: bold;">Edit</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="delete_service?id=<?php echo encrypt($ServiceData[$i]->id) ;?>">
                                      <i class="fa fa-recycle"></i>
                                      <span class="text-danger" style="font-family: Tahome;font-weight: bold;">Delete</span>
                                    </a>
                                  </li>
                                </ul>
                                  <?php
                                }
                                 ?>

                              </diV>
                          </th>
                        </tr>
                      @endfor

                    @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Name</th>
                  <th>Title</th>
                  <th>Status</th>
                  <th>Order</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
