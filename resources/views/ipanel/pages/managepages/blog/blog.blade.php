@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Blog Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Add new Blog</li>
  </ol>
</section>
<!-- Main content -->
<!-- /.content -->

    <section class="content">
      <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Fill required Fields</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- .box-body -->
              <div class="box-body">
                @if(Session::has('action_result'))

                      <?php $saved_message= []; $saved_message=Session::pull('action_result');?>
                      <?php if($saved_message[0] == 'success')
                      {
                        ?>
                        <div  class="alert alert-success">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      elseif ($saved_message[0] == 'failed') {
                        # code...
                        ?>
                        <div  class="alert alert-danger">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Error! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      ?>

                @endif
                <!-- input states -->
                <form action="{{ url('/ipanel/save_new_blog') }}" method="post" role="form" enctype="multipart/form-data" >
                  {{ csrf_field() }}
                  <input type="hidden" name="inp_idpage" value="debt_collection">
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Language --}}
                  <div class="form-group {{ $errors->has('opt_lang') ? ' has-error' : '' }}">
                    <label>Language:</label>
                    <select id="opt_langID" name="opt_lang" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Language --</option>
                      @if(count($activeLang) > 0)
                        @for ($i = 0; $i < count($activeLang) ; $i++)
                          <option value="{{ $activeLang[$i]->id }}">{{ $activeLang[$i]->name }} - {{ $activeLang[$i]->regional }}</option>
                        @endfor
                      @endif
                    </select>
                    <span class="help-block " id="span_opt_langID">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Name --}}
                  <div class="form-group {{ $errors->has('inp_name') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_name">Name:</label>
                    <input value="{{ old('inp_name') }}" type="text" class="form-control" id="inp_order" name="inp_name" placeholder="Name...">
                    <span id="span_inp_name" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Title --}}
                  <div class="form-group {{ $errors->has('inp_title') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_title">Title:</label>
                    <input value="{{ old('inp_title') }}" type="text" class="form-control" id="inp_order" name="inp_title" placeholder="Title...">
                    <span id="span_inp_title" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Writer Name --}}
                  <div class="form-group {{ $errors->has('inp_writer_name') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_title">Name Writer:</label>
                    <input value="{{ old('inp_writer_name') }}" type="text" class="form-control" id="inp_writer_name" name="inp_writer_name" placeholder="Name Writer...">
                    <span id="span_inp_writer_name" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Writer title --}}
                  <div class="form-group {{ $errors->has('inp_writer_title') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_title">Name title:</label>
                    <input value="{{ old('inp_writer_title') }}" type="text" class="form-control" id="inp_writer_title" name="inp_writer_title" placeholder="Name title...">
                    <span id="span_inp_writer_title" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Writer Date --}}
                  <!-- Date -->
                  <div class="form-group {{ $errors->has('inp_writer_date') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_title">Date Writer:</label>

                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input value="{{ old('inp_writer_date') }}" type="text" class="form-control pull-right" id="datepicker" name="inp_writer_date" placeholder="Name date...">
                      <span id="span_inp_writer_date" class="help-block">
                      </span>
                    </div>
                    <!-- /.input group -->
                  </div>

                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Description --}}
                  <div class="form-group {{ $errors->has('inp_desc') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Description:</label>
                    <textarea id="editor1" name="inp_desc" rows="10" cols="80" placeholder="Description of training course">
                      Description of training course
                    </textarea>
                    <span id="span_inp_desc" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Banner Status --}}
                  <div class="form-group {{ $errors->has('opt_status') ? ' has-error' : '' }}">
                    <label>Status:</label>
                    <select id="opt_statusID" name="opt_status" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Status --</option>
                      <option value="1">Active</option>
                      <option value="2">Not-active</option>
                    </select>
                    <span id="span_opt_statusID" class="help-block">

                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Picture --}}
                  <div class="form-group{{ $errors->has('inp_thumbnail') ? ' has-error' : '' }}">
                    <label for="file">thumbnail:</label>
                    <input value="{{ old('inp_thumbnail') }}"  type="file" id="img_user_fname" name="inp_thumbnail" accept=".jpg,.png,.gif" class="filestyle" data-input="true">
                    <p class="help-block">Only jpg,png file with maximum size of 1.5 MB is allowed.</p>
                    <span id="span_inp_thumbnail" class="help-block">

                    </span>
                  </div>


                  <!--<button onclick="validateTblFields()" id="btn_add_input" type="button" class="btn btn-primary">Add thumbnail</button>
                  <table id="example" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>language</th>
                        <th>place</th>
                        <th>status</th>
                        <th>order</th>
                        <th>thumbnail title</th>
                        <th>thumbnail alt</th>
                        <th>thumbnail</th>
                        <th>action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table>-->
              <!-- /.box-body -->
              <div class="box-footer with-border">
                <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
    <script src="{{asset('js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/lib/file_upload/img.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/ipages/banner_page.js')}}" type="text/javascript"></script>
  </body>
  </html>
@endsection
