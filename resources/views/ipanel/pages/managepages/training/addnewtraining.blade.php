@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Banner Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Add new Banner</li>
  </ol>
</section>
<!-- Main content -->
<!-- /.content -->

    <section class="content">
      <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Fill required Fields</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- .box-body -->
              <div class="box-body">
                @if(Session::has('banneraction_result'))

                      <?php $saved_message= []; $saved_message=Session::pull('banneraction_result');?>
                      <?php if($saved_message[0] == 'success')
                      {
                        ?>
                        <div  class="alert alert-success">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      elseif ($saved_message[0] == 'failed') {
                        # code...
                        ?>
                        <div  class="alert alert-danger">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Error! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      ?>

                @endif
                <!-- input states -->
                <form action="{{ url('/ipanel/savenewtrainingcourse') }}" method="post" role="form" enctype="multipart/form-data" >
                  {{ csrf_field() }}
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Language --}}
                  <div class="form-group {{ $errors->has('opt_type') ? ' has-error' : '' }}">
                    <label>Language:</label>
                    <select id="opt_langID" name="opt_lang" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Language --</option>
                      @if(count($activeLang) > 0)
                        @for ($i = 0; $i < count($activeLang) ; $i++)
                          <option value="{{ $activeLang[$i]->id }}">{{ $activeLang[$i]->name }} - {{ $activeLang[$i]->regional }}</option>
                        @endfor
                      @endif
                    </select>
                    <span class="help-block " id="span_opt_lang">

                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Name --}}
                  <div class="form-group {{ $errors->has('inp_name') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_inp_name">Name:</label>
                    <input type="text" class="form-control" id="inp_nameID" name="inp_name" placeholder="Name..."
                    value="{{ old('inp_name') }}" >
                    <span id="span_inp_name" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Title --}}
                  <div class="form-group {{ $errors->has('inp_title') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_inp_title">Title:</label>
                    <input type="text" class="form-control" id="inp_titleID" name="inp_title" placeholder="Title..."
                    value="{{ old('inp_title') }}" >
                    <span id="span_inp_title" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Description --}}
                  <div class="form-group {{ $errors->has('inp_desc') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Description:</label>
                    <textarea id="editor1" name="inp_desc" rows="10" cols="80" placeholder="Description of training course">
                      Description of training course
                    </textarea>
                    <span id="span_inp_desc" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Link --}}
                  <div class="form-group{{ $errors->has('inp_link') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Link:<small style="color:red;">example:https://url.com</small></label>
                  </div>
                  <div class="input-group {{ $errors->has('inp_link') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-external-link"></i></span>
                    <input value="{{ old('inp_link') }}" type="text" class="form-control" id="inp_linkID" name="inp_link" placeholder="URL...">
                    <span id="span_inp_link" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Order --}}
                  <div class="form-group {{ $errors->has('inp_order') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Order:</label>
                    <input value="{{ old('inp_order') }}" type="text" class="form-control" id="inp_orderID" name="inp_order" placeholder="Order...">
                    <span id="span_inp_order" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Status --}}
                  <div class="form-group {{ $errors->has('opt_status') ? ' has-error' : '' }}">
                    <label>Status:</label>
                    <select id="opt_statusID" name="opt_status" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Status --</option>
                      <option value="Active">Active</option>
                      <option value="Not-active">Not-active</option>
                    </select>
                    <span id="span_opt_status" class="help-block">

                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Old Price --}}
                  <div class="form-group {{ $errors->has('inp_oldprice') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Old Price:</label>
                    <input value="{{ 0 }}" type="text" class="form-control" id="inp_oldpriceID" name="inp_oldprice" placeholder="Old Price..">
                    <span id="span_inp_oldprice" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- New Prices --}}
                  <div class="form-group {{ $errors->has('inp_newprice') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_newprice">New Price:</label>
                    <input value="{{ old('inp_newprice') }}" type="text" class="form-control" id="inp_newpriceID" name="inp_newprice" placeholder="New Price...">
                    <span id="span_inp_newprice" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  {{-- Picture --}}
                  <div class="form-group{{ $errors->has('inp_avatar') ? ' has-error' : '' }}">
                    <label for="file">Avatar:</label>
                    <input value="{{ old('inp_avatar') }}"  type="file" id="img_user_fname" name="inp_avatar" accept=".jpg,.png,.gif" class="filestyle" data-input="true">
                    <p class="help-block">Only jpg,png file with maximum size of 1.5 MB is allowed.</p>
                    <span id="span_inp_avatar" class="help-block">
                    </span>
                  </div>
                  <!----------------------------------------------------------------------------------------------------------->
                  <div class="row">
                    <div class="col-xs-3">
                      <div class="form-group {{ $errors->has('opt_hotdeals') ? ' has-error' : '' }}">
                        <label>Hot Deals</label>
                        <select id="opt_hotdealsID" name="opt_hotdeals" class="form-control select2" style="width: 100%;">
                          <option value="0" selected="selected">--Choose Hot deals--</option>
                          <option value="Yes" @if(old('opt_hotdeals') == 'Yes') selected="selected" @endif>Yes</option>
                          <option value="No" @if(old('opt_hotdeals') == 'No') selected="selected" @endif>No</option>
                        </select>
                        <span id="span_opt_hotdeals" class="help-block">
                        </span>
                      </div>
                    </div>
                    <!----------------------------------------------------------------------------------------------------------->
                    <div class="col-xs-3">
                      <div class="form-group {{ $errors->has('opt_dealofmonth') ? ' has-error' : '' }}">
                        <label>Deal of Month</label>
                        <select id="opt_dealofmonthID" name="opt_dealofmonth" class="form-control select2" style="width: 100%;">
                          <option value="0" selected="selected">--Choose Deal of Month--</option>
                          <option value="Yes" @if(old('opt_dealofmonth') == 'Yes') selected="selected" @endif>Yes</option>
                          <option value="No" @if(old('opt_dealofmonth') == 'No') selected="selected" @endif>No</option>
                        </select>
                        <span id="span_opt_dealofmonth" class="help-block">
                        </span>
                      </div>
                    </div>
                    <!----------------------------------------------------------------------------------------------------------->
                    <div class="col-xs-3">
                      <div class="form-group {{ $errors->has('opt_shownew') ? ' has-error' : '' }}">
                        <label>Show New</label>
                        <select id="opt_shownewID" name="opt_shownew" class="form-control select2" style="width: 100%;">
                          <option value="0" selected="selected">--Choose Show New--</option>
                          <option value="Yes" @if(old('opt_shownew') == 'Yes') selected="selected" @endif>Yes</option>
                          <option value="No" @if(old('opt_shownew') == 'No') selected="selected" @endif>No</option>
                        </select>
                        <span id="span_opt_shownew" class="help-block">
                        </span>
                      </div>
                    </div>
                    <!----------------------------------------------------------------------------------------------------------->
                    <div class="col-xs-3">
                      <div class="form-group {{ $errors->has('opt_showoos') ? ' has-error' : '' }}">
                        <label>Show Out of Stock</label>
                        <select id="opt_showoosID" name="opt_showoos" class="form-control select2" style="width: 100%;">
                          <option value="0" selected="selected">--Choose Out of Stock--</option>
                          <option value="Yes" @if(old('opt_showoos') == 'Yes') selected="selected" @endif>Yes</option>
                          <option value="No" @if(old('opt_showoos') == 'No') selected="selected" @endif>No</option>
                        </select>
                        <span id="span_opt_showoos" class="help-block">
                        </span>
                      </div>
                    </div>
                    <!----------------------------------------------------------------------------------------------------------->
                    <div class="col-xs-3">
                      <div class="form-group {{ $errors->has('opt_showonhome') ? ' has-error' : '' }}">
                        <label>Show on Home</label>
                        <select id="opt_showonhomeID" name="opt_showonhome" class="form-control select2" style="width: 100%;">
                          <option value="0" selected="selected">--Choose Show Home--</option>
                          <option value="Yes" @if(old('opt_showonhome') == 'Yes') selected="selected" @endif>Yes</option>
                          <option value="No" @if(old('opt_showonhome') == 'No') selected="selected" @endif>No</option>
                        </select>
                        <span id="span_opt_showonhome" class="help-block">
                        </span>
                      </div>
                    </div>
                    <!----------------------------------------------------------------------------------------------------------->
                    <!--<div class="row">
                      <div class="col-xs-12">
                        <button onclick="validateTblFields()" id="btn_add_input" type="button" class="btn btn-primary">Add thumbnail</button>
                      </div>
                    </div>-->
                    <!----------------------------------------------------------------------------------------------------------->
                    <!--<table id="example" class="table table-bordered table-hover table-responsive">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>language</th>
                          <th>Name</th>
                          <th>Title</th>
                          <th>Desc</th>
                          <th>Link</th>
                          <th>Order</th>
                          <th>Status</th>
                          <th>Old price</th>
                          <th>New price</th>
                          <th>Hot Deals</th>
                          <th>deal of month</th>
                          <th>Show New</th>
                          <th>Show out of stock</th>
                          <th>Show Home</th>
                          <th>avatar</th>
                          <th>action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>-->
              <!-- /.box-body -->
              <div class="row">
                <div class="col-xs-12">
                  <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
                </div>
              </div>
              
            </form>
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
    <script src="{{asset('js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/lib/file_upload/img.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/ipages/training_page.js')}}" type="text/javascript"></script>
  </body>
  </html>
@endsection
