@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Banner Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Add new Banner</li>
  </ol>
</section>
<!-- Main content -->
<!-- /.content -->

    <section class="content">
      <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Fill required Fields</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- .box-body -->
              <div class="box-body">
                @if(Session::has('banneraction_result'))

                      <?php $saved_message= []; $saved_message=Session::pull('banneraction_result');?>
                      <?php if($saved_message[0] == 'success')
                      {
                        ?>
                        <div  class="alert alert-success">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      elseif ($saved_message[0] == 'failed') {
                        # code...
                        ?>
                        <div  class="alert alert-danger">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Error! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      ?>

                @endif
                <!-- input states -->
                <form action="{{ url('/ipanel/savenewbanner') }}" method="post" role="form" enctype="multipart/form-data" >
                  {{ csrf_field() }}
                  {{-- Color --}}
                  <div class="form-group {{ $errors->has('opt_type') ? ' has-error' : '' }}">
                    <label>Color:</label>
                    <select id="opt_langID" name="opt_lang" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Color --</option>
                      <option data-foo="bar"><span class="text-aqua" href="#"><i class="fa fa-square"></i>aqua</span></option>
                      <option data-foo="bar"><span class="text-blue" href="#"><i class="fa fa-square"></i>blue</span></option>
                      <option><span class="text-light-blue" href="#"><i class="fa fa-square"></i>light-blue</span></option>
                      <option><span class="text-teal" href="#"><i class="fa fa-square"></i>teal</span></option>
                      <option><span class="text-yellow" href="#"><i class="fa fa-square"></i>yellow</span></option>
                      <option><span class="text-orange" href="#"><i class="fa fa-square"></i>orange</span></option>
                      <option><span class="text-green" href="#"><i class="fa fa-square"></i>green</span></option>
                      <option><span class="text-lime" href="#"><i class="fa fa-square"></i>lime</span></option>
                      <option><span class="text-red" href="#"><i class="fa fa-square"></i>red</span></option>
                      <option><span class="text-purple" href="#"><i class="fa fa-square"></i>purple</span></option>
                      <option><span class="text-fuchsia" href="#"><i class="fa fa-square"></i>fuchsia</span></option>
                      <option><span class="text-muted" href="#"><i class="fa fa-square"></i>muted</span></option>
                      <option><span class="text-navy" href="#"><i class="fa fa-square"></i>navy</span></option>
                    </select>
                    <span class="help-block " id="span_opt_langID">

                    </span>
                  </div>



                  {{-- Name --}}
                  <div class="form-group {{ $errors->has('txt_order') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Name:</label>
                    <input value="{{ old('txt_name') }}" type="text" class="form-control" id="inp_name" name="txt_name" placeholder="Name...">
                    <span id="span_inp_name" class="help-block">
                    </span>
                  </div>
              <!-- /.box-body -->
              <div class="box-footer with-border">
                <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
    <script src="{{asset('js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/lib/file_upload/img.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/ipages/banner_page.js')}}" type="text/javascript"></script>
  </body>
  </html>
@endsection
