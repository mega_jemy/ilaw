@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Banner Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Show Banner Data</li>
  </ol>
</section>
<!-- Main content -->
<!-- /.content -->
    <section class="content">
      <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Categories Data</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <a href="addnewfixedevent"><h5 class="box-title">Add New Fixed Event</h5></a>
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('banneraction_result'))

                    <?php $saved_message= []; $saved_message=Session::pull('banneraction_result');?>
                    <?php if($saved_message[0] == 'success')
                    {
                      ?>
                      <div  class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    elseif ($saved_message[0] == 'failed') {
                      # code...
                      ?>
                      <div  class="alert alert-danger">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    ?>

              @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Name</th>
                  <th>Color</th>
                  <th>registered at</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @if (count($AppointmentControllerData) >= 1)
                      @for ($i = 0; $i < count($AppointmentControllerData) ; $i++)
                        <tr>
                          <td>{{ ($i+1) }}</th>
                          <td hidden="hidden">{{ $AppointmentControllerData[$i]->id }}</td>
                          <td>{{ $AppointmentControllerData[$i]->name }}</td>
                          <td><span class="badge" style="background-color:{{ $AppointmentControllerData[$i]->color }}">{{ $AppointmentControllerData[$i]->color }}<span></td>
                          <td>{{ $AppointmentControllerData[$i]->created_at }}</td>
                          <td>
                            <?php
                            if($AppointmentControllerData[$i]->softdelete != "Delete")
                            {
                              ?>
                              <span>
                                <a href="showcustomfixedevent?_access={{ $AppointmentControllerData[$i]->typeban }}&id=<?php echo encrypt($AppointmentControllerData[$i]->id) ;?>">
                                  <i class="fa fa-edit"></i>
                                  <span class="text-success" style="font-family: Tahome;font-weight: bold;">Edit</span>
                                </a>
                              </span>
                              <span>
                                |
                              </span>
                              <span>
                                <a href="deletefixedevent?_access={{ $AppointmentControllerData[$i]->typeban }}&id=<?php echo encrypt($AppointmentControllerData[$i]->id) ;?>">
                                  <i class="fa fa-recycle"></i>
                                  <span class="text-danger" style="font-family: Tahome;font-weight: bold;">Delete</span>
                                </a>
                              </span>
                              <?php
                            }
                             ?>
                          </th>
                        </tr>
                      @endfor

                    @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Name</th>
                  <th>Color</th>
                  <th>registered at</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
