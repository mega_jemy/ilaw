@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
  <section class="content-header">
    <h1>
      Enquiries Management
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Show Message Enquiry</li>
    </ol>
  </section>
<!-- Main content -->
    <section class="content">
      <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Enquiries Message Data</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('enquiriesmessageaction_result'))

                    <?php $saved_message= []; $saved_message=Session::pull('enquiriesmessageaction_result');?>
                    <?php if($saved_message[0] == 'success')
                    {
                      ?>
                      <div  class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    elseif ($saved_message[0] == 'failed') {
                      # code...
                      ?>
                      <div  class="alert alert-danger">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    ?>

              @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Delete</th>
                  <th>Created at</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if (count($EnquerymessageData) >= 1)
                    @for ($i = 0; $i < count($EnquerymessageData) ; $i++)
                      <tr>
                        <td>{{ ($i+1) }}</th>
                        <td hidden="hidden">{{ $EnquerymessageData[$i]->id }}</td>
                        <td>{{ $EnquerymessageData[$i]->email }}</td>
                        <td>{{ $EnquerymessageData[$i]->softdelete }}</td>
                        <td>{{ $EnquerymessageData[$i]->created_at }}</td>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-info">Action</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <?php
                            if($EnquerymessageData[$i]->softdelete != "Delete")
                            {
                              ?>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                  <a href="#" data-toggle="modal" data-target="#showdetailModel{{ ($i+1) }}">
                                    <i class="fa fa-search"></i>
                                    <span class="text-info" style="font-family: Tahome;font-weight: bold;">See More</span>
                                  </a>
                                </li>
                                <li>
                                    <a href="deleteenquiriesmessage?id=<?php echo encrypt($EnquerymessageData[$i]->id) ;?>&type=<?php echo encrypt(1); ?>">
                                      <i class="fa fa-recycle"></i>
                                      <span class="text-danger" style="font-family: Tahome;font-weight: bold;">Delete</span>
                                    </a>
                                </li>
                            </ul>
                            <!--begin search Modal -->
                            <div class="modal fade" id="showdetailModel{{ ($i+1) }}" role="dialog">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Show Message</h4>
                                  </div>

                                  <div class="modal-body">
                                    <div class="nav-tabs-custom">
                                      @if (count($EnquerymessageData) >= 1)
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                              <a href="#{{ $EnquerymessageData[$i]->id }}" data-toggle="tab">
                                                Message Detail
                                              </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="active tab-pane" id="{{ $EnquerymessageData[$i]->id }}">
                                              <table id="example1" class="table table-bordered table-striped">
                                                <tbody>
                                                  <tr>
                                                    <th>Name:</th>
                                                    <td>{{$EnquerymessageData[$i]->fullname}}</td>
                                                  </tr>
                                                  <tr>
                                                    <th>Email:</th>
                                                    <td>{{$EnquerymessageData[$i]->email}}</td>
                                                  </tr>
                                                  <tr>
                                                    <th>Subject:</th>
                                                    <td>{{$EnquerymessageData[$i]->subject}}</td>
                                                  </tr>
                                                  <tr>
                                                    <th>Message:</th>
                                                    <td>{{$EnquerymessageData[$i]->message}}</td>
                                                  </tr>
                                                  <tr>
                                                    <th>Create at:</th>
                                                    <td>{{$EnquerymessageData[$i]->created_at}}</td>
                                                  </tr>
                                                  <tr>
                                                    <th>IsMember?:</th>
                                                    <td>{{$EnquerymessageData[$i]->ismember}}</td>
                                                  </tr>
                                                  <tr>
                                                    <th>IsDelete?:</th>
                                                    <td>{{$EnquerymessageData[$i]->softdelete}}</td>
                                                  </tr>


                                                </tbody>
                                              </table>
                                            </div>
                                          </div>

                                      @endif

                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                            <!--end search Modal -->
                          </div>
                          <?php
                        }
                        ?>
                        </th>
                      </tr>
                    @endfor

                  @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Delete</th>
                  <th>Created at</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
