@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Payment Page</title>
    <script>
    /****************************************************************************************************/
    PaymentSession.configure({
        fields: {
            // ATTACH HOSTED FIELDS TO YOUR PAYMENT PAGE FOR A CREDIT CARD
            card: {
              number: "#card-number",
              securityCode: "#security-code",
              expiryMonth: "#expiry-month",
              expiryYear: "#expiry-year",
            }
        },
        //SPECIFY YOUR MITIGATION OPTION HERE
        frameEmbeddingMitigation: ["javascript"],
        callbacks: {
            formSessionUpdate: function(response) {
                // HANDLE RESPONSE FOR UPDATE SESSION
                if (response.status) {
                    if ("ok" == response.status) {
                      session_id = response.session.id;
                        console.log("Session updated with data: " + response.session.id);
                        console.log("Data:"+ JSON.stringify(response.session));
                        console.log("response:"+ JSON.stringify(response));
                        //check if the security code was provided by the user
                        if (response.sourceOfFunds.provided.card.securityCode) {
                            console.log("Security code was provided.");
                        }

                        //check if the user entered a MasterCard credit card
                        if (response.sourceOfFunds.provided.card.scheme == 'MASTERCARD') {
                            console.log("The user entered a MasterCard credit card.")
                        }
                       //response
                        //var route = 'https://eu-gateway.mastercard.com/api/rest/version/44/merchant/TEST7004102/order/10/transaction/10/';
                    //    window.location.replace(route);
                    } else if ("fields_in_error" == response.status)  {

                        console.log("Session update failed with field errors.");
                        if (response.errors.cardNumber) {
                            console.log("Card number invalid or missing.");
                        }
                        if (response.errors.expiryYear) {
                            console.log("Expiry year invalid or missing.");
                        }
                        if (response.errors.expiryMonth) {
                            console.log("Expiry month invalid or missing.");
                        }
                        if (response.errors.securityCode) {
                            console.log("Security code invalid.");
                        }
                    } else if ("request_timeout" == response.status)  {
                        console.log("Session update failed with request timeout: " + response.errors.message);
                    } else if ("system_error" == response.status)  {
                        console.log("Session update failed with system error: " + response.errors.message);
                    }
                } else {
                    console.log("Session update failed: " + response);
                }
            }
          }
      });
    /****************************************************************************************************/
    Checkout.configure({
      merchant: '7000670',
      order: {
        amount: function() {
          return 1;

        },
        currency: 'AED',
        description:  function() {
          return 'دورة المهارات المتقدمه فى فن صياغه العقود';
        },
        id: function() {
          return $("#lastID").val();
        }
      },
      billing : {
        address: {
          city : function() {
            return 'Sharjah';
          },

        }
      },
      customer: {
        email:  function() {
          return $("#inp_email").val();
        },
        phone:  function() {
          return $("#inp_phone").val();
        }
      },
      interaction: {
        merchant: {
          name   : 'ilaw',
          address: {
            line1: 'United Arab Emirates – Sharjah – Cornish AL Buhairah – Bel Rashid Tower – Office 1704 BR1',
            line2: 'BR2 United Arab Emirates – Ras AL Kaiymah –  Julphar Avenue 1st Floor ',
          },
          email  : 'infoline@ilaw.ae',
          phone  : '+971 6 556 5566',
          logo   : 'https://www.ilaw.ae/images/logo.png'

        },
        locale        : 'en_US',
        theme         : 'default',
        displayControl: {
          billingAddress  : 'HIDE',
          customerEmail   : 'SHOW',
          orderSummary    : 'SHOW',
          shipping        : 'HIDE',
        }
      }
    });
    /**********************************************************************************************/
    </script>
    <script src="https://eu-gateway.mastercard.com/checkout/version/42/checkout.js"
    data-error="errorCallback"
    data-cancel="cancelCallback"
    data-complete="completeCallback">
    </script>
    <script type="text/javascript">
            function completeCallback(resultIndicator, sessionVersion) {
                /*var OrderID_id = $('#OrderID_id').val();
                var tracknumber = 'SC-'+OrderID_id;
                var route = "https://silvercity.ae/bill-print/"+tracknumber;
                location.replace(route);*/
            }
            function cancelCallback()
            {
              /*var OrderID_id = $('#OrderID_id').val();
              var tracknumber = 'SC-'+OrderID_id;
              var route = "https://silvercity.ae/cancel-order/"+tracknumber;
              location.replace(route);*/
            }
            function errorCallback()
            {
              /*var OrderID_id = $('#OrderID_id').val();
              var tracknumber = 'SC-'+OrderID_id;
              var route = "https://silvercity.ae/error-order/"+tracknumber;
              location.replace(route);*/
            }

        </script>
        <!-- INCLUDE SESSION.JS JAVASCRIPT LIBRARY -->
        <script src="https://eu-gateway.mastercard.com/form/version/42/merchant/7000670/session.js"></script>
</head>
<body>
<section class="content-header">
  <h1>
    Payment Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Payment Page</li>
  </ol>
</section>
<!-- Main content -->
    <section class="content">
      <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Payment Page</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <button onclick="Checkout.showLightbox();" type="button" class="btn btn-info btn-sm"
                      title="Pay">
                <i class="fa fa-card"></i> Pay
              </button>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
