@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Banner Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Edit Banner Data</li>
  </ol>
</section>
<!-- Main content -->
<!-- /.content -->

    <section class="content">
      <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Fill required Fields</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- .box-body -->
              <div class="box-body">
                @if(Session::has('banneraction_result'))

                      <?php $saved_message= []; $saved_message=Session::pull('banneraction_result');?>
                      <?php if($saved_message[0] == 'success')
                      {
                        ?>
                        <div  class="alert alert-success">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      elseif ($saved_message[0] == 'failed') {
                        # code...
                        ?>
                        <div  class="alert alert-danger">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Error! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      ?>

                @endif
                <!-- input states -->
                {!! Form::open(array('url' => '/ipanel/editnewbanner','method' => 'post','files' => true)) !!}
                  {{ csrf_field() }}
                  <input type="text" hidden="hidden" name="_access" value="<?php echo $_access; ?>" />
                  <input type="text" hidden="hidden" name="id" value="<?php echo encrypt($BannerControl_var['id']); ?>" />
                  {{-- Banner type --}}
                  <div class="form-group {{ $errors->has('opt_type') ? ' has-error' : '' }}">
                    <label>Status:</label>
                    <select name="opt_type" class="form-control select2" style="width: 100%;">
                      <option <?php if($BannerControl_var['typeban'] == "top") { ?>selected="selected"<?php } ?>>Top</option>
                      <option <?php if($BannerControl_var['typeban'] == "right") { ?>selected="selected"<?php } ?>>Right</option>
                      <option <?php if($BannerControl_var['typeban'] == "footer") { ?>selected="selected"<?php } ?>>Footer</option>
                    </select>
                    @if ($errors->has('opt_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('opt_type') }}</strong>
                        </span>
                    @endif
                  </div>
                  {{-- Banner Status --}}
                  <div class="form-group {{ $errors->has('opt_status') ? ' has-error' : '' }}">
                    <label>Status:</label>
                    <select name="opt_status" class="form-control select2" style="width: 100%;">
                      <option <?php if($BannerControl_var['status'] == "Active") { ?>selected="selected"<?php } ?>>Active</option>
                      <option <?php if($BannerControl_var['status'] == "Not-active") { ?>selected="selected"<?php } ?>>Not-active</option>
                    </select>
                    @if ($errors->has('opt_status'))
                        <span class="help-block">
                            <strong>{{ $errors->first('opt_status') }}</strong>
                        </span>
                    @endif
                  </div>

                  {{-- Banner Order --}}
                  <div class="form-group {{ $errors->has('txt_order') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Order:</label>
                    <input value="{{ $BannerControl_var['order'] }}" required type="text" class="form-control" id="inputSuccess" name="txt_order" placeholder="Color Order...">
                    @if ($errors->has('txt_order'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_order') }}</strong>
                        </span>
                    @endif
                  </div>

                  <h3 class="box-title">English Banner</h3>
                  {{-- Image title --}}
                  <div class="form-group {{ $errors->has('txt_imagetitle_en') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_imagetitle_en">Image title:</label>
                    <input type="text" class="form-control" id="inputSuccess" name="txt_imagetitle_en" placeholder="Image Title..."
                    value="{{ $BannerControl_var['image_title'] }}" required autofocus>
                    @if ($errors->has('txt_imagetitle_en'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_imagetitle_en') }}</strong>
                        </span>
                    @endif
                  </div>

                  {{-- Image Alt --}}
                  <div class="form-group {{ $errors->has('txt_imagealt_en') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_imagealt_en">Image title:</label>
                    <input type="text" class="form-control" id="inputSuccess" name="txt_imagealt_en" placeholder="Image Alt..."
                    value="{{ $BannerControl_var['image_alt'] }}" required autofocus>
                    @if ($errors->has('txt_imagealt_en'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_imagealt_en') }}</strong>
                        </span>
                    @endif
                  </div>

                  <h3 class="box-title">Arabic Banner</h3>
                  {{-- Image title --}}
                  <div class="form-group {{ $errors->has('txt_imagetitle_ar') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_imagetitle_ar">Image title:</label>
                    <input type="text" class="form-control" id="inputSuccess" name="txt_imagetitle_ar" placeholder="Image Title..."
                    value="{{ $BannerControl_var['image_titlear'] }}" required autofocus>
                    @if ($errors->has('txt_imagetitle_ar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_imagetitle_ar') }}</strong>
                        </span>
                    @endif
                  </div>

                  {{-- Image Alt --}}
                  <div class="form-group {{ $errors->has('txt_imagealt_ar') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_imagealt_ar">Image title:</label>
                    <input type="text" class="form-control" id="inputSuccess" name="txt_imagealt_ar" placeholder="Image Alt..."
                    value="{{ $BannerControl_var['image_altar'] }}" required autofocus>
                    @if ($errors->has('txt_imagealt_ar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_imagealt_ar') }}</strong>
                        </span>
                    @endif
                  </div>

              <!-- /.box-body -->
              <div class="box-footer with-border">
                <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            {!! Form::close() !!}
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
    <script src="{{asset('assets/cpanel/dist/js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/cpanel/dist/js/lib/file_upload/img.js')}}" type="text/javascript"></script>
  </body>
  </html>
@endsection
