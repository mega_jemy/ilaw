@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Banner Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Edit Banner</li>
  </ol>
</section>
<!-- Main content -->
<!-- /.content -->

    <section class="content">
      <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Fill required Fields</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- .box-body -->
              <div class="box-body">
                @if(Session::has('banneraction_result'))

                      <?php $saved_message= []; $saved_message=Session::pull('banneraction_result');?>
                      <?php if($saved_message[0] == 'success')
                      {
                        ?>
                        <div  class="alert alert-success">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      elseif ($saved_message[0] == 'failed') {
                        # code...
                        ?>
                        <div  class="alert alert-danger">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Error! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      ?>

                @endif
                <!-- input states -->
                <form action="{{ url('/ipanel/editnewbanner') }}" method="post" role="form" enctype="multipart/form-data" >
                  {{ csrf_field() }}
                  <input type="text" hidden="hidden" name="_access" value="<?php echo $_access; ?>" />
                  <input type="text" hidden="hidden" name="id" value="<?php echo encrypt($BannerControl_var['id']); ?>" />
                  {{-- Pages --}}
                  <div class="form-group {{ $errors->has('opt_pages') ? ' has-error' : '' }}">
                    <label>Pages:</label>
                    <select id="opt_pagesID" name="opt_pages" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Pages --</option>
                      <option value="4" <?php if($BannerControl_var['idpages'] == 4) { ?>selected="selected"<?php } ?> >Home</option>
                      <option value="5" <?php if($BannerControl_var['idpages'] == 5) { ?>selected="selected"<?php } ?>>Courses</option>
                    </select>
                    <span class="help-block " id="span_opt_langID">

                    </span>
                  </div>
                  {{-- Language --}}
                  <div class="form-group {{ $errors->has('opt_lang') ? ' has-error' : '' }}">
                    <label>Language:</label>
                    <select id="opt_langID" name="opt_lang" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Language --</option>
                      @if(count($activeLang) > 0)
                        @for ($i = 0; $i < count($activeLang) ; $i++)
                          <option value="{{ $activeLang[$i]->id }}" <?php if($BannerControl_var['idlang'] == $activeLang[$i]->id) { ?>selected="selected"<?php } ?>>{{ $activeLang[$i]->name }} - {{ $activeLang[$i]->regional }}</option>
                        @endfor
                      @endif
                    </select>
                    <span class="help-block " id="span_opt_langID">

                    </span>
                  </div>
                  {{-- Banner type --}}
                  <div class="form-group {{ $errors->has('opt_type') ? ' has-error' : '' }}">
                    <label>Status:</label>
                    <select id="opt_typeID" name="opt_type" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Banner Place --</option>
                      <option value="top" <?php if($BannerControl_var['typeban'] == "top") { ?>selected="selected"<?php } ?>>Top</option>
                    </select>
                    <span id="span_opt_typeID" class="help-block">

                    </span>
                  </div>
                  {{-- Banner Status --}}
                  <div class="form-group {{ $errors->has('opt_status') ? ' has-error' : '' }}">
                    <label>Status:</label>
                    <select id="opt_statusID" name="opt_status" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">-- Choose Status --</option>
                      <option value="Active" <?php if($BannerControl_var['status'] == "Active") { ?>selected="selected"<?php } ?>>Active</option>
                      <option value="Not-active" <?php if($BannerControl_var['status'] == "Not-active") { ?>selected="selected"<?php } ?>>Not-active</option>
                    </select>
                    <span id="span_opt_statusID" class="help-block">

                    </span>
                  </div>

                  {{-- Banner Order --}}
                  <div class="form-group {{ $errors->has('txt_order') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_order">Order:</label>
                    <input value="{{ $BannerControl_var['order'] }}" type="text" class="form-control" id="inp_order" name="txt_order" placeholder="Order...">
                    <span id="span_inp_order" class="help-block">
                    </span>
                  </div>

                  {{-- Image title --}}
                  <div class="form-group {{ $errors->has('txt_imagetitle_en') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_imagetitle_en">Image title:</label>
                    <input type="text" class="form-control" id="inp_title" name="txt_imagetitle_en" placeholder="Image Title..."
                    value="{{ $BannerControl_var['img_title']  }}" >
                    <span id="span_inp_title" class="help-block">
                    </span>
                  </div>

                  {{-- Image Alt --}}
                  <div class="form-group {{ $errors->has('txt_imagealt_en') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_imagealt_en">Image Alt:</label>
                    <input type="text" class="form-control" id="inp_alt" name="txt_imagealt_en" placeholder="Image Alt..."
                    value="{{ $BannerControl_var['img_alt'] }}" >
                    <span id="span_inp_alt" class="help-block">
                    </span>
                  </div>
                  <?php $imgpath_st = $BannerControl_var['imgpath_st'];
                  $img_path = "/images/banner/back/".$imgpath_st;
                   ?>
                   <img src="{{$img_path}}" />
                  {{-- Picture --}}
                  <div class="form-group{{ $errors->has('inp_image_st') ? ' has-error' : '' }}">
                    <label for="file">Slider Backgroound:</label>
                    <input value="{{ old('inp_image_st') }}"  type="file" id="img_user_fname" name="inp_image_st" accept=".jpg,.png,.gif" class="filestyle" data-input="true">
                    <p class="help-block">Only jpg,png file with maximum size of 1.5 MB is allowed.</p>
                    <span id="span_inp_image_st" class="help-block">

                    </span>
                  </div>
                  <?php $imgpath_nd = $BannerControl_var['imgpath_nd'];
                  $img_path = "/images/banner/single/".$imgpath_nd;
                   ?>
                   <img src="{{$img_path}}" />
                  {{-- Picture --}}
                  <div class="form-group{{ $errors->has('inp_image_nd') ? ' has-error' : '' }}">
                    <label for="file">Slider Image:</label>
                    <input value="{{ old('inp_image_nd') }}"  type="file" id="img_user_nd" name="inp_image_nd" accept=".jpg,.png,.gif" class="filestyle" data-input="true">
                    <p class="help-block">Only jpg,png file with maximum size of 1.5 MB is allowed.</p>
                    <span id="span_inp_image_nd" class="help-block">

                    </span>
                  </div>

                  <!--<button onclick="validateTblFields()" id="btn_add_input" type="button" class="btn btn-primary">Add thumbnail</button>
                  <table id="example" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>language</th>
                        <th>place</th>
                        <th>status</th>
                        <th>order</th>
                        <th>thumbnail title</th>
                        <th>thumbnail alt</th>
                        <th>thumbnail</th>
                        <th>action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table>-->
              <!-- /.box-body -->
              <div class="box-footer with-border">
                <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
    <script src="{{asset('js/lib/jquery-2.2.0.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/lib/file_upload/img.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/ipages/banner_page.js')}}" type="text/javascript"></script>
  </body>
  </html>
@endsection
