@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Main content -->
<section class="content-header">
  <h1>
    Capital Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Show Country Data</li>
  </ol>
</section>
    <section class="content">
      <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Memory Data</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <a href="{{ url('/ipanel/addnewcountry') }}"><h5 class="box-title">Add New Country</h5></a>
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('countryaction_result'))

                    <?php $saved_message= []; $saved_message=Session::pull('countryaction_result');?>
                    <?php if($saved_message[0] == 'success')
                    {
                      ?>
                      <div  class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    elseif ($saved_message[0] == 'failed') {
                      # code...
                      ?>
                      <div  class="alert alert-danger">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    ?>

              @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Country Name</th>
                  <th>Name Code</th>
                  <th>Phone Code</th>
                  <th>Status</th>
                  <th>Shippment</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if (count($CountryControlData) >= 1)
                    @for ($i = 0; $i < count($CountryControlData) ; $i++)
                      <tr>
                        <td>{{ ($i+1) }}</th>
                        <td hidden="hidden">{{ $CountryControlData[$i]->id }}</td>
                        <td>{{ $CountryControlData[$i]->name }}</td>
                        <td>{{ $CountryControlData[$i]->name_code }}</td>
                        <td>{{ $CountryControlData[$i]->c_code }}</td>
                        <td>{{ $CountryControlData[$i]->status }}</td>
                        <td>{{ $CountryControlData[$i]->shippmentcost }}</td>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-info">Action</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <li>
                                <a href="showcutsomcountry?id=<?php echo encrypt($CountryControlData[$i]->id) ;?>">
                                  <i class="fa fa-edit"></i>
                                  <span class="text-success" style="font-family: Tahome;font-weight: bold;">Edit</span>
                                </a>
                              </li>

                            </ul>
                          </div>
                        </th>
                      </tr>
                    @endfor

                  @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Country Name</th>
                  <th>Name Code</th>
                  <th>Phone Code</th>
                  <th>Status</th>
                  <th>Shippment</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
