@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
<!-- Main content -->
<section class="content-header">
  <h1>
    Capital Management
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Edit Country Data</li>
  </ol>
</section>
    <section class="content">
      <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Fill required Fields</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- .box-body -->
              <div class="box-body">
                @if(Session::has('countryaction_result'))

                      <?php $saved_message= []; $saved_message=Session::pull('countryaction_result');?>
                      <?php if($saved_message[0] == 'success')
                      {
                        ?>
                        <div  class="alert alert-success">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      elseif ($saved_message[0] == 'failed') {
                        # code...
                        ?>
                        <div  class="alert alert-danger">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Error! </strong><?php echo $saved_message[1]; ?>
                        </div>
                        <?php
                      }
                      ?>

                @endif
                <!-- input states -->
                {!! Form::open(array('url' => '/ipanel/editcountry','method' => 'post')) !!}
                  {{ csrf_field() }}
                  <input value="{{ encrypt($CountryControlvar['id']) }}" type="text" hidden="hidden" name="_access">
                  {{-- Country Name --}}
                  <div class="form-group {{ $errors->has('txt_cname') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_cname">Country Name:</label>
                    <input type="text" class="form-control" id="inputSuccess" name="txt_cname" placeholder="Country Name..."
                    value="{{ $CountryControlvar['name'] }}" required autofocus>
                    @if ($errors->has('txt_cname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_cname') }}</strong>
                        </span>
                    @endif
                  </div>

                  {{-- Country Status --}}
                  <div class="form-group {{ $errors->has('opt_status') ? ' has-error' : '' }}">
                    <label>Status:</label>
                    <select name="opt_status" class="form-control select2" style="width: 100%;">
                      <option value="0" selected="selected">Choose Currency Country</option>
                      <option value="1" <?php if($CountryControlvar['status'] == "sale" ) { ?>selected="selected"<?php } ?>>Sale</option>
                      <option value="2" <?php if($CountryControlvar['status'] == "not-sale" ) { ?>selected="selected"<?php } ?>>Not-Sale</option>
                    </select>
                    @if ($errors->has('opt_status'))
                        <span class="help-block">
                            <strong>{{ $errors->first('opt_status') }}</strong>
                        </span>
                    @endif
                  </div>

                  {{-- Country Code Name --}}
                  <div class="form-group {{ $errors->has('txt_cncodename') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_cncodename">Country Name Code:</label>
                    <input type="text" class="form-control" id="inputSuccess" name="txt_cncodename" placeholder="Country Name Code..."
                    value="{{ $CountryControlvar['name_code'] }}" required autofocus>
                    @if ($errors->has('txt_cncodename'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_cncodename') }}</strong>
                        </span>
                    @endif
                  </div>

                  {{-- Country Code Phone --}}
                  <div class="form-group {{ $errors->has('txt_cpcodename') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_cpcodename">Country Phone Code:</label>
                    <input type="text" class="form-control" id="inputSuccess" name="txt_cpcodename" placeholder="Country Phone Code..."
                    value="{{ $CountryControlvar['c_code'] }}" required autofocus>
                    @if ($errors->has('txt_cpcodename'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_cpcodename') }}</strong>
                        </span>
                    @endif
                  </div>

                  {{-- Country Shippment --}}
                  <div class="form-group {{ $errors->has('txt_cshippment') ? ' has-error' : '' }}">
                    <label class="control-label" name="lbl_cshippment">Country Shippment:</label>
                    <input type="text" class="form-control" id="inputSuccess" name="txt_cshippment" placeholder="Country Shippment..."
                    value="{{ $CountryControlvar['shippmentcost'] }}" required autofocus>
                    @if ($errors->has('txt_cshippment'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txt_cshippment') }}</strong>
                        </span>
                    @endif
                  </div>

              <!-- /.box-body -->
              <div class="box-footer with-border">
                <input id="btn_save_input" type="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            {!! Form::close() !!}
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
