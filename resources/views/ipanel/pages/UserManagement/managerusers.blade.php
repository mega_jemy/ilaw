@extends('ipanel.layout.master')
@section('content')
<html>
<head>
    <title>ASC-AE | Dashboard</title>
</head>
<body>
  <section class="content-header">
    <h1>
      User Management
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/ipanel/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Show User Data</li>
    </ol>
  </section>
<!-- Main content -->
    <section class="content">
      <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Users Data</h3>

              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('useraction_result'))

                    <?php $saved_message= []; $saved_message=Session::pull('useraction_result');?>
                    <?php if($saved_message[0] == 'success')
                    {
                      ?>
                      <div  class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    elseif ($saved_message[0] == 'failed') {
                      # code...
                      ?>
                      <div  class="alert alert-danger">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error! </strong><?php echo $saved_message[1]; ?>
                      </div>
                      <?php
                    }
                    ?>

              @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Delete</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if (count($UserData) >= 1)
                    @for ($i = 0; $i < count($UserData) ; $i++)
                      <tr>
                        <td>{{ ($i+1) }}</th>
                        <td hidden="hidden">{{ $UserData[$i]->id }}</td>
                        <td>{{ $UserData[$i]->fullname }}</td>
                        <td>{{ $UserData[$i]->email }}</td>
                        <td>{{ $UserData[$i]->active }}</td>
                        <td>{{ $UserData[$i]->softdelete }}</td>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-info">Action</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <?php
                              if($UserData[$i]->softdelete != "Delete")
                              {
                                ?>
                                <li>
                                  <a href="deleteuser?id=<?php echo encrypt($UserData[$i]->id) ;?>">
                                    <i class="fa fa-recycle"></i>
                                    <span class="text-danger" style="font-family: Tahome;font-weight: bold;">Delete</span>
                                  </a>
                                </li>
                              </ul>
                                <?php
                              }
                               ?>
                            </diV>
                        </th>
                      </tr>
                    @endfor
                  @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th hidden="hidden">ID</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Delete</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
