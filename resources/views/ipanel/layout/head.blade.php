<!--================================================== -->
<meta charset="utf-8">
<title>iLAW Official Website | AdminPanel</title>
<!-- Mobile Specific Metas
<!--================================================== -->
<!-- SEO Meta Tags-->
<meta name="description" content="iLAW Official Website">
<meta name="keywords" content="iLAW Official Website">
<meta name="author" content="iLAW">
<!-- Mobile Specific Meta Tag-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<!-- Favicon and Apple Icons-->
<link rel="icon" type="image/x-icon" href="{{asset('images/log.png')}}">
<link rel="icon" type="image/png" href="{{asset('images/log.png')}}">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/font-awesome/css/font-awesome.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/Ionicons/css/ionicons.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('assets/ipanel/dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('assets/ipanel/dist/css/skins/_all-skins.min.css')}}">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/select2/dist/css/select2.min.css')}}">
<!-- Morris chart -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/morris.js/morris.css')}}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/jvectormap/jquery-jvectormap.css')}}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{asset('assets/ipanel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<!-- fullCalendar -->
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/ipanel/bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
