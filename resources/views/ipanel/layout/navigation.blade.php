<!-- sidebar: style can be found in sidebar.less -->
<html>
<head>
</head>
<body>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('assets/ipanel/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php
      if(!empty($RuleMData))
      {
        $MenuData = $RuleMData['MenuData'];
        $MenuItemData = $RuleMData['MenuItemData'];
        $RolesData = $RuleMData['RolesData'];
        $EventsData = $RuleMData['EventsData'];
        $AuthUserRoleData = $RuleMData['AuthUserRoleData'];

        $collect_RolesData = [];
        if(!empty($RolesData))
        {
          $collect_RolesData = collect($RolesData)->where('isdefault','=','Yes')->values()->toArray();
        }
        else {
          $collect_RolesData = [];
        }

        #############################################################################
        if(!empty($MenuData))
        {
          for($i=0;$i<count($MenuData);$i++)
          {

            $MenuData_id  = $MenuData[$i]['id'];
            $MenuData_name = $MenuData[$i]['name'];
            ?>
            <li <?php if(decrypt($activemenu) == $MenuData_id) { ?> class="active treeview"  <?php } else{ ?> class=" treeview" <?php } ?>>
            <a href="#">
              <i class="fa fa-files-o"></i> <span>{{ $MenuData_name }}</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <?php
            $custom_menuitem = collect($MenuItemData)->where('menu_id','=',$MenuData_id)->values()->toArray();
            //var_dump($custom_menuitem);

            if(!empty($custom_menuitem) &&
            !empty($collect_RolesData) && !empty($EventsData) && !empty($AuthUserRoleData))
            {

              ?>
              <ul class="treeview-menu">
                <?php
                  for($j=0;$j<count($custom_menuitem);$j++)
                  {
                    $MenuItemData_id = $custom_menuitem[$j]['id'];
                    $MenuItemData_name = $custom_menuitem[$j]['name'];
                    $res_RolesData = collect($collect_RolesData)->where('menuitem_id','=',$MenuItemData_id)->values()->toArray();
                    if(!empty($res_RolesData))
                    {
                      $linkhead = $res_RolesData[0]['link'];
                      $checkactive = encrypt($MenuItemData_id);

                      if(!empty($AuthUserRoleData))
                      {
                        $rolesid = $AuthUserRoleData[0]['roles_id'];
                        $activeid = $AuthUserRoleData[0]['active'];
                        $split_rolesid = explode(',',$rolesid);
                        $split_activeid = explode(',',$activeid);

                        $index_key = array_search($MenuItemData_id, $split_rolesid);

                        if ($index_key != "" || $index_key == 0) {
                          $lampCheck = $split_activeid[$index_key];
                          $checkedID =$lampCheck;
                          if($checkedID == 1)
                          {
                            ?>
                              <li  @if(decrypt($activepage) == decrypt($checkactive))  class="active"  @endif>
                                <a href="{{ url($linkhead) }}">
                                  <i class="fa fa-circle-o text-aqua"></i>
                                  {{ $MenuItemData_name }}
                                </a>
                              </li>
                            <?php
                          }

                        }
                        else {
                        }
                      }
                      else {
                      }


                    }
                  }
                  ?>
              </ul>
              <?php
            }
            ?>
            </li>
            <?php
          }
        }
      }
        ?>
    <!---------------------------------------------------->

    <!---------------------------------------------------->
        <!--<li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Admin Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i>General Configuration</a></li>
            <li><a href="{{url('/admin/showadminuser')}}"><i class="fa fa-circle-o"></i>Manage Admin User</a></li>
            <li><a href="{{url('/admin/change-password')}}"><i class="fa fa-circle-o"></i>Change Password</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Capital Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Manage Country</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>User Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/admin/showuser')}}"><i class="fa fa-circle-o"></i> Manage Users</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Newsletter Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/admin/shownewsletter')}}"><i class="fa fa-circle-o"></i> Manage Newsletter</a></li>
            <li><a href="{{url('/admin/showcontactus')}}"><i class="fa fa-circle-o"></i> Manage Contact Us</a></li>
            <li><a href="{{url('/admin/showenquiriesmessage')}}"><i class="fa fa-circle-o"></i> Manage Enquiries</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Manage Pages</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Home Page
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('/admin/showetopbanner') }}"><i class="fa fa-circle-o"></i>Manage Banners</a></li>
                <!--<li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Firm Page</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Club Page</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Blog Page</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> media Page</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Kids Page</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Conference Page</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Training Page</a></li>

          </ul>
        </li>-->
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<!-- /.sidebar -->
</body>
</html>
