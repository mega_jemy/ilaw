<?php

return [

    /*
    |--------------------------------------------------------------------------
    | index Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */

    'service_title' => '<h2>Our <strong>Services</strong></h2>',
    'office_title' => 'Ibrahim Al Hosani Advocate, Legal Services & Training” offers a wide range of professional and high quality services in all chief fields of law, including',

    'fields_title' => '<h2>Our <strong>FIELDS</strong></h2>',
    'office_fields_title' => 'The law firm offers a wide range of professional specialized high quality services in all significant fields of law, including',


    'fields_one_title' => 'Criminal Law',
    'fields_one_desc' => 'As the United Arab Emirates are characterized with safe, security, and development, as well laws amendment constantly to be up to date with the era, as it is proactive in issuing and legislating the laws that keep up with this development, including the law of discrimination and hatred, Information Technology law and Wadima law along with the penal law, and we offer legal services in criminal matters and all its branches (penalties - discrimination and hatred – Information Technology - Traffic) through advocacy and pleading before the criminal courts and departments through attending its hearings with the defendants or personal rights plaintiffs (civil) alike, as well as submitting the necessary services starting from police and prosecutors stations until the completion of the consideration of the cases and reaching the best possible results. The firm is advocated in various kinds of cases and in its all stages and levels and provides financial provisional release on bail or personal requests. Our experience also extends to the civil claim for compensating the client against the damages that has been sustained by him as a result of the crime; obtain a judgment for compensation whether before the criminal or civil courts.',

    'fields_two_title' => 'Commerical LAW',
    'fields_two_desc' => 'Due to the vitality of commercial activity in the United Arab Emirates and its strategic role in the prosperity of the overall economy of the state, the firm is submitting legal services in various fields including: insurance - Commercial mortgages - sales contracts and the merges - Banking works- Business financing contracts - Equipment rental - commercial contracts - sales and purchase agreements – ownership of commercial assets - incorporation and licensing businesses and joint ventures- bankruptcy and insolvency - the international trade, preparation and the drafting of international trade contracts - commercial agencies and commercial sales - land, sea and air transportation - illegal competition in commercial transactions.',

    'fields_three_title' => 'Commerical Companies LAW',
    'fields_three_desc' => 'The United Arab Emirates is attracting a huge number of foreign and regional investments each year, and providing investment facilities for several procedures in establishing all kinds of commercial companies. Recently, the state enacted the modern Commercial Companies Law to cope with the economic development. Accordingly, our firm is providing legal services concerning corporate cases and the preparation of the statement of claims that are related thereto for defending and pleading before the courts to achieve the best possible findings through the perfect knowledge of commercial companies law and understanding of what is new in this field.',

    'fields_four_title' => 'Civil LAW',
    'fields_four_desc' => 'Whereas the civil law has paramount importance in knowing the rights of persons and organize all civil transactions between them, accordingly , our firm is interested in providing the best legal services concerning all different kinds of civil cases (such as compensation claims - guardianship - Contracting – mortgage - sale - donation – contracts…).',

    'fields_five_title' => 'Labor LAW',
    'fields_five_desc' => 'The United Arab Emirates is considered to be one of the most labor attractions. It is also one of the most active countries in enacting the laws sponsoring the rights of both employees and employers. Accordingly, our firm is providing the best legal services in relation to labor law through advocacy and the defense for both employers and employees to achieve the best possible findings and give everyone his right.',

    'fields_six_title' => 'Real Estate LAW',
    'fields_six_desc' => 'The United Arab Emirates is encouraging real estate investment, and the state is classified among the best global attractive destinations for investors in the real estate field, which would make it a forerunner in the legislation of real estate laws that guarantees the rights of the parties to the real estate transactions. Therefore, our firm is offering the best legal services in the real estate field, which includes the representation of real estate companies or individuals in real estate claims and the preparation of statement of claims concerning real estate also advocacy, defending, pleading and preparing all the memoranda that are related to this case to achieve the best possible and satisfactory results for our clients.',

    'fields_seven_title' => 'Tenancy LAW',
    'fields_seven_desc' => 'The rental transaction is the link between lessor and lessee, and governed by the Tenancy Laws of each Emirate, and our firm has a full knowledge of each Emirates Tenancy Law within the UAE and we provide the best legal services in this significant field in many ways that includes the representation of the lessor or lessee before committees of resolving rental disputes and prepare a statement of claims for rental disputes and defend the rental parties in order to achieve the best possible and satisfactory results for our clients.',

    'fields_eight_title' => 'Maritime LAW',
    'fields_eight_desc' => 'The United Arab Emirates is considered to be one of the most important countries embracing the largest seaports in the world. and our firm is offering the best legal services in the field of maritime law and maritime trade that are related to all maritime claims (such as maritime lien – disputes of marine insurance – claims for compensation- the responsibility of the shipping carrier – maritime mortgage...).',

    'fields_nine_title' => 'Personal Status LAW',
    'fields_nine_desc' => 'The family is the core of the society and the basis of its renaissance, and since the United Arab Emirates is concerned with the family and organizing the family relations through the enactment and legislation of Personal Status Law, Our firm is offering the best legal services in the field of personal status law through all kinds of personal status cases which may include , for example, (alimonies - custodianship - guardianship - inheritance - the estates and testaments- the visitation right - kinship ...). We are doing our best firstly for settling the conflict between the parties to maintain the family bonding and preventing the dispersion of children and reaching an amicable agreement preserves the rights of all parties while taking all necessary legal action to grant our client the legitimate rights guaranteed by the law.',



];
