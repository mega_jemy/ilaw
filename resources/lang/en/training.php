<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Training Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */

    'about_training_title' => '<h1 class="mb-2 word-rotator-title">
      About
      <strong class="inverted">
        <span class="word-rotator" data-plugin-options="{"delay": 2000, "animDelay": 300}">
          <span class="word-rotator-items">
            <span>iLAW</span>
          </span>
        </span>
      </strong>
      Training.
    </h1>',
    'about_training_message' => '<p style="text-align:left;" class="lead">In our organization, we believe that training is a culture and not just a knowledge or professional activity. We seek to develop the content of the training culture and to ensure the quality of its outputs through hard work within the values ​of adherence to the rules and perseverance in the work and therefore our training plans are flexible plans that are designed according to the stage and requirements. Where the real need to provide service, and proactive management of the expected outcomes of the provision of training service, to achieve the satisfaction of the customer service applicant and its recipients and supervisors in the case of private and government training. We strive to be the best
    We do not consider ourselves in the competition in its commercial form. Through our real practical and academic experience, relying on the cumulative amount of outputs of the activities and programs that have been completed, we consider the competitors as an opportunity to invest and benefit from them.
    Through the design of an innovative system to provide legal training services through a training system designed by the development team in iLAW, based on three pillars starting with the future needs in training and services, and then analyze the personal and institutional competencies and the completion of the design of training programs according to measurable productivity standards.
    Hence, providing training and consulting services according to the highest quality indicators.
    Our service continues after the provision of training services through follow-up, update and ensure improvement and development.</p>',

    'trainee_course_title' => '<h1 class="mb-0"><strong>Current Course</strong></h1>',
    'trainee_title' => '<h2>The <strong>Lecturers</strong></h2>',

    'trainee_one_name' => 'Dr.Mohamed Mahmoud Al-Kemaliy',
    'trainee_one_title' => 'Director General of the Judicial Institute of Sharjah',
    'trainee_one_desc' => 'Lecturer at the conference on developing professional and academic performance in legal work',

    'trainee_two_name' => 'Dr.Gamal Al-Semety',
    'trainee_two_title' => 'Director General of Dubai Judicial Institute',
    'trainee_two_desc' => 'Lecturer at the conference on developing professional and academic performance in legal work',

    'trainee_three_name' => 'Dr.Mansour Ben Nassar',
    'trainee_three_title' => 'Director of the Legal Department of the Office of the Ruler',
    'trainee_three_desc' => 'Lecturer at the conference on developing professional and academic performance in legal work',

    'trainee_four_name' => 'Dr. Ibrahim Al-Malaa',
    'trainee_four_title' => 'Attorney',
    'trainee_four_desc' => 'Lecturer at the conference on developing professional and academic performance in legal work',


];
