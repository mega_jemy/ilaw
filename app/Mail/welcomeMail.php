<?php

namespace ilaw\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use ilaw\User;
class welcomeMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->view('mail.welcome')->with([
        'fullname' => $this->user->fullname,
        'confirmation_code'=>$this->user->confirmation_code,
        'id' =>$this->user->id,
      ]);
    }
}
