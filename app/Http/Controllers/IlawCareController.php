<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\IlawCare;
class IlawCareController extends Controller
{
    //
    public function store(Request $request)
    {
      $inp_idcountry = $request->input('opt_countryname');
      $inp_type = $request->input('inp_rdtype');
      $inp_fullname = $request->input('inp_fullname');
      $inp_email = $request->input('inp_email');
      $inp_phone = $request->input('inp_phone');
      $inp_job = $request->input('inp_job');
      $inp_message = $request->input('inp_message');
      $inp_branch =  $request->input('inp_rdbranch');
      if(Auth::check())
      {
        $inp_ismember = "Yes";
      }
      else {
        $inp_ismember = "No";
      }
      $inp_softdelete = "Not-delete";

      $IlawCare_var = new IlawCare();
      $IlawCare_var->idcountry = $inp_idcountry;
      $IlawCare_var->ismember = $inp_ismember;
      $IlawCare_var->lang = $inp_fullname;
      $IlawCare_var->type = $inp_type;
      $IlawCare_var->fullname = $inp_fullname;
      $IlawCare_var->email = $inp_email;
      $IlawCare_var->phone = $inp_phone;
      $IlawCare_var->job = $inp_job;
      $IlawCare_var->message = $inp_message;
      $IlawCare_var->branch = $inp_softdelete;
      $IlawCare_var->softdelete = $inp_softdelete;
      if($IlawCare_var->save())
      {
        return redirect()->back();
      }
      else {
        return redirect()->back();
      }
    }
}
