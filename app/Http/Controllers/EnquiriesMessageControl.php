<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\EnquiriesMessageM;

class EnquiriesMessageControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($symbol)
    {
      $EnquiriesMessageMData = EnquiriesMessageM::where('idtype' , $symbol, 0)->orderby('created_at','DESC')->get();
      return $EnquiriesMessageMData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $inp_idtype = $request->input('inp_idtype');
      $inp_name = $request->input('pre_name_enq');
      $inp_email = $request->input('pre_email_enq');
      $inp_subject = $request->input('pre_subject_enq');
      $inp_message = $request->input('pre_details_enq');
      if(Auth::check())
      {
        $inp_ismember = "Yes";
      }
      else {
        $inp_ismember = "No";
      }
      $inp_softdelete = "Not-delete";

      $EnquiriesMessageM_var = new EnquiriesMessageM();
      $EnquiriesMessageM_var->idtype = $inp_idtype;
      $EnquiriesMessageM_var->fullname = $inp_name;
      $EnquiriesMessageM_var->email = $inp_email;
      $EnquiriesMessageM_var->subject = $inp_subject;
      $EnquiriesMessageM_var->message = $inp_message;
      $EnquiriesMessageM_var->ismember = $inp_ismember;
      $EnquiriesMessageM_var->softdelete = $inp_softdelete;
      if($EnquiriesMessageM_var->save())
      {
        return redirect()->back();
      }
      else {
        return redirect()->back();
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $id = decrypt($request->input('id'));
        $type = decrypt($request->input('type'));
        if($EnquiriesMessageM_var=EnquiriesMessageM::find($id))
        {
          if($EnquiriesMessageM_var->destroy($id))
          {
            Session::push('newsletteraction_result','success');
            Session::push('newsletteraction_result','Successfully Delete Data');
          }
          else {
            Session::push('newsletteraction_result','failed');
            Session::push('newsletteraction_result','Failed Delete Data');

          }
        }
        else {
          Session::push('newsletteraction_result','failed');
          Session::push('newsletteraction_result','Failed Delete Data');
        }
        if($type == 0)
        {
          return redirect('ipanel/showcontactus');
        }
        else {
          return redirect('ipanel/showenquiriesmessage');
        }

    }
}
