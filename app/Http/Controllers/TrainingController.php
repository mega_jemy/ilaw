<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\TrainingM;
use DB;
use ilaw\Http\Controllers\AdminView;
class TrainingController extends Controller
{
    //
    public function index()
    {

      $TrainingMdata = TrainingM::where('status','=','Active')->where('softdelete','=','Not-delete')->get();
      return $TrainingMdata;
    }
    public function indexByregional($regional)
    {

      $TrainingMdata = TrainingM::where('status','=','Active')->where('softdelete','=','Not-delete')->
      where('idlang','=',$regional)->where('showhome','=','Yes')->get();
      return $TrainingMdata;
    }
    public function store_Chk_Valid(Request $request)
    {

      //validate tha data from the request
      $validation = Validator::make($request->all(),TrainingM::$rules,[],TrainingM::$niceNames);
      //return the error messages if any
      if ($validation->fails())
       {
         //return json_encode(['response'=>$request->toArray()]);
         return json_encode(['response'=>$validation->errors()]);
       }
       else {
         return json_encode(['response'=>"valid"]);
       }
    }
    public function CreateStoreTraining(Request $request)
    {
      /*$user_Data = $request->toArray();

      var_dump(collect($user_Data)->values());
      return ;*/
      DB::beginTransaction();
      try {

          $res = self::storeTrainig($request);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('categoryaction_result','success');
          Session::push('categoryaction_result','Successfully Saved Data');
          return redirect('ipanel/addnewtrainingcourse');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('categoryaction_result','failed');
        Session::push('categoryaction_result','Failed Saved Data');
        return redirect('ipanel/addnewtrainingcourse');
      }
    }
    public function UpdateTraining(Request $request)
    {
      $user_Data = $request->toArray();

      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = $request->input('id');
      DB::beginTransaction();
      try {

          $res = self::update($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Updated Data');
          return redirect('ipanel/show_training?id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Update Data');
        return redirect('ipanel/show_training?id='.encrypt($id));
      }
    }

    public function RemoveTraining(Request $request)
    {
      $user_Data = $request->toArray();

      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = decrypt($request->input('id'));
      DB::beginTransaction();
      try {

          $res = self::destroy($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Remove Data');
          return redirect('ipanel/manage_training');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Remove Data');
        return redirect('ipanel/manage_training');
      }
    }

    public function storeTrainig(Request $request)
    {
      $user_Data = $request->toArray();
      //variable gets from user
      $inp_idlang = $user_Data['opt_lang'];
      $inp_name = $user_Data['inp_name'];
      $inp_title = $user_Data['inp_title'];
      $inp_desc = $user_Data['inp_desc'];
      $inp_link = $user_Data['inp_link'];
      $inp_status = $user_Data['opt_status'];
      $inp_order = $user_Data['inp_order'];
      $inp_softdelete = "Not-delete";
      $inp_oldprice = $user_Data['inp_oldprice'];
      $inp_newprice = $user_Data['inp_newprice'];
      $inp_hotdeals = $user_Data['opt_hotdeals'];
      $inp_monthoffer = $user_Data['opt_dealofmonth'];
      $inp_shownew = $user_Data['opt_shownew'];
      $inp_showoutofstock = $user_Data['opt_showoos'];
      $inp_showhome = $user_Data['opt_showonhome'];
      //create new category from table Category
      $TrainingM_var = new TrainingM();
      $TrainingM_var->idlang = $inp_idlang;
      $TrainingM_var->name = $inp_name;
      $TrainingM_var->title = $inp_title;
      $TrainingM_var->desc = $inp_desc;
      $TrainingM_var->link = $inp_link;
      $TrainingM_var->status = $inp_status;
      $TrainingM_var->order = $inp_order;
      $TrainingM_var->softdelete = $inp_softdelete;
      $TrainingM_var->oldprice = $inp_oldprice;
      $TrainingM_var->newprice = $inp_newprice;
      $TrainingM_var->hotdeals = $inp_hotdeals;
      $TrainingM_var->monthoffer = $inp_monthoffer;
      $TrainingM_var->shownew = $inp_shownew;
      $TrainingM_var->showoutofstock = $inp_showoutofstock;
      $TrainingM_var->showhome = $inp_showhome;

      if($TrainingM_var->save())
      {
        if(Input::file())
        {
          $image_en = $request->file('inp_avatar');
          $inp_filename_en  = $TrainingM_var->id . '_en.' . strtolower($image_en->getClientOriginalExtension());

          $path_en = 'images/training/'. $inp_filename_en;
          Image::make($image_en->getRealPath())->resize(698,326)->save($path_en);

          return $TrainingM_var;

        }
        return $TrainingM_var;
      }
      else {
        return $TrainingM_var;
      }




    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($TrainingMdata=TrainingM::find($id))
      {
        return $TrainingMdata;
      }
      else {
        $TrainingMdata = "";
        return $TrainingMdata;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $AdminView_var = new AdminView();
      $RuleMData = $AdminView_var->getRules();
      $TrainingMdata_var = self::show($request);

      $AdminView_var = new AdminView();

      $activeLang = $AdminView_var->getLanguageIsActive();
      if(count($TrainingMdata_var->toArray()) > 0)
      {
        //var_dump($TrainingMdata_var->toArray());

        return view('ipanel.pages.managepages.training.edittraining',[
          'TrainingMdata_var'=>$TrainingMdata_var,
          'activeLang'=>$activeLang,
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(2),
          'activemenu'=>encrypt(2),
        ]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $TrainingM_var = [];
        if($TrainingM_var=TrainingM::find($id))
        {
          $user_Data = $request->toArray();
          //variable gets from user
          $inp_idlang = $user_Data['opt_lang'];
          $inp_name = $user_Data['inp_name'];
          $inp_title = $user_Data['inp_title'];
          $inp_desc = $user_Data['inp_desc'];
          $inp_link = $user_Data['inp_link'];
          $inp_status = $user_Data['opt_status'];
          $inp_order = $user_Data['inp_order'];
          $inp_oldprice = $user_Data['inp_oldprice'];
          $inp_newprice = $user_Data['inp_newprice'];
          $inp_hotdeals = $user_Data['opt_hotdeals'];
          $inp_monthoffer = $user_Data['opt_dealofmonth'];
          $inp_shownew = $user_Data['opt_shownew'];
          $inp_showoutofstock = $user_Data['opt_showoos'];
          $inp_showhome = $user_Data['opt_showonhome'];
          //create new category from table Category

          $TrainingM_var->idlang = $inp_idlang;
          $TrainingM_var->name = $inp_name;
          $TrainingM_var->title = $inp_title;
          $TrainingM_var->desc = $inp_desc;
          $TrainingM_var->link = $inp_link;
          $TrainingM_var->status = $inp_status;
          $TrainingM_var->order = $inp_order;
          $TrainingM_var->oldprice = $inp_oldprice;
          $TrainingM_var->newprice = $inp_newprice;
          $TrainingM_var->hotdeals = $inp_hotdeals;
          $TrainingM_var->monthoffer = $inp_monthoffer;
          $TrainingM_var->shownew = $inp_shownew;
          $TrainingM_var->showoutofstock = $inp_showoutofstock;
          $TrainingM_var->showhome = $inp_showhome;
          if($TrainingM_var->save())
          {
            if(Input::file())
            {
              $image_en = $request->file('inp_avatar');
              $inp_filename_en  = $TrainingM_var->id . '_en.' . strtolower($image_en->getClientOriginalExtension());

              $path_en = 'images/training/'. $inp_filename_en;
              Image::make($image_en->getRealPath())->resize(698,326)->save($path_en);

              return $TrainingM_var;

            }
            Session::push('action_result','success');
            Session::push('action_result','Successfully Update Data');

          }
          else {
            Session::push('action_result','failed');
            Session::push('action_result','Failed Update Data');

          }
          return $TrainingM_var;
          //validate tha data from the request
          /*$validation = Validator::make($request->all(),CategoryM::$category_rules);
          //return the error messages if any
          if ($validation->fails())
          {
            return false;
          }
          else {

          }*/
        }
        else {
          Session::push('action_result','failed');
          Session::push('action_result','Failed Update Data');
        return $TrainingM_var;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      $TrainingM_var = [];
      if($TrainingM_var=TrainingM::find($id))
      {
        $TrainingM_var->softdelete = 'Delete';
        if($TrainingM_var->save())
        {
          Session::push('action_result','success');
          Session::push('action_result','Successfully Delete Data');

        }
        else {
          Session::push('action_result','failed');
          Session::push('action_result','Failed Delete Data');
        }
        return $TrainingM_var;
      }
      else {
        Session::push('action_result','failed');
        Session::push('action_result','Failed Delete Data');
        return $TrainingM_var;
      }
    }
}
