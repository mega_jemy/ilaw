<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\BlogM;
use ilaw\Http\Controllers\AdminView;
use DB;
use LaravelLocalization;

class BlogControl extends Controller
{
    //
    public function getIdLang($lang_regional)
    {
      $AdminView = new AdminView();
      $getLangData = $AdminView->getLanguageIsActive();
      $coll_getLangData = collect($getLangData)->where('regional','=',$lang_regional)->values()->toArray();
      $id_lang = $coll_getLangData[0]['id'];
      return $id_lang;
    }
    public function index()
    {
      $BlogMdata = BlogM::where('softdelete' ,'=',"Not-delete")->get();
      return $BlogMdata;
    }
    public function indexPaginate()
    {
      $lang_regional = LaravelLocalization::getCurrentLocale();
      $idlang = self::getIdLang($lang_regional);
      $BlogMdata = BlogM::where('softdelete' ,'=',"Not-delete")->where('status','=','active')->
      where('idlang','=',$idlang)->paginate(2);
      return $BlogMdata;
    }
    public function CreateStoreBlog(Request $request)
    {

      /*$user_Data = $request->toArray();

      var_dump(collect($user_Data)->values());
      return ;*/
      DB::beginTransaction();
      try {

          $res = self::store($request);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Saved Data');
          return redirect('ipanel/add_new_blog');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Saved Data');
        return redirect('ipanel/add_new_blog');
      }
    }

    public function UpdateBlog(Request $request)
    {
      $user_Data = $request->toArray();
      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = $request->input('id');

      DB::beginTransaction();
      try {

          $res = self::update($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Updated Data');
          return redirect('ipanel/show_blog?id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Update Data');
        return redirect('ipanel/show_blog?id='.encrypt($id));
      }
    }

    public function RemoveBlog(Request $request)
    {
      $user_Data = $request->toArray();
      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = decrypt($request->input('id'));
      DB::beginTransaction();
      try {

          $res = self::destroy($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Remove Data');
          return redirect('ipanel/manage_blog');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Remove Data');
        return redirect('ipanel/manage_blog');
      }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user_Data = $request->toArray();
      //variable gets from user
      $inp_idlang = $user_Data['opt_lang'];
      $inp_name = $user_Data['inp_name'];
      $inp_title = $user_Data['inp_title'];
      $inp_writer_name = $user_Data['inp_writer_name'];
      $inp_writer_title = $user_Data['inp_writer_title'];
      $inp_writer_date = $user_Data['inp_writer_date'];
      $inp_desc = $user_Data['inp_desc'];
      $inp_status = $user_Data['opt_status'];

      $inp_softdelete = "Not-delete";
      //create new category from table Category
      $BlogM_var = new BlogM();
      $BlogM_var->idlang = $inp_idlang;
      $BlogM_var->name = $inp_name;
      $BlogM_var->title = $inp_title;
      $BlogM_var->writer_name = $inp_writer_name;
      $BlogM_var->writer_title = $inp_writer_title;
      $BlogM_var->write_date = $inp_writer_date;
      $BlogM_var->desc = $inp_desc;
      $BlogM_var->softdelete = $inp_softdelete;
      $BlogM_var->status = $inp_status;
      if($BlogM_var->save())
      {
        if(Input::file())
        {
          $image = $request->file('inp_thumbnail');
          $inp_filename  = $BlogM_var->id . '.' . strtolower($image->getClientOriginalExtension());
          $BlogM_var->img_path = $inp_filename;
          if($BlogM_var->save())
          {
            $path = 'images/blog/'. $inp_filename;
            Image::make($image->getRealPath())->save($path);

          }
        }
        return $BlogM_var;
      }
      else {
        return $BlogM_var;
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($BlogM_var=BlogM::find($id))
      {
        return $BlogM_var;
      }
      else {
        $BlogM_var = "";
        return $BlogM_var;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $AdminView_var = new AdminView();
      $RuleMData = $AdminView_var->getRules();
      $activeLang = $AdminView_var->getLanguageIsActive();
      $BlogMData_var = self::show($request)->toArray();
      if(count($BlogMData_var) > 0)
      {
        /*var_dump($BlogMData_var);
        return;*/
        return view('ipanel.pages.managepages.blog.edit_blog',[
          'BlogMData_var'=>$BlogMData_var,
          'activeLang'=>$activeLang,
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(2),
          'activemenu'=>encrypt(2),
        ]);
      }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
      $BlogM_var = [];
      if($BlogM_var=BlogM::find($id))
      {
        $user_Data = $request->toArray();
        //variable gets from user
        $inp_idlang = $user_Data['opt_lang'];
        $inp_name = $user_Data['inp_name'];
        $inp_title = $user_Data['inp_title'];
        $inp_writer_name = $user_Data['inp_writer_name'];
        $inp_writer_title = $user_Data['inp_writer_title'];
        $inp_writer_date = $user_Data['inp_writer_date'];
        $inp_desc = $user_Data['inp_desc'];
        $inp_status = $user_Data['opt_status'];

        $inp_softdelete = "Not-delete";
        //create new category from table Category
        $BlogM_var->idlang = $inp_idlang;
        $BlogM_var->name = $inp_name;
        $BlogM_var->title = $inp_title;
        $BlogM_var->writer_name = $inp_writer_name;
        $BlogM_var->writer_title = $inp_writer_title;
        $BlogM_var->write_date = $inp_writer_date;
        $BlogM_var->desc = $inp_desc;
        $BlogM_var->softdelete = $inp_softdelete;
        $BlogM_var->status = $inp_status;

        if($BlogM_var->save())
        {
          if(Input::file())
          {


            $image = $request->file('inp_thumbnail');
            $inp_filename  = $BlogM_var->id . '.' . strtolower($image->getClientOriginalExtension());
            $BlogM_var->img_path = $inp_filename;
            if($BlogM_var->save())
            {
              $path = 'images/blog/'. $inp_filename;
              Image::make($image->getRealPath())->save($path);

            }
          }
          return $BlogM_var;
        }
        else {
          return $BlogM_var;
        }
      }
      else {
        return $BlogM_var;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      $BlogM_var = [];
      if($BlogM_var=BlogM::find($id))
      {
        $BlogM_var->softdelete = 'Delete';
        if($BlogM_var->save())
        {
          return $BlogM_var;
        }
        else {
          return $BlogM_var;
        }

      }
      else {
        return $BlogM_var;
      }
    }
}
