<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\TrainingSuggestM;
use DB;
use ilaw\Http\Controllers\AdminView;
class TrainingSuggestControl extends Controller
{
    //
    public function store(Request $request)
    {
      $inp_qualifiy_name = $request->input('inp_qualifiy_name');
      $inp_suggest_name = $request->input('inp_suggest_name');
      $inp_s_price = $request->input('inp_s_price');
      $inp_n_days = $request->input('inp_n_days');
      $inp_desc = $request->input('inp_desc');
      $inp_other_suggest = $request->input('inp_other_suggest');
      $inp_softdelete = "No";

      $TrainingSuggestM_var = new TrainingSuggestM();
      $TrainingSuggestM_var->iduser = 1;
      $TrainingSuggestM_var->qualifiy_name = $inp_qualifiy_name;
      $TrainingSuggestM_var->suggest_name = $inp_suggest_name;
      $TrainingSuggestM_var->s_price = $inp_s_price;
      $TrainingSuggestM_var->n_days = $inp_n_days;
      $TrainingSuggestM_var->desc = $inp_desc;
      $TrainingSuggestM_var->other_suggest = $inp_other_suggest;
      $TrainingSuggestM_var->isdelete = $inp_softdelete;
      if($TrainingSuggestM_var->save())
      {
        return redirect()->back();
      }
      else {
        return redirect()->back();
      }
    }
}
