<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\ServiceM;
use DB;
use ilaw\Http\Controllers\AdminView;
class ServiceController extends Controller
{
    //
    public function index()
    {

      $ServiceMdata = ServiceM::with('LanguageM')->where('status','=','Active')->where('softdelete','=','Not-delete')->get();
      return $ServiceMdata;
    }
    public function CreateStoreService(Request $request)
    {
      /*$user_Data = $request->toArray();
      var_dump($user_Data);
      return ;*/
      DB::beginTransaction();
      try {

          $res = self::storeServiceM($request);

          DB::commit();
          Session::push('serviceaction_result','success');
          Session::push('serviceaction_result','Successfully Saved Data');
          return redirect('ipanel/addnewservice');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('serviceaction_result','failed');
        Session::push('serviceaction_result','Failed Saved Data');
        return redirect('ipanel/addnewservice');
      }
    }
    public function UpdateService(Request $request)
    {
      $user_Data = $request->toArray();

      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = $request->input('id');
      DB::beginTransaction();
      try {

          $res = self::update($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Updated Data');
          return redirect('ipanel/show_service?id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Update Data');
        return redirect('ipanel/show_service?id='.encrypt($id));
      }
    }

    public function RemoveService(Request $request)
    {
      $user_Data = $request->toArray();

      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = decrypt($request->input('id'));
      DB::beginTransaction();
      try {

          $res = self::destroy($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Remove Data');
          return redirect('ipanel/manage_services');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Remove Data');
        return redirect('ipanel/manage_services');
      }
    }

    public function storeServiceM(Request $request)
    {
      $user_Data = $request->toArray();
      //variable gets from user
      $inp_idlang = $user_Data['opt_lang'];
      $inp_name = $user_Data['inp_name'];
      $inp_title = $user_Data['inp_title'];
      $inp_desc = $user_Data['inp_desc'];
      $inp_link = $user_Data['inp_link'];
      $inp_status = $user_Data['opt_status'];
      $inp_order = $user_Data['inp_order'];
      $inp_softdelete = "Not-delete";
      //create new category from table Category
      $ServiceM_var = new ServiceM();
      $ServiceM_var->idlang = $inp_idlang;
      $ServiceM_var->name = $inp_name;
      $ServiceM_var->title = $inp_title;
      $ServiceM_var->desc = $inp_desc;
      $ServiceM_var->link = $inp_link;
      $ServiceM_var->status = $inp_status;
      $ServiceM_var->order = $inp_order;
      $ServiceM_var->softdelete = $inp_softdelete;

      if($ServiceM_var->save())
      {
        if(Input::file())
        {
          $inp_avatar_st = $request->file('inp_avatar_st');
          $inp_avatar_nd = $request->file('inp_avatar_nd');

          $inp_filename_st  = $ServiceM_var->id . '_st.' . strtolower($inp_avatar_st->getClientOriginalExtension());
          $inp_filename_nd  = $ServiceM_var->id . '_nd.' . strtolower($inp_avatar_nd->getClientOriginalExtension());

          $path_st = 'images/service/'. $inp_filename_st;
          $path_nd = 'images/service/'. $inp_filename_nd;
          $ServiceM_var->img_path_st = $inp_filename_st;
          $ServiceM_var->img_path_nd = $inp_filename_nd;
          $ServiceM_var->save();
          Image::make($inp_avatar_st->getRealPath())->save($path_st);
          Image::make($inp_avatar_nd->getRealPath())->save($path_nd);

          return $ServiceM_var;

        }
        return $ServiceM_var;
      }
      else {
        return $ServiceM_var;
      }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($ServiceM_var=ServiceM::find($id))
      {
        return $ServiceM_var;
      }
      else {
        $ServiceM_var = "";
        return $ServiceM_var;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $AdminView_var = new AdminView();
      $RuleMData = $AdminView_var->getRules();
      $activeLang = $AdminView_var->getLanguageIsActive();
      $ServiceMContorller_var = self::show($request)->toArray();
      $_id = $request->input('id');
      if(count($ServiceMContorller_var) > 0)
      {
        /*var_dump($iClubController_var);
        return;*/
        return view('ipanel.pages.managepages.services.editservice',[
          'ServiceMContorller_var'=>$ServiceMContorller_var,
          'activeLang'=>$activeLang,
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(2),
          'activemenu'=>encrypt(2),
          'id'=>$_id
        ]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
      $ServiceM_var = [];
      if($ServiceM_var=ServiceM::find($id))
      {
        $user_Data = $request->toArray();
        //variable gets from user
        $inp_idlang = $user_Data['opt_lang'];
        $inp_name = $user_Data['inp_name'];
        $inp_title = $user_Data['inp_title'];
        $inp_desc = $user_Data['inp_desc'];
        $inp_link = $user_Data['inp_link'];
        $inp_status = $user_Data['opt_status'];
        $inp_order = $user_Data['inp_order'];
        $inp_softdelete = "Not-delete";
        //create new category from table Category
        $ServiceM_var->idlang = $inp_idlang;
        $ServiceM_var->name = $inp_name;
        $ServiceM_var->title = $inp_title;
        $ServiceM_var->desc = $inp_desc;
        $ServiceM_var->link = $inp_link;
        $ServiceM_var->status = $inp_status;
        $ServiceM_var->order = $inp_order;
        $ServiceM_var->softdelete = $inp_softdelete;

        if($ServiceM_var->save())
        {
          if(Input::file('inp_avatar_st'))
          {
            $inp_avatar_st = $request->file('inp_avatar_st');
            $inp_filename_st  = $ServiceM_var->id . '_st.' . strtolower($inp_avatar_st->getClientOriginalExtension());
            $path_st = 'images/service/'. $inp_filename_st;
            $ServiceM_var->img_path_st = $inp_filename_st;
            Image::make($inp_avatar_st->getRealPath())->save($path_st);
          }
          if(Input::file('inp_avatar_nd'))
          {
            $inp_avatar_nd = $request->file('inp_avatar_nd');
            $inp_filename_nd  = $ServiceM_var->id . '_nd.' . strtolower($inp_avatar_nd->getClientOriginalExtension());
            $path_nd = 'images/service/'. $inp_filename_nd;
            $ServiceM_var->img_path_nd = $inp_filename_nd;
            Image::make($inp_avatar_nd->getRealPath())->save($path_nd);
          }
          $ServiceM_var->save();
          return $ServiceM_var;
        }
        else {
          return $ServiceM_var;
        }
      }
      else {
        return $ServiceM_var;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      $ServiceM_var = [];
      if($ServiceM_var=ServiceM::find($id))
      {
        $ServiceM_var->softdelete = 'Delete';
        if($ServiceM_var->save())
        {
          return $ServiceM_var;
        }
        else {
          return $ServiceM_var;
        }

      }
      else {
        return $ServiceM_var;
      }
    }
}
