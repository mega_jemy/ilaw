<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\Banner;
class BannerControl extends Controller
{
    //
    public function index($locationbnr)
    {

      $BannerMdata = Banner::where('typeban' ,'=',$locationbnr)->get();
      return $BannerMdata;
    }
    public function getBannerByIdPage($idpage)
    {
      $BannerMdata = Banner::where('idpages' ,'=',$idpage)
      ->where('status','=','Active')
      ->where('softdelete','=','Not-delete')
      ->where('typeban','=','top')
      ->get();
      return $BannerMdata;
    }
    public function store_Chk_Valid(Request $request)
    {

      //validate tha data from the request
      $validation = Validator::make($request->all(),Banner::$rules,[],Banner::$niceNames);
      //return the error messages if any
      if ($validation->fails())
       {
         //return json_encode(['response'=>$request->toArray()]);
         return json_encode(['response'=>$validation->errors()]);
       }
       else {
         return json_encode(['response'=>"valid"]);
       }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //variable gets from user
      /*var_dump($request->toArray());
      return;*/
      $inp_idpages = $request->input('opt_pages');
      $inp_idlang = $request->input('opt_lang');
      $inp_img_name = $request->input('txt_imagetitle_en');
      $inp_img_title = $request->input('txt_imagetitle_en');
      $inp_img_alt = $request->input('txt_imagealt_en');
      $inp_img_desc = $request->input('txt_imagealt_en');
      $inp_link = $request->input('txt_imagealt_en');
      $inp_status = $request->input('opt_status');
      $inp_order = $request->input('txt_order');
      $inp_typeban = $request->input('opt_type');
      $inp_softdelete = "Not-delete";
      /*$validation = Validator::make($request->all(),Banner::$rules,[],Banner::$niceNames);
      //return the error messages if any
      if ($validation->fails())
      {
        Session::push('banneraction_result','failed');
        Session::push('banneraction_result','Failed Store Data');
        return redirect('ipanel/addnewbanner')->with($validation->errors());
      }
      else {*/
        //create new category from table Category
        $BannerM_var = new Banner();
        $BannerM_var->idpages = $inp_idpages;
        $BannerM_var->idlang = $inp_idlang;
        $BannerM_var->img_name = $inp_img_name;
        $BannerM_var->img_title = $inp_img_title;
        $BannerM_var->img_alt = $inp_img_alt;
        $BannerM_var->img_desc = $inp_img_desc;
        $BannerM_var->link = $inp_link;
        $BannerM_var->status = $inp_status;
        $BannerM_var->order = $inp_order;
        $BannerM_var->typeban = $inp_typeban;
        $BannerM_var->softdelete = $inp_softdelete;

        if($BannerM_var->save())
        {

          if(Input::file('inp_image_st'))
          {
            $image_st = $request->file('inp_image_st');
            $inp_filename_st  = $BannerM_var->id . '.' . strtolower($image_st->getClientOriginalExtension());
            $BannerM_var->imgpath_st = $inp_filename_st;

          }
          if(Input::file('inp_image_nd'))
          {
            $image_nd = $request->file('inp_image_nd');
            $inp_filename_nd  = $BannerM_var->id . '.' . strtolower($image_nd->getClientOriginalExtension());
            $BannerM_var->imgpath_nd = $inp_filename_nd;



          }
          if($BannerM_var->save())
          {
            $path_st = 'images/banner/back/'. $inp_filename_st;
            Image::make($image_st->getRealPath())->save(public_path($path_nd));
            $path_nd = 'images/banner/single/'. $inp_filename_nd;
            Image::make($image_nd->getRealPath())->save(public_path($path_nd));
            Session::push('banneraction_result','success');
            Session::push('banneraction_result','Successfully Store Data');
            return redirect('ipanel/addnewbanner');
          }
          else {
            Session::push('banneraction_result','failed');
            Session::push('banneraction_result','Failed Store Data');
            return redirect('ipanel/addnewbanner');
          }
        }
        else {
          Session::push('banneraction_result','failed');
          Session::push('banneraction_result','Failed Store Data');
          return redirect('ipanel/addnewbanner');
        }
      /*}*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($BannerM_var=Banner::find($id))
      {
        return $BannerM_var;
      }
      else {
        $BannerM_var = "";
        return $BannerM_var;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $AdminView_var = new AdminView();
      $RuleMData = $AdminView_var->getRules();
      $activeLang = $AdminView_var->getLanguageIsActive();
      $BannerControl_var = BannerControl::show($request)->toArray();
      $_access = $request->input('_access');
      if(count($BannerControl_var) > 0)
      {
        //var_dump($CategoryControl_var->toArray());

        return view('ipanel.pages.BannerManagement.edit_banner',[
          'BannerControl_var'=>$BannerControl_var,
          '_access'=>$_access,
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(2),
          'activemenu'=>encrypt(2),
        ]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $_access = $request->input('_access');
        $id = decrypt($request->input('id'));

        if($BannerM_var=Banner::find($id))
        {
          $inp_idpages = $request->input('opt_pages');
          $inp_idlang = $request->input('opt_lang');
          $inp_img_name = $request->input('txt_imagetitle_en');
          $inp_img_title = $request->input('txt_imagetitle_en');
          $inp_img_alt = $request->input('txt_imagealt_en');
          $inp_img_desc = $request->input('txt_imagealt_en');
          $inp_link = $request->input('txt_imagealt_en');
          $inp_status = $request->input('opt_status');
          $inp_order = $request->input('txt_order');
          $inp_typeban = $request->input('opt_type');
          $inp_softdelete = "Not-delete";
          //create new category from table Category
          $BannerM_var->idpages = $inp_idpages;
          $BannerM_var->idlang = $inp_idlang;
          $BannerM_var->img_name = $inp_img_name;
          $BannerM_var->img_title = $inp_img_title;
          $BannerM_var->img_alt = $inp_img_alt;
          $BannerM_var->img_desc = $inp_img_desc;
          $BannerM_var->link = $inp_link;
          $BannerM_var->status = $inp_status;
          $BannerM_var->order = $inp_order;
          $BannerM_var->typeban = $inp_typeban;
          $BannerM_var->softdelete = $inp_softdelete;
          if($BannerM_var->save())
          {
            if(Input::file('inp_image_st'))
            {
              $image_st = $request->file('inp_image_st');
              $inp_filename_st  = $BannerM_var->id . '.' . strtolower($image_st->getClientOriginalExtension());
              $BannerM_var->imgpath_st = $inp_filename_st;
              if($BannerM_var->save())
              {
                $path_st = 'images/banner/back/'. $inp_filename_st;
                Image::make($image_st->getRealPath())->save($path_st);
              }
              else {
                Session::push('banneraction_result','failed');
                Session::push('banneraction_result','Failed Store Data');
              }
            }
            if(Input::file('inp_image_nd'))
            {
              $image_nd = $request->file('inp_image_nd');
              $inp_filename_nd  = $BannerM_var->id . '.' . strtolower($image_nd->getClientOriginalExtension());
              $BannerM_var->imgpath_nd = $inp_filename_nd;
              if($BannerM_var->save())
              {
                $path_nd = 'images/banner/single/'. $inp_filename_nd;
                Image::make($image_nd->getRealPath())->save($path_nd);
              }
              else {
                Session::push('banneraction_result','failed');
                Session::push('banneraction_result','Failed Store Data');

              }

            }
            Session::push('banneraction_result','success');
            Session::push('banneraction_result','Successfully Update Data');
            return redirect('ipanel/showcutsombanner?_access='.encrypt($_access)."&id=".encrypt($id));

          }
          else {
            Session::push('banneraction_result','failed');
            Session::push('banneraction_result','Failed Update Data');

          }
          return redirect('ipanel/showcutsombanner?_access='.encrypt($_access)."&id=".encrypt($id));
          //validate tha data from the request
          /*$validation = Validator::make($request->all(),CategoryM::$category_rules);
          //return the error messages if any
          if ($validation->fails())
          {
            return false;
          }
          else {

          }*/
        }
        else {
          Session::push('banneraction_result','failed');
          Session::push('banneraction_result','Failed Update Data');
        return redirect('ipanel/showcutsombanner?_access='.encrypt($_access)."&id=".encrypt($id));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id = decrypt($request->input('id'));
      $_access = $request->input('_access');
      if($BannerM_var=Banner::find($id))
      {
        $BannerM_var->softdelete = 'Delete';
        if($BannerM_var->save())
        {
          Session::push('banneraction_result','success');
          Session::push('banneraction_result','Successfully Delete Data');

        }
        else {
          Session::push('banneraction_result','failed');
          Session::push('banneraction_result','Failed Delete Data');


        }
        return redirect('ipanel/showbanner');
      }
      else {
        Session::push('banneraction_result','failed');
        Session::push('banneraction_result','Failed Delete Data');
        return redirect('ipanel/showbanner');
      }
    }
}
