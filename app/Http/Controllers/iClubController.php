<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\iClubM;
use ilaw\Http\Controllers\AdminView;
use DB;
class iClubController extends Controller
{
    //
    public function index()
    {

      $iClubMdata = iClubM::where('softdelete' ,'=',"Not-delete")->get();
      return $iClubMdata;
    }
    public function CreateStoreiClub(Request $request)
    {
      /*$user_Data = $request->toArray();

      var_dump(collect($user_Data)->values());
      return ;*/
      DB::beginTransaction();
      try {

          $res = self::store($request);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Saved Data');
          return redirect('ipanel/addnewiclub');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Saved Data');
        return redirect('ipanel/addnewiclub');
      }
    }

    public function UpdateiClub(Request $request)
    {
      $user_Data = $request->toArray();

      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = $request->input('id');
      DB::beginTransaction();
      try {

          $res = self::update($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Updated Data');
          return redirect('ipanel/showiclub?id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Update Data');
        return redirect('ipanel/showiclub?id='.encrypt($id));
      }
    }

    public function RemoveiClub(Request $request)
    {
      $user_Data = $request->toArray();

      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = decrypt($request->input('id'));
      DB::beginTransaction();
      try {

          $res = self::destroy($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Remove Data');
          return redirect('ipanel/manage_club?id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Remove Data');
        return redirect('ipanel/manage_club?id='.encrypt($id));
      }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user_Data = $request->toArray();
      //variable gets from user
      $inp_idlang = $user_Data['opt_lang'];
      $inp_desc = $user_Data['inp_desc'];
      $inp_softdelete = "Not-delete";
      //create new category from table Category
      $iClubM_var = new iClubM();
      $iClubM_var->idlang = $inp_idlang;
      $iClubM_var->desc = $inp_desc;

      $iClubM_var->softdelete = $inp_softdelete;
      if($iClubM_var->save())
      {
        return $iClubM_var;
      }
      else {
        return $iClubM_var;
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($iClubM_var=iClubM::find($id))
      {
        return $iClubM_var;
      }
      else {
        $iClubM_var = "";
        return $iClubM_var;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $AdminView_var = new AdminView();
      $RuleMData = $AdminView_var->getRules();
      $activeLang = $AdminView_var->getLanguageIsActive();
      $iClubController_var = self::show($request)->toArray();
      $_id = $request->input('id');
      if(count($iClubController_var) > 0)
      {
        /*var_dump($iClubController_var);
        return;*/
        return view('ipanel.pages.managepages.club.edit_iclub',[
          'iClubController_var'=>$iClubController_var,
          'activeLang'=>$activeLang,
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(2),
          'activemenu'=>encrypt(2),
          'id'=>$_id
        ]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
      $iClubM_var = [];
      if($iClubM_var=iClubM::find($id))
      {
        $user_Data = $request->toArray();
        //variable gets from user
        $inp_idlang = $user_Data['opt_lang'];
        $inp_desc = $user_Data['inp_desc'];
        //create new category from table Category
        $iClubM_var->idlang = $inp_idlang;
        $iClubM_var->desc = $inp_desc;

        if($iClubM_var->save())
        {
          return $iClubM_var;
        }
        else {
          return $iClubM_var;
        }
      }
      else {
        return $iClubM_var;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      $iClubM_var = [];
      if($iClubM_var=iClubM::find($id))
      {
        $iClubM_var->softdelete = 'Delete';
        if($iClubM_var->save())
        {
          return $iClubM_var;
        }
        else {
          return $iClubM_var;
        }

      }
      else {
        return $iClubM_var;
      }
    }
}
