<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\CommonPagesM;
use ilaw\Http\Controllers\AdminView;
use DB;
class CommonPagesControl extends Controller
{
    //
    public function getIdPage($page)
    {
      switch ($page) {
        case 'bussines_setup':
          return 1;
          break;
        case 'intellectual_property':
          return 2;
          break;
        case 'debt_collection':
          return 3;
          break;
        case 'litigation_dispute':
          return 4;
          break;
        default:
          return 0;
          break;
      }
    }
    public function getReturnPage($page,$idpage)
    {
      switch ($page) {
        case 'bussines_setup':
          return self::customizePageBussinesSetup($idpage);
          break;
        case 'intellectual_property':
          return self::customizePageIntellectualProperty($idpage);
          break;
        case 'debt_collection':
          return self::customizePageDebtCollection($idpage);
          break;
        case 'litigation_dispute':
          return self::customizePageLitigationDispute($idpage);
          break;
        default:
          return 0;
          break;
      }
    }
    public function customizePageBussinesSetup($id)
    {
      switch ($id) {
        case 1:
          return 'ipanel/add_new_bussines_setup';
          break;
        case 2:
          return 'ipanel/show_bussines_setup';
          break;
        case 3:
          return 'ipanel/manage_bussines_setup';
          break;
        default:
          // code...
          break;
      }
    }
    public function customizePageIntellectualProperty($id)
    {
      switch ($id) {
        case 1:
          return 'ipanel/add_new_intellectual_property';
          break;
        case 2:
          return 'ipanel/show_intellectual_property';
          break;
        case 3:
          return 'ipanel/manage_intellectual_property';
          break;
        default:
          // code...
          break;
      }
    }
    public function customizePageDebtCollection($id)
    {
      switch ($id) {
        case 1:
          return 'ipanel/add_new_debt_collection';
          break;
        case 2:
          return 'ipanel/show_debt_collection';
          break;
        case 3:
          return 'ipanel/manage_debt_collection';
          break;
        default:
          // code...
          break;
      }
    }

    public function customizePageLitigationDispute($id)
    {
      switch ($id) {
        case 1:
          return 'ipanel/add_new_litigation_dispute';
          break;
        case 2:
          return 'ipanel/show_litigation_dispute';
          break;
        case 3:
          return 'ipanel/manage_litigation_dispute';
          break;
        default:
          // code...
          break;
      }
    }
    public function index($page)
    {
      $idpage= self::getIdPage($page);
      $CommonPagesMdata = CommonPagesM::where('idpage','=',$idpage)->where('softdelete' ,'=',"Not-delete")->get();
      return $CommonPagesMdata;
    }
    public function CreateStoreCommon(Request $request)
    {
      $page = $request->input('inp_idpage');
      /*$user_Data = $request->toArray();

      var_dump(collect($user_Data)->values());
      return ;*/
      DB::beginTransaction();
      try {

          $res = self::store($request);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Saved Data');
          return redirect(self::getReturnPage($page,1));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Saved Data');
        return redirect(self::getReturnPage($page,1));
      }
    }

    public function UpdateCommon(Request $request)
    {
      $user_Data = $request->toArray();
      $page = $user_Data['inp_idpage'];
      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = $request->input('id');

      DB::beginTransaction();
      try {

          $res = self::update($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Updated Data');
          return redirect(self::getReturnPage($page,2).'?page='.$page.'&id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Update Data');
        return redirect(self::getReturnPage($page,2).'?page='.$page.'&id='.encrypt($id));
      }
    }

    public function RemoveCommon(Request $request)
    {
      $user_Data = $request->toArray();
      $page = $user_Data['page'];
      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = decrypt($request->input('id'));
      DB::beginTransaction();
      try {

          $res = self::destroy($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Remove Data');
          return redirect(self::getReturnPage($page,3).'?id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Remove Data');
        return redirect(self::getReturnPage($page,3).'?id='.encrypt($id));
      }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user_Data = $request->toArray();
      //variable gets from user
      $inp_idlang = $user_Data['opt_lang'];
      $inp_idpage = $user_Data['inp_idpage'];
      $inp_name = $user_Data['inp_name'];
      $inp_title = $user_Data['inp_title'];
      $inp_desc = $user_Data['inp_desc'];

      $inp_softdelete = "Not-delete";
      //create new category from table Category
      $CommonPagesM_var = new CommonPagesM();
      $CommonPagesM_var->idlang = $inp_idlang;
      $CommonPagesM_var->idpage = self::getIdPage($inp_idpage);
      $CommonPagesM_var->name = $inp_name;
      $CommonPagesM_var->title = $inp_title;
      $CommonPagesM_var->desc = $inp_desc;
      $CommonPagesM_var->softdelete = $inp_softdelete;
      if($CommonPagesM_var->save())
      {
        if(Input::file())
        {
          $image = $request->file('inp_thumbnail');
          $inp_filename  = $CommonPagesM_var->id . '.' . strtolower($image->getClientOriginalExtension());
          $CommonPagesM_var->img_path = $inp_filename;
          if($CommonPagesM_var->save())
          {
            $path = 'images/'.$inp_idpage.'/'. $inp_filename;
            Image::make($image->getRealPath())->save($path);

          }
        }
        return $CommonPagesM_var;
      }
      else {
        return $CommonPagesM_var;
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($CommonPagesM_var=CommonPagesM::find($id))
      {
        return $CommonPagesM_var;
      }
      else {
        $CommonPagesM_var = "";
        return $CommonPagesM_var;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $AdminView_var = new AdminView();
      $RuleMData = $AdminView_var->getRules();
      $activeLang = $AdminView_var->getLanguageIsActive();
      $CommonPagesController_var = self::show($request)->toArray();
      $_id = $request->input('id');
      if(count($CommonPagesController_var) > 0)
      {
        /*var_dump($AboutController_var);
        return;*/
        $page = $request->input('page');
        return self::getCommonView($page,$CommonPagesController_var,$activeLang,$RuleMData,encrypt(2),encrypt(2),$_id);
        /*return view('ipanel.pages.managepages.bussines_setup.edit_bussines_setup',[
          'CommonPagesController_var'=>$CommonPagesController_var,
          'activeLang'=>$activeLang,
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(2),
          'activemenu'=>encrypt(2),
          'id'=>$_id
        ]);*/
      }
    }

    public function getCommonView($page,$CommonPagesController_var,$activeLang,$RuleMData,$activepage,$activemenu,$_id)
    {
      switch ($page) {
        case 'bussines_setup':
            return view('ipanel.pages.managepages.bussines_setup.edit_bussines_setup',[
              'CommonPagesController_var'=>$CommonPagesController_var,
              'activeLang'=>$activeLang,
              'RuleMData'=>$RuleMData,
              'activepage'=>encrypt(2),
              'activemenu'=>encrypt(2),
              'id'=>$_id
            ]);
          break;
        case 'intellectual_property':
            return view('ipanel.pages.managepages.intellectual_property.edit_intellectual_property',[
              'CommonPagesController_var'=>$CommonPagesController_var,
              'activeLang'=>$activeLang,
              'RuleMData'=>$RuleMData,
              'activepage'=>encrypt(2),
              'activemenu'=>encrypt(2),
              'id'=>$_id
            ]);
          break;
        case 'debt_collection':
            return view('ipanel.pages.managepages.debt_collection.edit_debt_collection',[
              'CommonPagesController_var'=>$CommonPagesController_var,
              'activeLang'=>$activeLang,
              'RuleMData'=>$RuleMData,
              'activepage'=>encrypt(2),
              'activemenu'=>encrypt(2),
              'id'=>$_id
            ]);
          break;
        case 'litigation_dispute':
            return view('ipanel.pages.managepages.litigation_dispute.edit_litigation_dispute',[
              'CommonPagesController_var'=>$CommonPagesController_var,
              'activeLang'=>$activeLang,
              'RuleMData'=>$RuleMData,
              'activepage'=>encrypt(2),
              'activemenu'=>encrypt(2),
              'id'=>$_id
            ]);
          break;
        default:
          return 0;
          break;
      }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
      $CommonPagesM_var = [];
      if($CommonPagesM_var=CommonPagesM::find($id))
      {
        $user_Data = $request->toArray();
        //variable gets from user
        $inp_idlang = $user_Data['opt_lang'];
        $inp_idpage = $user_Data['inp_idpage'];
        $inp_name = $user_Data['inp_name'];
        $inp_title = $user_Data['inp_title'];
        $inp_desc = $user_Data['inp_desc'];

        $inp_softdelete = "Not-delete";
        //create new category from table Category
        $CommonPagesM_var->idlang = $inp_idlang;
        $CommonPagesM_var->idpage = self::getIdPage($inp_idpage);
        $CommonPagesM_var->name = $inp_name;
        $CommonPagesM_var->title = $inp_title;
        $CommonPagesM_var->desc = $inp_desc;
        $CommonPagesM_var->softdelete = $inp_softdelete;

        if($CommonPagesM_var->save())
        {
          if(Input::file())
          {


            $image = $request->file('inp_thumbnail');
            $inp_filename  = $CommonPagesM_var->id . '.' . strtolower($image->getClientOriginalExtension());
            $CommonPagesM_var->img_path = $inp_filename;
            if($CommonPagesM_var->save())
            {
              $path = 'images/'.$inp_idpage.'/'. $inp_filename;
              Image::make($image->getRealPath())->save($path);

            }
          }
          return $CommonPagesM_var;
        }
        else {
          return $CommonPagesM_var;
        }
      }
      else {
        return $CommonPagesM_var;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      $CommonPagesM_var = [];
      if($CommonPagesM_var=CommonPagesM::find($id))
      {
        $CommonPagesM_var->softdelete = 'Delete';
        if($CommonPagesM_var->save())
        {
          return $CommonPagesM_var;
        }
        else {
          return $CommonPagesM_var;
        }

      }
      else {
        return $CommonPagesM_var;
      }
    }
}
