<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mail;
use Auth;
use Session;
use ilaw\Mail\welcomeMail;
use ilaw\User;
class MailControl extends Controller
{
    //
    public function welcomemail(User $user)
    {
      \Mail::to($user['email'])->send(new WelcomeMail($user));
    }
}
