<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\AboutM;
use ilaw\Http\Controllers\AdminView;
use DB;
class AboutController extends Controller
{
    //
    public function index()
    {

      $AboutMdata = AboutM::where('softdelete' ,'=',"Not-delete")->get();
      return $AboutMdata;
    }
    public function indexByRegional($regional)
    {
      $AboutMdata = AboutM::where('softdelete' ,'=',"Not-delete")->where('idlang','=',$regional)->get();
      return $AboutMdata;
    }
    public function CreateStoreAbout(Request $request)
    {
      /*$user_Data = $request->toArray();

      var_dump(collect($user_Data)->values());
      return ;*/
      DB::beginTransaction();
      try {

          $res = self::store($request);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Saved Data');
          return redirect('ipanel/addnewabout');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Saved Data');
        return redirect('ipanel/addnewabout');
      }
    }

    public function UpdateAbout(Request $request)
    {
      $user_Data = $request->toArray();

      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = $request->input('id');
      DB::beginTransaction();
      try {

          $res = self::update($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Updated Data');
          return redirect('ipanel/showabout?id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Update Data');
        return redirect('ipanel/showabout?id='.encrypt($id));
      }
    }

    public function RemoveAbout(Request $request)
    {
      $user_Data = $request->toArray();

      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = decrypt($request->input('id'));
      DB::beginTransaction();
      try {

          $res = self::destroy($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Remove Data');
          return redirect('ipanel/manage_about?id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Remove Data');
        return redirect('ipanel/manage_about?id='.encrypt($id));
      }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user_Data = $request->toArray();
      //variable gets from user
      $inp_idlang = $user_Data['opt_lang'];
      $inp_desc = $user_Data['inp_desc'];
      $inp_vision = $user_Data['inp_vision'];
      $inp_mission = $user_Data['inp_mission'];
      $inp_history = $user_Data['inp_history'];
      $inp_softdelete = "Not-delete";
      //create new category from table Category
      $AboutM_var = new AboutM();
      $AboutM_var->idlang = $inp_idlang;
      $AboutM_var->desc = $inp_desc;
      $AboutM_var->our_vision = $inp_vision;
      $AboutM_var->our_mission = $inp_mission;
      $AboutM_var->our_history = $inp_history;
      $AboutM_var->softdelete = $inp_softdelete;
      if($AboutM_var->save())
      {
        return $AboutM_var;
      }
      else {
        return $AboutM_var;
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($AboutM_var=AboutM::find($id))
      {
        return $AboutM_var;
      }
      else {
        $AboutM_var = "";
        return $AboutM_var;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $AdminView_var = new AdminView();
      $RuleMData = $AdminView_var->getRules();
      $activeLang = $AdminView_var->getLanguageIsActive();
      $AboutController_var = self::show($request)->toArray();
      $_id = $request->input('id');
      if(count($AboutController_var) > 0)
      {
        /*var_dump($AboutController_var);
        return;*/
        return view('ipanel.pages.managepages.about.edit_about',[
          'AboutController_var'=>$AboutController_var,
          'activeLang'=>$activeLang,
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(2),
          'activemenu'=>encrypt(2),
          'id'=>$_id
        ]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
      $AboutM_var = [];
      if($AboutM_var=AboutM::find($id))
      {
        $user_Data = $request->toArray();
        //variable gets from user
        $inp_idlang = $user_Data['opt_lang'];
        $inp_desc = $user_Data['inp_desc'];
        $inp_vision = $user_Data['inp_vision'];
        $inp_mission = $user_Data['inp_mission'];
        $inp_history = $user_Data['inp_history'];
        $inp_softdelete = "Not-delete";
        //create new category from table Category
        $AboutM_var->idlang = $inp_idlang;
        $AboutM_var->desc = $inp_desc;
        $AboutM_var->our_vision = $inp_vision;
        $AboutM_var->our_mission = $inp_mission;
        $AboutM_var->our_history = $inp_history;

        if($AboutM_var->save())
        {
          return $AboutM_var;
        }
        else {
          return $AboutM_var;
        }
      }
      else {
        return $AboutM_var;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      $AboutM_var = [];
      if($AboutM_var=AboutM::find($id))
      {
        $AboutM_var->softdelete = 'Delete';
        if($AboutM_var->save())
        {
          return $AboutM_var;
        }
        else {
          return $AboutM_var;
        }

      }
      else {
        return $AboutM_var;
      }
    }
}
