<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\NewsletterM;
class NewsletterControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $NewsletterM_data = NewsletterM::get();
      return $NewsletterM_data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $inp_email = $request->input('email');
      $inp_softdelete = 'Not-delete';
      //create new category from table Category
      $NewsletterM_var = new NewsletterM();
      //validate tha data from the request
      $NewsletterM_var->email = $inp_email;
      $NewsletterM_var->softdelete = $inp_softdelete;
      if($NewsletterM_var->save())
      {

        return 1;
      }
      else {
        return 0;
      }

      //variable gets from user

      /*$validation = Validator::make($request->all(),NewsletterM::$rules,[],NewsletterM::$niceNames);
      //return the error messages if any
      if ($validation->fails())
      {
        return redirect('/news-letter')->withErrors($validation)->withInput();
      }
      else {

      }*/

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($NewsletterM_var=NewsletterM::find($id))
      {
        $NewsletterM_var->softdelete = 'Delete';
        if($NewsletterM_var->save())
        {
          Session::push('newsletteraction_result','success');
          Session::push('newsletteraction_result','Successfully Delete Data');
          return redirect('ipanel/shownewsletter');
        }
        else {
          Session::push('newsletteraction_result','failed');
          Session::push('newsletteraction_result','Failed Delete Data');
          return redirect('ipanel/shownewsletter');
        }

      }
      else {
        Session::push('newsletteraction_result','failed');
        Session::push('newsletteraction_result','Failed Delete Data');
        return redirect('ipanel/shownewsletter');
      }
    }
}
