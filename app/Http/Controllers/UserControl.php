<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\User;
use ilaw\Http\Controllers\MailControl;
class UserControl extends Controller
{
  public function getUserToken(Request $request)
  {
    $email = $request->input('email');
    $Token = User::where('email','=',$email)->get();
    return $Token;
  }
  public function getRules()
  {
    $RolesController_var = new RolesController();
    $res = $RolesController_var->AuthgetRules();
    return $res;
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($usertype)
    {
      if($usertype != "")
      {
        $UserData = User::where('typeacc','=',$usertype)->get();
        return $UserData;
      }
      else {
        $UserData = "";
        return $UserData;
      }

    }
    public function ConfirmChangePassword(Request $request)
    {
      if(Auth::check())
      {
        var_dump($request->toArray());
        $inp_txt_oldpassword = $request->input('txt_oldpassword');
        $inp_txt_newpassword = $request->input('txt_newpassword');
        $inp_txt_confirmpassword = $request->input('txt_confirmpassword');
        $validation = Validator::make($request->all(),User::$changepassword_rules,[],User::$changepassword_niceNames);
        //return the error messages if any
        var_dump($validation->errors()->messages());
        //return;
        if ($validation->fails())
        {
          return redirect('ipanel/change-password')
                        ->withErrors($validation)
                        ->withInput();
        }
        else {
          $newmsg_oldpassword = "";
          $UserData = User::find(Auth::user()->id);
          //var_dump($UserData->password);
          //var_dump(bcrypt($inp_txt_oldpassword));
          $email = Auth::user()->email;
          if(!Auth::attempt(['email' => $email,'password' =>$inp_txt_oldpassword]))
          {
            $newmsg_oldpassword = "The Old Password must match old password store before.";
          }
          $newmsg_newpassword = "";
          if($inp_txt_newpassword != $inp_txt_confirmpassword)
          {
            $newmsg_newpassword = "New Password must match confirm password.";
          }

          if(!Auth::attempt(['email' => $email,'password' =>$inp_txt_oldpassword]) || $inp_txt_newpassword != $inp_txt_confirmpassword)
          {
            Session::push('changepasswordaction_result','failed');
            Session::push('changepasswordaction_result','Failed Saved Data');
            Session::push('changepasswordaction_result',$newmsg_oldpassword);
            Session::push('changepasswordaction_result',$newmsg_newpassword);
            return redirect('ipanel/change-password')->withInput();
          }
          else {
            $UserData = User::find(Auth::user()->id);
            $UserData->password = bcrypt($inp_txt_newpassword);
            if($UserData->save())
            {
              Auth::login($UserData);
              Session::push('changepasswordaction_result','success');
              Session::push('changepasswordaction_result','Successfully Saved Data');
              return redirect('ipanel/change-password');
            }
            else {
              Session::push('changepasswordaction_result','failed');
              Session::push('changepasswordaction_result','Failed Saved Data');
              return redirect('ipanel/change-password');
            }
          }
        }
      }
    }
    public function UserConfirmChangePassword(Request $request)
    {
      if(Auth::check())
      {
        $inp_txt_oldpassword = $request->input('txt_oldpassword');
        $inp_txt_newpassword = $request->input('txt_newpassword');
        $inp_txt_confirmpassword = $request->input('txt_confirmpassword');
        $validation = Validator::make($request->all(),User::$changepassword_rules,[],User::$changepassword_niceNames);
        //return the error messages if any
        //var_dump($validation->errors()->messages());
        //return;
        if ($validation->fails())
        {
          return redirect('/account-change-password')
                        ->withErrors($validation)
                        ->withInput();
        }
        else {
          $newmsg_oldpassword = "";
          $UserData = User::find(Auth::user()->id);
          //var_dump($UserData->password);
          //var_dump(bcrypt($inp_txt_oldpassword));

          if(!Auth::attempt(['email' => $UserData['email'],'password' =>$inp_txt_oldpassword]))
          {
            $newmsg_oldpassword = "The Old Password must match old password store before.";
          }
          $newmsg_newpassword = "";
          if($inp_txt_newpassword != $inp_txt_confirmpassword)
          {
            $newmsg_newpassword = "New Password must match confirm password.";
          }

          if(!Auth::attempt(['email' => $UserData['email'],'password' =>$inp_txt_oldpassword]) || $inp_txt_newpassword != $inp_txt_confirmpassword)
          {
            Session::push('changepasswordaction_result','failed');
            Session::push('changepasswordaction_result','Failed Saved Data');
            Session::push('changepasswordaction_result',$newmsg_oldpassword);
            Session::push('changepasswordaction_result',$newmsg_newpassword);
            return redirect('account-change-password')->withInput();
          }
          else {
            $UserData = User::find(Auth::user()->id);
            $UserData->password = bcrypt($inp_txt_newpassword);
            if($UserData->save())
            {
              Auth::login($UserData);
              Session::push('changepasswordaction_result','success');
              Session::push('changepasswordaction_result','Successfully Change Password');
              return redirect('account-change-password');
            }
            else {
              Session::push('changepasswordaction_result','failed');
              Session::push('changepasswordaction_result','Failed Change Password');
              return redirect('account-change-password');
            }
          }
        }
      }
    }
    public function UserResetPassword(Request $request)
    {
      $inp_email = $request->input('email');
      $inp_password = $request->input('password');
      $inp_password_confirmation = $request->input('password_confirmation');

      $newmsg_oldpassword = "";
      $UserData = self::getUserToken($request);
      if(!empty($UserData))
      {
        $id = $UserData[0]['id'];

        $UserData = User::find($id);
        $UserData->password = bcrypt($inp_password);
        if($UserData->save())
        {
          Session::push('reset_password','success');
          Session::push('reset_password','Successfully Change Password');
          return redirect('/');
        }
        else {
          Session::push('reset_password','failed');
          Session::push('reset_password','Failed Change Password');
          return redirect('/');
        }
      }
    }
    public function verifyUserAccount(Request $request,$confrimationcode,$id)
    {
      if($id != "")
      {
        $UnitofWorkCustomer_var = new UnitofWorkCustomer();
        $id = decrypt($id);
        if($UserData = User::find($id))
        {
          if($UserData->confirmation_code == $confrimationcode)
          {
            $UserData->confirmacc = 1;
          }

          if($UserData->save())
          {
            Session::push('userconfrim_result','success');
            Session::push('userconfrim_result','Successfully Confirm your account,please login know');
            return $UnitofWorkCustomer_var->ManageLoginUser($request);
          }
          else {
            Session::push('userconfrim_result','failed');
            Session::push('userconfrim_result','Failed to confirm your account,please try agian later');
            return $UnitofWorkCustomer_var->ManageLoginUser($request);
          }
        }
        else {
          Session::push('userconfrim_result','failed');
          Session::push('userconfrim_result','Failed to confirm your account,please try agian later');
          return $UnitofWorkCustomer_var->ManageLoginUser($request);
        }
      }
      else {
        Session::push('userconfrim_result','failed');
        Session::push('userconfrim_result','Failed to confirm your account,please try agian later');
        return $UnitofWorkCustomer_var->ManageLoginUser($request);
      }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //variable gets from user
      $inp_opt_countryname = $request->input('opt_countryname');
      $inp_idusercity = $request->input('opt_cityname');
      $inp_typeacc = 'user';
      $inp_firstname = $request->input('txt_firstname');
      $inp_lastname = $request->input('txt_lastname');
      $inp_gender = $request->input('opt_gender');
      $inp_username = '';
      $inp_address = '';// $request->input('txt_address');
      $inp_phone = $request->input("txt_phone");
      $inp_email = $request->input("reg_email");
      $inp_password = $request->input('password');
      $confirm_password = $request->input('password_confirmation');
      $inp_softdelete = "Not-delete";
      $inp_status = 'Active';

        //create new user from table wallpaper
        $User_var = new User();
        //validate tha data from the request
        $validation = Validator::make($request->all(),User::$registerUserRules,[],User::$registerUserNiceNames);
        //return the error messages if any

        if ($validation->fails())
        {
          return redirect('login')
                        ->withErrors($validation)
                        ->withInput();
        }
        else {
          $User_var->idcountry = $inp_opt_countryname;
          $User_var->typeacc = $inp_typeacc;
          $User_var->status = $inp_status;
          $User_var->fname = $inp_firstname;
          $User_var->lname = $inp_lastname;
          $User_var->email = $inp_email;
          //$User_var->username = $inp_username;
          $User_var->password = bcrypt($inp_password);
          $User_var->gender = $inp_gender;
          //$User_var->address = $inp_address;
          $User_var->phone = $inp_phone;
          $User_var->confirmation_code = Hash('md5',str_random(30));
          $User_var->confirmacc = '0';
          $User_var->active = 'Offline';
          $User_var->softdelete = $inp_softdelete;
          $User_var->imgpath = '';
          $User_var->cityname = $inp_idusercity;
          if($User_var->save())
          {
            $id_saved = $User_var->id;
            $data['verification_code']  = $User_var->confirmation_code;
            $data['fullname'] = $User_var->fullname;
            $data['email'] = $User_var->email;

            Session::push('userregister_result','success');
            Session::push('userregister_result','Successfully Saved Data,you can login now');
            /*$MailControl_var = new MailControl();
            $resmail = $MailControl_var->welcomemail($User_var);*/
            Auth::login($User_var);
            return redirect('/');


          }
          else {
            Session::push('userregister_result','failed');
            Session::push('userregister_result','Failed Saved Data');
            return redirect('register');
          }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //variable gets from user
      $inp_idusercity = $request->input('opt_cityname');
      $inp_typeacc = $request->input('opt_typeacc');
      $inp_firstname = $request->input('txt_firstname');
      $inp_lastname = $request->input('txt_lastname');
      $inp_gender = $request->input('opt_gender');
      $inp_username = $request->input('username');
      $inp_phone = $request->input("txt_phone");
      $inp_email = $request->input('email');
      $inp_password = $request->input('password');
      $confirm_password = $request->input('confirm_password');
      $user_image = $request->file('user_image');
      $inp_softdelete = "Not-delete";
      $inp_status = $request->input('opt_status');

        //create new user from table wallpaper
        $User_var = new User();
        //validate tha data from the request
        $validation = Validator::make($request->all(),User::$registerRules);
        //return the error messages if any
        if ($validation->fails())
        {
          return redirect('ipanel/addnewadminuser')
                        ->withErrors($validation)
                        ->withInput();
        }
        else {
          $User_var->idusercity = $inp_idusercity;
          $User_var->typeacc = $inp_typeacc;
          $User_var->status = $inp_status;
          $User_var->fullname = $inp_firstname." ".$inp_lastname;
          $User_var->email = $inp_email;
          $User_var->username = $inp_username;
          $User_var->password = bcrypt($inp_password);
          $User_var->gender = $inp_gender;
          $User_var->address = '';
          $User_var->phone = $inp_phone;
          $User_var->confirmation_code = Hash('md5',str_random(30));
          $User_var->confirmacc = '0';
          $User_var->active = 'Offline';
          $User_var->softdelete = $inp_softdelete;
          $User_var->imgpath = '';
          if($User_var->save())
          {
            $id_saved = $User_var->id;
            $data['verification_code']  = $User_var->confirmation_code;
            $data['first_name'] = $User_var->first_name;
            $data['last_name'] = $User_var->last_name;
            $data['email'] = $User_var->email;

            Session::push('userindexdata_result','success');
            Session::push('userindexdata_result','Successfully Saved Data');
            return redirect('ipanel/addnewadminuser');

          }
          else {
            Session::push('userindexdata_result','failed');
            Session::push('userindexdata_result','Failed Saved Data');
            return redirect('ipanel/addnewadminuser');
          }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      $user ="";
      if($user = User::find($id))
      {
        return $user;
      }
      else {
        return $user;
      }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $RuleMData = self::getRules();
      $UserControl_var = UserControl::show($request);
      if(count($UserControl_var) > 0)
      {
        //var_dump($UserControl_var);
        return view('ipanel.pages.UserManagement.editadmin',['UserControl_var'=>$UserControl_var]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($User_var=User::find($id))
      {
        $User_var->softdelete = 'Delete';
        if($User_var->save())
        {
          Session::push('useraction_result','success');
          Session::push('useraction_result','Successfully Delete Data');
          return redirect('ipanel/showuser');
        }
        else {
          Session::push('useraction_result','failed');
          Session::push('useraction_result','Failed Delete Data');
          return redirect('ipanel/showuser');
        }

      }
      else {
        Session::push('useraction_result','failed');
        Session::push('useraction_result','Failed Delete Data');
        return redirect('ipanel/showuser');
      }
    }
    public static function getUser($id)
    {
        $user = User::find($id);
        return json_encode(array('result' => $user));
    }
    public function authenticate(Request $request)
    {
      var_dump($request->toArray());

      $email=$request->input('email');
      $password=$request->input('password');
      $validation = Validator::make($request->all(),User::$loginRules,[],User::$loginNiceNames);
      if ($validation->fails())
      {
        return redirect('ipanel')
                      ->withErrors($validation)
                      ->withInput();
      }
      else {
        if (filter_var($request->input('email'),FILTER_VALIDATE_EMAIL))
        {
            if(Auth::attempt(['email' => $email,'password' =>$password,'status'=>'Active']))
            {

                $user_id=User::where('email',$email)->first()->id;
                $userData = self::getUser($user_id);
                //Auth::login($request);
                return self::returnPage($request,$userData,1);
            }
            else
            {
                $userData = [];
                return self::returnPage($request,$userData,0);
            }
        }
        else {
          $userData = [];
          return self::returnPage($request,$userData,0);
        }
      }
    }
    public function checkReturnPage($moodpage)
    {
      if($moodpage == "admin" || $moodpage == "super-admin")
      {
        return "ipanel/dashboard";
      }
      elseif($moodpage == "user") {
        return "/";
      }

    }
    public function returnPage(Request $request,$userData,$mood)
    {
      $validation['email'] = 'These credentials do not match our records.';
      if(!empty($userData) && $mood == 1)
      {
        if(Auth::check())
        {
          $softdelete = Auth::user()->softdelete;
          $confirmacc = Auth::user()->confirmacc;
          if($softdelete == "Not-delete" && $confirmacc == 1)
          {
            $request->session()->put('sessionUser','1');
            $typeaccount =  Auth::user()->typeacc;
            $url = self::checkReturnPage($typeaccount);
            return redirect($url);
          }
          elseif ($confirmacc == 0) {
            $validation['email'] = 'Please check your mail to confirm your account';
            return redirect('/ipanel')
                          ->withErrors($validation)
                          ->withInput();
          }
          else {
            return redirect('/ipanel')
                          ->withErrors($validation)
                          ->withInput();
          }
        }
        else {
          return redirect('/ipanel')
                        ->withErrors($validation)
                        ->withInput();
        }
      }
      else {
        return redirect('/ipanel')
                      ->withErrors($validation)
                      ->withInput();
      }
    }
}
