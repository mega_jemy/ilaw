<?php

namespace ilaw\Http\Controllers;


use Illuminate\Http\Request;
use ilaw\Http\Controllers\RolesController;
use ilaw\Http\Controllers\UserControl;
use ilaw\Http\Controllers\NewsletterControl;
use ilaw\Http\Controllers\EnquiriesMessageControl;
use ilaw\Http\Controllers\BannerControl;
use ilaw\Http\Controllers\LanguageController;
use ilaw\Http\Controllers\AppointmentControler;
use ilaw\Http\Controllers\TrainingController;
use ilaw\Http\Controllers\ServiceController;
use ilaw\Http\Controllers\iClubController;
use ilaw\Http\Controllers\AboutController;
use ilaw\Http\Controllers\CommonPagesControl;
use ilaw\Http\Controllers\MediaControl;
use ilaw\Http\Controllers\BlogControl;
use Auth;
class AdminView extends Controller
{
    //
    public function getRules()
    {
      $RolesController_var = new RolesController();
      $res = $RolesController_var->AuthgetRules();
      return $res;
    }
    public function getLanguageIsActive()
    {
      $LanguageController_var = new LanguageController();
      $res = $LanguageController_var->getActiveLanguage();
      return $res;
    }

    public function getServiceData()
    {
      $ServiceController_var = new ServiceController();
      $res = $ServiceController_var->index();
      return $res;
    }
    public function getClubData()
    {
      $iClubController_var = new iClubController();
      $res = $iClubController_var->index();
      return $res;
    }
    public function getDashboard()
    {
      $RuleMData = self::getRules();
      //var_dump($RuleMData);
      return view('ipanel.pages.Dashboard.dashboard',[
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(1),
        'activemenu'=>encrypt(1),
      ]);
    }
    #-----------------------Begin Banner management--------------------------------------
  public function ManageBannerIndex()
  {
    $RuleMData = self::getRules();
    $BannerControl_data = new BannerControl();
    $BannerData = $BannerControl_data->index('top');
    return view('ipanel.pages.BannerManagement.ShowBanner',[
      'BannerData'=>$BannerData,
      'RuleMData'=>$RuleMData,
      'activepage'=>encrypt(18),
      'activemenu'=>encrypt(8),
    ]);
  }


  public function AddNewBannerIndex()
  {
    $RuleMData = self::getRules();
    $activeLang = self::getLanguageIsActive();
    return view('ipanel.pages.BannerManagement.addnewbanner',[
      'RuleMData'=>$RuleMData,
      'activeLang'=>$activeLang,
      'activepage'=>encrypt(18),
      'activemenu'=>encrypt(8),
    ]);
  }
  #-----------------------End Banner management--------------------------------------
  public function loginPage()
    {
      if(Auth::check())
      {
        return self::getDashboard();
      }
      else {
        return view('auth.cpanel.login');
      }

    }
    #-----------------------Begin Newsletter management--------------------------------------
    public function ManageNewsletterIndex()
    {
      $RuleMData = self::getRules();
      $NewsletterControl_data = new NewsletterControl();
      $NewsletterData = $NewsletterControl_data->index();
      return view('ipanel.pages.NewsletterManagement.managenewsletter',[
        'NewsletterData'=>$NewsletterData,
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(11),
        'activemenu'=>encrypt(4),
      ]);
    }
    #-----------------------End Newsletter management--------------------------------------
    #-----------------------Begin Enquiries message management--------------------------------------
    public function ManageContactUsIndex()
    {
      $RuleMData = self::getRules();
      $EnquiriesMessageControl_data = new EnquiriesMessageControl();
      $EnquerymessageData = $EnquiriesMessageControl_data->index('==');
      return view('ipanel.pages.EnquiriesManagement.managecontactus',[
        'EnquerymessageData'=>$EnquerymessageData,
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(12),
        'activemenu'=>encrypt(5),
      ]);
    }
    public function ManageEnquiriesMessageIndex()
    {
      $RuleMData = self::getRules();
      $EnquiriesMessageControl_data = new EnquiriesMessageControl();
      $EnquerymessageData = $EnquiriesMessageControl_data->index('!=');
      return view('ipanel.pages.EnquiriesManagement.manageenquiries',[
        'EnquerymessageData'=>$EnquerymessageData,
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(13),
        'activemenu'=>encrypt(5),
      ]);
    }
    #-----------------------End Enquiries message management--------------------------------------
    #-----------------------Begin Capital management--------------------------------------
    public function ManageCountryIndex()
    {
      $RuleMData = self::getRules();
      $CountryControl_var = new CountryControl();
      $CountryControlData = $CountryControl_var->index();
      return view('ipanel.pages.CapitalManagement.managecountry.show-country',[
        'CountryControlData'=>$CountryControlData,
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(14),
        'activemenu'=>encrypt(6),
      ]);
    }
    public function AddNewCountryIndex()
    {
      $RuleMData = self::getRules();
      return view('ipanel.pages.CapitalManagement.managecountry.add-new-country',[
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(14),
        'activemenu'=>encrypt(6),
      ]);
    }
    #-----------------------End Capital management--------------------------------------
    #-----------------------Begin Booking management--------------------------------------
    public function ManageBookingIndex()
    {
      $RuleMData = self::getRules();
      $AppointmentController_var = new AppointmentController();
      $AppointmentControllerData = $AppointmentController_var->getFixedAppointment();
      return view('ipanel.pages.Booking_Management.manage_booking',[
        'AppointmentControllerData'=>$AppointmentControllerData,
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(24),
        'activemenu'=>encrypt(9),
      ]);
    }
    public function ManageFiexdBookingIndex()
    {
      $RuleMData = self::getRules();
      $AppointmentController_var = new AppointmentController();
      $AppointmentControllerData = $AppointmentController_var->getFixedAppointment();
      return view('ipanel.pages.Booking_Management.FixedBookingEvent.ShowFixedEvent',[
        'AppointmentControllerData'=>$AppointmentControllerData,
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(25),
        'activemenu'=>encrypt(9),
      ]);
    }
    public function ManageAddFiexdBookingIndex()
    {
      $RuleMData = self::getRules();
      return view('ipanel.pages.Booking_Management.FixedBookingEvent.addNewFixedEvent',[
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(25),
        'activemenu'=>encrypt(9),
      ]);
    }

    #-----------------------End Booking management--------------------------------------
    #-----------------------Begin  management--------------------------------------

    public function addNewAdminUserIndex()
    {
      $RuleMData = self::getRules();
      $CountryControl_var = new CountryControl();
      $country_var = $CountryControl_var->index();
      return view('ipanel.pages.UserManagement.createadmin',[
        "country_var" => $country_var,
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(16),
        'activemenu'=>encrypt(7),
      ]);
    }
    public function ManageUserIndex()
    {
      $RuleMData = self::getRules();
      $UserControl_data = new UserControl();
      $UserData = $UserControl_data->index('user');
      return view('ipanel.pages.UserManagement.managerusers',[
        'UserData'=>$UserData,
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(10),
        'activemenu'=>encrypt(3),
      ]);
    }
    public function ManageAdminUserIndex()
    {
      $RuleMData = self::getRules();
      $UserControl_data = new UserControl();
      $UserData_superadmin = $UserControl_data->index('super-admin');
      $UserData_admin = $UserControl_data->index('admin');
      return view('ipanel.pages.AdminSection.manageradminusers',[
        'UserDataSuperAdmin'=>$UserData_superadmin,
        'UserData_admin'=>$UserData_admin,
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(16),
        'activemenu'=>encrypt(7),
      ]);
    }
    public function ManageChangePassword()
    {
      $RuleMData = self::getRules();
      return view('ipanel.pages.AdminSection.change-password',[
        'RuleMData'=>$RuleMData,
        'activepage'=>encrypt(17),
        'activemenu'=>encrypt(7),
      ]);
    }
    public function ManageAdminUserPermissionsIndex(Request $request)
    {
      $RuleMData = self::getRules();
      $id = $request->input('_access');
      if(!empty($id))
      {
        $idaccess = decrypt($id);
        $UserRoleData = self::getRulesPermission($idaccess);
        return view('ipanel.pages.AdminSection.user-roles-permission',[
          'RuleMData'=>$RuleMData,
          'UserRoleData'=> $UserRoleData,
          'activepage'=>encrypt(17),
          'activemenu'=>encrypt(7),
          '_access'=> $id
        ]);
      }
      else {
        return self::getDashboard();
      }

    }
    #-----------------------End management--------------------------------------
    #-----------------------Begin Pages management--------------------------------------
    public function ManageFirmIndex(Request $request)
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        return view('ipanel.pages.AdminSection.change-password',[
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(2),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageServicesIndex(Request $request)
    {
      $RuleMData = self::getRules();
      $ServiceData = self::getServiceData();
      /*var_dump($ServiceData->toArray());
      return;*/
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.services.showservice',[
          'RuleMData'=>$RuleMData,
          'ServiceData'=>$ServiceData,
          'activepage'=>encrypt(3),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function AddNewServiceIndex(Request $request)
    {
      $RuleMData = self::getRules();
      $activeLang = self::getLanguageIsActive();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.services.addservice',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(3),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageClubIndex(Request $request)
    {
      $RuleMData = self::getRules();
      $activeLang = self::getLanguageIsActive();
      $iClubData = self::getClubData();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.club.show_iclub',[
          'RuleMData'=>$RuleMData,
          'iClubData'=>$iClubData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(4),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function AddNewiClubIndex()
    {
      $RuleMData = self::getRules();
      $activeLang = self::getLanguageIsActive();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.club.iclub',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(4),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageCareIndex(Request $request)
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        return view('ipanel.pages.AdminSection.change-password',[
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(5),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageTrainingIndex(Request $request)
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {

        $TrainingController_data = new TrainingController();
        $TrainingData = $TrainingController_data->index();
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.training.showtraining',[
          'RuleMData'=>$RuleMData,
          'TrainingData'=>$TrainingData,
          'activepage'=>encrypt(9),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }

    public function ManageAboutIndex()
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $AboutController_data = new AboutController();
        $AboutData = $AboutController_data->index();
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.about.show_about',[
          'RuleMData'=>$RuleMData,
          'AboutData'=>$AboutData,
          'activepage'=>encrypt(19),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageMediaImagesIndex()
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $MediaControl_data = new MediaControl();
        $MediaData = $MediaControl_data->index('I');
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.media.images.showimages',[
          'RuleMData'=>$RuleMData,
          'MediaData'=>$MediaData,
          'activepage'=>encrypt(7),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageMediaVideosIndex()
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $MediaControl_data = new MediaControl();
        $MediaData = $MediaControl_data->index('V');
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.media.videos.show_videos',[
          'RuleMData'=>$RuleMData,
          'MediaData'=>$MediaData,
          'activepage'=>encrypt(8),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageBussinesSetupIndex()
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $CommonPagesControl_data = new CommonPagesControl();
        $CommonPagesData = $CommonPagesControl_data->index('bussines_setup');
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.bussines_setup.show_bussines_setup',[
          'RuleMData'=>$RuleMData,
          'CommonPagesData'=>$CommonPagesData,
          'activepage'=>encrypt(20),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageIntellectualPropertyIndex()
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $CommonPagesControl_data = new CommonPagesControl();
        $CommonPagesData = $CommonPagesControl_data->index('intellectual_property');
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.intellectual_property.show_intellectual_property',[
          'RuleMData'=>$RuleMData,
          'CommonPagesData'=>$CommonPagesData,
          'activepage'=>encrypt(21),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageDebtCollectionIndex()
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $CommonPagesControl_data = new CommonPagesControl();
        $CommonPagesData = $CommonPagesControl_data->index('debt_collection');
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.debt_collection.show_debt_collection',[
          'RuleMData'=>$RuleMData,
          'CommonPagesData'=>$CommonPagesData,
          'activepage'=>encrypt(22),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }

    public function ManageLitigationDisputeIndex()
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $CommonPagesControl_data = new CommonPagesControl();
        $CommonPagesData = $CommonPagesControl_data->index('litigation_dispute');
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.litigation_dispute.show_litigation_dispute',[
          'RuleMData'=>$RuleMData,
          'CommonPagesData'=>$CommonPagesData,
          'activepage'=>encrypt(23),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function ManageBlogIndex()
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $BlogControl_data = new BlogControl();
        $BlogControlData = $BlogControl_data->index();
        $RuleMData = self::getRules();
        return view('ipanel.pages.managepages.blog.show_blog',[
          'RuleMData'=>$RuleMData,
          'BlogControlData'=>$BlogControlData,
          'activepage'=>encrypt(6),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }

    public function ManagePayPageIndex()
    {
      $RuleMData = self::getRules();
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $BlogControl_data = new BlogControl();
        $BlogControlData = $BlogControl_data->index();
        $RuleMData = self::getRules();
        return view('ipanel.pages.PaymentManagement.paypage',[
          'RuleMData'=>$RuleMData,
          'BlogControlData'=>$BlogControlData,
          'activepage'=>encrypt(6),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }



    #-----------------------End Pages management--------------------------------------
    #-----------------------Begin Training --------------------------------------
    public function AddNewTrainingIndex()
    {
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        $activeLang = self::getLanguageIsActive();
        $NewsletterControl_data = new NewsletterControl();
        $NewsletterData = $NewsletterControl_data->index();
        return view('ipanel.pages.managepages.training.addnewtraining',[
          'NewsletterData'=>$NewsletterData,
          'activeLang'=>$activeLang,
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(9),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }


    }
    public function AddNewAboutIndex()
    {

      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        $activeLang = self::getLanguageIsActive();
        return view('ipanel.pages.managepages.about.about',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(19),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function AddNewBussinessSetupIndex()
    {

      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        $activeLang = self::getLanguageIsActive();
        return view('ipanel.pages.managepages.bussines_setup.bussines_setup',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(20),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function AddNewIntellectualPropertyIndex()
    {

      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        $activeLang = self::getLanguageIsActive();
        return view('ipanel.pages.managepages.intellectual_property.intellectual_property',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(21),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function AddNewDebtCollectionIndex()
    {

      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        $activeLang = self::getLanguageIsActive();
        return view('ipanel.pages.managepages.debt_collection.debt_collection',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(22),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function AddNewLitigationDisputeIndex()
    {

      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        $activeLang = self::getLanguageIsActive();
        return view('ipanel.pages.managepages.litigation_dispute.litigation_dispute',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(23),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function AddNewMediaImageIndex()
    {

      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        $activeLang = self::getLanguageIsActive();
        return view('ipanel.pages.managepages.media.images.addnew_images',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(7),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }
    public function AddNewMediaVideosIndex()
    {


      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        $activeLang = self::getLanguageIsActive();
        return view('ipanel.pages.managepages.media.videos.addnew_videos',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(8),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }

    public function AddNewBlogIndex()
    {
      $id = Auth::user()->id;
      if(!empty($id))
      {
        $RuleMData = self::getRules();
        $activeLang = self::getLanguageIsActive();
        return view('ipanel.pages.managepages.blog.blog',[
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(6),
          'activemenu'=>encrypt(2),
        ]);
      }
      else {
        return self::getDashboard();
      }
    }

    #-----------------------End Training--------------------------------------

}
