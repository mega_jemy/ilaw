<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use ilaw\Model\Banner;
use ilaw\Model\MenuM;
use ilaw\Model\MenuItemM;
use ilaw\Model\EventsMethodM;
use ilaw\Model\RolesM;
use ilaw\Model\RolesPermissionM;
use Auth;
use Session;
class RolesController extends Controller
{
    //
    public function getMenu()
    {
      $MenuM_var = MenuM::with('MenuItemM')->get();
      return $MenuM_var;
    }
    public function getMenuItem()
    {
      $MenuItemM_var = MenuItemM::with('RolesM','MenuM')->get();
      return $MenuItemM_var;
    }
    public function getRoles()
    {
      $RolesM_var = RolesM::with('EventsMethodM','MenuItemM')->get();
      return $RolesM_var;
    }
    public function getEvents()
    {
      $EventsMethodM_var = EventsMethodM::with('RolesM')->get();
      return $EventsMethodM_var;
    }
    public function getRules($idaccess)
    {
      $MenuData = self::getMenu()->toArray();
      $MenuItemData = self::getMenuItem()->toArray();
      $RolesData = self::getRoles()->toArray();
      $EventsData = self::getEvents()->toArray();
      $AuthUserRoleData = self::UsergetRules($idaccess);
      $res = array(
        'MenuData'=>$MenuData,
        'MenuItemData'=>$MenuItemData,
        'RolesData'=>$RolesData,
        'EventsData'=>$EventsData,
        'AuthUserRoleData' =>$AuthUserRoleData,
      );
      return $res;
    }
    public function AuthgetRules()
    {
      if(Auth::check())
      {
        $res = self::getRules(Auth::user()->id);
        return $res;
      }
      else {
        $res = array(
          'MenuData'=> [],
          'MenuItemData'=>[],
          'RolesData'=>[],
          'EventsData'=>[],
          'AuthUserRoleData' =>[],
        );
        return $res;
      }
    }
    public function UsergetRules($user_id)
    {
      $RolesPermissionM_var = RolesPermissionM::where('user_id','=',$user_id)->get()->toArray();
      return $RolesPermissionM_var;
    }
    public function userstore(Request $request)
    {
      //
      $res = self::getRoles();
      if(!empty($res))
      {
        $id = decrypt($request->input('_access'));
        $Data = $request->toArray();
        //var_dump($Data);
        $inp_roles_id = "";
        $inp_active_id = "";
        for($m=0;$m<count($res);$m++)
        {
          $idmenuItem = $res[$m]['id'];
          $key = 'checkbox_'.$idmenuItem;

          if(array_key_exists($key, $Data))
          {

            $inp_roles_id .= $idmenuItem . ',';
            if($Data[$key] == 'on')
            {
              $inp_active_id .= 1 .',';
            }
            else {
              $inp_active_id .= 0 .',';
            }

          }
          else {
            $inp_roles_id .= $idmenuItem . ',';
            $inp_active_id .= 0 .',';
          }
        }
        $RolesPermissionMData = RolesPermissionM::where('user_id','=',$id)->get()->toArray();
        if(!empty($RolesPermissionMData))
        {
          $RolesPermissionMID = $RolesPermissionMData[0]['id'];
          if($RolesPermissionM_var = RolesPermissionM::find($RolesPermissionMID))
          {
            $RolesPermissionM_var->roles_id = $inp_roles_id;
            $RolesPermissionM_var->active = $inp_active_id;
            if($RolesPermissionM_var->save())
            {
              Session::push('setpermission_result','success');
              Session::push('setpermission_result','Successfully Update Data');
            }
            else {
              Session::push('setpermission_result','failed');
              Session::push('setpermission_result','Failed Update Data');
            }
          }
          else {
            $RolesPermissionM_var = new RolesPermissionM();
            $RolesPermissionM_var->user_id = $id;
            $RolesPermissionM_var->roles_id = $inp_roles_id;
            $RolesPermissionM_var->active = $inp_active_id;
            if($RolesPermissionM_var->save())
            {
              Session::push('setpermission_result','success');
              Session::push('setpermission_result','Successfully Saved Data');
            }
            else {
              Session::push('setpermission_result','failed');
              Session::push('setpermission_result','Failed Saved Data');
            }
          }

          return redirect('ipanel/permissions?_access='.encrypt($id));
        }
        else {
          $RolesPermissionM_var = new RolesPermissionM();
          $RolesPermissionM_var->user_id = $id;
          $RolesPermissionM_var->roles_id = $inp_roles_id;
          $RolesPermissionM_var->active = $inp_active_id;
          if($RolesPermissionM_var->save())
          {
            Session::push('changepasswordaction_result','success');
            Session::push('changepasswordaction_result','Successfully Saved Data');
          }
          else {
            Session::push('changepasswordaction_result','failed');
            Session::push('changepasswordaction_result','Failed Saved Data');
          }
          return redirect('ipanel/permissions?_access='.encrypt($id));
        }


      }

    }
}
