<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\CountryM;


class CountryControl extends Controller
{
  public function getRules()
  {
    $RolesController_var = new RolesController();
    $res = $RolesController_var->AuthgetRules();
    return $res;
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $varCountryData = CountryM::get();
        return $varCountryData;
    }
    public function indexwithCity()
    {
      $varCountryData = CountryM::with('CityM')->get();
      return $varCountryData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $inp_status = $request->input('opt_status');
      $inp_name = $request->input('txt_cname');
      $inp_name_code = $request->input('txt_cncodename');
      $inp_c_code = $request->input('txt_cpcodename');
      $inp_c_shippmnet = $request->input('txt_cshippment');
      $validation = Validator::make($request->all(),CountryM::$rules,[],CountryM::$niceNames);
      //return the error messages if any
      if ($validation->fails())
      {
        return redirect('ipanel/addnewcountry')->withErrors($validation)->withInput();
      }
      else {
        $CountryM_var = new CountryM();
        //validate tha data from the request
        $CountryM_var->name = $inp_name;
        $CountryM_var->status = $inp_status;
        $CountryM_var->name_code = $inp_name_code;
        $CountryM_var->c_code = $inp_c_code;
        $CountryM_var->shippmentcost = $inp_c_shippmnet;
        if($CountryM_var->save())
        {
          Session::push('countryaction_result','success');
          Session::push('countryaction_result','Successfully Saved Data');
          return redirect('ipanel/addnewcountry');
        }
        else {
          Session::push('countryaction_result','failed');
          Session::push('countryaction_result','Failed Saved Data');
          return redirect('ipanel/addnewcountry');
        }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($CountryM_var=CountryM::find($id))
      {
        return $CountryM_var;
      }
      else {
        $CountryM_var = "";
        return $CountryM_var;
      }
    }

    public function showData($id)
    {
      if($CountryM_var=CountryM::find($id))
      {
        return $CountryM_var;
      }
      else {
        $CountryM_var = "";
        return $CountryM_var;
      }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $RuleMData = self::getRules();
      $CountryControlvar = CountryControl::show($request)->toArray();
      if(count($CountryControlvar) > 0)
      {

        return view('ipanel.pages.CapitalManagement.managecountry.edit-country',[
          'CountryControlvar'=>$CountryControlvar,
          'RuleMData'=>$RuleMData,
          'activepage'=>encrypt(17),
          'activemenu'=>encrypt(9)
        ]);
      }
      else {
        $UnitOfWork_var = new UnitOfWork();
        return $UnitOfWork_var->ManageCountryIndex();
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $id = $request->input('_access');
      //echo $id;
      if($id != "")
      {
        $id = decrypt($id);
        if($CountryM_var=CountryM::find($id))
        {
          $inp_status = $request->input('opt_status');
          $inp_name = $request->input('txt_cname');
          $inp_name_code = $request->input('txt_cncodename');
          $inp_c_code = $request->input('txt_cpcodename');
          $inp_c_shippmnet = $request->input('txt_cshippment');
          //validate tha data from the request
          $validation = Validator::make($request->all(),CountryM::$rules_upd,[],CountryM::$niceNames);
          //return the error messages if any
          if ($validation->fails())
          {
            return redirect('ipanel/showcutsomcountry?id='.encrypt($id))->withErrors($validation)->withInput();
          }
          else {
            $CountryM_var->name = $inp_name;
            $CountryM_var->status = $inp_status;
            $CountryM_var->name_code = $inp_name_code;
            $CountryM_var->c_code = $inp_c_code;
            $CountryM_var->shippmentcost =$inp_c_shippmnet;
            if($CountryM_var->save())
            {
              Session::push('countryaction_result','success');
              Session::push('countryaction_result','Successfully Update Data');
            }
            else {
              Session::push('countryaction_result','failed');
              Session::push('countryaction_result','Failed Update Data');

            }
            return redirect('ipanel/showcutsomcountry?id='.encrypt($id));
          }

        }
        else {
          $UnitOfWork_var = new UnitOfWork();
          return $UnitOfWork_var->ManageCountryIndex();
        }
      }
      else {
        $UnitOfWork_var = new UnitOfWork();
        return $UnitOfWork_var->ManageCountryIndex();
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
