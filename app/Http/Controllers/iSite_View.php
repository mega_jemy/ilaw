<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use ilaw\Http\Controllers\TrainingController;
use ilaw\Http\Controllers\CountryControl;
use ilaw\Http\Controllers\BannerControl;
use ilaw\Http\Controllers\CommonPagesControl;
use ilaw\Http\Controllers\LanguageController;
use ilaw\Http\Controllers\BlogControl;
use ilaw\Http\Controllers\ServiceController;
use ilaw\Http\Controllers\AboutController;
use LaravelLocalization;
use Auth;
class iSite_View extends Controller
{
    //
    public function getBlogData()
    {
      $BlogControl_var = new BlogControl();
      $BlogData = $BlogControl_var->indexPaginate();
      return $BlogData;
    }

    public function getLangData()
    {
      $LanguageController_var = new LanguageController();
      $LangData = $LanguageController_var->getActiveLanguage();
      return $LangData;
    }
    public function getLangDataByRegional($regional)
    {
      $LanguageController_var = new LanguageController();
      $LangData = $LanguageController_var->getActiveLanguageByRegional($regional);
      return $LangData;
    }
    public function getServiceData()
    {
      $ServiceController_var = new ServiceController();
      $LangData = $ServiceController_var->index();
      return $LangData;
    }
    public function getAboutDataByRegional($regional)
    {
      $AboutController_var = new AboutController();
      $AboutData = $AboutController_var->indexByRegional($regional);
      return $AboutData;
    }
    public function getTrainingCourse($regional)
    {

      $TrainingController_var = new TrainingController();
      $trainigData = $TrainingController_var->indexByregional($regional);
      return $trainigData;
    }
    public function getIdPages($page)
    {
      switch ($page) {
        case 'bussines_setup':
          return 1;
          break;
        case 'intellectual_property':
          return 2;
          break;
        case 'debt_collection':
          return 3;
          break;
        case 'litigation_dispute':
          return 4;
          break;
        case 'home':
          return 4;
          break;
        case 'courses':
          return 5;
          break;
        case 'club':
          return 6;
          break;
        default:
          // code...
          break;
      }
    }
    public function getBanner($CurrentPages)
    {
      $idpages = self::getIdPages($CurrentPages);
      $BannerControlData_var = new BannerControl();
      $BannerData = $BannerControlData_var->getBannerByIdPage($idpages);
      return $BannerData;

    }
    public function commenMethod()
    {

      $CountryControl  = new  CountryControl();
      $country_var = $CountryControl->index();
      return $country_var;
    }
    public function getCommonPages($CurrentPages)
    {
      $CommonPagesControl_var  = new  CommonPagesControl();
      $CPCData = $CommonPagesControl_var->index($CurrentPages)->where('softdelete','=','Not-delete')->where('active','=','active');

      return $CPCData;

    }
    #-----------------------Start Fixed Functions--------------------------------------
    public function getTrainingCoursesDataSortedByOrder($regional)
    {
      $id = self::getLangDataByRegional($regional)->toArray()[0]['id'];
      $TrainingController_var = new TrainingController();
      $TrainingControllerData = $TrainingController_var->indexByregional($id)->sortBy('order');
      return $TrainingControllerData;
    }
    public function getTrainingCoursesDataSortedByOrderById($id)
    {
      $TrainingController_var = new TrainingController();
      $TrainingControllerData = $TrainingController_var->index()->where('id','=',$id);
      return $TrainingControllerData;
    }
    #-----------------------End Fixed Functions----------------------------------------


    #-----------------------Start Home page--------------------------------------
    public function getHomePage()
    {
      $getBannerData = self::getBanner('home');
      $getServiceData = self::getServiceData()->toArray();
      $getTrainingCourse = self::getTrainingCourse(LaravelLocalization::getCurrentLocale())->toArray();
      $getDataLnag = self::getLangDataByRegional(LaravelLocalization::getCurrentLocale())->toArray();
      return view('isite.pages.index',[
        'activepage'=>1,
        'getBannerData'=>$getBannerData,
        'getServiceData'=>$getServiceData,
        'getDataLnag'=> $getDataLnag,
        'getTrainingCourse'=>$getTrainingCourse
      ]);
    }
    #-----------------------end Home page--------------------------------------

    #-----------------------Start Services page--------------------------------------
    public function getServicesPage()
    {
      return view('isite.pages.services',[
        'activepage'=>2,
      ]);
    }
    #-----------------------end Services page--------------------------------------

    #-----------------------Start Firm page--------------------------------------
    #3,4,5,6,7

    public function getFirmPage()
    {
      return view('isite.pages.Firm.firm',[
        'activepage'=>3,
      ]);
    }
    public function getOnlineConsultacyPage()
    {
      return view('isite.pages.Firm.online_consultacy',[
        'activepage'=>3,
      ]);
    }
    public function getBussinessSetupPage()
    {
      $getCPData = self::getCommonPages('bussines_setup');
      $getLangData = self::getLangData();
      return view('isite.pages.Firm.bussiness_setup',[
        'getCPData' =>$getCPData->toArray(),
        'getLangData'=>$getLangData,
        'activepage'=>4,
      ]);
    }

    public function getDebtCollectionPage()
    {
      $getCPData = self::getCommonPages('debt_collection');
      $getLangData = self::getLangData();
      return view('isite.pages.Firm.debt_collection',[
        'getCPData' =>$getCPData->toArray(),
        'getLangData'=>$getLangData,
        'activepage'=>5,
      ]);
    }
    public function getLitigationDisputePage()
    {
      $getCPData = self::getCommonPages('litigation_dispute');
      $getLangData = self::getLangData();
      return view('isite.pages.Firm.litigation_dispute',[
        'getCPData' =>$getCPData->toArray(),
        'getLangData'=>$getLangData,
        'activepage'=>6,
      ]);
    }
    public function getIntellectualPropertyPage()
    {
      $getCPData = self::getCommonPages('intellectual_property');
      $getLangData = self::getLangData();
      return view('isite.pages.Firm.intellectual_property',[
        'getCPData' =>$getCPData->toArray(),
        'getLangData'=>$getLangData,
        'activepage'=>6,
      ]);
    }

    #-----------------------end Firm page--------------------------------------

    #-----------------------Start Training page--------------------------------------
    #8,9
    public function getTrainingSuggestPage()
    {
      return view('isite.pages.Training.t_suggest',[
        'activepage'=>24,
        'locallang'=>LaravelLocalization::getCurrentLocale()
      ]);
    }
    public function getTrainingCoursesPage()
    {
      $TrainingCoursesData = self::getTrainingCoursesDataSortedByOrder(LaravelLocalization::getCurrentLocale());
      $getBannerData = self::getBanner('courses');

      return view('isite.pages.Training.t_courses',[
        'TrainingCoursesData'=>$TrainingCoursesData->toArray(),
        'getBannerData' => $getBannerData->toArray(),
        'activepage'=>8,
        'locallang'=>LaravelLocalization::getCurrentLocale()
      ]);
    }
    public function getTrainingCoursesDetailPage(Request $request)
    {
      $id = $request->input('id');
      $TrainingCoursesData = self::getTrainingCoursesDataSortedByOrderById($id);
      //var_dump($TrainingCoursesData->toArray());
      return view('isite.pages.Training.t_course_detail',[
        'TrainingCoursesData'=>$TrainingCoursesData->toArray(),
        'activepage'=>8,
        'locallang'=>LaravelLocalization::getCurrentLocale()
      ]);
    }
    public function getTrainingConferencesPage()
    {
      return view('isite.pages.Training.t_conference',[
        'activepage'=>9,
      ]);
    }
    #-----------------------end Training page--------------------------------------

    #-----------------------Start Club page--------------------------------------
    #10,11,12,13
    public function getClubMemberShipPage()
    {
      $getLangData = self::getLangData();
      return view('isite.pages.Club.c_membership',[
        'activepage'=>10,
        'locallang'=>LaravelLocalization::getCurrentLocale(),
        'getLangData'=>$getLangData,
      ]);
    }
    public function getiLAWClubPage()
    {
      $getBlogData = self::getBlogData();
      $getLangData = self::getLangData();
      $getBannerData = self::getBanner('club');
      return view('isite.pages.Club.ilaw_club',[
        'activepage'=>11,
        'locallang'=>LaravelLocalization::getCurrentLocale(),
        'getLangData'=>$getLangData,
        'getBlogData'=>$getBlogData,
        'getBannerData'=>$getBannerData
      ]);
    }
    public function getClubBlogPage()
    {
      $getBlogData = self::getBlogData();
      $getLangData = self::getLangData();
      return view('isite.pages.Club.blog',[
        'activepage'=>11,
        'locallang'=>LaravelLocalization::getCurrentLocale(),
        'getLangData'=>$getLangData,
        'getBlogData'=>$getBlogData
      ]);
    }
    public function getClubMediaGalleryPage()
    {
      $getLangData = self::getLangData();
      return view('isite.pages.Club.Media.gallery',[
        'activepage'=>12,
        'locallang'=>LaravelLocalization::getCurrentLocale(),
        'getLangData'=>$getLangData,
      ]);
    }
    public function getClubMediaVideosPage()
    {
      $getLangData = self::getLangData();
      return view('isite.pages.Club.Media.videos',[
        'activepage'=>13,
        'locallang'=>LaravelLocalization::getCurrentLocale(),
        'getLangData'=>$getLangData,
      ]);
    }
    #-----------------------end Club page--------------------------------------

    #-----------------------Start Care page--------------------------------------
    #16
    public function getCarePage()
    {
      $country_var = self::commenMethod();
      return view('isite.pages.Care.care',[
        "country_var" => $country_var,
        'activepage'=>16,
      ]);
    }
    #-----------------------end Care page--------------------------------------

    #-----------------------Start Careers page--------------------------------------
    #17
    public function getCareersPage()
    {
      return view('isite.pages.Careers.index',[
        'activepage'=>17,
      ]);
    }
    #-----------------------end Careers page--------------------------------------


    #-----------------------Start About page--------------------------------------
    public function getAboutPage()
    {
      $getAboutData = self::getAboutDataByRegional(LaravelLocalization::getCurrentLocale());
      return view('isite.pages.about',[
        'activepage'=>18,
        'getAboutData'=>$getAboutData,
      ]);
    }
    #-----------------------end About page--------------------------------------

    #-----------------------Start Contact page--------------------------------------
    public function getContactPage()
    {
      return view('isite.pages.contact',[
        'activepage'=>19,
      ]);
    }
    #-----------------------end Contact page--------------------------------------

    #-----------------------Start Appointment page--------------------------------------
    public function getAppointmentPage()
    {
      return view('isite.pages.appointment',[
        'activepage'=>20,
      ]);
    }
    #-----------------------end Appointment page--------------------------------------


    #-----------------------Start Footer page--------------------------------------
    public function getTermsAndConditionPage()
    {
      return view('isite.pages.Footer.terms_condition',[
        'activepage'=>21,
      ]);
    }
    public function getPrivacyPolicyPage()
    {
      return view('isite.pages.Footer.privacy_policy',[
        'activepage'=>22,
      ]);
    }
    public function getAdvertisePage()
    {
      return view('isite.pages.Footer.advertise',[
        'activepage'=>23,
      ]);
    }
    #-----------------------end Footer page--------------------------------------

    #-----------------------Start Appointment page--------------------------------------
    public function getFeedbackPage()
    {
      return view('isite.pages.feedback',[
        'activepage'=>20,
      ]);
    }
    #-----------------------end Appointment page--------------------------------------
    public function ManageLoginUser(Request $request)
    {
      if(Auth::check())
      {
        return self::getHomePage($request);
      }
      else {
        $country_var = self::commenMethod();
        return view('isite.pages.account-login',[
          'country_var'=>$country_var,
          'activepage'=>20,
        ]);
      }
    }
    public function ManageRegisterUser()
    {
      if(Auth::check())
      {
        return self::getHomePage($request);
      }
      else {
        $country_var = self::commenMethod();
        return view('isite.pages.account-register',[
          'country_var'=>$country_var,
          'activepage'=>20,
        ]);
      }
    }

}
