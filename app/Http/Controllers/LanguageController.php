<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\LanguageM;
class LanguageController extends Controller
{
    //
    public function getActiveLanguage()
    {
      $LanguageM_var = LanguageM::where('isactive','=',1)->get();
      return $LanguageM_var;
    }
    public function getActiveLanguageByRegional($regional)
    {
      $LanguageM_var = LanguageM::where('regional','=',$regional)->get();
      return $LanguageM_var;
    }
}
