<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\AppointmentReserved;
use ilaw\Model\FixedAppointment;
class AppointmentController extends Controller
{
    //
    public function getFixedAppointment()
    {
      $FixedAppointmentdata = FixedAppointment::get();
      return $FixedAppointmentdata;
    }
}
