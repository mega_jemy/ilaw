<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use Session;
use ilaw\Model\MediaM;
use ilaw\Http\Controllers\AdminView;
use DB;
class MediaControl extends Controller
{
    //
    public function index($locationbnr)
    {

      $MediaMdata = MediaM::where('typeban' ,'=',$locationbnr)->get();
      return $MediaMdata;
    }
    public function CreateStoreMedia(Request $request)
    {
      $page = $request->input('inp_idpage');
      /*$user_Data = $request->toArray();

      var_dump(collect($user_Data)->values());
      return ;*/
      DB::beginTransaction();
      try {

          $res = self::store($request);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Saved Data');
          return redirect('ipanel/add_new_image');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Saved Data');
        return redirect('ipanel/add_new_image');
      }
    }

    public function UpdateMedia(Request $request)
    {
      $user_Data = $request->toArray();
      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id =  $request->input('id');

      DB::beginTransaction();
      try {

          $res = self::update($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Updated Data');
          return redirect('ipanel/show_media_image?id='.encrypt($id));
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Update Data');
        return redirect('ipanel/show_media_image?id='.encrypt($id));
      }
    }

    public function RemoveMedia(Request $request)
    {
      $user_Data = $request->toArray();
      /*var_dump(collect($user_Data)->values());
      return ;*/
      $id = decrypt($request->input('id'));
      DB::beginTransaction();
      try {

          $res = self::destroy($request,$id);

          //$id = $res->id;
          //self::storeTrainigDet($request,$id);
          DB::commit();
          Session::push('action_result','success');
          Session::push('action_result','Successfully Remove Data');
          return redirect('ipanel/manage_media_photo');
      } catch (Exception $e) {

        DB::rollback();
        Session::push('action_result','failed');
        Session::push('action_result','Failed Remove Data');
        return redirect('ipanel/manage_media_photo');
      }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //variable gets from user
      $inp_idlang = $request->input('opt_lang');
      $inp_name = $request->input('inp_name');
      $inp_title = $request->input('inp_title');
      $inp_alt = $request->input('inp_alt');
      $inp_link = $request->input('inp_link');
      $inp_status = $request->input('opt_status');
      $inp_order = $request->input('inp_order');
      $inp_typeban = $request->input('opt_type');
      $inp_softdelete = "Not-delete";
      //create new category from table Category
      $MediaM_var = new MediaM();
      $MediaM_var->idlang = $inp_idlang;
      $MediaM_var->name = $inp_name;
      $MediaM_var->title = $inp_title;
      $MediaM_var->alt = $inp_alt;
      $MediaM_var->link = $inp_link;
      $MediaM_var->status = $inp_status;
      $MediaM_var->order = $inp_order;
      $MediaM_var->typeban = $inp_typeban;
      $MediaM_var->softdelete = $inp_softdelete;
      if($MediaM_var->save())
      {
        if(Input::file())
        {
          $image = $request->file('inp_img');
          $inp_filename  = $MediaM_var->id . '.' . strtolower($image->getClientOriginalExtension());
          $MediaM_var->img_path = $inp_filename;
          if($MediaM_var->save())
          {
            $path = 'images/media/'. $inp_filename;
            Image::make($image->getRealPath())->save($path);
            Session::push('action_result','success');
            Session::push('action_result','Successfully Store Data');
            return redirect('ipanel/add_new_image');
          }
          else {
            Session::push('action_result','failed');
            Session::push('action_result','Failed Store Data');
            return redirect('ipanel/add_new_image');
          }

        }
      }
      else {
        Session::push('action_result','failed');
        Session::push('action_result','Failed Store Data');
        return redirect('ipanel/add_new_image');
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($MediaM_var=MediaM::find($id))
      {
        return $MediaM_var;
      }
      else {
        $MediaM_var = "";
        return $MediaM_var;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $AdminView_var = new AdminView();
      $RuleMData = $AdminView_var->getRules();
      $activeLang = $AdminView_var->getLanguageIsActive();
      $MediaControl_var = self::show($request)->toArray();
      if(count($MediaControl_var) > 0)
      {
        //var_dump($CategoryControl_var->toArray());

        return view('ipanel.pages.managepages.media.images.edit_new_images',[
          'MediaControl_var'=>$MediaControl_var,
          'RuleMData'=>$RuleMData,
          'activeLang'=>$activeLang,
          'activepage'=>encrypt(2),
          'activemenu'=>encrypt(2),
        ]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $id = $request->input('id');
        $MediaM_var = [];
        if($MediaM_var=MediaM::find($id))
        {
          $inp_idlang = $request->input('opt_lang');
          $inp_name = $request->input('inp_name');
          $inp_title = $request->input('inp_title');
          $inp_alt = $request->input('inp_alt');
          $inp_link = $request->input('inp_link');
          $inp_status = $request->input('opt_status');
          $inp_order = $request->input('inp_order');
          $inp_typeban = $request->input('opt_type');

          //create new category from table Category
          $MediaM_var->idlang = $inp_idlang;
          $MediaM_var->name = $inp_name;
          $MediaM_var->title = $inp_title;
          $MediaM_var->alt = $inp_alt;
          $MediaM_var->link = $inp_link;
          $MediaM_var->status = $inp_status;
          $MediaM_var->order = $inp_order;
          $MediaM_var->typeban = $inp_typeban;
          if($MediaM_var->save())
          {
            if(Input::file())
            {
              $image = $request->file('inp_img');
              $inp_filename  = $MediaM_var->id . '.' . strtolower($image->getClientOriginalExtension());
              $MediaM_var->img_path = $inp_filename;
              if($MediaM_var->save())
              {
                $path = 'images/media/'. $inp_filename;
                Image::make($image->getRealPath())->save($path);
                Session::push('action_result','success');
                Session::push('action_result','Successfully Store Data');
                return redirect('ipanel/add_new_image');
              }
              else {
                Session::push('action_result','failed');
                Session::push('action_result','Failed Store Data');
                return redirect('ipanel/add_new_image');
              }

            }
            Session::push('action_result','success');
            Session::push('action_result','Successfully Update Data');

          }
          else {
            Session::push('action_result','failed');
            Session::push('action_result','Failed Update Data');

          }
          return $MediaM_var;
          //validate tha data from the request
          /*$validation = Validator::make($request->all(),CategoryM::$category_rules);
          //return the error messages if any
          if ($validation->fails())
          {
            return false;
          }
          else {

          }*/
        }
        else {
          Session::push('action_result','failed');
          Session::push('action_result','Failed Update Data');
          return $MediaM_var;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id = decrypt($request->input('id'));
      if($MediaM_var=MediaM::find($id))
      {
        $MediaM_var->softdelete = 'Delete';
        if($MediaM_var->save())
        {
          Session::push('action_result','success');
          Session::push('action_result','Successfully Delete Data');

        }
        else {
          Session::push('action_result','failed');
          Session::push('action_result','Failed Delete Data');


        }
        return $MediaM_var;

      }
      else {
        Session::push('action_result','failed');
        Session::push('action_result','Failed Delete Data');
        return $MediaM_var;
      }
    }
}
