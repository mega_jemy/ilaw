<?php

namespace ilaw\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\File;
use ilaw\Http\Controllers\AdminView;
use ilaw\Model\OrderPayment;
use Session;
use DB;

class TransactionPayment extends Controller
{
    //
    public function CheckValid_storeOrderUser(Request $request)
    {
      $checkvalid = 0;
      $validation = [];
      $paytype_id = $request->input('paytypeID');
      if($paytype_id == 0)//paoff
      {
        $validation = Validator::make($request->all(),OrderPayment::$rules,[],OrderPayment::$niceNames);
      }
      else if($paytype_id == 1)//ponaoff
      {
        $validation = Validator::make($request->all(),OrderPayment::$rules,[],OrderPayment::$niceNames);
      }
      $res = $validation->errors()->toArray();

      if (!empty($res))
      {
        return  json_encode(['response'=>-1,'invalid'=>$validation->errors()]);
        //redirect()->route('/en/process-check-out')->withErrors($validation)->withInput();
      }
      else {
        return json_encode(['response'=>1,'invalid'=>[]]);
      }
    }
    public function storeCoursePay(Request $request)
    {
      $user_Data = $request->toArray();

      /*var_dump(collect($user_Data)->values());
      return ;*/

      DB::beginTransaction();
      try {
        $inp_name = $user_Data['inp_name'];
        $inp_email = $user_Data['inp_email'];
        $inp_emp_at = $user_Data['inp_emp_at'];
        $inp_jobtitle = $user_Data['inp_jobtitle'];
        $inp_phone = $user_Data['inp_phone'];
        $inp_crsinfo = $user_Data['inp_crsinfo'];
        $inp_isdelete = "No";

        //create new category from table Category
        $OrderPayment_var = new OrderPayment();
        $OrderPayment_var->iduser = 1;
        $OrderPayment_var->emp_at = $inp_emp_at;
        $OrderPayment_var->jobtitle = $inp_jobtitle;
        $OrderPayment_var->sm_type = $inp_crsinfo;
        $OrderPayment_var->ispay = 'No';
        $OrderPayment_var->isdelete = $inp_isdelete;
        if($OrderPayment_var->save())
        {
          //return  json_encode(['data'=>$user_Data]);
          $id = $OrderPayment_var->id;
          DB::commit();
          return json_encode(['response'=>1,'lastID'=>$id]);
        }
        else {
          DB::rollback();
          return json_encode(['response'=>-1]);
        }

      } catch (Exception $e) {
        DB::rollback();
        return json_encode(['response'=>-1]);
      }
    }
    public function store(Request $request)
    {

    }
}
