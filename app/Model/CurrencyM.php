<?php

namespace silvercity;

use Illuminate\Database\Eloquent\Model;

class CurrencyM extends Model
{
    //
    protected $table = "currency";
    
    public function CountryM()
    {
      $new_CurrencyM = new CurrencyM;
      return $new_CurrencyM->belongsTo('ilaw\CountryM','idcountrycur','id');
    }
    public static  $rules = array(
         'opt_country' =>'bail|required|not_in:0',
         'txt_cname'=>'required|regex:/^[0-9\pL\s]+$/u|min:2|max:20',
         'txt_cbreif'=> 'required|min:1|max:20',
     );
     public static $niceNames = array(
       'opt_country' =>'Country',
       'txt_cname' =>'Currency Name',
       'txt_cbreif' =>'Currency Brief',
     );
}
