<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $table = 'banner';
    public function BannerDetM()
    {
      $new_Banner = new Banner;
      return $new_Banner->hasMany('ilaw\Model\BannerDetM','id_banner','id');
    }
    public static  $rules = array(
      'opt_pages'=>'bail|required|not_in:0',
      'opt_langID'=>'bail|required|not_in:0',
      'opt_typeID'=>'bail|required|not_in:0',
      'opt_statusID'=>'bail|required|not_in:0',
      'inp_order'=>'bail|required|numeric|unique:banner,order',
      'inp_title'=>'required|min:3|max:20',
      'inp_alt'=>'required|min:3|max:20',
      /*'inp_image_st'=>'required|max:5000|mimes:jpeg,bmp,png',
      'inp_image_nd'=>'required|max:5000|mimes:jpeg,bmp,png',*/
     );
     public static  $niceNames = array(
       'opt_langID'=>'Lnaguage',
       'opt_typeID'=>'Banner type',
       'opt_statusID'=>'Status',
       'inp_order'=>'order',
       'inp_title'=>'title',
       'inp_alt'=>'alt',
       /*'inp_image_st'=>'thumbnail background',
       'inp_image_nd'=>'thumbnail single',*/
      );
}
