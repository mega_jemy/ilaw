<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class TrainingSuggestM extends Model
{
    //
    protected $dates = ['deleted_at'];
    protected $table = 'training_suggest';
}
