<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class NewsletterM extends Model
{
    //
    protected $table = "newsletter";
    public static  $rules = array(
         'email'=>'bail|required|email',
     );
     public static $niceNames = array(
       'email' =>'Email',
     );
}
