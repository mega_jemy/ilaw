<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class CommonPagesM extends Model
{
    //
    protected $dates = ['deleted_at'];
    protected $table = 'common_pages';
}
