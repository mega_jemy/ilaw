<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class BlogM extends Model
{
    //
    protected $dates = ['deleted_at'];
    protected $table = 'blog';
}
