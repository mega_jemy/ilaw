<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceM extends Model
{
    //
    protected $dates = ['deleted_at'];
    protected $table = 'services';
    public function LanguageM()
    {
      return $this->belongsTo('ilaw\Model\LanguageM','idlang','id');
    }
    public static  $rules = array(
      'opt_lang'=>'bail|required|not_in:0',
      'inp_name'=> 'bail|required|unique:training,name|min:3|max:20',
      'inp_title'=> 'bail|required|min:3|max:20',
      'inp_desc'=>'bail|required|regex:/^[0-9\pL\s]+$/u|min:20',
      //'inp_link'=>'bail|required',
      'inp_order'=>'bail|required|numeric|unique:banner,order',
      'opt_status'=>'bail|required|not_in:0',
      'inp_oldprice'=>'bail|required|numeric',
      'inp_newprice'=>'bail|required|numeric',
      'inp_avatar'=>'bail|required',
      'opt_hotdeals'=>'bail|required|not_in:0',
      'opt_dealofmonth'=>'bail|required|not_in:0',
      'opt_shownew'=>'bail|required|not_in:0',
      'opt_showoos'=>'bail|required|not_in:0',
      'opt_showonhome'=>'bail|required|not_in:0',
     );
     public static  $niceNames = array(
       'opt_lang'=>'Lnaguage',
       'inp_name'=>'Training Course name',
       'inp_title'=>'Training Course title',
       'inp_desc'=> 'Training Course Description',
       //'inp_link'=> 'Course URL',
       'inp_order'=>'order',
       'opt_status'=>'Status',
       'inp_oldprice'=>'Old Price',
       'inp_newprice'=>'New Price',
       'inp_avatar'=>'thumbnail',
       'opt_hotdeals'=>'hot deals',
       'opt_dealofmonth'=>'deal of month',
       'opt_shownew'=>'show new product',
       'opt_showoos'=>'show product out of stock',
       'opt_showonhome'=>'show product on home',
      );
}
