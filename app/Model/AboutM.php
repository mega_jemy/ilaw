<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class AboutM extends Model
{
    //
    protected $dates = ['deleted_at'];
    protected $table = 'about';
}
