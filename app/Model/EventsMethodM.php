<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class EventsMethodM extends Model
{
    //
    protected $table = "eventmethod";
    public function RolesM()
    {
      return $this->hasMany('ilaw\Model\RolesM','events_id','id');
    }
}
