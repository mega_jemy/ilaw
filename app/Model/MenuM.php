<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class MenuM extends Model
{
    //
    protected $table = "menu";
    public function MenuItemM()
    {
      return $this->hasMany('ilaw\Model\MenuItemM','menu_id','id');
    }
}
