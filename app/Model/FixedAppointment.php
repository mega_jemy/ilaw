<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class FixedAppointment extends Model
{
    //

    protected $table = 'fixed_appointment';
    public function AppointmentReserved()
    {
      return $this->hasMany('ilaw\Model\AppointmentReserved','id_freserve','id');
    }
}
