<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class CountryM extends Model
{
    //
    protected $table = "country";

    public function CurrencyM()
    {
      $new_CountryM = new CountryM;
      return $new_CountryM->hasMany('ilaw\CurrencyM','idcountrycur','id');
    }
    public static  $rules = array(
         'txt_cname' =>'required|unique:country,name',
         'opt_status'=>'bail|required|not_in:0',
     );
     public static  $rules_upd = array(
          'txt_cname' =>'required|min:4|max:20',
          'opt_status'=>'bail|required|not_in:0',
      );
   public static  $niceNames = array(
        'txt_cname' =>'Country name',
        'opt_status'=>'Country status',
    );
}
