<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    //
    protected $dates = ['deleted_at'];
    protected $table = 'ord_payment';
    public static  $rules = array(
         'inp_name' =>'required|min:3|max:20',
         'inp_email' =>'required|email',
         'inp_emp_at' =>'required|min:3|max:20',
         'inp_jobtitle' =>'required|min:3|max:20',
         'inp_phone' =>'required|numeric',
         'inp_crsinfo'=>'bail|required|not_in:0',
     );
   public static  $niceNames = array(
     'inp_name' =>'Name',
     'inp_email' =>'E-mail',
     'inp_emp_at' =>'Employee at',
     'inp_jobtitle' =>'Job title',
     'inp_phone' =>'Phone',
     'inp_crsinfo'=>'Social media information',
    );
}
