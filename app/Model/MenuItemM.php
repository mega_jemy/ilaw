<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class MenuItemM extends Model
{
    //
    protected $table = "menu_item";
    public function RolesM()
    {
      return $this->hasMany('ilaw\Model\RolesM','menuitem_id','id');
    }
    public function MenuM()
    {
      return $this->belongsTo('ilaw\Model\MenuM','menu_id','id');
    }
}
