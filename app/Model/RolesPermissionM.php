<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class RolesPermissionM extends Model
{
    //
    protected $table = "roles_permission";
    public function Users()
    {
      return $this->belongsTo('ilaw\User','user_id','id');
    }
}
