<?php

namespace ilaw\Model;

use Illuminate\Database\Eloquent\Model;

class RolesM extends Model
{
    //
    protected $table = "roles";
    public function EventsMethodM()
    {
      return $this->belongsTo('ilaw\Model\EventsMethodM','events_id','id');
    }
    public function MenuItemM()
    {
      return $this->belongsTo('ilaw\Model\MenuItemM','events_id','id');
    }
}
