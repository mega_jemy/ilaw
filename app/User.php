<?php

namespace ilaw;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
     protected $hidden = [

     ];

     public function RolesPermissionM()
     {
       return $this->hasMany('silvercity\RolesPermissionM','user_id','id');
     }
     
     public static  $registerRules= array(
        'opt_typeacc'=>'bail|required|not_in:0',
        'opt_cityname'=>'required|regex:/^[0-9a-zA-Z\pL\s]+$/u|min:3|max:100',
        'txt_firstname'=>'bail|required|min:3|max:50',
        'txt_lastname'=>'bail|required|min:3|max:50',
        'username'=>'bail|required|max:50|min:2|unique:users',
        'txt_phone'=>'bail|required',
        'email'=>'bail|required|email|unique:users',
        'password'=>'bail|required|min:8|confirmed',
        'opt_gender'=>'bail|required|not_in:0',
        'user_image'=>'bail|required|dimensions:min_width=100,min_height=200|mimes:jpeg,bmp,png'
    );
    public static  $registerUserRules= array(
       'opt_countryname'=>'bail|required|not_in:0',
       'opt_cityname'=>'required|regex:/^[0-9a-zA-Z\pL\s]+$/u|min:3|max:100',
       'txt_firstname'=>'bail|required|min:3|max:50',
       'txt_lastname'=>'bail|required|min:3|max:50',

       'txt_phone'=>'bail|required',
       'reg_email'=>'bail|required|email|unique:users,email',
       'password'=>'bail|required|min:8|confirmed',
   );
   public static  $registerUserNiceNames= array(
     'opt_countryname'=>'Country name',
     'opt_cityname'=>'city name',
     'txt_firstname'=>'first name',
     'txt_lastname'=>'last name',
     'opt_gender'=>'gender',
     'txt_phone'=>'phone',
     'reg_email'=>'email',
     'email'=>'email',
     'password'=>'password',
  );
    public static $changepassword_rules = array(
      'txt_oldpassword'=>'bail|required|min:8|',
      'txt_newpassword'=>'bail|required|min:8|',
      'txt_confirmpassword'=>'bail|required|min:8',
    );
    public static $changepassword_niceNames = array(
      'txt_oldpassword'=>'Old Password',
      'txt_newpassword'=>'New Password',
      'txt_confirmpassword'=>'Confirm Password',
    );
    public static $loginRules =  array(
      'email'=>'bail|required|email',
      'password'=>'bail|required|min:8',
    );
    public static $loginNiceNames =  array(
      'email'=>'email',
      'password'=>'password',
    );
    public static $UpdateProfileRules =  array(
      'txt_fullname'=>'bail|required',
      //'email'=>'bail|required|min:8',
      'txt_phone'=>'bail|required',
    );
    public static  $UpdateProfileRulesrNiceNames= array(
      'txt_fullname'=>'Full Name',
      //'txt_phone'=>'phone',
      'txt_phone'=>'Phone',
   );

}
