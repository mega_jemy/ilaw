<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>  'force_https_url_scheme','prefix' => '/'],function(){
	Route::get('/ipanel', 'AdminView@loginPage');
	Route::post('authenticateadmin','UserControl@authenticate');
});
Route::group(['middleware'=> 'authenticated', 'force_https_url_scheme','prefix' => '/ipanel'],function(){
  ################################################################
	/**
	*Manage Dashboard
	*/
  ################################################################
	Route::get('dashboard','AdminView@getDashboard');
	################################################################

  ################################################################
	/**
	*Banners Management
	*/
  ################################################################
	/**
	*Banner
	*/
	Route::get('/showbanner','AdminView@ManageBannerIndex');
	Route::get('/addnewbanner', 'AdminView@AddNewBannerIndex');
	Route::get('showcutsombanner','BannerControl@edit');
	Route::get('/deletebanner','BannerControl@destroy');
	Route::post('/savenewbanner','BannerControl@store');
	Route::post('/editnewbanner','BannerControl@update');
  Route::get('/banner_chkvalid','BannerControl@store_Chk_Valid');
	################################################################

	################################################################
	/**
	*Training Management
	*/
  ################################################################
	/**
	*training course
	*/
	Route::get('/addnewtrainingcourse', 'AdminView@AddNewTrainingIndex');
	Route::get('show_training','TrainingController@edit');
	Route::get('/delete_training','TrainingController@RemoveTraining');
	Route::post('/savenewtrainingcourse','TrainingController@CreateStoreTraining');
	Route::post('/edit_training','TrainingController@UpdateTraining');
  Route::get('/training_chkvalid','TrainingController@store_Chk_Valid');
	################################################################

	################################################################
	/**
	*Service Management
	*/
  ################################################################
	/**
	*service course
	*/
	Route::get('/addnewservice', 'AdminView@AddNewServiceIndex');
	Route::get('/show_service','ServiceController@edit');
	Route::get('/delete_service','ServiceController@RemoveService');
	Route::post('/savenewservice','ServiceController@CreateStoreService');
	Route::post('/edit_service','ServiceController@UpdateService');
	################################################################

	################################################################
	/**
	*iClub Management
	*/
  ################################################################
	/**
	*iClub
	*/
	Route::get('/addnewiclub', 'AdminView@AddNewiClubIndex');
	Route::get('/showiclub','iClubController@edit');
	Route::get('/deleteiclub','iClubController@RemoveiClub');
	Route::post('/savenewiclub','iClubController@CreateStoreiClub');
	Route::post('/editiclub','iClubController@UpdateiClub');
	################################################################


	################################################################
	/**
	*About Management
	*/
  ################################################################
	/**
	*About
	*/
	Route::get('/addnewabout', 'AdminView@AddNewAboutIndex');
	Route::get('/showabout','AboutController@edit');
	Route::get('/deleteabout','AboutController@RemoveAbout');
	Route::post('/savenewabout','AboutController@CreateStoreAbout');
	Route::post('/editabout','AboutController@UpdateAbout');
	################################################################

	################################################################
	/**
	*Common Pages Management
	*/
  ################################################################
	/**
	*Common Pages
	*/
	Route::post('/save_commonpages','CommonPagesControl@CreateStoreCommon');
	Route::post('/edit_commonpages','CommonPagesControl@UpdateCommon');
	Route::get('/delete_commonpages','CommonPagesControl@RemoveCommon');
	################################################################


	################################################################
	/**
	*Bussines Setup Management
	*/
  ################################################################
	/**
	*Bussines Setup
	*/
	Route::get('/add_new_bussines_setup', 'AdminView@AddNewBussinessSetupIndex');
	Route::get('/show_bussines_setup','CommonPagesControl@edit');
	################################################################

	################################################################
	/**
	*DebtCollection Management
	*/
  ################################################################
	/**
	*Debt Collection
	*/
	Route::get('/add_new_debt_collection', 'AdminView@AddNewDebtCollectionIndex');
	Route::get('/show_debt_collection','CommonPagesControl@edit');
	################################################################

	################################################################
	/**
	*Intellectual Property Management
	*/
  ################################################################
	/**
	*Intellectual Property
	*/
	Route::get('/add_new_intellectual_property', 'AdminView@AddNewIntellectualPropertyIndex');
	Route::get('/show_intellectual_property','CommonPagesControl@edit');
	################################################################

	################################################################
	/**
	*DebtCollection Management
	*/
  ################################################################
	/**
	*DebtCollection
	*/
	Route::get('/add_new_litigation_dispute', 'AdminView@AddNewLitigationDisputeIndex');
	Route::get('/show_litigation_dispute','CommonPagesControl@edit');
	################################################################

	/**
	*Newletter Management
	*/
  ################################################################
	/**
	*newsletter
	*/
	Route::get('shownewsletter','AdminView@ManageNewsletterIndex');
	Route::get('/deletenewsletter','NewsletterControl@destroy');

	################################################################
	/**
	*Media Management
	*/
	################################################################
	Route::get('/delete_media','MediaControl@RemoveMedia');
	Route::post('/save_new_media','MediaControl@CreateStoreMedia');
	Route::post('/edit_media_image','MediaControl@UpdateMedia');
	/**
	*Image
	*/
	Route::get('/add_new_image', 'AdminView@AddNewMediaImageIndex');
	Route::get('/show_media_image','MediaControl@edit');
	/**
	*Videos
	*/
	Route::get('/add_new_videos', 'AdminView@AddNewMediaVideosIndex');
	Route::get('/show_media_videos','MediaControl@edit');


	################################################################

	################################################################
	/**
	*Blog Management
	*/
	################################################################
	Route::get('/delete_blog','BlogControl@RemoveBlog');
	Route::post('/save_new_blog','BlogControl@CreateStoreBlog');
	Route::post('/edit_blog','BlogControl@UpdateBlog');
	Route::get('/add_new_blog', 'AdminView@AddNewBlogIndex');
	Route::get('/show_blog','BlogControl@edit');



	################################################################


	################################################################
	/**
	*Enquiries Management
	*/
  ################################################################
	/**
	*enquiries message
	*/
	Route::get('showcontactus','AdminView@ManageContactUsIndex');
	Route::get('showenquiriesmessage','AdminView@ManageEnquiriesMessageIndex');
	Route::get('/deleteenquiriesmessage','EnquiriesMessageControl@destroy');

	################################################################
	/**
	*Capital Management
	*/
  ################################################################
	/**
	*Country
	*/
	Route::get('/showcountry','AdminView@ManageCountryIndex');
	Route::get('/addnewcountry', 'AdminView@AddNewCountryIndex');
	Route::get('showcutsomcountry','CountryControl@edit');
	Route::get('/deletecountry','CountryControl@destroy');
	Route::post('/addnewcountry','CountryControl@store');
	Route::post('/editcountry','CountryControl@update');
	################################################################
	################################################################

	/**
	*Booking  Management
	*/
  ################################################################
	/**
	*Booking
	*/

	################################################################
	/**
	*Booking fixed event
	*/
	Route::get('/addnewfixedevent', 'AdminView@ManageAddFiexdBookingIndex');
	Route::get('showcustomfixedevent','CountryControl@edit');
	Route::get('/deletefixedevent','CountryControl@destroy');
	Route::post('/addnewfixedevent','CountryControl@store');
	Route::post('/editfixedevent','CountryControl@update');
	################################################################
	/**
	*User Management
	*/
  ################################################################
	Route::get('showuser','AdminView@ManageUserIndex');
	Route::get('showadminuser','AdminView@ManageAdminUserIndex');
	Route::get('/addnewadminuser', 'AdminView@addNewAdminUserIndex');
	Route::get('showspecificuser','UserControl@edit');
	Route::get('/deleteuser','UserControl@destroy');
	Route::get('/permissions','AdminView@ManageAdminUserPermissionsIndex');
	Route::post('setpermission','RolesController@userstore');
	Route::get('createuser','AdminView@userindex');
	Route::get('/change-password','AdminView@ManageChangePassword');
	Route::post('/addnewadminuser','UserControl@store');
	Route::post('/editnewadminuser','UserControl@update');
	Route::post('changepassword','UserControl@ConfirmChangePassword');
	################################################################

	################################################################
	/**
	*Page Management
	*/
  ################################################################
	Route::get('/manage_firm','AdminView@ManageFirmIndex');
	Route::get('/manage_services','AdminView@ManageServicesIndex');
	Route::get('/manage_club','AdminView@ManageClubIndex');
	Route::get('/manage_care','AdminView@ManageCareIndex');
	Route::get('/manage_blog','AdminView@ManageBlogIndex');
	Route::get('/manage_media_photo','AdminView@ManageMediaImagesIndex');
	Route::get('/manage_media_video','AdminView@ManageMediaVideosIndex');
	Route::get('/manage_confernece','AdminView@ManageConferenceIndex');
	Route::get('/manage_training','AdminView@ManageTrainingIndex');
	Route::get('/manage_about','AdminView@ManageAboutIndex');
	Route::get('/manage_bussines_setup','AdminView@ManageBussinesSetupIndex');
	Route::get('/manage_intellectual_property','AdminView@ManageIntellectualPropertyIndex');
	Route::get('/manage_debt_collection','AdminView@ManageDebtCollectionIndex');
	Route::get('/manage_litigation_dispute','AdminView@ManageLitigationDisputeIndex');
	Route::get('/manage_booking','AdminView@ManageBookingIndex');
	Route::get('/manage_fixed_booking','AdminView@ManageFiexdBookingIndex');
	Route::get('/pay_page','AdminView@ManagePayPageIndex');

});

Auth::routes();
Route::group([
	'prefix' => LaravelLocalization::setLocale(),

	'middleware' => [ 'localize', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function()
{
	Route::get('/', 'iSite_View@getHomePage');
	Route::get('/services', 'iSite_View@getServicesPage');
	Route::get('/firm', 'iSite_View@getFirmPage');
	Route::get('/online_consultacy', 'iSite_View@getOnlineConsultacyPage');
	Route::get('/bussiness_setup', 'iSite_View@getBussinessSetupPage');
	Route::get('/debt_collection', 'iSite_View@getDebtCollectionPage');
	Route::get('/intellectual_property', 'iSite_View@getIntellectualPropertyPage');
	Route::get('/litigation_dispute', 'iSite_View@getLitigationDisputePage');
	Route::get('/training_courses', 'iSite_View@getTrainingCoursesPage');
	Route::get('/training_courses_detail', 'iSite_View@getTrainingCoursesDetailPage');
	Route::get('/training_conferences', 'iSite_View@getTrainingConferencesPage');
	Route::get('/training_suggest', 'iSite_View@getTrainingSuggestPage');
	Route::get('/club_membership', 'iSite_View@getClubMemberShipPage');

	Route::get('/ilaw_club', 'iSite_View@getiLAWClubPage');
	Route::get('/club_blog', 'iSite_View@getClubBlogPage');
	Route::get('/club_media_gallery', 'iSite_View@getClubMediaGalleryPage');
	Route::get('/club_media_videos', 'iSite_View@getClubMediaVideosPage');
	Route::get('/ilaw_care', 'iSite_View@getCarePage');
	Route::get('/ilaw_careers', 'iSite_View@getCareersPage');
	Route::get('/about_us', 'iSite_View@getAboutPage');
	Route::get('/contact_us', 'iSite_View@getContactPage');
	Route::get('/appointment', 'iSite_View@getAppointmentPage');
	Route::get('/terms_and_condition', 'iSite_View@getTermsAndConditionPage');
	Route::get('/privacy_policy', 'iSite_View@getPrivacyPolicyPage');
	Route::get('/advertise', 'iSite_View@getAdvertisePage');
	Route::get('/ilaw_feedback', 'iSite_View@getFeedbackPage');


	Route::get('/login', array('uses' => 'iSite_View@ManageLoginUser'));
	Route::get('/register',array('uses' => 'iSite_View@ManageRegisterUser'));
});


/*
* Post Method
*
*/
Route::group([
	'prefix' => LaravelLocalization::setLocale(),
], function()
{
Route::get('/storenewsletter','NewsletterControl@store');
Route::post('/storenewenquiries','EnquiriesMessageControl@store');
Route::post('/store_training_course','TrainingSuggestControl@store');
Route::post('/storeilawcare','IlawCareController@store');
Route::post('register-new-user','UserControl@create');
Route::post('/chk_pay_course_fileds','TransactionPayment@CheckValid_storeOrderUser');
Route::post('/store_pay_course','TransactionPayment@storeCoursePay');
});
