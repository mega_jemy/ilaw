var main_URL = '/';
var Locallang = document.documentElement.lang+'/';
var newsletter_URL = 'storenewsletter';
var printError = function(error, explicit) {
    console.log(`[${explicit ? 'EXPLICIT' : 'INEXPLICIT'}] ${error.name}: ${error.message}`);
}
function saveNewsletter()
{
  try {
    var email = $('#newsletter').val();
    var _token = $('input[name = "_token"]').val();
    //alert(_token);
    action = 'setproductcart';
    data = {
      action:action ,
      _token:_token,
      Locallang:Locallang,
      email:email
    };

    var route_url = main_URL + Locallang +  newsletter_URL;
    alert(route_url);
    ajax_opertaion_func('GET',route_url,'json',action,data);
  } catch (e) {
    if (e instanceof SyntaxError) {
      printError(e, true);
    } else {
        printError(e, false);
    }
  } finally {

  }
}

function ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data)
{
  try {
    $.ajax({

      type: type_name,
      url: route_path_name,
      // The 'contentType' property sets the 'Content-Type' header.
      // The JQuery default for this property is
      // 'application/x-www-form-urlencoded; charset=UTF-8', which does not trigger
      // a preflight. If you set this value to anything other than
      // application/x-www-form-urlencoded, multipart/form-data, or text/plain,
      // you will trigger a preflight request.
      //contentType: 'text/javascript',
      xhrFields: {
        // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
        // This can be used to set the 'withCredentials' property.
        // Set the value to 'true' if you'd like to pass cookies to the server.
        // If this is enabled, your server must respond with the header
        //'Access-Control-Allow-Credentials: true'.
        withCredentials: false
      },
      headers: {
        // Set any custom headers here.
        // If you set any non-simple headers, your server must include these
        // headers in the 'Access-Control-Allow-Headers' response header.
      },
      async: false,
      //processData:false,
      dataType: data_type_name,
      cache:false,
      data : data,
      success: function(response){
        alert('after: '+JSON.stringify(response));
        //console.log(JSON.stringify(response));
        var parsedData = JSON.stringify(data);
        obj = JSON.parse(parsedData);
        action = obj.action;

        if(action == "setproductcart")
        {
          var LocalID = obj.LocalID;

          var parsedData = JSON.stringify(response);

          obj = JSON.parse(parsedData);
          var idstore = obj.idvirtual;
          if(Locallang == "ar"){
            title = "النتيجه";
            message = 'تمت الاضافه للعربه انتظر لعمليه اخرى';
          }
          else {
            title = "Result";
            message = 'Added to Cart wait for another process';
          }
          ResponseMessage(0,title,message);
        }



        /********************************************************************************/
      },
      error:function (XMLHttpRequest, textStatus, errorThrown,response) {
        var parsedData = JSON.stringify(data);
        obj = JSON.parse(parsedData);
        LocalID = obj.LocalID;
        if(LocalID == "en" || LocalID == 0)
        {
          title = "Result";
          message = 'Error happened, Please Try agin later';
        }
        else if(LocalID == "ar" || LocalID == 1){
          title = "النتيجه";
          message = 'حدث خطأ من فضلك حاول مره اخرى';
        }
        else {
          title = "Result";
          message = 'Error happened, Please Try agin later';
        }
        ResponseMessage(1,title,message);
        //alert('after: '+JSON.stringify(response));
        //alert( "Error" );
        //console.clear();
        //console.log(errorThrown);
        //console.log(arguments);
        //console.log (XMLHttpRequest, textStatus, errorThrown);

        }

    });
  } catch (e) {
    if (e instanceof SyntaxError) {
      printError(e, true);
    } else {
        printError(e, false);
    }

  } finally {

  }
}

function ResponseMessage(num,title,message)
{
  if(num == 0)
  {
    iziToast.success({
      theme: 'dark',
      icon: 'icon-circle-check',
      title: title,
      message: message,
      position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
      progressBarColor: 'rgb(0, 255, 184)',
    });
  }
  else if (num == 1) {
    iziToast.error({
      theme: 'dark',
      icon: 'icon-circle-check',
      title: title,
      message: message,
      position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
      progressBarColor: 'rgb(0, 255, 184)',
    });
  }
}
