var main_route = '/demo/';
var chk_valid_route = 'chk_pay_course_fileds';
var save_route = 'store_pay_course';
var merchant_id = '7000670';
  var amount_var = 1200;
  var currency_var  = 'AED';
  var description_var;
  var street_var;
  var city_var;
  var country_var;
  var m_name_var;
  var session_id;
  var id_var;
  var email_var;
  var phone_var;
  var counter = 0;
  var printError = function(error, explicit) {
    console.log(`[${explicit ? 'EXPLICIT' : 'INEXPLICIT'}] ${error.name}: ${error.message}`);
}
$(document).ready(function(){
  $(".validationFields").click(function(e){
    var paytypeID = 1;
    var _token = $('input[name = "_token"]').val();
    var inp_name = $("#inp_name").val();
    var inp_email = $("#inp_email").val();
    var inp_emp_at = $("#inp_emp_at").val();
    var inp_jobtitle = $("#inp_jobtitle").val();
    var inp_phone = $("#inp_phone").val();
    var inp_crsinfo = $("#inp_crsinfo").val();
    action = "act_chk_saveReserve";
    data={
      action:action,
      _token:_token,
      inp_name:inp_name,
      inp_email:inp_email,
      inp_emp_at:inp_emp_at,
      inp_jobtitle:inp_jobtitle,
      inp_phone:inp_phone,
      inp_crsinfo:inp_crsinfo,
      paytypeID:paytypeID
    };
    var route_url = main_route + chk_valid_route;
    ajax_opertaion_func('post',route_url,'json',action,data);
  });
  function ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data)
  {

    try {
      alert('before: '+JSON.stringify(data));
      //Ajax function
      $.ajax({
        type: type_name,
        url: route_path_name,
        //headers:{'X-Requested-With':"XMLHttpRequest"},
        //contentType: false,
        async: true,
        //processData:false,
        dataType: data_type_name,
        cache:false,
        data : data,
        //beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));},
        success: function(response){
          alert('after: '+JSON.stringify(response));
          var parsedData = JSON.stringify(data);
          obj = JSON.parse(parsedData);
          action = obj.action;

          if (action == "getcreatesession"){
            var parsedData = JSON.stringify(response);
            obj = JSON.parse(parsedData);
            console.log(JSON.stringify(response));
            if(obj.repsonse == 1)
            {
              var resultorg = obj.result;
              if(resultorg == "SUCCESS")
              {
                var sID = obj.sessionid;
                //alert(sID);
                $("#sessionID").val(sID);
                Checkout.showLightbox();
                //Checkout.showPaymentPage();
              }
            }

          }
          else if (action == "act_chk_saveReserve") {
            var parsedData = JSON.stringify(response);
            obj = JSON.parse(parsedData);
            var res = obj.response;
            //alert(res);
            if(res == -1)
            {
              var inp_name_valid = obj.inp_name;
              var inp_email_valid = obj.inp_email;
              var inp_emp_at_valid = obj.inp_emp_at;
              var inp_jobtitle_valid = obj.inp_jobtitle;
              var inp_phone_valid = obj.inp_phone;
              var inp_crsinfo_valid = obj.inp_crsinfo;
              if(inp_name_valid != "")
              {
                $(".span_name").html("<span class='label label-danger'>Name Field is Required</span>");
              }
              else {
                $(".span_name").html("");
              }

              if(inp_email_valid != "")
              {
                $(".span_email").html("<span class='label label-danger'>Email Field is Required</span>");
              }
              else {
                $(".span_email").html("");
              }
              if(inp_emp_at_valid != "")
              {
                $(".span_emp_at").html("<span class='label label-danger'>Empolyer at Field is Required</span>");
              }
              else {
                $(".span_emp_at").html("");

              }
              if(inp_jobtitle_valid != "")
              {
                $(".span_jobtitle").html("<span class='label label-danger'>Job Title Field is Required</span>");
              }
              else {
                $(".span_jobtitle").html("");

              }
              if(inp_phone_valid != "")
              {
                $(".span_phone").html("<span class='label label-danger'>Phone number Field is Required</span>");
              }
              else {
                $(".span_phone").html("");
              }
              if(inp_crsinfo_valid != "")
              {
                $(".span_crsinfo").html("<span class='label label-danger'>Social media info Field is Required</span>");
              }
              else {
                $(".span_crsinfo").html("");
              }
              //resrv_type == 0 ||
              if ( inp_name_valid != "" || inp_email_valid != "" || inp_emp_at_valid != "" ||
              inp_jobtitle_valid != "" || inp_phone_valid != "" || inp_crsinfo_valid != "") {

              }
              else {
                $(".span_name").html("");
                $(".span_email").html("");
                $(".span_emp_at").html("");
                $(".span_jobtitle").html("");
                $(".span_phone").html("");
                $(".span_crsinfo").html("");
                saveReserve(0);
              }
            }
            else {
              $(".span_name").html("");
              $(".span_email").html("");
              $(".span_emp_at").html("");
              $(".span_jobtitle").html("");
              $(".span_phone").html("");
              $(".span_crsinfo").html("");
              saveReserve(0);
            }
          }
          else if (action == "act_saveReserve") {
            var isltr = obj.islater;
            alert(isltr);
            var parsedData = JSON.stringify(response);
            obj = JSON.parse(parsedData);
            //console.log(JSON.stringify(response['response']));
            //alert(obj.response['response']);
            var res = obj.response;
            alert(res);
            if(res == 1)
            {
              var sID = obj.lastID;
              alert(sID);
              $("#lastID").val(sID);
              //alert(JSON.stringify(response['response']));
              //getSession();
              if(isltr == 0)
              {
                Checkout.showLightbox();
              }
              else if(isltr == 1){
                window.location = "paylater.php"
              }

            }
          }

          /********************************************************************************/
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
          //alert( "Error" );
          //console.log(errorThrown);
          //console.log(arguments);
          //console.log (XMLHttpRequest, textStatus, errorThrown);
          }
      });
    } catch (e) {
      if (e instanceof SyntaxError) {
        printError(e, true);
      } else {
        printError(e, false);
      }
    } finally {

    }
  }
  /****************************************************************************************************/
  function saveReserve(islater)
  {
    var paytypeID = 1;
    var _token = $('input[name = "_token"]').val();
    var inp_name = $("#inp_name").val();
    var inp_email = $("#inp_email").val();
    var inp_emp_at = $("#inp_emp_at").val();
    var inp_jobtitle = $("#inp_jobtitle").val();
    var inp_phone = $("#inp_phone").val();
    var inp_crsinfo = $("#inp_crsinfo").val();
    action = "act_saveReserve";
    data={
      action:action,
      _token:_token,
      inp_name:inp_name,
      inp_email:inp_email,
      inp_emp_at:inp_emp_at,
      inp_jobtitle:inp_jobtitle,
      inp_phone:inp_phone,
      inp_crsinfo:inp_crsinfo,
      paytypeID:paytypeID,
      islater:islater
    };
    var route_url = main_route + save_route;
    ajax_opertaion_func('post',route_url,'json',action,data);
  }
  /****************************************************************************************************/
  function getSession() {

    var apiOperation = "CREATE_CHECKOUT_SESSION";
    var ordeID = 10;
    var orderAmount = 100.00;
    var orderCurrency = "AED";
    action = "getcreatesession";
    data={
      action:action,
      apiOperation:apiOperation,
      ordeID:ordeID,
      orderAmount:orderAmount,
      orderCurrency:orderCurrency,
    };
    var route_url = "processCreateSession.php";
    ajax_opertaion_func('get',route_url,'json',action,data);
  }
  /****************************************************************************************************/
  PaymentSession.configure({
      fields: {
          // ATTACH HOSTED FIELDS TO YOUR PAYMENT PAGE FOR A CREDIT CARD
          card: {
            number: "#card-number",
            securityCode: "#security-code",
            expiryMonth: "#expiry-month",
            expiryYear: "#expiry-year",
          }
      },
      //SPECIFY YOUR MITIGATION OPTION HERE
      frameEmbeddingMitigation: ["javascript"],
      callbacks: {
          formSessionUpdate: function(response) {
              // HANDLE RESPONSE FOR UPDATE SESSION
              if (response.status) {
                  if ("ok" == response.status) {
                    session_id = response.session.id;
                      console.log("Session updated with data: " + response.session.id);
                      console.log("Data:"+ JSON.stringify(response.session));
                      console.log("response:"+ JSON.stringify(response));
                      //check if the security code was provided by the user
                      if (response.sourceOfFunds.provided.card.securityCode) {
                          console.log("Security code was provided.");
                      }

                      //check if the user entered a MasterCard credit card
                      if (response.sourceOfFunds.provided.card.scheme == 'MASTERCARD') {
                          console.log("The user entered a MasterCard credit card.")
                      }
                     //response
                      //var route = 'https://eu-gateway.mastercard.com/api/rest/version/44/merchant/TEST7004102/order/10/transaction/10/';
                  //    window.location.replace(route);
                  } else if ("fields_in_error" == response.status)  {

                      console.log("Session update failed with field errors.");
                      if (response.errors.cardNumber) {
                          console.log("Card number invalid or missing.");
                      }
                      if (response.errors.expiryYear) {
                          console.log("Expiry year invalid or missing.");
                      }
                      if (response.errors.expiryMonth) {
                          console.log("Expiry month invalid or missing.");
                      }
                      if (response.errors.securityCode) {
                          console.log("Security code invalid.");
                      }
                  } else if ("request_timeout" == response.status)  {
                      console.log("Session update failed with request timeout: " + response.errors.message);
                  } else if ("system_error" == response.status)  {
                      console.log("Session update failed with system error: " + response.errors.message);
                  }
              } else {
                  console.log("Session update failed: " + response);
              }
          }
        }
    });
  /****************************************************************************************************/
  Checkout.configure({
    merchant: merchant_id,
    order: {
      amount: function() {
        return 1;

      },
      currency: currency_var,
      description:  function() {
        return 'دورة المهارات المتقدمه فى فن صياغه العقود';
      },
      id: function() {
        return $("#lastID").val();
      }
    },
    billing : {
      address: {
        city : function() {
          return 'Sharjah';
        },

      }
    },
    customer: {
      email:  function() {
        return $("#inp_email").val();
      },
      phone:  function() {
        return $("#inp_phone").val();
      }
    },
    interaction: {
      merchant: {
        name   : 'ilaw',
        address: {
          line1: 'United Arab Emirates – Sharjah – Cornish AL Buhairah – Bel Rashid Tower – Office 1704 BR1',
          line2: 'BR2 United Arab Emirates – Ras AL Kaiymah –  Julphar Avenue 1st Floor ',
        },
        email  : 'infoline@ilaw.ae',
        phone  : '+971 6 556 5566',
        logo   : 'https://www.ilaw.ae/images/logo.png'

      },
      locale        : 'en_US',
      theme         : 'default',
      displayControl: {
        billingAddress  : 'HIDE',
        customerEmail   : 'HIDE',
        orderSummary    : 'HIDE',
        shipping        : 'HIDE',
      }
    }
  });
  /**********************************************************************************************/

});



/**********************************************************************************************/
function saveReservelater()
{
  saveReserve(1)
}
