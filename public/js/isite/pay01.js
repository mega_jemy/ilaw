$(document).ready(function(){
  $('#btn_firstiteration').show();
  $('#btn_seconditeration').hide();
  $('#btn_thirditeration').hide();
  $('#btn_fourthiteration').hide();
  $('#msgcashondelivery').hide();
  $('#lbl_termconditions').hide();
  var merchant_id = '7004102';
  var amount_var;
  var currency_var  = 'AED';
  var description_var;
  var street_var;
  var city_var;
  var country_var;
  var m_name_var;
  var session_id;
  var id_var;
  var email_var;
  var phone_var;
  $('input:radio[name=payment_option]').click(function () {
      $('input:radio[name=payment_option]').each(function () {
          if ($(this).is(':checked')) {
              $(this).addClass('active');
              $(this).parent('li').children('label').css('font-weight', 'bold');
              $(this).parent('li').children('div.details').show();
          }
          else {
              $(this).removeClass('active');
              $(this).parent('li').children('label').css('font-weight', 'normal');
              $(this).parent('li').children('div.details').hide();
          }
      });
  });
  $('#btn_continue').click(function () {
      var paymentMethod = $('input:radio[name=payment_option]:checked').val();
      if(paymentMethod == '' || paymentMethod === undefined || paymentMethod === null) {
          alert('Pelase Select Payment Method!');
          return;
      }
      if(paymentMethod == 'cc_merchantpage') {
          window.location.href = 'confirm-order.php?payment_method='+paymentMethod;
      }
      if(paymentMethod == 'cc_merchantpage2') {
          var isValid = payfortFortMerchantPage2.validateCcForm();
          if(isValid) {
              getPaymentPage(paymentMethod);
          }
      }
      else{
          getPaymentPage(paymentMethod);
      }
  });
/********************************************************************************************************/
$('body').delegate('.btn_checkvalidation','click',function(event){
  var paytypeID = 1;
  var _token = $('input[name = "_token"]').val();
  var inp_name = $('input[name = "inp_name"]').val();
  var inp_email = $('input[name = "inp_email"]').val();
  var inp_emp_at = $('input[name = "inp_emp_at"]').val();
  var inp_jobtitle = $('input[name = "inp_jobtitle"]').val();
  var inp_phone =  $('input[name = "inp_phone"]').val();
  var inp_wpay =  $('#lbl_wpay').val();
  //var discountval =
  var paytypeID = paytype;
  action = 'checksetstoreData';
  data = {
    action:action,
    _token:_token,
    inp_name:inp_name,
    inp_email:inp_email,
    inp_emp_at:inp_emp_at,
    inp_jobtitle:inp_jobtitle,
    inp_phone:inp_phone,
    inp_wpay:inp_wpay,
  };
  var route_url = 'checkstoreorder';
  if(paytypeID == 0) {
    //alert('Check Cash On Delvivery');
    ajax_opertaion_func('post',route_url,'json',action,data);
  }
  else if (paytypeID == 1) {
    alert('Check Pay Creadit/Debit Card');
    ajax_opertaion_func('post',route_url,'json',action,data);
  }
  else if (paytypeID == 2) {
    var paymentMethod = $('input:radio[name=payment_option]:checked').val();
    if(paymentMethod == '' || paymentMethod === undefined || paymentMethod === null) {
      $('input:radio[name=payment_option]').addClass('animated rubberBand').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
      function() {
        $(this).removeClass('animated rubberBand');
      });
      return;
    }
    /*if(paymentMethod == 'cc_merchantpage') {
        window.location.href = 'confirm-order.php?payment_method='+paymentMethod;
    }*/
    if(paymentMethod == 'cc_merchantpage2') {
        var isValid = payfortFortMerchantPage2.validateCcForm();
        if(isValid) {
            getPaymentPage(paymentMethod);
        }
    }
    else{
      //alert('Check Payfort Creadit/Debit Card');
      ajax_opertaion_func('post',route_url,'json',action,data);

    }
  }

});
  $('body').delegate('.btn_checkout','click',function(event){
    if($(".minicheck").is(":checked")) {

      var _token = $('input[name = "_token"]').val();
      var inp_name = $('input[name = "inp_name"]').val();
      var inp_address = $('input[name = "inp_address"]').val();
      var opt_countryname = $('#optcountryname').select2('val');
      var opt_cityname = $('input[name = "opt_cityname"]').val();
      var inp_phone = $('input[name = "inp_phone"]').val();
      var inp_landline =  $('input[name = "inp_landline"]').val();
      var addressID = $('input[name = "addressID"]').val();
      var paytype =  $('input[name = "paytype"]').val();

      var addresstype = $('input[name = "addresstype"]').val();
      var selectaddressID = $('input[name = "selectaddressID"]').val();
      var paytypeID = paytype;
      var codshippmentcostID = $('input[name = "codshippmentcostID"]').val();
      var onlinepaycostID = $('input[name = "onlinepaycostname"]').val();
      var cod_optionID = $('input[name = "cod_optionname"]').val();
      if(cod_optionID == 0 && paytype == 0)
      {
        $("#msgcashondelivery").show();
        $('#msgcashondelivery').html("Shippment out of UAE ,Please Pay Using Credit/Debit Card <br> للشحن لخارج دوله الامارات يرجع الدفع باستخدام بطاقه الائتمان");
      }
      else{
        $("#msgcashondelivery").hide();
        action = 'setstoreData';
        var route_url = 'storeorder';
        var parsedData = JSON.stringify(data);
        obj = JSON.parse(parsedData);
        data = {
          action:action,
          _token:_token,
          inp_name:inp_name,
          inp_address:inp_address,
          opt_countryname:opt_countryname,
          opt_cityname:opt_cityname,
          inp_phone:inp_phone,
          inp_landline:inp_landline,
          addressID:addressID,
          paytype:paytype,
          addresstype:addresstype,
          selectaddressID:selectaddressID
        };
        //alert(JSON.stringify(data));
        if(obj.paytype == 0) {
          //alert('Cash On Delvivery');
          ajax_opertaion_func('post',route_url,'json',action,data);
        }
        else if (obj.paytype == 1) {
          //alert('Pay Creadit/Debit Card');
          ajax_opertaion_func('post',route_url,'json',action,data);
        }
        else if (obj.paytype == 2) {
          //alert('Pay Creadit/Debit Card');
          ajax_opertaion_func('post',route_url,'json',action,data);
        }


        /*action = 'checksetstoreData';
        data = {
          action:action,
          _token:_token,
          inp_name:inp_name,
          inp_address:inp_address,
          opt_countryname:opt_countryname,
          opt_cityname:opt_cityname,
          inp_phone:inp_phone,
          inp_landline:inp_landline,
          addressID:addressID,
          paytype:paytype,
          addresstype:addresstype,
          selectaddressID:selectaddressID
        };
        var route_url = 'checkstoreorder';
        alert(route_url);

        if(paytypeID == 0) {
          alert('Check Cash On Delvivery');
          ajax_opertaion_func('post',route_url,'json',action,data);
        }
        else if (paytypeID == 1) {
          alert('Check Pay Creadit/Debit Card');
          ajax_opertaion_func('post',route_url,'json',action,data);
        }
        else if (paytypeID == 2) {
          var paymentMethod = $('input:radio[name=payment_option]:checked').val();
          if(paymentMethod == '' || paymentMethod === undefined || paymentMethod === null) {
            $('input:radio[name=payment_option]').addClass('animated rubberBand').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function() {
              $(this).removeClass('animated rubberBand');
            });
            return;
          }
          /*if(paymentMethod == 'cc_merchantpage') {
              window.location.href = 'confirm-order.php?payment_method='+paymentMethod;
          }
          if(paymentMethod == 'cc_merchantpage2') {
              var isValid = payfortFortMerchantPage2.validateCcForm();
              if(isValid) {
                  getPaymentPage(paymentMethod);
              }
          }
          else{
            //alert('Check Payfort Creadit/Debit Card');
            ajax_opertaion_func('post',route_url,'json',action,data);

          }

        }*/
      }

    }
    else {
      $('#check_boxterms').addClass('animated rubberBand').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
      function() {
        $(this).removeClass('animated rubberBand');
      }
    );

    }
  });
  $('body').delegate('.rdcheckcls','ifChecked', function(event){
     //alert(event.type + ' callback');
    var SelectID = $(this).data('id');
    //alert(SelectID);
    $("#selectaddressID").val(SelectID);
    action = 'getAddressdataACT';
    var _token = $('input[name = "_token"]').val();
    data = {
      action:action,
      _token:_token,
      SelectID:SelectID,
    }
    var route_url = 'getAddressdata';
    ajax_opertaion_func('post',route_url,'json',action,data);
  });

  $('body').delegate('#optcountryname','change',function(){
    var _token = $('input[name = "_token"]').val();
    var idCountry = $('#optcountryname').select2('val');
    action = 'getbyidCountry';
    data = {
      action:action ,
      _token:_token,
      idCountry:idCountry
    };
    var route_url = document.URL +'/getbyidCountryuser';
    ajax_opertaion_func('get',route_url,'json',action,data);
  });
  $('body').delegate('#cashdelivery','ifChecked', function(event){
    $('#paytypeID').val(0);
    var amountcost = document.getElementById('amountvalue').innerHTML;
    var taxcost = document.getElementById('taxvalue').innerHTML;
    var shippmentCost = $('#codshippmentcostID').val();
    var balancecost = parseFloat(amountcost) + parseFloat(taxcost) + parseFloat(shippmentCost);



    //$('#shippmentvalue').empty();
    $('#shippmentvalues').html(shippmentCost);


    $('#balancevalue').empty();
    $('#balancevalue').html(balancecost);
    $('#balancevalue_total').empty();
    $('#balancevalue_total').html(balancecost);
    $("#amountvar_id").val(balancecost);

  });
  $('body').delegate('#cashbank','ifChecked', function(event){
    $('#paytypeID').val(1);
    var amountcost = document.getElementById('amountvalue').innerHTML;
    var taxcost = document.getElementById('taxvalue').innerHTML;
    var shippmentCost = $('#onlinepaycostID').val();

    var balancecost = parseFloat(amountcost) + parseFloat(taxcost) + parseFloat(shippmentCost);


    //$('#shippmentvalue').empty();
    $('#shippmentvalues').html(shippmentCost);
    //alert($('#shippmentvalue').html());

    $('#balancevalue').empty();
    $('#balancevalue').html(balancecost);
    $('#balancevalue_total').empty();
    $('#balancevalue_total').html(balancecost);
    $("#amountvar_id").val(balancecost);
  });

  $('body').delegate('#backfirststep','click',function(){


    $('input[name = "_currentiteration"]').val(1);
    $('#sec_firstiteration').show();
    $('#sec_thirditeration').hide();
    $('#sec_fourthiteration').hide();
    $('#sec_seconditeration').hide();

    $("#cart_step").addClass('active');
    $("#info_step").removeClass('active');
    $("#shippment_step").removeClass('active');
    $("#payment_step").removeClass('active');

    $('#btn_firstiteration').show();
    $('#btn_seconditeration').hide();
    $('#btn_thirditeration').hide();
    $('#btn_fourthiteration').hide();
    $('#msgcashondelivery').hide();
    $('#lbl_termconditions').hide();
  });

$('body').delegate('#backsecondstep','click',function(){
    $('input[name = "_currentiteration"]').val(2);
    $('#sec_firstiteration').hide();
    $('#sec_thirditeration').hide();
    $('#sec_fourthiteration').hide();
    $('#sec_seconditeration').show();

    $("#cart_step").removeClass('active');
    $("#info_step").addClass('active');
    $("#shippment_step").removeClass('active');
    $("#payment_step").removeClass('active');

    $('#btn_firstiteration').hide();
    $('#btn_seconditeration').show();
    $('#btn_thirditeration').hide();
    $('#btn_fourthiteration').hide();
    $('#msgcashondelivery').hide();
    $('#lbl_termconditions').hide();
  });
  $('body').delegate('#backthirdstep','click',function(){
    $('input[name = "_currentiteration"]').val(2);
    $('#sec_firstiteration').hide();
    $('#sec_thirditeration').show();
    $('#sec_fourthiteration').hide();
    $('#sec_seconditeration').hide();

    $("#cart_step").removeClass('active');
    $("#info_step").removeClass('active');
    $("#shippment_step").addClass('active');
    $("#payment_step").removeClass('active');

    $('#btn_firstiteration').hide();
    $('#btn_seconditeration').hide();
    $('#btn_thirditeration').show();
    $('#btn_fourthiteration').hide();
    $('#msgcashondelivery').hide();
    $('#lbl_termconditions').hide();
  });

  $('body').delegate('#btn_firstiteration','click',function(){
    $('input[name = "_currentiteration"]').val(2);
    $('#sec_firstiteration').hide();
    $('#sec_thirditeration').hide();
    $('#sec_fourthiteration').hide();
    $('#sec_seconditeration').show();

    $("#info_step").addClass('active');
    $("#cart_step").removeClass('active');
    $("#shippment_step").removeClass('active');
    $("#payment_step").removeClass('active');

    $('#btn_firstiteration').hide();
    $('#btn_seconditeration').show();
    $('#btn_thirditeration').hide();
    $('#btn_fourthiteration').hide();
    $('#msgcashondelivery').hide();
    $('#lbl_termconditions').hide();



  });
  /*$('body').delegate('#btn_seconditeration','click',function(){

  });*/
  $('body').delegate('#btn_thirditeration','click',function(){
    $('input[name = "_currentiteration"]').val(4);
    $('#sec_firstiteration').hide();
    $('#sec_seconditeration').hide();
    $('#sec_thirditeration').hide();
    $('#sec_fourthiteration').show();

    $("#info_step").removeClass('active');
    $("#cart_step").removeClass('active');
    $("#shippment_step").removeClass('active');
    $("#payment_step").addClass('active');

    $('#btn_firstiteration').hide();
    $('#btn_seconditeration').hide();
    $('#btn_thirditeration').hide();
    $('#btn_fourthiteration').show();
    $('#msgcashondelivery').show();
    $('#lbl_termconditions').show();

    var amountcost = document.getElementById('amountvalue').innerHTML;
    var taxcost = document.getElementById('taxvalue').innerHTML;
    var shippmentCost = $('#onlinepaycostID').val();
    var balancecost = parseFloat(amountcost) + parseFloat(taxcost) + parseFloat(shippmentCost);


    //$('#shippmentvalue').empty();
    $('#shippmentvalues').html(shippmentCost);


    $('#balancevalue').empty();
    $('#balancevalue').html(balancecost);
    $('#balancevalue_total').empty();
    $('#balancevalue_total').html(balancecost);
    $("#amountvar_id").val(balancecost);

  });
  $('body').delegate('#payforinstallment','click',function(){
    $('#paytypeID').val(2);

  });
  $('body').delegate('#tab_addnewID','click',function(){
    $('#optcountryname').val(0).trigger('change');
    //$('#shippmentvalue').empty();
    //$('#shippmentvalue').html(0);

    $('#taxvalue').empty();
    $('#taxvalue').html(0);

    $('#addresstypeID').val(0);
  });
  $('body').delegate('#tab_oldID','click',function(){
    //document.getElementById('optcountryname').value = '0';
    $('.rdcheckcls').iCheck('uncheck');
    $('#optcountryname').val(0).trigger('change');
    $("#optcountryname").val(0);
    $('#addresstypeID').val(1);
  });
  $('body').delegate('#submitnewaddress','click',function(){
    var _token = $('input[name = "_token"]').val();
    var inp_name = $('input[name = "inp_name"]').val();
    var inp_address = $('input[name = "inp_address"]').val();
    var opt_countryname = $('input[name = "opt_countryname"]').val();
    var opt_cityname = $('input[name = "opt_cityname"]').val();
    var inp_phone = $('input[name = "inp_phone"]').val();
    var inp_landline = $('input[name = "inp_landline"]').val();
    action = 'setnewaddress';
    data = {
      action:action ,
      _token:_token,
      inp_name:inp_name,
      inp_address:inp_address,
      opt_countryname:opt_countryname,
      opt_cityname:opt_cityname,
      inp_phone:inp_phone,
      inp_landline:inp_landline,
    };
    var route_url = window.location.href +'/set_new_adreess';
    ajax_opertaion_func('post',route_url,'json',action,data);
  });
  /**************************************************************************************/
  function ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data)
  {
    alert('before: '+JSON.stringify(data));
    //Ajax function
    $.ajax({
      type: type_name,
      url: route_path_name,
      headers:{'X-Requested-With':"XMLHttpRequest"},
      //contentType: false,
      async: true,
      //processData:false,
      dataType: data_type_name,
      cache:false,
      data : data,
      //beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));},
      success: function(response){
        alert('after: '+JSON.stringify(response));
        var parsedData = JSON.stringify(data);
        obj = JSON.parse(parsedData);
        action = obj.action;
        if(action == "getbyidCountry")
        {
          $('input[name = "selectaddressID"]').val(obj.idCountry);
          var parsedData = JSON.stringify(response);
          obj = JSON.parse(parsedData);

          var shippmentcost = obj.shippmentcost;
          var taxcost = obj.taxcost;
          var opay_cost = obj.opay_cost;
          var cod_option = obj.cod_option;


          var amountcost = document.getElementById('amountvalue').innerHTML;

          var amount_taxcost = (parseFloat(amountcost) * parseFloat(taxcost))/100;
          var balancecost = parseFloat(amountcost) + parseFloat(amount_taxcost);

          $('#cod_optionID').val(cod_option);
          $('#codshippmentcostID').val(shippmentcost);
          $('#onlinepaycostID').val(opay_cost);
          //$('#shippmentvalue').empty();
          $('#shippmentvalues').html(0);


          $('#taxvalue').empty();
          $('#taxvalue').html(amount_taxcost);

          $('#balancevalue').empty();
          $('#balancevalue').html(balancecost);
          $('#balancevalue_total').empty();
          $('#balancevalue_total').html(balancecost);
          $("#amountvar_id").val(balancecost);
        }
        else if (action == "setnewaddress") {
          //alert('after: '+JSON.stringify(response));
          var parsedData = JSON.stringify(response);
          obj = JSON.parse(parsedData);
          var resp = obj.response;
          aler(resp);
          if(resp == 1)
          {
            $('#rowclassalertsuccess').show();
            $('#rowclassalertfail').hide();
          }
          else {
            $('#rowclassalertfail').show();
            $('#rowclassalertsuccess').hide();
          }
        }
        else if (action == 'checksetstoreData') {

          var paytype = obj.paytype;
          var addresstype = obj.addresstype;
          var parsedData = JSON.stringify(response);
          obj = JSON.parse(parsedData);
          var responseRes = obj.response;
          //alert(responseRes);
          if(responseRes == -1)
          {
            if(paytype == 0 && addresstype == 0)
            {
              inp_name = obj.invalid['inp_name'];
              inp_address = obj.invalid['inp_address'];
              opt_countryname = obj.invalid['opt_countryname'];
              opt_cityname = obj.invalid['opt_cityname'];
              inp_phone = obj.invalid['inp_phone'];
              inp_landline = obj.invalid['inp_landline'];
              specialorderintruction = obj.invalid['specialorderintruction'];
              if(inp_name)
              {
                $("#div_inp_name").addClass('has-error');
                $('.inp_name').empty();
                $('.inp_name').html('<strong>'+inp_name+'</strong>');
                $('.inp_name').show();
              }
              else {
                $("#div_inp_name").removeClass('has-error');
                $('.inp_name').empty();
                $('.inp_name').hide();
              }
              if(inp_address)
              {
                $("#div_inp_address").addClass('has-error');
                $('.inp_address').empty();
                $('.inp_address').html('<strong>'+inp_address+'</strong>');
                $('.inp_address').show();
              }
              else {
                $("#div_inp_address").removeClass('has-error');
                $('.inp_address').empty();
                $('.inp_address').hide();
              }
              if(opt_countryname)
              {
                $("#div_opt_countryname").addClass('has-error');
                $('.opt_countryname').empty();
                $('.opt_countryname').html('<strong>'+opt_countryname+'</strong>');
                $('.opt_countryname').show();
              }
              else {
                $("#div_opt_countryname").removeClass('has-error');
                $('.opt_countryname').empty();
                $('.opt_countryname').hide();
              }
              if(opt_cityname)
              {
                $("#div_opt_cityname").addClass('has-error');
                $('.opt_cityname').empty();
                $('.opt_cityname').html('<strong>'+opt_cityname+'</strong>');
                $('.opt_cityname').show();
              }
              else {
                $("#div_opt_cityname").removeClass('has-error');
                $('.opt_cityname').empty();
                $('.opt_cityname').hide();
              }
              if(inp_phone)
              {
                $("#div_inp_phone").addClass('has-error');
                $('.inp_phone').empty();
                $('.inp_phone').html('<strong>'+inp_phone+'</strong>');
                $('.inp_phone').show();
              }
              else {
                $("#div_inp_phone").removeClass('has-error');
                $('.inp_phone').empty();
                $('.inp_phone').hide();
              }
              if(inp_landline)
              {
                $("#div_inp_landline").addClass('has-error');
                $('.inp_landline').empty();
                $('.inp_landline').html('<strong>'+inp_landline+'</strong>');
                $('.inp_landline').show();
              }
              else {
                $("#div_inp_landline").removeClass('has-error');
                $('.inp_landline').empty();
                $('.inp_landline').hide();
              }
              if(specialorderintruction)
              {
                $("#div_specialorderintruction").addClass('has-error');
                $('.specialorderintruction').empty();
                $('.specialorderintruction').html('<strong>'+specialorderintruction+'</strong>');
                $('.specialorderintruction').show();
              }
              else {
                $("#div_specialorderintruction").removeClass('has-error');
                $('.specialorderintruction').empty();
                $('.specialorderintruction').hide();
              }
            }
            else if (paytype == 0 && addresstype == 1) {
              var selectaddressID = obj.invalid['selectaddressID'];
              if(selectaddressID)
              {
                $("#div_inp_selectaddressID").addClass('has-error');
                $('.inp_selectaddressID').empty();
                $('.inp_selectaddressID').html('<strong>'+selectaddressID+'</strong>');
                $('.inp_selectaddressID').show();
              }
              else {
                $("#div_inp_selectaddressID").removeClass('has-error');
                $('.inp_selectaddressID').empty();
                $('.inp_selectaddressID').hide();
              }
            }
            else if (paytype == 1 && addresstype == 0) {
              inp_name = obj.invalid['inp_name'];
              inp_address = obj.invalid['inp_address'];
              opt_countryname = obj.invalid['opt_countryname'];
              opt_cityname = obj.invalid['opt_cityname'];
              inp_phone = obj.invalid['inp_phone'];
              inp_landline = obj.invalid['inp_landline'];
              specialorderintruction = obj.invalid['specialorderintruction'];
              if(inp_name)
              {
                $("#div_inp_name").addClass('has-error');
                $('.inp_name').empty();
                $('.inp_name').html('<strong>'+inp_name+'</strong>');
                $('.inp_name').show();
              }
              else {
                $("#div_inp_name").removeClass('has-error');
                $('.inp_name').empty();
                $('.inp_name').hide();
              }
              if(inp_address)
              {
                $("#div_inp_address").addClass('has-error');
                $('.inp_address').empty();
                $('.inp_address').html('<strong>'+inp_address+'</strong>');
                $('.inp_address').show();
              }
              else {
                $("#div_inp_address").removeClass('has-error');
                $('.inp_address').empty();
                $('.inp_address').hide();
              }
              if(opt_countryname)
              {
                $("#div_opt_countryname").addClass('has-error');
                $('.opt_countryname').empty();
                $('.opt_countryname').html('<strong>'+opt_countryname+'</strong>');
                $('.opt_countryname').show();
              }
              else {
                $("#div_opt_countryname").removeClass('has-error');
                $('.opt_countryname').empty();
                $('.opt_countryname').hide();
              }
              if(opt_cityname)
              {
                $("#div_opt_cityname").addClass('has-error');
                $('.opt_cityname').empty();
                $('.opt_cityname').html('<strong>'+opt_cityname+'</strong>');
                $('.opt_cityname').show();
              }
              else {
                $("#div_opt_cityname").removeClass('has-error');
                $('.opt_cityname').empty();
                $('.opt_cityname').hide();
              }
              if(inp_phone)
              {
                $("#div_inp_phone").addClass('has-error');
                $('.inp_phone').empty();
                $('.inp_phone').html('<strong>'+inp_phone+'</strong>');
                $('.inp_phone').show();
              }
              else {
                $("#div_inp_phone").removeClass('has-error');
                $('.inp_phone').empty();
                $('.inp_phone').hide();
              }
              if(inp_landline)
              {
                $("#div_inp_landline").addClass('has-error');
                $('.inp_landline').empty();
                $('.inp_landline').html('<strong>'+inp_landline+'</strong>');
                $('.inp_landline').show();
              }
              else {
                $("#div_inp_landline").removeClass('has-error');
                $('.inp_landline').empty();
                $('.inp_landline').hide();
              }
              if(specialorderintruction)
              {
                $("#div_specialorderintruction").addClass('has-error');
                $('.specialorderintruction').empty();
                $('.specialorderintruction').html('<strong>'+specialorderintruction+'</strong>');
                $('.specialorderintruction').show();
              }
              else {
                $("#div_specialorderintruction").removeClass('has-error');
                $('.specialorderintruction').empty();
                $('.specialorderintruction').hide();
              }
            }
            else if (paytype == 1 && addresstype == 1) {
              var selectaddressID = obj.invalid['selectaddressID'];
              if(selectaddressID)
              {
                $("#div_inp_selectaddressID").addClass('has-error');
                $('.inp_selectaddressID').empty();
                $('.inp_selectaddressID').html('<strong>'+selectaddressID+'</strong>');
                $('.inp_selectaddressID').show();
              }
              else {
                $("#div_inp_selectaddressID").removeClass('has-error');
                $('.inp_selectaddressID').empty();
                $('.inp_selectaddressID').hide();
              }
            }
            else if (paytype == 2 && addresstype == 0) {
              inp_name = obj.invalid['inp_name'];
              inp_address = obj.invalid['inp_address'];
              opt_countryname = obj.invalid['opt_countryname'];
              opt_cityname = obj.invalid['opt_cityname'];
              inp_phone = obj.invalid['inp_phone'];
              inp_landline = obj.invalid['inp_landline'];
              specialorderintruction = obj.invalid['specialorderintruction'];
              if(inp_name)
              {
                $("#div_inp_name").addClass('has-error');
                $('.inp_name').empty();
                $('.inp_name').html('<strong>'+inp_name+'</strong>');
                $('.inp_name').show();
              }
              else {
                $("#div_inp_name").removeClass('has-error');
                $('.inp_name').empty();
                $('.inp_name').hide();
              }
              if(inp_address)
              {
                $("#div_inp_address").addClass('has-error');
                $('.inp_address').empty();
                $('.inp_address').html('<strong>'+inp_address+'</strong>');
                $('.inp_address').show();
              }
              else {
                $("#div_inp_address").removeClass('has-error');
                $('.inp_address').empty();
                $('.inp_address').hide();
              }
              if(opt_countryname)
              {
                $("#div_opt_countryname").addClass('has-error');
                $('.opt_countryname').empty();
                $('.opt_countryname').html('<strong>'+opt_countryname+'</strong>');
                $('.opt_countryname').show();
              }
              else {
                $("#div_opt_countryname").removeClass('has-error');
                $('.opt_countryname').empty();
                $('.opt_countryname').hide();
              }
              if(opt_cityname)
              {
                $("#div_opt_cityname").addClass('has-error');
                $('.opt_cityname').empty();
                $('.opt_cityname').html('<strong>'+opt_cityname+'</strong>');
                $('.opt_cityname').show();
              }
              else {
                $("#div_opt_cityname").removeClass('has-error');
                $('.opt_cityname').empty();
                $('.opt_cityname').hide();
              }
              if(inp_phone)
              {
                $("#div_inp_phone").addClass('has-error');
                $('.inp_phone').empty();
                $('.inp_phone').html('<strong>'+inp_phone+'</strong>');
                $('.inp_phone').show();
              }
              else {
                $("#div_inp_phone").removeClass('has-error');
                $('.inp_phone').empty();
                $('.inp_phone').hide();
              }
              if(inp_landline)
              {
                $("#div_inp_landline").addClass('has-error');
                $('.inp_landline').empty();
                $('.inp_landline').html('<strong>'+inp_landline+'</strong>');
                $('.inp_landline').show();
              }
              else {
                $("#div_inp_landline").removeClass('has-error');
                $('.inp_landline').empty();
                $('.inp_landline').hide();
              }
              if(specialorderintruction)
              {
                $("#div_specialorderintruction").addClass('has-error');
                $('.specialorderintruction').empty();
                $('.specialorderintruction').html('<strong>'+specialorderintruction+'</strong>');
                $('.specialorderintruction').show();
              }
              else {
                $("#div_specialorderintruction").removeClass('has-error');
                $('.specialorderintruction').empty();
                $('.specialorderintruction').hide();
              }
            }
            else if (paytype == 2 && addresstype == 1) {
              var selectaddressID = obj.invalid['selectaddressID'];
              if(selectaddressID)
              {
                $("#div_inp_selectaddressID").addClass('has-error');
                $('.inp_selectaddressID').empty();
                $('.inp_selectaddressID').html('<strong>'+selectaddressID+'</strong>');
                $('.inp_selectaddressID').show();
              }
              else {
                $("#div_inp_selectaddressID").removeClass('has-error');
                $('.inp_selectaddressID').empty();
                $('.inp_selectaddressID').hide();
              }
            }
          }
          else {

            $('input[name = "_currentiteration"]').val(3);
            $('#sec_firstiteration').hide();
            $('#sec_seconditeration').hide();
            $('#sec_fourthiteration').hide();
            $('#sec_thirditeration').show();

            $('#btn_firstiteration').hide();
            $('#btn_seconditeration').hide();
            $('#btn_thirditeration').show();
            $('#btn_fourthiteration').hide();
            $('#msgcashondelivery').hide();
            $('#lbl_termconditions').hide();

            $("#shippment_step").addClass('active');
            $("#info_step").removeClass('active');
            $("#cart_step").removeClass('active');
            $("#payment_step").removeClass('active');



            /*
            inp_address:inp_address,
            opt_countryname:opt_countryname,
            opt_cityname:opt_cityname,
            inp_phone:inp_phone,
            inp_landline:inp_landline,
            addressID:addressID,
            paytype:paytype,
            addresstype:addresstype,
            selectaddressID:selectaddressID*/
            var parsedData = JSON.stringify(data);
            obj = JSON.parse(parsedData);
            var inp_name = obj.inp_name;
            var inp_address = obj.inp_address;
            var opt_countryname = obj.opt_countryname;
            var opt_cityname = obj.opt_cityname;
            var inp_phone = obj.inp_phone;
            var inp_landline =  obj.inp_landline;
            var addressID = obj.addressID;
            var paytype =  obj.paytype;
            var CountryCity = opt_countryname + opt_cityname;

            document.getElementById('name_inp').innerHTML = inp_name;
            document.getElementById('country_inp').innerHTML = CountryCity;
            document.getElementById('address_inp').innerHTML = inp_address;
            document.getElementById('contact_inp').innerHTML = inp_phone;

          }

        }
        else if (action == 'setstoreData') {
          var paytype = obj.paytype;
          var addresstype = obj.addresstype;
          var parsedData = JSON.stringify(response);
          if(paytype == 0)
          {
            obj = JSON.parse(parsedData);
            var OrderID = obj.OrderID;
            var tracknumber = 'SC-'+OrderID;
            var route = document.URL +"/bill-print/"+tracknumber;
            location.replace(route);
            //var route_url = 'verify-order-mail';
            //var action = 'verifyOrderMail';
            /*data = {
              OrderID:OrderID,
              tracknumber:tracknumber,
              paytype:0,
            };
            ajax_opertaion_func('post',route_url,'json',action,data);*/

            //var route = document.URL +"/bill-print/"+tracknumber;
            //location.replace(route);
          }
          else if(paytype == 1) {
            obj = JSON.parse(parsedData);
            var OrderID = obj.OrderID;
            id_var = OrderID;
            amount_var = $("#amountvar_id").val();

            description_var = obj.description_var;
            $("#description_varID").val(description_var);
            street_var = obj.street_var;
            $("#street_varID").val(street_var);
            city_var = obj.city_var;
            $("#city_var_ID").val(city_var);
            country_var = obj.country_var;
            $("#country_varID").val(country_var);
            email_var = obj.email_var;
            $("#email_varID").val(email_var);
            phone_var = obj.phone_var;
            $("#phone_varID").val(phone_var);
            $('#OrderID_id').val(OrderID);
            getSession(OrderID,$('#amountvar_id').val(),currency_var);
            //alert(OrderID);
            //pay();

            //var tracknumber = 'SC-'+OrderID;
            //var route = document.URL +"/bill-print/"+tracknumber;
            //location.replace(route);
          }
          else if (paytype == 2) {
            amount_var = $("#amountvar_id").val();
            var paymentMethod = $('input:radio[name=payment_option]:checked').val();
            getPaymentPage(paymentMethod,amount_var,"AED",amount_var);
            /*obj = JSON.parse(parsedData);
            var OrderID = obj.OrderID;
            var tracknumber = 'SC-'+OrderID;
            var route = "https://silvercity.ae/bill-print/"+tracknumber;
            location.replace(route);*/
          }
        }
        else if (action == "getAddressdataACT") {
          var parsedData = JSON.stringify(response);
          obj = JSON.parse(parsedData);
          var countryData = obj.countryData;
          var name_code =  obj.countryData['name_code'];
          var taxcost = obj.countryData['taxcost'];

          var opay_cost = obj.countryData['opay_cost'];
          var cod_option = obj.countryData['cod_option'];



          var shippmentcost = obj.countryData['shippmentcost'];
          var amountcost = document.getElementById('amountvalue').innerHTML;

          var amount_taxcost = (parseFloat(amountcost) * parseFloat(taxcost))/100;
          var balancecost = parseFloat(amountcost) + parseFloat(amount_taxcost);

          //alert(shippmentcost);
          $('#codshippmentcostID').val(shippmentcost);
          $('#onlinepaycostID').val(opay_cost);
          $('#cod_optionID').val(cod_option);


          $('#taxvalue').empty();
          $('#taxvalue').html(amount_taxcost);

          $('#balancevalue').empty();
          $('#balancevalue').html(balancecost);
          $('#balancevalue_total').empty();
          $('#balancevalue_total').html(balancecost);
          $("#amountvar_id").val(balancecost);
        }
        else if (action == "getcreatesession"){
          var parsedData = JSON.stringify(response);
          obj = JSON.parse(parsedData);
          console.log(JSON.stringify(response));
          if(obj.repsonse == 1)
          {
            var resultorg = obj.result;
            if(resultorg == "SUCCESS")
            {
              var sID = obj.sessionid;
              //alert(sID);
              $("#sessionID").val(sID);
              Checkout.showLightbox();
              //Checkout.showPaymentPage();
            }
          }

        }
        else if (action == 'verifyOrderMail') {
          var paytype = obj.paytype;
          var parsedData = JSON.stringify(response);
          if(paytype == 0)
          {
            var OrderID = obj.OrderID;
            var tracknumber = obj.tracknumber;
            obj = JSON.parse(parsedData);
            if(obj.repsonse == 1)
            {
              //alert('S');
              var route = this.url()+"/bill-print/"+tracknumber;
              location.replace(route);
            }
            else {
              //alert('Failed');
            }

          }
          else if (paytype == 1) {

          }
          else if (paytype == 2) {

          }
        }
        /********************************************************************************/
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        //alert( "Error" );
        console.log(errorThrown);
        console.log(arguments);
        console.log (XMLHttpRequest, textStatus, errorThrown);
        }
    });
  }
/**************************************End Document*******************************************************/
//Checkout.showPaymentPage()
/****************************************************************************************/
function getPageState(data)
{
  console.log("DataBefore->"+JSON.stringify(data));
}
function restorePageState(data) {
  //set page state from data object
  console.log("DataBefore->"+JSON.stringify(data));
}
function errorCallback(error) {
  console.log("errorCallback:"+JSON.stringify(error));
  document.getElementById("error").innerHTML = JSON.stringify(error);
}
function completeCallback(resultIndicator, sessionVersion) {
  //alert(resultIndicator,sessionVersion);
}
function cancelCallback() {
  console.log('Payment cancelled');
}
function pay() {
    // UPDATE THE SESSION WITH THE INPUT FROM HOSTED FIELDS
    PaymentSession.updateSessionFromForm('card');
}

function getSession(ordeID,orderAmount,orderCurrency) {
  var _token = $('input[name = "_token"]').val();
  var apiOperation = "CREATE_CHECKOUT_SESSION";
  //var ordeID = 10;
  //var orderAmount = 100.00;
  //var orderCurrency = "AED";
  action = "getcreatesession";
  data={
    action:action,
    _token:_token,
    apiOperation:apiOperation,
    ordeID:ordeID,
    orderAmount:orderAmount,
    orderCurrency:orderCurrency,
  };
  var route_url = "createpaymentsession";
  ajax_opertaion_func('post',route_url,'json',action,data);
}
PaymentSession.configure({
    fields: {
        // ATTACH HOSTED FIELDS TO YOUR PAYMENT PAGE FOR A CREDIT CARD
        card: {
          number: "#card-number",
          securityCode: "#security-code",
          expiryMonth: "#expiry-month",
          expiryYear: "#expiry-year",
        }
    },
    //SPECIFY YOUR MITIGATION OPTION HERE
    frameEmbeddingMitigation: ["javascript"],
    callbacks: {
        formSessionUpdate: function(response) {
            // HANDLE RESPONSE FOR UPDATE SESSION
            if (response.status) {
                if ("ok" == response.status) {
                  session_id = response.session.id;
                    console.log("Session updated with data: " + response.session.id);
                    console.log("Data:"+ JSON.stringify(response.session));
                    console.log("response:"+ JSON.stringify(response));
                    //check if the security code was provided by the user
                    if (response.sourceOfFunds.provided.card.securityCode) {
                        console.log("Security code was provided.");
                    }

                    //check if the user entered a MasterCard credit card
                    if (response.sourceOfFunds.provided.card.scheme == 'MASTERCARD') {
                        console.log("The user entered a MasterCard credit card.")
                    }
                   //response
                    //var route = 'https://eu-gateway.mastercard.com/api/rest/version/44/merchant/TEST7004102/order/10/transaction/10/';
                //    window.location.replace(route);
                } else if ("fields_in_error" == response.status)  {

                    console.log("Session update failed with field errors.");
                    if (response.errors.cardNumber) {
                        console.log("Card number invalid or missing.");
                    }
                    if (response.errors.expiryYear) {
                        console.log("Expiry year invalid or missing.");
                    }
                    if (response.errors.expiryMonth) {
                        console.log("Expiry month invalid or missing.");
                    }
                    if (response.errors.securityCode) {
                        console.log("Security code invalid.");
                    }
                } else if ("request_timeout" == response.status)  {
                    console.log("Session update failed with request timeout: " + response.errors.message);
                } else if ("system_error" == response.status)  {
                    console.log("Session update failed with system error: " + response.errors.message);
                }
            } else {
                console.log("Session update failed: " + response);
            }
        }
      }
  });

Checkout.configure({
  session: {

    id: function() {

      return $("#sessionID").val();
      }
    },

  merchant: merchant_id,
  order: {
    amount: function() {
      return $('#amountvar_id').val();
    },
    currency: currency_var,
    description:  function() {
      return $('#description_varID').val();
    },
    id: function() {
      return $('#OrderID_id').val();
    }
  },
  billing : {
    address: {
      street : function() {
        return $('#street_varID').val();
      },
      city : function() {
        return $('#city_var_ID').val();
      },
      country : function() {
        return $('#country_varID').val();
      }
    }
  },
  customer: {
    email:  function() {
      return $('#email_varID').val();
    },
    phone:  function() {
      return $('#phone_varID').val();
    }
  },
  interaction: {
    merchant: {
      name   : 'silvercity',
      address: {
        line1: 'Office #110, Makateb Bldg,Opp.Dnata,Port Saeed, Deira, Dubai, U.A.E.',
      },
      email  : 'info@silvercity.ae',
      phone  : '+971 4 236 7 444',
      logo   : 'https://www.silvercity.ae/assets/customerpanel/images/logo.png'

    },
    locale        : 'en_US',
    theme         : 'default',
    displayControl: {
      billingAddress  : 'HIDE',
      customerEmail   : 'HIDE',
      orderSummary    : 'HIDE',
      shipping        : 'HIDE',
    }
  }
});
});
/****************************************Payfort Integration***********************************************************/
function getPaymentPage(paymentMethod,amount,currency,total_amount) {
    var check3ds = getUrlParameter('3ds');
    var url = 'payfortroute?r=getPaymentPage';
    if(check3ds == 'no') {
       url = url+'&3ds=no';
    }
    $.ajax({
       url: url,
       type: 'post',
       dataType: 'json',
       data: {paymentMethod: paymentMethod,amount:amount,currency:currency,total_amount:total_amount},
       success: function (response) {
            if (response.form) {
                $('body').append(response.form);
                if(response.paymentMethod == 'cc_merchantpage') {
                    showMerchantPage(response.url);
                }
                else if(response.paymentMethod == 'cc_merchantpage2') {
                    var expDate = $('#payfort_fort_mp2_expiry_year').val()+''+$('#payfort_fort_mp2_expiry_month').val();
                    var mp2_params = {};
                    mp2_params.card_holder_name = $('#payfort_fort_mp2_card_holder_name').val();
                    mp2_params.card_number = $('#payfort_fort_mp2_card_number').val();
                    mp2_params.expiry_date = expDate;
                    mp2_params.card_security_code = $('#payfort_fort_mp2_cvv').val();
                    $.each(mp2_params, function(k, v){
                        $('<input>').attr({
                            type: 'hidden',
                            id: k,
                            name: k,
                            value: v
                        }).appendTo('#payfort_payment_form');
                    });
                    $('#payfort_payment_form input[type=submit]').click();
                }
                else{
                  //alert('asd');
                    $('#payfort_payment_form input[type=submit]').click();
                }
            }
       }
    });
}
function showMerchantPage(merchantPageUrl) {
    if($("#payfort_merchant_page").size()) {
        $( "#payfort_merchant_page" ).remove();
    }
    $('<iframe name="payfort_merchant_page" id="payfort_merchant_page" height="430px" width="100%" frameborder="0" scrolling="no"></iframe>').appendTo('#pf_iframe_content');

    $( "#payfort_merchant_page" ).attr("src", merchantPageUrl);
    $( "#payfort_payment_form" ).attr("action", merchantPageUrl);
    $( "#payfort_payment_form" ).attr("target","payfort_merchant_page");
    $( "#payfort_payment_form" ).attr("method","POST");
    $('#payfort_payment_form input[type=submit]').click();
    //$( "#payfort_payment_form" ).submit();
    $( "#div-pf-iframe" ).show();
}
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


var payfortFort = (function () {
   return {
        validateCreditCard: function(element) {
            var isValid = false;
            var eleVal = $(element).val();
            eleVal = this.trimString(element.val());
            eleVal = eleVal.replace(/\s+/g, '');
            $(element).val(eleVal);
            $(element).validateCreditCard(function(result) {
                /*$('.log').html('Card type: ' + (result.card_type == null ? '-' : result.card_type.name)
                         + '<br>Valid: ' + result.valid
                         + '<br>Length valid: ' + result.length_valid
                         + '<br>Luhn valid: ' + result.luhn_valid);*/
                isValid = result.valid;
            });
            return isValid;
        },
        validateCardHolderName: function(element) {
            $(element).val(this.trimString(element.val()));
            var cardHolderName = $(element).val();
            if(cardHolderName.length > 50) {
                return false;
            }
            return true;
        },
        validateCvc: function(element) {
            $(element).val(this.trimString(element.val()));
            var cvc = $(element).val();
            if(cvc.length > 4 || cvc.length == 0) {
                return false;
            }
            if(!this.isPosInteger(cvc)) {
                return false;
            }
            return true;
        },
        isDefined: function(variable) {
            if (typeof (variable) === 'undefined' || typeof (variable) === null) {
                return false;
            }
            return true;
        },
        trimString: function(str){
            return str.trim();
        },
        isPosInteger: function(data) {
            var objRegExp  = /(^\d*$)/;
            return objRegExp.test( data );
        }
   };
})();
function showMerchantPage(merchantPageUrl) {
    if($("#payfort_merchant_page").size()) {
        $( "#payfort_merchant_page" ).remove();
    }
    $('<iframe name="payfort_merchant_page" id="payfort_merchant_page" height="430px" width="100%" frameborder="0" scrolling="no"></iframe>').appendTo('#pf_iframe_content');

    $( "#payfort_merchant_page" ).attr("src", merchantPageUrl);
    $( "#payfort_payment_form" ).attr("action", merchantPageUrl);
    $( "#payfort_payment_form" ).attr("target","payfort_merchant_page");
    $( "#payfort_payment_form" ).attr("method","POST");
    $('#payfort_payment_form input[type=submit]').click();
    //$( "#payfort_payment_form" ).submit();
    $( "#div-pf-iframe" ).show();
}
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
var payfortFort = (function () {
   return {
        validateCreditCard: function(element) {
            var isValid = false;
            var eleVal = $(element).val();
            eleVal = this.trimString(element.val());
            eleVal = eleVal.replace(/\s+/g, '');
            $(element).val(eleVal);
            $(element).validateCreditCard(function(result) {
                /*$('.log').html('Card type: ' + (result.card_type == null ? '-' : result.card_type.name)
                         + '<br>Valid: ' + result.valid
                         + '<br>Length valid: ' + result.length_valid
                         + '<br>Luhn valid: ' + result.luhn_valid);*/
                isValid = result.valid;
            });
            return isValid;
        },
        validateCardHolderName: function(element) {
            $(element).val(this.trimString(element.val()));
            var cardHolderName = $(element).val();
            if(cardHolderName.length > 50) {
                return false;
            }
            return true;
        },
        validateCvc: function(element) {
            $(element).val(this.trimString(element.val()));
            var cvc = $(element).val();
            if(cvc.length > 4 || cvc.length == 0) {
                return false;
            }
            if(!this.isPosInteger(cvc)) {
                return false;
            }
            return true;
        },
        isDefined: function(variable) {
            if (typeof (variable) === 'undefined' || typeof (variable) === null) {
                return false;
            }
            return true;
        },
        trimString: function(str){
            return str.trim();
        },
        isPosInteger: function(data) {
            var objRegExp  = /(^\d*$)/;
            return objRegExp.test( data );
        }
   };
})();
var payfortFortMerchantPage2 = (function () {
    return {
        validateCcForm: function () {
            this.hideError();
            var isValid = payfortFort.validateCardHolderName($('#payfort_fort_mp2_card_holder_name'));
            if(!isValid) {
                this.showError('Invalid Card Holder Name');
                return false;
            }
            isValid = payfortFort.validateCreditCard($('#payfort_fort_mp2_card_number'));
            if(!isValid) {
                this.showError('Invalid Credit Card Number');
                return false;
            }
            isValid = payfortFort.validateCvc($('#payfort_fort_mp2_cvv'));
            if(!isValid) {
                this.showError('Invalid Card CVV');
                return false;
            }
            return true;
        },
        showError: function(msg) {
            //alert(msg);
        },
        hideError: function() {
            return;
        }
    };
})();
