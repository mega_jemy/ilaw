var data;
var action;
var newTitle;
var files;
var files_input;
var reader;
$(document).ready(function(e){
  /**************************************************************************************/
  //edit_record
  $('body').delegate('.limit_pr','click',function(){
    var _token = $('input[name = "_token"]').val();
    var id = $(this).data('id');
    action = 'updateuserpr';
    data = {action:action ,_token:_token,id:id};
    ajax_opertaion_func('POST','../../ipanel/updateuserpriv','json',action,data);
  });
  /**************************************************************************************/
  	function response_check_message(id_name,mood,message)
  	{
  		if(mood == 'success')
  		{
  			return $(id_name).html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
  				'<strong>Success!</strong> '+message+'.</div>')
  		}
  		else if(mood == 'info')
  		{
  			return $(id_name).html('<div  class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
  				'<strong>Info!</strong> '+message+'.</div>')
  		}
  		else if(mood == 'warning')
  		{
  			return $(id_name).html('<div  class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
  				'<strong>Warning!</strong> '+message+'.</div>')
  		}
  		else if(mood == 'error')
  		{
  			return $(id_name).html('<div  class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
  				'<strong>Danger!</strong> '+message+'.</div>')
  		}

  	}
    /**************************************************************************************/
    function ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data)
    {
      //alert('before: '+JSON.stringify(data));
      //Ajax function
      $.ajax({
        type: type_name,
        url: route_path_name,
        headers:{'X-Requested-With':"XMLHttpRequest"},
        //contentType: false,
        async: true,
        //processData:false,
        dataType: data_type_name,
        cache:false,
        data : data,
        //beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));},
        success: function(response){
          //alert('after: '+JSON.stringify(response));
          var parsedData = JSON.stringify(data);
          obj = JSON.parse(parsedData);
          action = obj.action;
          if(action == "updateuserpr")
          {
            var parsedData = JSON.stringify(response);
            obj = JSON.parse(parsedData);
            res = obj.response;
            id_user = obj.idusername;
            if(res == 1)
            {
              $("#span_error_msg").show();
              $("#span_error_msg").append(response_check_message(span_error_msg,'success','Successfully Update data'));
            }
            else if(res == 0){
              $("#span_error_msg").show();
              $("#span_error_msg").append(response_check_message(span_error_msg,'error','Failed Update data'));
            }
            else {
              $("#span_error_msg").show();
              $("#span_error_msg").append(response_check_message(span_error_msg,'error','Failed Update data'));
            }
            display_all_users_records(id_user);
          }
          else if(action == "getuserprbyid")
          {
            var parsedData = JSON.stringify(response);
  					obj = JSON.parse(parsedData);
  					var count_loop = obj.response.length;
            var usernamevar = obj.username;
  					var id;var counter;
            var t = $('#example1').DataTable();
            t.clear().draw();
            for (i = 0; i <= count_loop-1; i++)
  					{
  						id = obj.response[i].id;
  						counter = i+1;
              var id_user = obj.response[i].users.id;
              var screen_name = obj.response[i].admin_screen.name;
              var active_screen = obj.response[i].active;
              if(active_screen == 0)
              {
                active_screen = "<span style='cursor:pointer' data-id='"+id+","+usernamevar+",0' class='label label-danger limit_pr'><a  style='text-decoration:none;color:white;'>dis-Appear</a></span>";
              }
              else {
                active_screen = "<span style='cursor:pointer' data-id='"+id+","+usernamevar+",1' class='label label-success limit_pr'><a style='text-decoration:none;color:white;'>Appear</a></span>";
              }
              t.row.add( [
                id,
                counter,
                screen_name,
                active_screen
              ]).draw( false );
  					}
          }
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
          //alert( "Error" );
          console.clear();
          console.log(errorThrown);
          console.log(arguments);
          console.log (XMLHttpRequest, textStatus, errorThrown);

          }
      });
    }
    function display_all_users_records(id_user)
    {
      var _token = $('input[name = "_token"]').val();
      action = 'getuserprbyid';
      data = {action:action ,_token:_token,id:id_user};
      ajax_opertaion_func('POST','../../ipanel/retriveusesrpr','json',action,data);
    }
});
