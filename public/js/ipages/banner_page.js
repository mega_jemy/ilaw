var main_URL = '/';
var prefix_URL = 'admin'
var addcart_URL = '/banner_chkvalid';
var counter = 1;
var isinsert = 0;
var printError = function(error, explicit) {
    console.log(`[${explicit ? 'EXPLICIT' : 'INEXPLICIT'}] ${error.name}: ${error.message}`);
}
function validateTblFields()
{
  var opt_langID = $("#opt_langID").val();
  var opt_typeID = $("#opt_typeID").val();
  var opt_statusID = $("#opt_statusID").val();
  var inp_order = $("#inp_order").val();
  var inp_title = $("#inp_title").val();
  var inp_alt = $("#inp_alt").val();
  var img_user_fname = $("#img_user_fname").val();

  /*if(opt_langID == 0){} else {}
  if(opt_typeID == 0){} else {}
  if(opt_statusID == 0){} else {}
  if(inp_order == ""){} else {}
  if(inp_title == ""){} else {}
  if(inp_alt == ""){} else {}
  if(img_user_fname == ""){} else {}*/

  if(opt_langID == 0 || opt_typeID == 0 || opt_statusID == 0 ||
  inp_order == "" || inp_title == "" || inp_alt == "" || img_user_fname == "")
  {
    var type_name = "get";
    var route_path_name = main_URL + prefix_URL + addcart_URL;
    var data_type_name = "json";
    var action = "check_valid_data";
    var data = {
      action:action,
      opt_langID:opt_langID,
      opt_typeID:opt_typeID,
      opt_statusID:opt_statusID,
      inp_order:inp_order,
      inp_title:inp_title,
      inp_alt:inp_alt,
      img_user_fname:img_user_fname

    };
    ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data);
  }
  else {
    return true;
  }

}
function addTblBanner(opt_langID,opt_typeID,opt_statusID,inp_order,inp_title,inp_alt,img_user_fname)
{
  var t = $('#example').DataTable();
    t.row.add( [
        counter,
        '<input value="" type="text" readonly value="'+opt_langID+'" class="form-control" id="inp_order" name="txt_order" placeholder="Order...">',
        '<input value="" type="text" readonly value="'+opt_typeID+'" class="form-control" id="inp_order" name="txt_order" placeholder="Order...">',
        '<input value="" type="text" readonly value="'+opt_statusID+'" class="form-control" id="inp_order" name="txt_order" placeholder="Order...">',
        '<input value="" type="text" readonly value="'+inp_order+'" class="form-control" id="inp_order" name="txt_order" placeholder="Order...">',
        '<input value="" type="text" readonly value="'+inp_title+'" class="form-control" id="inp_order" name="txt_order" placeholder="Order...">',
        '<input value="" type="text" readonly value="'+inp_alt+'" class="form-control" id="inp_order" name="txt_order" placeholder="Order...">',
        '<input value="" type="text" readonly value="'+img_user_fname+'" class="form-control" id="inp_order" name="txt_order" placeholder="Order...">',
        '<span class="badge">remove</span>',
    ] ).draw( false );

    counter++;
}
/*******************************************************************************/
function response_check_message(id_name,mood,message)
{
  if(mood == 'success')
  {
    return $(id_name).html('<div id="'+id_name+'" class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Success!</strong> '+message+'.</div>');
  }
  else if(mood == 'info')
  {
    return $(id_name).html('<div id="'+id_name+'"  class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Info!</strong> '+message+'.</div>');
  }
  else if(mood == 'warning')
  {
    return $(id_name).html('<div id="'+id_name+'"  class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Warning!</strong> '+message+'.</div>');
  }
  else if(mood == 'error')
  {
    return $(id_name).html('<div id="'+id_name+'"  class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Danger!</strong> '+message+'.</div>');
  }
  else {
    return $(id_name).html('');
  }

}
/**************************************************************************************/
function ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data)
{
  try {
    //alert('before: '+JSON.stringify(data));
    //Ajax function
    $.ajax({
      type: type_name,
      url: route_path_name,
      //headers:{'X-Requested-With':"XMLHttpRequest"},
      //contentType: false,
      async: true,
      //processData:false,
      dataType: data_type_name,
      cache:false,
      data : data,
      //beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));},
      success: function(response){
        //alert('after: '+JSON.stringify(response));
        var parsedData = JSON.stringify(data);
        obj = JSON.parse(parsedData);
        action = obj.action;
        if(action == "check_valid_data")
        {
          var opt_langID_var = obj.opt_langID
          var opt_typeID_var = obj.opt_typeID;
          var opt_statusID_var = obj.opt_statusID;
          var inp_order_var = obj.inp_order;
          var inp_title_var = obj.inp_title;
          var inp_alt_var = obj.inp_alt;
          var img_user_fname_var = obj.img_user_fname;

          var parsedData = JSON.stringify(response);
          obj = JSON.parse(parsedData);
          var opt_langID  = obj.response.opt_langID;
          var opt_typeID = obj.response.opt_typeID;
          var opt_statusID = obj.response.opt_statusID;
          var inp_order = obj.response.inp_order;
          var inp_title = obj.response.inp_title;
          var inp_alt = obj.response.inp_alt;
          var img_user_fname = obj.response.img_user_fname;

          var opt_langID_isvalid  = 0;
          var opt_typeID_isvalid = 0;
          var opt_statusID_isvalid = 0;
          var inp_order_isvalid = 0;
          var inp_title_isvalid = 0;
          var inp_alt_isvalid = 0;
          var img_user_fname_isvalid = 0;
          alert("opt_langID_isvalid")
          if(typeof opt_langID === 'undefined' || opt_langID == "" || opt_langID == null)
          {
            $("#span_opt_langID").empty();
            opt_langID_isvalid = 0;
          } else {
            response_check_message("#span_opt_langID","error",opt_langID);
            opt_langID_isvalid = 1;
          }

          if(typeof opt_typeID === 'undefined' || opt_typeID == "" || opt_typeID == null)
          {
            $("#span_opt_typeID").empty();
            opt_typeID_isvalid = 0;
          } else {
            response_check_message("#span_opt_typeID","error",opt_typeID);
            opt_typeID_isvalid = 1;
          }

          if(typeof opt_statusID === 'undefined' || opt_statusID == "" || opt_statusID == null)
          {
            $("#span_opt_statusID").empty();
            opt_statusID_isvalid = 0;
          } else {
            response_check_message("#span_opt_statusID","error",opt_statusID);
            opt_statusID_isvalid = 1;
          }

          if(typeof inp_order === 'undefined' || inp_order == "" || inp_order == null)
          {
            $("#span_inp_order").empty();
            inp_order_isvalid = 0;
          } else {
            response_check_message("#span_inp_order","error",inp_order);
            inp_order_isvalid = 1;
          }

          if(typeof inp_title === 'undefined' || inp_title == "" || inp_title == null)
          {
            $("#span_inp_title").empty();
            inp_title_isvalid = 0;
          } else {
            response_check_message("#span_inp_title","error",inp_title);
            inp_title_isvalid = 1;
          }

          if(typeof inp_alt === 'undefined' || inp_alt == "" || inp_alt == null)
          {
            $("#span_inp_alt").empty();
            inp_alt_isvalid = 0;
          } else {
            response_check_message("#span_inp_alt","error",inp_alt);
            inp_alt_isvalid = 1;
          }

          if(typeof img_user_fname === 'undefined' || inp_alt == "" || inp_alt == null)
          {
            $("#span_inp_img").empty();
            img_user_fname_isvalid = 0;
          } else {
            response_check_message("#span_inp_img","error",img_user_fname);
            img_user_fname_isvalid = 1;
          }

          if(opt_langID_isvalid  == 1
          || opt_typeID_isvalid == 1
          || opt_statusID_isvalid == 1
          || inp_order_isvalid == 1
          || inp_title_isvalid == 1
          || inp_alt_isvalid == 1
          || img_user_fname_isvalid == 1)
          {

          }
          else {
            alert('asd');
            addTblBanner(opt_langID_var,opt_typeID_var,opt_statusID_var,inp_order_var,inp_title_var,inp_alt_var,img_user_fname_var);
            isinsert = 1;

          }
        }
        /********************************************************************************/
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        //alert( "Error" );
        //console.log(errorThrown);
        //console.log(arguments);
        //console.log (XMLHttpRequest, textStatus, errorThrown);

        }
    });
  } catch (e) {
    if (e instanceof SyntaxError) {
      printError(e, true);
    } else {
        printError(e, false);
    }
  } finally {

  }
}
/**************************************End Document*******************************************************/
