
$(document).ready(function(){
  $(function(){
    liveChat();
  });
  /**************************************************************************************/
  $('#sendmessageajax').click(function(event){
    var message = $('input[name = "inputmessageuser"]').val();
    var _token = $('input[name = "_token"]').val();
    var tosend = $('input[name = "tosend"]').val();
    if(message != '')
    {
      action = 'saveusermsg';
      data = {
        action:action ,
        _token:_token,
        message:message,
        tosend:tosend
      };
      ajax_opertaion_func('POST','storeusermessage','json',action,data);
    }
  });
  function liveChat()
  {
    var _token = $('input[name = "_token"]').val();
    action = 'livechatting';
    data = {
      action:action ,
      _token:_token
    };
    ajax_opertaion_func('POST','livechataj','json',action,data);
  }
  function ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data)
  {
    //alert('before: '+JSON.stringify(data));
    //Ajax function
    $.ajax({
      type: type_name,
      url: route_path_name,
      headers:{'X-Requested-With':"XMLHttpRequest"},
      //contentType: false,
      async: true,
      //processData:false,
      dataType: data_type_name,
      cache:false,
      data : data,
      //beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));},
      success: function(response){
        //alert('after: '+JSON.stringify(response));
        var parsedData = JSON.stringify(data);
        obj = JSON.parse(parsedData);
        action = obj.action;
        if(action == "saveusermsg")
        {
          var parsedData = JSON.stringify(response);
          obj = JSON.parse(parsedData);
          /*$('.chat').append('<div class="item">'+
                                      '<img src="../assets/imges/profilepicture/'+obj.idone['img']+'" alt="user image" class="online"/>'+
                                      '<p class="message">'+
                                        '<a href="#" class="name">'+
                                          '<small class="text-muted pull-right"><i class="fa fa-clock-o"></i>'+obj.lastmessage['created_at']+'</small>'+
                                          obj.idone['full_name']+
                                        '</a>'+
                                        obj.lastmessage['message']+
                                      '</p>'+
                                    '</div><!-- /.item -->');*/

        }
        else if (action == "livechatting") {
          var parsedData = JSON.stringify(response);
          obj = JSON.parse(parsedData);
          $('.chat').append('<div class="item">'+
                                      '<img src="../assets/imges/profilepicture/'+obj.idone['img']+'" alt="user image" class="online"/>'+
                                      '<p class="message">'+
                                        '<a href="#" class="name">'+
                                          '<small class="text-muted pull-right"><i class="fa fa-clock-o"></i>'+obj.lastmessage['created_at']+'</small>'+
                                          obj.idone['full_name']+
                                        '</a>'+
                                        obj.lastmessage['message']+
                                      '</p>'+
                                    '</div><!-- /.item -->');
          setTimeout(liveChat,1000);
        }
        /********************************************************************************/
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        if(action == "saveusermsg")
        {

        }
        else if (action == "livechatting") {
          setTimeout(liveChat,5000);
        }
        console.log(errorThrown);
        console.log(arguments);
        console.log (XMLHttpRequest, textStatus, errorThrown);

        }
    });
  }
});
