/**************************************************************************************/
$(document).ready(function(){
  $('body').delegate('#optid_partition','change',function(){
    var _token = $('input[name = "_token"]').val();
    var idpartiton = $('#optid_partition').val();
    action = 'getbyidpartition';
    data = {
      action:action ,
      _token:_token,
      idPartition:idpartiton
    };
    ajax_opertaion_func('POST','getbyidpartition','json',action,data);
  });

    /**************************************************************************************/
  /**************************************************************************************/
    function response_check_message(id_name,mood,message)
    {
      if(mood == 'success')
      {
        return $(id_name).html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
          '<strong>Success!</strong> '+message+'.</div>')
      }
      else if(mood == 'info')
      {
        return $(id_name).html('<div  class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
          '<strong>Info!</strong> '+message+'.</div>')
      }
      else if(mood == 'warning')
      {
        return $(id_name).html('<div  class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
          '<strong>Warning!</strong> '+message+'.</div>')
      }
      else if(mood == 'error')
      {
        return $(id_name).html('<div  class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
          '<strong>Danger!</strong> '+message+'.</div>')
      }

    }
    /**************************************************************************************/
    function ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data)
    {
      //alert('before: '+JSON.stringify(data));
      //Ajax function
      $.ajax({
        type: type_name,
        url: route_path_name,
        headers:{'X-Requested-With':"XMLHttpRequest"},
        //contentType: false,
        async: true,
        //processData:false,
        dataType: data_type_name,
        cache:false,
        data : data,
        //beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));},
        success: function(response){
          //alert('after: '+JSON.stringify(response));
          var parsedData = JSON.stringify(data);
          obj = JSON.parse(parsedData);
          action = obj.action;
          if(action == "getbyidpartition")
          {
            var parsedData = JSON.stringify(response);
            obj = JSON.parse(parsedData);
            var count_loop = obj.response.length;
            $("#optid_parentcategory").empty();
            $("#optid_parentcategory").append('<option value="0" selected="selected">-- Choose Parent Category --</option>');
            for(i=0;i<count_loop;i++)
            {
              var id =obj.response[i]['id'];
              var name = obj.response[i]['name'];
              $("#optid_parentcategory").append('<option value="'+ id +'">'+ name +'</option>');
            }
          }
          /********************************************************************************/
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
          alert( "Error" );
          console.log(errorThrown);
          console.log(arguments);
          console.log (XMLHttpRequest, textStatus, errorThrown);

          }
      });
    }
  /**************************************End Document*******************************************************/
});
