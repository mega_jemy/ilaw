var main_URL = '/';
var prefix_URL = 'ipanel'
var trainingcheckValid_URL = '/training_chkvalid';
var counter = 1;
var isinsert = 0;
var printError = function(error, explicit) {
    console.log(`[${explicit ? 'EXPLICIT' : 'INEXPLICIT'}] ${error.name}: ${error.message}`);
}
function validateTblFields()
{
  var _token = $('input[name = "_token"]').val();
  var opt_lang = $("#opt_langID").val();
  var inp_name = $("#inp_nameID").val();
  var inp_title = $("#inp_titleID").val();
  var inp_desc = $("#editor1").val();
  var inp_link = $("#inp_linkID").val();
  var inp_order = $("#inp_orderID").val();
  var opt_status = $("#opt_statusID").val();
  var inp_oldprice = $("#inp_oldpriceID").val();
  var inp_newprice = $("#inp_newpriceID").val();
  var inp_avatar = $("#img_user_fname").val();
  var opt_hotdeals =$("#opt_hotdealsID").val();
  var opt_dealofmonth = $("#opt_dealofmonthID").val();
  var opt_shownew = $("#opt_shownewID").val();
  var opt_showoos = $("#opt_showoosID").val();
  var opt_showonhome = $("#opt_showonhomeID").val();


  /*if(opt_langID == 0){} else {}
  if(opt_typeID == 0){} else {}
  if(opt_statusID == 0){} else {}
  if(inp_order == ""){} else {}
  if(inp_title == ""){} else {}
  if(inp_alt == ""){} else {}
  if(img_user_fname == ""){} else {}*/

  if(opt_langID == 0 || inp_nameID == "" || inp_title == "" || inp_desc == "" ||
  inp_link == "" || inp_order == "" || opt_status == 0 || inp_oldprice == "" || inp_newprice == ""
   || inp_avatar == "" || opt_hotdeals == 0 || opt_dealofmonth == 0 || opt_shownew == 0
   || opt_showoos == 0 || opt_showonhome == 0)
  {
    var type_name = "get";
    var route_path_name = main_URL + prefix_URL + trainingcheckValid_URL;
    var data_type_name = "json";
    var action = "check_valid_data";
    var data = {
      action:action,
      _token:_token,
      opt_lang :opt_lang,
      inp_name :inp_name,
      inp_title :inp_title,
      inp_desc :inp_desc,
      inp_link :inp_link,
      inp_order :inp_order,
      opt_status :opt_status,
      inp_oldprice :inp_oldprice,
      inp_newprice :inp_newprice,
      inp_avatar :inp_avatar,
      opt_hotdeals :opt_hotdeals,
      opt_dealofmonth :opt_dealofmonth,
      opt_shownew :opt_shownew,
      opt_showoos :opt_showoos,
      opt_showonhome :opt_showonhome,
    };
    ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data);
  }
  else {
    return true;
  }

}
function addTbl(opt_lang,inp_name,inp_title,inp_desc,inp_link,inp_order,opt_status,inp_oldprice,inp_newprice,inp_avatar,opt_hotdeals,opt_dealofmonth,opt_shownew,opt_showoos,opt_showonhome)
{
  alert(opt_lang+","+inp_name+","+inp_title+","+inp_desc+","+inp_link+","+inp_order+","+opt_status+","+
  inp_oldprice+","+inp_newprice+","+inp_avatar+","+opt_hotdeals+","+opt_dealofmonth+","+opt_shownew
  +","+opt_showoos+","+opt_showonhome);
  var t = $('#example').DataTable();
    t.row.add( [
        counter,
        '<input width="50px" type="text" readonly value="'+opt_lang+'" class="form-control" id="opt_lang" name="opt_lang[]">',
        '<input width="50px" type="text" readonly value="'+inp_name+'" class="form-control" id="inp_name" name="inp_name[]">',
        '<input width="50px" type="text" readonly value="'+inp_title+'" class="form-control" id="inp_title" name="inp_title[]">',
        '<input width="50px" type="text" readonly value="'+inp_desc+'" class="form-control" id="inp_desc" name="inp_desc"[]>',
        '<input width="50px" type="text" readonly value="'+inp_link+'" class="form-control" id="inp_link" name="inp_link[]">',
        '<input width="50px" type="text" readonly value="'+inp_order+'" class="form-control" id="inp_order" name="inp_order[]">',
        '<input width="50px" type="text" readonly value="'+opt_status+'" class="form-control" id="opt_status" name="opt_status[]">',
        '<input width="50px" type="text" readonly value="'+inp_oldprice+'" class="form-control" id="inp_oldprice" name="inp_oldprice[]">',
        '<input width="50px" type="text" readonly value="'+inp_newprice+'" class="form-control" id="inp_newprice" name="inp_newprice[]">',
        '<input width="50px" type="text" readonly value="'+opt_hotdeals+'" class="form-control" id="opt_hotdeals" name="opt_hotdeals[]">',
        '<input width="50px" type="text" readonly value="'+opt_dealofmonth+'" class="form-control" id="opt_dealofmonth" name="opt_dealofmonth[]">',
        '<input width="50px" type="text" readonly value="'+opt_shownew+'" class="form-control" id="opt_shownew" name="opt_shownew[]">',
        '<input width="50px" type="text" readonly value="'+opt_showoos+'" class="form-control" id="opt_showoos" name="opt_showoos[]">',
        '<input width="50px" type="text" readonly value="'+opt_showonhome+'" class="form-control" id="opt_showonhome" name="opt_showonhome[]">',
        '<input width="50px" type="text" readonly value="'+inp_avatar+'" class="form-control" id="inp_avatar" name="inp_avatar[]" multiple>',
        '<span class="badge">remove</span>',
    ] ).draw( false );

    counter++;
    $("#opt_langID").val(0);
    $("#inp_nameID").val("");
    $("#inp_titleID").val("");
    $("#editor1").val();
    $("#inp_linkID").val("");
    $("#inp_orderID").val("");
    $("#opt_statusID").val(0);
    $("#inp_oldpriceID").val(0);
    $("#inp_newpriceID").val("");
    $("#img_user_fname").val();
    $("#opt_hotdealsID").val(0);
    $("#opt_dealofmonthID").val(0);
    $("#opt_shownewID").val(0);
    $("#opt_showoosID").val(0);
    $("#opt_showonhomeID").val(0);
}
/*******************************************************************************/
function response_check_message(id_name,mood,message)
{
  if(mood == 'success')
  {
    return $(id_name).html('<div id="'+id_name+'" class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Success!</strong> '+message+'.</div>');
  }
  else if(mood == 'info')
  {
    return $(id_name).html('<div id="'+id_name+'"  class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Info!</strong> '+message+'.</div>');
  }
  else if(mood == 'warning')
  {
    return $(id_name).html('<div id="'+id_name+'"  class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Warning!</strong> '+message+'.</div>');
  }
  else if(mood == 'error')
  {
    return $(id_name).html('<div id="'+id_name+'"  class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Danger!</strong> '+message+'.</div>');
  }
  else {
    return $(id_name).html('');
  }

}
/**************************************************************************************/
function ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data)
{
  try {
    //alert('before: '+JSON.stringify(data));
    //Ajax function
    $.ajax({
      type: type_name,
      url: route_path_name,
      //headers:{'X-Requested-With':"XMLHttpRequest"},
      //contentType: false,
      async: true,
      //processData:false,
      dataType: data_type_name,
      cache:false,
      data : data,
      //beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));},
      success: function(response){
        alert('after: '+JSON.stringify(response));
        var parsedData = JSON.stringify(data);
        obj = JSON.parse(parsedData);
        action = obj.action;
        if(action == "check_valid_data")
        {
          var opt_lang_var = obj.opt_lang;
          var inp_name_var = obj.inp_name;
          var inp_title_var = obj.inp_title;
          var inp_desc_var = obj.inp_desc;
          var inp_link_var = obj.inp_link;
          var inp_order_var = obj.inp_order;
          var opt_status_var = obj.opt_status;
          var inp_oldprice_var = obj.inp_oldprice;
          var inp_newprice_var = obj.inp_newprice;
          var inp_avatar_var = obj.inp_avatar;
          var opt_hotdeals_var = obj.opt_hotdeals;
          var opt_dealofmonth_var = obj.opt_dealofmonth;
          var opt_shownew_var = obj.opt_shownew;
          var opt_showoos_var = obj.opt_showoos;
          var opt_showonhome_var = obj.opt_showonhome;


          var parsedData = JSON.stringify(response);
          obj = JSON.parse(parsedData);
          var opt_lang  = obj.response.opt_lang;
          var inp_name = obj.response.inp_name;
          var inp_title = obj.response.inp_title;
          var inp_desc = obj.response.inp_desc;
          var inp_link = obj.response.inp_link;
          var inp_order = obj.response.inp_order;
          var opt_status = obj.response.opt_status;
          var inp_oldprice = obj.response.inp_oldprice;
          var inp_newprice = obj.response.inp_newprice;
          var inp_avatar = obj.response.inp_avatar;
          var opt_hotdeals = obj.response.opt_hotdeals;
          var opt_dealofmonth = obj.response.opt_dealofmonth;
          var opt_shownew = obj.response.opt_shownew;
          var opt_showoos = obj.response.opt_showoos;
          var opt_showonhome = obj.response.opt_showonhome;


          var opt_lang_isvalid  = 0;
          var inp_name_isvalid  = 0;
          var inp_title_isvalid  = 0;
          var inp_desc_isvalid  = 0;
          var inp_link_isvalid  = 0;
          var inp_order_isvalid  = 0;
          var opt_status_isvalid  = 0;
          var inp_oldprice_isvalid  = 0;
          var inp_newprice_isvalid  = 0;
          var inp_avatar_isvalid  = 0;
          var opt_hotdeals_isvalid  = 0;
          var opt_dealofmonth_isvalid  = 0;
          var opt_shownew_isvalid  = 0;
          var opt_showoos_isvalid  = 0;
          var opt_showonhome_isvalid  = 0;
          alert(opt_lang);
/***************************************************************************************************************/
          if(typeof opt_lang === 'undefined' || opt_lang == "" || opt_lang == null)
          {
            $("#span_opt_lang").empty();
            opt_lang_isvalid = 0;
          } else {
            response_check_message("#span_opt_lang","error",opt_lang);
            opt_lang_isvalid = 1;
          }
/***************************************************************************************************************/
          if(typeof inp_name === 'undefined' || inp_name == "" || inp_name == null)
          {
            $("#span_inp_name").empty();
            inp_name_isvalid = 0;
          } else {
            response_check_message("#span_inp_name","error",inp_name);
            inp_name_isvalid = 1;
          }
/***************************************************************************************************************/
          if(typeof inp_title === 'undefined' || inp_title == "" || inp_title == null)
          {
            $("#span_inp_title").empty();
            inp_title_isvalid = 0;
          } else {
            response_check_message("#span_inp_title","error",inp_title);
            inp_title_isvalid = 1;
          }
/***************************************************************************************************************/
          if(typeof inp_desc === 'undefined' || inp_desc == "" || inp_desc == null)
          {
            $("#span_inp_desc").empty();
            inp_desc_isvalid = 0;
          } else {
            response_check_message("#span_inp_desc","error",inp_desc);
            inp_desc_isvalid = 1;
          }
/***************************************************************************************************************/
          if(typeof inp_link === 'undefined' || inp_link == "" || inp_link == null)
          {
            $("#span_inp_link").empty();
            inp_link_isvalid = 0;
          } else {
            response_check_message("#span_inp_link","error",inp_title);
            inp_link_isvalid = 1;
          }
/***************************************************************************************************************/
          if(typeof inp_order === 'undefined' || inp_order == "" || inp_order == null)
          {
            $("#span_inp_order").empty();
            inp_order_isvalid = 0;
          } else {
            response_check_message("#span_inp_order","error",inp_order);
            inp_order_isvalid = 1;
          }
/***************************************************************************************************************/
          if(typeof opt_status === 'undefined' || opt_status == "" || opt_status == null)
          {
            $("#span_opt_status").empty();
            opt_status_isvalid = 0;
          } else {
            response_check_message("#span_opt_status","error",opt_status);
            opt_status_isvalid = 1;
          }
/***************************************************************************************************************/
          if(typeof inp_oldprice === 'undefined' || inp_oldprice == "" || inp_oldprice == null)
          {
            $("#span_inp_oldprice").empty();
            inp_oldprice_isvalid = 0;
          } else {
            response_check_message("#span_inp_oldprice","error",inp_oldprice);
            inp_oldprice_isvalid = 1;
          }
/***************************************************************************************************************/
          if(typeof inp_newprice === 'undefined' || inp_newprice == "" || inp_newprice == null)
          {
            $("#span_inp_newprice").empty();
            inp_newprice_isvalid = 0;
          } else {
            response_check_message("#span_inp_newprice","error",inp_newprice);
            inp_newprice_isvalid = 1;
          }
/***************************************************************************************************************/
          if(typeof inp_avatar === 'undefined' || inp_avatar == "" || inp_avatar == null)
          {
            $("#span_inp_avatar").empty();
            inp_avatar_isvalid = 0;
          } else {
            response_check_message("#span_inp_avatar","error",inp_avatar);
            inp_avatar_isvalid = 1;
          }
/***************************************************************************************************************/
        if(typeof opt_hotdeals === 'undefined' || opt_hotdeals == "" || opt_hotdeals == null)
        {
          $("#span_opt_hotdeals").empty();
          opt_hotdeals_isvalid = 0;
        } else {
          response_check_message("#span_opt_hotdeals","error",opt_hotdeals);
          opt_hotdeals_isvalid = 1;
        }
/***************************************************************************************************************/
        if(typeof opt_dealofmonth === 'undefined' || opt_dealofmonth == "" || opt_dealofmonth == null)
        {
          $("#span_opt_dealofmonth").empty();
          opt_dealofmonth_isvalid = 0;
        } else {
          response_check_message("#span_opt_dealofmonth","error",opt_dealofmonth);
          opt_dealofmonth_isvalid = 1;
        }
/***************************************************************************************************************/
        if(typeof inp_newprice === 'undefined' || opt_shownew == "" || opt_shownew == null)
        {
          $("#span_opt_shownew").empty();
          opt_shownew_isvalid = 0;
        } else {
          response_check_message("#span_opt_shownew","error",opt_shownew);
          opt_shownew_isvalid = 1;
        }
/***************************************************************************************************************/
        if(typeof inp_newprice === 'undefined' || opt_showoos == "" || opt_showoos == null)
        {
          $("#span_opt_showoos").empty();
          opt_showoos_isvalid = 0;
        } else {
          response_check_message("#span_opt_showoos","error",opt_showoos);
          opt_showoos_isvalid = 1;
        }
/***************************************************************************************************************/
        if(typeof opt_showonhome === 'undefined' || opt_showonhome == "" || opt_showonhome == null)
        {
          $("#span_opt_showonhome").empty();
          opt_showonhome_isvalid = 0;
        } else {
          response_check_message("#span_opt_showonhome","error",opt_showonhome);
          opt_showonhome_isvalid = 1;
        }
/***************************************************************************************************************/
          if(opt_lang_isvalid  == 1 ||
          inp_name_isvalid  == 1 ||
          inp_title_isvalid  == 1 ||
          inp_desc_isvalid  == 1 ||
          inp_link_isvalid  == 1 ||
          inp_order_isvalid  == 1 ||
          opt_status_isvalid  == 1 ||
          inp_oldprice_isvalid  == 1 ||
          inp_newprice_isvalid  == 1 ||
          inp_avatar_isvalid  == 1 ||
          opt_hotdeals_isvalid  == 1 ||
          opt_dealofmonth_isvalid  == 1 ||
          opt_shownew_isvalid  == 1 ||
          opt_showoos_isvalid  == 1 ||
          opt_showonhome_isvalid  == 1)
          {

          }
          else {
            alert('asd');
            addTbl(opt_lang_var,inp_name_var,inp_title_var,inp_desc_var,
              inp_link_var,inp_order_var,opt_status_var,inp_oldprice_var,
              inp_newprice_var,inp_avatar_var,opt_hotdeals_var,opt_dealofmonth_var,
              opt_shownew_var,opt_showoos_var,opt_showonhome_var);
            isinsert = 1;

          }
        }
        /********************************************************************************/
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        //alert( "Error" );
        //console.log(errorThrown);
        //console.log(arguments);
        //console.log (XMLHttpRequest, textStatus, errorThrown);

        }
    });
  } catch (e) {
    if (e instanceof SyntaxError) {
      printError(e, true);
    } else {
        printError(e, false);
    }
  } finally {

  }
}
/**************************************End Document*******************************************************/
