/**************************************************************************************/
var data = "";
$(document).ready(function(){
  $('body').delegate('.dbl_state','change',function(){
    var order_id = $(this).data('id');
    var id = '#dbl_stateid'+order_id;
    var state_id = $(id).val();
    //alert(order_id+','+state_id);
    var _token = $('input[name = "_token"]').val();
    action = 'changestate';
    data = {
      action:action ,
      _token:_token,
      order_id:order_id,
      state_id:state_id
    };
    var route_url = window.location.href +'/../changeorderstate';
    ajax_opertaion_func('POST',route_url,'json',action,data);
  });

    /**************************************************************************************/
  /**************************************************************************************/
    function response_check_message(id_name,mood,message)
    {
      if(mood == 'success')
      {
        return $(id_name).html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
          '<strong>Success!</strong> '+message+'.</div>')
      }
      else if(mood == 'info')
      {
        return $(id_name).html('<div  class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
          '<strong>Info!</strong> '+message+'.</div>')
      }
      else if(mood == 'warning')
      {
        return $(id_name).html('<div  class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
          '<strong>Warning!</strong> '+message+'.</div>')
      }
      else if(mood == 'error')
      {
        return $(id_name).html('<div  class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
          '<strong>Danger!</strong> '+message+'.</div>')
      }

    }
    /**************************************************************************************/
    function ajax_opertaion_func(type_name,route_path_name,data_type_name,action,data)
    {
      //alert('before: '+JSON.stringify(data));
      //Ajax function
      $.ajax({
        type: type_name,
        url: route_path_name,
        headers:{'X-Requested-With':"XMLHttpRequest"},
        //contentType: false,
        async: true,
        //processData:false,
        dataType: data_type_name,
        cache:false,
        data : data,
        //beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));},
        success: function(response){
          //alert('after: '+JSON.stringify(response));
          var parsedData = JSON.stringify(data);
          obj = JSON.parse(parsedData);
          action = obj.action;
          if(action == "changestate")
          {
            var parsedData = JSON.stringify(response);
            obj = JSON.parse(parsedData);
            data = "";
            location.reload();

          }
          /********************************************************************************/
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
          alert( "Error" );
          console.log(errorThrown);
          console.log(arguments);
          console.log (XMLHttpRequest, textStatus, errorThrown);

          }
      });
    }
  /**************************************End Document*******************************************************/
});
