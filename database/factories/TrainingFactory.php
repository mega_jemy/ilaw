<?php

use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'title' => $faker->title,
        'img_path' => $faker->img_path,
        'desc' => $faker->desc,
        'link' => $faker->link,
        'status' => $faker->status,
        'order' => $faker->order,
        'softdelete' => $faker->softdelete,
        'created_at' => $faker->created_at,
        'updated_at' => $faker->updated_at,
        'deleted_at' => $faker->deleted_at,
        'oldprice' => $faker->oldprice,
        'newprice' => $faker->newprice,
        'hotdeals' => $faker->hotdeals,
        'monthoffer' => $faker->monthoffer,
        'shownew' => $faker->shownew,
        'showoutofstock' => $faker->showoutofstock,
        'showhome' => $faker->showhome,
    ];
});
