/*
Navicat MySQL Data Transfer

Source Server         : local_mysql
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : ilawae

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-06-23 07:05:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_name` varchar(254) DEFAULT NULL,
  `img_title` varchar(254) DEFAULT NULL,
  `img_alt` varchar(254) DEFAULT NULL,
  `img_desc` text,
  `link` text,
  `status` enum('Not-active','Active') DEFAULT NULL,
  `order` varchar(11) DEFAULT '0',
  `typeban` enum('footer','right','top') DEFAULT NULL,
  `softdelete` enum('Not-delete','Delete') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner
-- ----------------------------

-- ----------------------------
-- Table structure for banner_det
-- ----------------------------
DROP TABLE IF EXISTS `banner_det`;
CREATE TABLE `banner_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_banner` int(11) DEFAULT NULL,
  `idlang` int(11) DEFAULT NULL,
  `img_name` varchar(254) DEFAULT NULL,
  `img_title` varchar(254) DEFAULT NULL,
  `img_alt` varchar(254) DEFAULT NULL,
  `img_desc` text,
  `link` text,
  `status` enum('Not-active','Active') DEFAULT NULL,
  `softdelete` enum('Not-delete','Delete') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_id_banner` (`id_banner`),
  CONSTRAINT `FK_id_banner` FOREIGN KEY (`id_banner`) REFERENCES `banner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner_det
-- ----------------------------

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `name_code` varchar(255) DEFAULT NULL,
  `status` enum('sale','not-sale') DEFAULT NULL,
  `shippmentcost` float(10,0) DEFAULT NULL,
  `c_code` varchar(200) DEFAULT NULL,
  `taxcost` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `opay_cost` float DEFAULT NULL,
  `cod_option` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('1', 'Afghanistan', 'AFG', 'not-sale', '0', '+93', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:21', '0', '0');
INSERT INTO `country` VALUES ('2', 'Åland Islands', 'ALA', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:21', '0', '0');
INSERT INTO `country` VALUES ('3', 'Albania', 'ALB', 'not-sale', '0', '+355', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:21', '0', '0');
INSERT INTO `country` VALUES ('4', 'Algeria', 'DZA', 'not-sale', '0', '+213', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:22', '0', '0');
INSERT INTO `country` VALUES ('5', 'American Samoa', 'ASM', 'not-sale', '0', '+1-684', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:22', '0', '0');
INSERT INTO `country` VALUES ('6', 'Andorra', 'AND', 'not-sale', '0', '+376', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:23', '0', '0');
INSERT INTO `country` VALUES ('7', 'Angola', 'AGO', 'not-sale', '0', '+244', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:23', '0', '0');
INSERT INTO `country` VALUES ('8', 'Anguilla', 'AIA', 'not-sale', '0', '+1-264', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:23', '0', '0');
INSERT INTO `country` VALUES ('9', 'Antarctica', 'ATA', 'not-sale', '0', '+672', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:24', '0', '0');
INSERT INTO `country` VALUES ('10', 'Antigua and Barbuda', 'ATG', 'not-sale', '0', '+1-268', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:24', '0', '0');
INSERT INTO `country` VALUES ('11', 'Argentina', 'ARG', 'not-sale', '0', '+54', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:25', '0', '0');
INSERT INTO `country` VALUES ('12', 'Armenia', 'ARM', 'not-sale', '0', '+374', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:25', '0', '0');
INSERT INTO `country` VALUES ('13', 'Aruba', 'ABW', 'not-sale', '0', '+297', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:25', '0', '0');
INSERT INTO `country` VALUES ('14', 'Australia', 'AUS', 'not-sale', '0', '+61', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:26', '0', '0');
INSERT INTO `country` VALUES ('15', 'Austria', 'AUT', 'not-sale', '0', '+43', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:26', '0', '0');
INSERT INTO `country` VALUES ('16', 'Azerbaijan', 'AZE', 'not-sale', '0', '+994', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:27', '0', '0');
INSERT INTO `country` VALUES ('17', 'Bahamas', 'BHS', 'not-sale', '0', '+1-242', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:27', '0', '0');
INSERT INTO `country` VALUES ('18', 'Bahrain', 'BHR', 'not-sale', '0', '+973', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:27', '0', '0');
INSERT INTO `country` VALUES ('19', 'Bangladesh', 'BGD', 'not-sale', '0', '+880', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:28', '0', '0');
INSERT INTO `country` VALUES ('20', 'Barbados', 'BRB', 'not-sale', '0', '+1-246', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:28', '0', '0');
INSERT INTO `country` VALUES ('21', 'Belarus', 'BLR', 'not-sale', '0', '+375', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:29', '0', '0');
INSERT INTO `country` VALUES ('22', 'Belgium', 'BEL', 'not-sale', '0', '+32', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:29', '0', '0');
INSERT INTO `country` VALUES ('23', 'Belize', 'BLZ', 'not-sale', '0', '+501', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:30', '0', '0');
INSERT INTO `country` VALUES ('24', 'Benin', 'BEN', 'not-sale', '0', '+229', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:30', '0', '0');
INSERT INTO `country` VALUES ('25', 'Bermuda', 'BMU', 'not-sale', '0', '+1-441', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:30', '0', '0');
INSERT INTO `country` VALUES ('26', 'Bhutan', 'BTN', 'not-sale', '0', '+975', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:31', '0', '0');
INSERT INTO `country` VALUES ('27', 'Bolivia', 'BOL', 'not-sale', '0', '+591', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:31', '0', '0');
INSERT INTO `country` VALUES ('28', 'Bonaire, Sint Eustatius and Saba', 'BES', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:32', '0', '0');
INSERT INTO `country` VALUES ('29', 'Bosnia and Herzegovina', 'BIH', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:32', '0', '0');
INSERT INTO `country` VALUES ('30', 'Botswana', 'BWA', 'not-sale', '0', '+267', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:32', '0', '0');
INSERT INTO `country` VALUES ('31', 'Bouvet Island', 'BVT', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:33', '0', '0');
INSERT INTO `country` VALUES ('32', 'Brazil', 'BRA', 'not-sale', '0', '+55', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:33', '0', '0');
INSERT INTO `country` VALUES ('33', 'British Indian Ocean Territory', 'IOT', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:34', '0', '0');
INSERT INTO `country` VALUES ('34', 'British Virgin Islands', 'VGB', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:34', '0', '0');
INSERT INTO `country` VALUES ('35', 'Brunei', 'BRN', 'not-sale', '0', '+673', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:34', '0', '0');
INSERT INTO `country` VALUES ('36', 'Bulgaria', 'BGR', 'not-sale', '0', '+359', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:35', '0', '0');
INSERT INTO `country` VALUES ('37', 'Burkina Faso', 'BFA', 'not-sale', '0', '+226', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:35', '0', '0');
INSERT INTO `country` VALUES ('38', 'Burundi', 'BDI', 'not-sale', '0', '+257', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:36', '0', '0');
INSERT INTO `country` VALUES ('39', 'Cambodia', 'KHM', 'not-sale', '0', '+855', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:36', '0', '0');
INSERT INTO `country` VALUES ('40', 'Cameroon', 'CMR', 'not-sale', '0', '+237', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:37', '0', '0');
INSERT INTO `country` VALUES ('41', 'Canada', 'CAN', 'not-sale', '0', '+1', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:37', '0', '0');
INSERT INTO `country` VALUES ('42', 'Cape Verde', 'CPV', 'not-sale', '0', '+238', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:37', '0', '0');
INSERT INTO `country` VALUES ('43', 'Cayman Islands', 'CYM', 'not-sale', '0', '+1-345', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:38', '0', '0');
INSERT INTO `country` VALUES ('44', 'Central African Republic', 'CAF', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:38', '0', '0');
INSERT INTO `country` VALUES ('45', 'Chad', 'TCD', 'not-sale', '0', '+235', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:39', '0', '0');
INSERT INTO `country` VALUES ('46', 'Chile', 'CHL', 'not-sale', '0', '+56', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:39', '0', '0');
INSERT INTO `country` VALUES ('47', 'China', 'CHN', 'not-sale', '0', '+86', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:39', '0', '0');
INSERT INTO `country` VALUES ('48', 'Christmas Island', 'CXR', 'not-sale', '0', '+61', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:40', '0', '0');
INSERT INTO `country` VALUES ('49', 'Cocos Islands', 'CCK', 'not-sale', '0', '+61', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:40', '0', '0');
INSERT INTO `country` VALUES ('50', 'Colombia', 'COL', 'not-sale', '0', '+57', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:41', '0', '0');
INSERT INTO `country` VALUES ('51', 'Comoros', 'COM', 'not-sale', '0', '+269', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:41', '0', '0');
INSERT INTO `country` VALUES ('52', 'Republic of Congo', 'COG', 'not-sale', '0', '+242', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:41', '0', '0');
INSERT INTO `country` VALUES ('53', 'Cook Islands', 'COK', 'not-sale', '0', '+682', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:42', '0', '0');
INSERT INTO `country` VALUES ('54', 'Costa Rica', 'CRI', 'not-sale', '0', '+506', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:42', '0', '0');
INSERT INTO `country` VALUES ('55', 'Côte dIvoire', 'CIV', 'not-sale', '0', '+225', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:43', '0', '0');
INSERT INTO `country` VALUES ('56', 'Croatia', 'HRV', 'not-sale', '0', '+385', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:43', '0', '0');
INSERT INTO `country` VALUES ('57', 'Cuba', 'CUB', 'not-sale', '0', '+53', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:44', '0', '0');
INSERT INTO `country` VALUES ('58', 'Curaçao', 'CUW', 'not-sale', '0', '+599', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:44', '0', '0');
INSERT INTO `country` VALUES ('59', 'cyprus', 'CYP', 'not-sale', '0', '+357', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:44', '0', '0');
INSERT INTO `country` VALUES ('60', 'Czech Republic', 'CZE', 'not-sale', '0', '+420', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:45', '0', '0');
INSERT INTO `country` VALUES ('61', 'Denmark', 'DNK', 'not-sale', '0', '+45', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:45', '0', '0');
INSERT INTO `country` VALUES ('62', 'Djibouti', 'DJI', 'not-sale', '0', '+253', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:46', '0', '0');
INSERT INTO `country` VALUES ('63', 'Dominica', 'DMA', 'not-sale', '0', '+1-767', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:46', '0', '0');
INSERT INTO `country` VALUES ('64', 'Dominican Republic', 'DOM', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:47', '0', '0');
INSERT INTO `country` VALUES ('65', 'Ecuador', 'ECU', 'not-sale', '0', '+593', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:47', '0', '0');
INSERT INTO `country` VALUES ('66', 'Egypt', 'EGY', 'not-sale', '0', '+20', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:47', '0', '0');
INSERT INTO `country` VALUES ('67', 'El Salvador', 'SLV', 'not-sale', '0', '+503', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:48', '0', '0');
INSERT INTO `country` VALUES ('68', 'Equatorial Guinea', 'GNQ', 'not-sale', '0', '+240', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:48', '0', '0');
INSERT INTO `country` VALUES ('69', 'Eritrea', 'ERI', 'not-sale', '0', '+291', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:49', '0', '0');
INSERT INTO `country` VALUES ('70', 'Estonia', 'EST', 'not-sale', '0', '+372', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:49', '0', '0');
INSERT INTO `country` VALUES ('71', 'Ethiopia', 'ETH', 'not-sale', '0', '+251', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:49', '0', '0');
INSERT INTO `country` VALUES ('72', 'Falkland Islands', 'FLK', 'not-sale', '0', '+500', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:50', '0', '0');
INSERT INTO `country` VALUES ('73', 'Faroe Islands', 'FRO', 'not-sale', '0', '+298', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:50', '0', '0');
INSERT INTO `country` VALUES ('74', 'Fiji', 'FJI', 'not-sale', '0', '+679', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:51', '0', '0');
INSERT INTO `country` VALUES ('75', 'Finland', 'FIN', 'not-sale', '0', '+358', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:51', '0', '0');
INSERT INTO `country` VALUES ('76', 'France', 'FRA', 'not-sale', '0', '+33', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:52', '0', '0');
INSERT INTO `country` VALUES ('77', 'French Guiana', 'GUF', 'not-sale', '0', '+594', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:52', '0', '0');
INSERT INTO `country` VALUES ('78', 'French Polynesia', 'PYF', 'not-sale', '0', '+689', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:52', '0', '0');
INSERT INTO `country` VALUES ('79', 'French Southen Territories', 'ATF', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:53', '0', '0');
INSERT INTO `country` VALUES ('80', 'Gabon', 'GAB', 'not-sale', '0', '+241', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:53', '0', '0');
INSERT INTO `country` VALUES ('81', 'Gambia', 'GMB', 'not-sale', '0', '+220', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:54', '0', '0');
INSERT INTO `country` VALUES ('82', 'Georgia', 'GEO', 'not-sale', '0', '+995', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:54', '0', '0');
INSERT INTO `country` VALUES ('83', 'Germany', 'DEU', 'not-sale', '0', '+49', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:54', '0', '0');
INSERT INTO `country` VALUES ('84', 'Ghana', 'GHA', 'not-sale', '0', '+233', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:55', '0', '0');
INSERT INTO `country` VALUES ('85', 'Gibraltar', 'GIB', 'not-sale', '0', '+350', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:55', '0', '0');
INSERT INTO `country` VALUES ('86', 'Greece', 'GRC', 'not-sale', '0', '+30', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:56', '0', '0');
INSERT INTO `country` VALUES ('87', 'Greenland', 'GRL', 'not-sale', '0', '+299', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:56', '0', '0');
INSERT INTO `country` VALUES ('88', 'Grenada', 'GRD', 'not-sale', '0', '+1-473', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:56', '0', '0');
INSERT INTO `country` VALUES ('89', 'Guadeloupe', 'GLP', 'not-sale', '0', '+590', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:57', '0', '0');
INSERT INTO `country` VALUES ('90', 'Guam', 'GUM', 'not-sale', '0', '+1-671', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:57', '0', '0');
INSERT INTO `country` VALUES ('91', 'Guatemala', 'GTM', 'not-sale', '0', '+502', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:58', '0', '0');
INSERT INTO `country` VALUES ('92', 'Guernsey', 'GGY', 'not-sale', '0', '+44-1481', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:58', '0', '0');
INSERT INTO `country` VALUES ('93', 'Guinea', 'GIN', 'not-sale', '0', '+224', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:59', '0', '0');
INSERT INTO `country` VALUES ('94', 'Guinea-Bissau', 'GNB', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:46:59', '0', '0');
INSERT INTO `country` VALUES ('95', 'Guyana', 'GUY', 'not-sale', '0', '+592', '5', '2017-07-25 06:54:12', '2018-01-21 08:46:59', '0', '0');
INSERT INTO `country` VALUES ('96', 'Haiti', 'HTI', 'not-sale', '0', '+509', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:00', '0', '0');
INSERT INTO `country` VALUES ('97', 'Heard Island And McDonald Islands', 'HMD', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:00', '0', '0');
INSERT INTO `country` VALUES ('98', 'Honduras', 'HND', 'not-sale', '0', '+504', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:01', '0', '0');
INSERT INTO `country` VALUES ('99', 'Hong Kong', 'HKG', 'not-sale', '0', '+852', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:01', '0', '0');
INSERT INTO `country` VALUES ('100', 'Hungary', 'HUN', 'not-sale', '0', '+36', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:01', '0', '0');
INSERT INTO `country` VALUES ('101', 'Iceland', 'ISL', 'not-sale', '0', '+354', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:02', '0', '0');
INSERT INTO `country` VALUES ('102', 'India', 'IND', 'not-sale', '0', '+91', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:02', '0', '0');
INSERT INTO `country` VALUES ('103', 'Indonesia', 'IDN', 'not-sale', '0', '+62', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:03', '0', '0');
INSERT INTO `country` VALUES ('104', 'Iran', 'IRN', 'not-sale', '0', '+98', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:03', '0', '0');
INSERT INTO `country` VALUES ('105', 'Iraq', 'IRQ', 'not-sale', '0', '+964', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:03', '0', '0');
INSERT INTO `country` VALUES ('106', 'Ireland', 'IRL', 'not-sale', '0', '+353', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:04', '0', '0');
INSERT INTO `country` VALUES ('107', 'Isle Of Man', 'IMN', 'not-sale', '0', '+44-1624', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:04', '0', '0');
INSERT INTO `country` VALUES ('108', 'Italy', 'ITA', 'not-sale', '0', '+39', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:05', '0', '0');
INSERT INTO `country` VALUES ('109', 'Jamaica', 'JAM', 'not-sale', '0', '+1-876', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:05', '0', '0');
INSERT INTO `country` VALUES ('110', 'Japan', 'JPN', 'not-sale', '0', '+81', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:05', '0', '0');
INSERT INTO `country` VALUES ('111', 'Jersey', 'JEY', 'not-sale', '0', '+44-1534', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:06', '0', '0');
INSERT INTO `country` VALUES ('112', 'Jordan', 'JOR', 'not-sale', '0', '+962', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:06', '0', '0');
INSERT INTO `country` VALUES ('113', 'Kazakhstan', 'KAZ', 'not-sale', '0', '+7', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:07', '0', '0');
INSERT INTO `country` VALUES ('114', 'Kenya', 'KEN', 'not-sale', '0', '+254', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:07', '0', '0');
INSERT INTO `country` VALUES ('115', 'Kiribati', 'KIR', 'not-sale', '0', '+686', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:08', '0', '0');
INSERT INTO `country` VALUES ('116', 'Kuwait', 'KWT', 'not-sale', '0', '+965', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:08', '0', '0');
INSERT INTO `country` VALUES ('117', 'Kyrgyzstan', 'KGZ', 'not-sale', '0', '+996', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:08', '0', '0');
INSERT INTO `country` VALUES ('118', 'Laos', 'LAO', 'not-sale', '0', '+856', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:09', '0', '0');
INSERT INTO `country` VALUES ('119', 'Latvia', 'LVA', 'not-sale', '0', '+371', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:09', '0', '0');
INSERT INTO `country` VALUES ('120', 'Lebanon', 'LBN', 'not-sale', '0', '+961', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:10', '0', '0');
INSERT INTO `country` VALUES ('121', 'Lesotho', 'LSO', 'not-sale', '0', '+266', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:10', '0', '0');
INSERT INTO `country` VALUES ('122', 'Liberia', 'LBR', 'not-sale', '0', '+231', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:10', '0', '0');
INSERT INTO `country` VALUES ('123', 'Libya', 'LBY', 'not-sale', '0', '+218', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:11', '0', '0');
INSERT INTO `country` VALUES ('124', 'Liechtenstein', 'LIE', 'not-sale', '0', '+423', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:11', '0', '0');
INSERT INTO `country` VALUES ('125', 'Lithuania', 'LTU', 'not-sale', '0', '+370', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:12', '0', '0');
INSERT INTO `country` VALUES ('126', 'Luxembourg', 'LUX', 'not-sale', '0', '+352', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:12', '0', '0');
INSERT INTO `country` VALUES ('127', 'Macao', 'MAC', 'not-sale', '0', '+853', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:12', '0', '0');
INSERT INTO `country` VALUES ('128', 'Macedonia', 'MKD', 'not-sale', '0', '+389', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:13', '0', '0');
INSERT INTO `country` VALUES ('129', 'Madagascar', 'MDG', 'not-sale', '0', '+261', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:13', '0', '0');
INSERT INTO `country` VALUES ('130', 'Malawi', 'MWI', 'not-sale', '0', '+265', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:14', '0', '0');
INSERT INTO `country` VALUES ('131', 'Malaysia', 'MYS', 'not-sale', '0', '+60', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:14', '0', '0');
INSERT INTO `country` VALUES ('132', 'Maldives', 'MDV', 'not-sale', '0', '+960', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:14', '0', '0');
INSERT INTO `country` VALUES ('133', 'Mali', 'MLI', 'not-sale', '0', '+223', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:15', '0', '0');
INSERT INTO `country` VALUES ('134', 'Malta', 'MLT', 'not-sale', '0', '+356', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:15', '0', '0');
INSERT INTO `country` VALUES ('135', 'Marshall Islands', 'MHL', 'not-sale', '0', '+692', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:16', '0', '0');
INSERT INTO `country` VALUES ('136', 'Martinique', 'MTQ', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:16', '0', '0');
INSERT INTO `country` VALUES ('137', 'Mauritania', 'MRT', 'not-sale', '0', '+222', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:17', '0', '0');
INSERT INTO `country` VALUES ('138', 'Mauritius', 'MUS', 'not-sale', '0', '+230', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:17', '0', '0');
INSERT INTO `country` VALUES ('139', 'Mayotte', 'MYT', 'not-sale', '0', '+262', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:17', '0', '0');
INSERT INTO `country` VALUES ('140', 'Mexico', 'MEX', 'not-sale', '0', '+52', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:18', '0', '0');
INSERT INTO `country` VALUES ('141', 'Micronesia', 'FSM', 'not-sale', '0', '+691', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:18', '0', '0');
INSERT INTO `country` VALUES ('142', 'Moldova', 'MDA', 'not-sale', '0', '+373', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:19', '0', '0');
INSERT INTO `country` VALUES ('143', 'Monaco', 'MCO', 'not-sale', '0', '+377', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:19', '0', '0');
INSERT INTO `country` VALUES ('144', 'Mongolia', 'MNG', 'not-sale', '0', '+976', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:19', '0', '0');
INSERT INTO `country` VALUES ('145', 'Montenegro', 'MNE', 'not-sale', '0', '+382', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:20', '0', '0');
INSERT INTO `country` VALUES ('146', 'Montserrat', 'MSR', 'not-sale', '0', '+1-664', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:20', '0', '0');
INSERT INTO `country` VALUES ('147', 'Morocco', 'MAR', 'not-sale', '0', '+212', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:21', '0', '0');
INSERT INTO `country` VALUES ('148', 'Mozambique', 'MOZ', 'not-sale', '0', '+258', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:21', '0', '0');
INSERT INTO `country` VALUES ('149', 'Myanmar', 'MMR', 'not-sale', '0', '+95', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:21', '0', '0');
INSERT INTO `country` VALUES ('150', 'Namibia', 'NAM', 'not-sale', '0', '+264', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:22', '0', '0');
INSERT INTO `country` VALUES ('151', 'Nauru', 'NRU', 'not-sale', '0', '+674', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:22', '0', '0');
INSERT INTO `country` VALUES ('152', 'Nepal', 'NPL', 'not-sale', '0', '+977', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:23', '0', '0');
INSERT INTO `country` VALUES ('153', 'Netherlands', 'NLD', 'not-sale', '0', '+31', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:23', '0', '0');
INSERT INTO `country` VALUES ('154', 'Netherlands Antilles', 'ANT', 'not-sale', '0', '+599', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:23', '0', '0');
INSERT INTO `country` VALUES ('155', 'New Caledonia', 'NCL', 'not-sale', '0', '+687', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:24', '0', '0');
INSERT INTO `country` VALUES ('156', 'New Zealand', 'NZL', 'not-sale', '0', '+64', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:24', '0', '0');
INSERT INTO `country` VALUES ('157', 'Nicaragua', 'NIC', 'not-sale', '0', '+505', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:25', '0', '0');
INSERT INTO `country` VALUES ('158', 'Niger', 'NER', 'not-sale', '0', '+227', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:25', '0', '0');
INSERT INTO `country` VALUES ('159', 'Nigeria', 'NGA', 'not-sale', '0', '+234', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:26', '0', '0');
INSERT INTO `country` VALUES ('160', 'Niue', 'NIU', 'not-sale', '0', '+683', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:26', '0', '0');
INSERT INTO `country` VALUES ('161', 'Norfolk Island', 'NFK', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:26', '0', '0');
INSERT INTO `country` VALUES ('162', 'North Korea', 'PRK', 'not-sale', '0', '+850', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:27', '0', '0');
INSERT INTO `country` VALUES ('163', 'Northern Mariana Islands', 'MNP', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:27', '0', '0');
INSERT INTO `country` VALUES ('164', 'Norway', 'NOR', 'not-sale', '0', '+47', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:28', '0', '0');
INSERT INTO `country` VALUES ('165', 'Oman', 'OMN', 'not-sale', '0', '+968', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:28', '0', '0');
INSERT INTO `country` VALUES ('166', 'Pakistan', 'PAK', 'not-sale', '0', '+92', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:28', '0', '0');
INSERT INTO `country` VALUES ('167', 'Palau', 'PLW', 'not-sale', '0', '+680', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:29', '0', '0');
INSERT INTO `country` VALUES ('168', 'Palestine', 'PSE', 'not-sale', '0', '+970', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:29', '0', '0');
INSERT INTO `country` VALUES ('169', 'Panama', 'PAN', 'not-sale', '0', '+507', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:30', '0', '0');
INSERT INTO `country` VALUES ('170', 'Papua New Guinea', 'PNG', 'not-sale', '0', '+675', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:30', '0', '0');
INSERT INTO `country` VALUES ('171', 'Paraguay', 'PRY', 'not-sale', '0', '+595', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:30', '0', '0');
INSERT INTO `country` VALUES ('172', 'Peru', 'PER', 'not-sale', '0', '+51', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:31', '0', '0');
INSERT INTO `country` VALUES ('173', 'Philippines', 'PHL', 'not-sale', '0', '+63', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:31', '0', '0');
INSERT INTO `country` VALUES ('174', 'Pitcairn', 'PCN', 'not-sale', '0', '+64', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:32', '0', '0');
INSERT INTO `country` VALUES ('175', 'Poland', 'POL', 'not-sale', '0', '+48', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:32', '0', '0');
INSERT INTO `country` VALUES ('176', 'Portugal', 'PRT', 'not-sale', '0', '+351', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:33', '0', '0');
INSERT INTO `country` VALUES ('177', 'Puerto Rico', 'PRI', 'not-sale', '0', '+1-787', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:33', '0', '0');
INSERT INTO `country` VALUES ('179', 'Reunion', 'REU', 'not-sale', '0', '+262', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:33', '0', '0');
INSERT INTO `country` VALUES ('180', 'Romania', 'ROU', 'not-sale', '0', '+40', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:34', '0', '0');
INSERT INTO `country` VALUES ('181', 'Russia', 'RUS', 'not-sale', '0', '+7', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:34', '0', '0');
INSERT INTO `country` VALUES ('182', 'Rwanda', 'RWA', 'not-sale', '0', '+250', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:35', '0', '0');
INSERT INTO `country` VALUES ('183', 'Saint Barthélemy', 'BLM', 'not-sale', '0', '+590', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:35', '0', '0');
INSERT INTO `country` VALUES ('184', 'Saint Helena', 'SHN', 'not-sale', '0', '+290', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:35', '0', '0');
INSERT INTO `country` VALUES ('185', 'Saint Kitts And Nevis', 'KNA', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:36', '0', '0');
INSERT INTO `country` VALUES ('186', 'Saint Lucia', 'LCA', 'not-sale', '0', '+1-758', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:36', '0', '0');
INSERT INTO `country` VALUES ('187', 'Saint Martin', 'MAF', 'not-sale', '0', '+590', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:37', '0', '0');
INSERT INTO `country` VALUES ('188', 'Saint Pierre And Miquelon', 'SPM', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:37', '0', '0');
INSERT INTO `country` VALUES ('189', 'Saint Vincent And The Grenadines', 'VCT', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:37', '0', '0');
INSERT INTO `country` VALUES ('190', 'Samoa', 'WSM', 'not-sale', '0', '+685', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:38', '0', '0');
INSERT INTO `country` VALUES ('191', 'San Marino', 'SMR', 'not-sale', '0', '+378', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:38', '0', '0');
INSERT INTO `country` VALUES ('192', 'Sao Tome And Principe', 'STP', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:39', '0', '0');
INSERT INTO `country` VALUES ('193', 'Saudi Arabia', 'SAU', 'not-sale', '0', '+966', '5', '2017-07-25 06:54:12', '2018-01-21 08:45:53', '0', '0');
INSERT INTO `country` VALUES ('194', 'Senegal', 'SEN', 'not-sale', '0', '+221', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:39', '0', '0');
INSERT INTO `country` VALUES ('195', 'Serbia', 'SRB', 'not-sale', '0', '+381', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:40', '0', '0');
INSERT INTO `country` VALUES ('196', 'Seychelles', 'SYC', 'not-sale', '0', '+248', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:40', '0', '0');
INSERT INTO `country` VALUES ('197', 'Sierra Leone', 'SLE', 'not-sale', '0', '+232', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:41', '0', '0');
INSERT INTO `country` VALUES ('198', 'Singapore', 'SGP', 'not-sale', '0', '+65', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:41', '0', '0');
INSERT INTO `country` VALUES ('199', 'Sint Maarten', 'SXM', 'not-sale', '0', '+1-721', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:42', '0', '0');
INSERT INTO `country` VALUES ('200', 'Slovakia', 'SVK', 'not-sale', '0', '+421', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:42', '0', '0');
INSERT INTO `country` VALUES ('201', 'Slovenia', 'SVN', 'not-sale', '0', '+386', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:42', '0', '0');
INSERT INTO `country` VALUES ('202', 'Solomon Islands', 'SLB', 'not-sale', '0', '+677', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:43', '0', '0');
INSERT INTO `country` VALUES ('203', 'Somalia', 'SOM', 'not-sale', '0', '+252', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:43', '0', '0');
INSERT INTO `country` VALUES ('204', 'South Africa', 'ZAF', 'not-sale', '0', '+27', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:44', '0', '0');
INSERT INTO `country` VALUES ('205', 'South Georgia And The South Sandwich Islands', 'SGS', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:44', '0', '0');
INSERT INTO `country` VALUES ('206', 'South Korea', 'KOR', 'not-sale', '0', '+82', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:44', '0', '0');
INSERT INTO `country` VALUES ('207', 'Spain', 'ESP', 'not-sale', '0', '+34', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:45', '0', '0');
INSERT INTO `country` VALUES ('208', 'Sri Lanka', 'LKA', 'not-sale', '0', '+94', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:45', '0', '0');
INSERT INTO `country` VALUES ('209', 'Sudan', 'SDN', 'not-sale', '0', '+249', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:46', '0', '0');
INSERT INTO `country` VALUES ('210', 'Suriname', 'SUR', 'not-sale', '0', '+597', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:46', '0', '0');
INSERT INTO `country` VALUES ('211', 'Svalbard And Jan Mayen', 'SJM', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:46', '0', '0');
INSERT INTO `country` VALUES ('212', 'Swaziland', 'SWZ', 'not-sale', '0', '+268', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:47', '0', '0');
INSERT INTO `country` VALUES ('213', 'Sweden', 'SWE', 'not-sale', '0', '+46', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:47', '0', '0');
INSERT INTO `country` VALUES ('214', 'Switzerland', 'CHE', 'not-sale', '0', '+41', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:48', '0', '0');
INSERT INTO `country` VALUES ('215', 'Syria', 'SYR', 'not-sale', '0', '+963', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:48', '0', '0');
INSERT INTO `country` VALUES ('216', 'Taiwan', 'TWN', 'not-sale', '0', '+886', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:49', '0', '0');
INSERT INTO `country` VALUES ('217', 'Tajikistan', 'TJK', 'not-sale', '0', '+992', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:49', '0', '0');
INSERT INTO `country` VALUES ('218', 'Tanzania', 'TZA', 'not-sale', '0', '+255', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:49', '0', '0');
INSERT INTO `country` VALUES ('219', 'Thailand', 'THA', 'not-sale', '0', '+66', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:50', '0', '0');
INSERT INTO `country` VALUES ('220', 'The Democratic Republic Of Congo', 'COD', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:50', '0', '0');
INSERT INTO `country` VALUES ('221', 'Timor-Leste', 'TLS', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:51', '0', '0');
INSERT INTO `country` VALUES ('223', 'Togo', 'TGO', 'not-sale', '0', '+228', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:51', '0', '0');
INSERT INTO `country` VALUES ('224', 'Tokelau', 'TKL', 'not-sale', '0', '+690', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:52', '0', '0');
INSERT INTO `country` VALUES ('225', 'Tonga', 'TON', 'not-sale', '0', '+676', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:52', '0', '0');
INSERT INTO `country` VALUES ('226', 'Trinidad and Tobago', 'TTO', 'not-sale', '0', '+1-868', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:53', '0', '0');
INSERT INTO `country` VALUES ('227', 'Tunisia', 'TUN', 'not-sale', '0', '+216', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:53', '0', '0');
INSERT INTO `country` VALUES ('228', 'Turkey', 'TUR', 'not-sale', '0', '+90', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:53', '0', '0');
INSERT INTO `country` VALUES ('229', 'Turkmenistan', 'TKM', 'not-sale', '0', '+993', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:54', '0', '0');
INSERT INTO `country` VALUES ('230', 'Turks And Caicos Islands', 'TCA', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:54', '0', '0');
INSERT INTO `country` VALUES ('231', 'Tuvalu', 'TUV', 'not-sale', '0', '+688', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:55', '0', '0');
INSERT INTO `country` VALUES ('232', 'U.S. Virgin Islands', 'VIR', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:55', '0', '0');
INSERT INTO `country` VALUES ('233', 'Uganda', 'UGA', 'not-sale', '0', '+256', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:55', '0', '0');
INSERT INTO `country` VALUES ('234', 'Ukraine', 'UKR', 'not-sale', '0', '+380', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:56', '0', '0');
INSERT INTO `country` VALUES ('235', 'United Arab Emirates', 'ARE', 'sale', '0', '+971', '5', '2017-07-25 06:54:12', '2018-01-21 09:07:48', '30', '1');
INSERT INTO `country` VALUES ('236', 'United Kingdom', 'GBR', 'not-sale', '0', '+44', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:57', '0', '0');
INSERT INTO `country` VALUES ('237', 'United States', 'USA', 'not-sale', '0', '+1', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:57', '0', '0');
INSERT INTO `country` VALUES ('238', 'Uited States Minor Outlying Islands', 'UMI', 'not-sale', '0', null, '5', '2017-07-25 06:54:12', '2018-01-21 08:47:57', '0', '0');
INSERT INTO `country` VALUES ('239', 'Uruguay', 'URY', 'not-sale', '0', '+598', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:58', '0', '0');
INSERT INTO `country` VALUES ('240', 'Uzbekistan', 'UZB', 'not-sale', '0', '+998', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:58', '0', '0');
INSERT INTO `country` VALUES ('241', 'Vanuatu', 'VUT', 'not-sale', '0', '+678', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:59', '0', '0');
INSERT INTO `country` VALUES ('242', 'Vatican', 'VAT', 'not-sale', '0', '+379', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:59', '0', '0');
INSERT INTO `country` VALUES ('243', 'Venezuela', 'VEN', 'not-sale', '0', '+58', '5', '2017-07-25 06:54:12', '2018-01-21 08:47:59', '0', '0');
INSERT INTO `country` VALUES ('244', 'Vietnam', 'VNM', 'not-sale', '0', '+84', '5', '2017-07-25 06:54:12', '2018-01-21 08:48:00', '0', '0');
INSERT INTO `country` VALUES ('245', 'Wallis And Futuna', 'WLF', 'not-sale', '0', '+681', '5', '2017-07-25 06:54:12', '2018-01-21 08:48:00', '0', '0');
INSERT INTO `country` VALUES ('246', 'Western Sahara', 'ESH', 'not-sale', '0', '+212', '5', '2017-07-25 06:54:12', '2018-01-21 08:48:01', '0', '0');
INSERT INTO `country` VALUES ('247', 'Yemen', 'YEM', 'not-sale', '0', '+967', '5', '2017-07-25 06:54:12', '2018-01-21 08:48:01', '0', '0');
INSERT INTO `country` VALUES ('248', 'Zambia', 'ZMB', 'not-sale', '0', '+260', '5', '2017-07-25 06:54:12', '2018-01-21 08:48:02', '0', '0');
INSERT INTO `country` VALUES ('249', 'Zimbabwe', 'ZWE', 'not-sale', '0', '+263', '5', '2017-07-25 06:54:12', '2018-01-21 08:48:02', '0', '0');

-- ----------------------------
-- Table structure for enquiriesmessage
-- ----------------------------
DROP TABLE IF EXISTS `enquiriesmessage`;
CREATE TABLE `enquiriesmessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idtype` int(11) DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `message` text CHARACTER SET latin1,
  `ismember` enum('yes','no') CHARACTER SET latin1 DEFAULT NULL,
  `softdelete` enum('Delete','Not-delete') CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enquiriesmessage
-- ----------------------------
INSERT INTO `enquiriesmessage` VALUES ('1', '0', 'Mina Gamil', 'minagamilriad@gmail.com', 'Test', 'test', 'no', 'Not-delete', '2017-08-22 08:49:24', '2017-08-22 08:49:24');
INSERT INTO `enquiriesmessage` VALUES ('2', '1', 'Mina Gamil', 'minagamilriad@gmail.com', '01235479', 'asdabsda', 'no', 'Not-delete', '2017-08-22 08:56:51', '2017-08-22 08:56:51');
INSERT INTO `enquiriesmessage` VALUES ('3', '1', 'Mina Gamil', 'minagamilriad@gmail.com', '00789413213', 'asdjabhda', 'no', 'Not-delete', '2017-08-28 19:59:08', '2017-08-28 19:59:08');
INSERT INTO `enquiriesmessage` VALUES ('4', '4', 'mina gamil riad', 'm@y.com', '126460', 's jad ax kaxs', 'no', 'Not-delete', '2017-09-24 21:49:23', '2017-09-24 21:49:23');
INSERT INTO `enquiriesmessage` VALUES ('5', '0', 'diliWemo diliWemo', 'pup.k.i.n.sva.ni.a@gmail.com', 'thesis editing rates', 'examples of dissertation\r\nthesis website\r\nbest thesis topics\r\ndissertation formats\r\nsample dissertation prospectus\r\n \r\n<a href=\"http://integraltechservice.by/index.php?option=com_k2&view=itemlist&task=user&id=743274\">dissertation to buy</a> \r\ndissertation help services\r\n \r\n<a href=\"http://sozdanie-saitov-solnechnogorsk.ru/index.php/your-profile?task=profile.edit&user_id=70957\">dissertation ideas</a> \r\ndissertation methodology example\r\n \r\n<a href=\"http://awcs.com.sg/index.php/index.php/using-joomla/extensions/components/users-component/user-profile?task=profile.edit&user_id=18668\">dissertation transcription services</a> \r\nthesis revision\r\n \r\n<a href=\"http://eternalplans.net.ph/index.php/index.php?option=com_k2&view=itemlist&task=user&id=4987\">custom dissertation</a> \r\nwhat is a dissertation\r\n \r\n<a href=\"http://xn--n1afo.xn--90aoijfinhb.xn--p1ai/index.php/index.php/administratorskij-razdel?task=profile.edit&user_id=7966\">custom dissertation writing help</a> \r\nwrite my thesis statement for me\r\n \r\n<a href=\"http://www.littlechicagoapparel.com/index.php/index.php/logout?task=profile.edit&user_id=97129\">dissertation writing service uk</a> \r\ndissertation writing services uk\r\n \r\n<a href=\"http://foodtappr.com/index.php/index.php/joomla-pages-ii/user-profile?task=profile.edit&user_id=427082\">dissertation acknowledgement sample</a> \r\nbest dissertation writers\r\n \r\n<a href=\"http://igea-anlagenbau.de/index.php/index.php?option=com_k2&view=itemlist&task=user&id=18468\">custom thesis writing services</a> \r\ndissertation writing company\r\n \r\n<a href=\"http://www.villalbaxtrem.xctrail.es/index.php/index.php?option=com_k2&view=itemlist&task=user&id=4909\">write my dissertation for me</a> \r\ndissertation editing services\r\neducation dissertation examples\r\nsample dissertation prospectus\r\ndissertation apa format\r\ncustom thesis writing\r\n \r\n<a href=\"http://integraltechservice.by/index.php?option=com_k2&view=itemlist&task=user&id=743766\">best dissertation writing service</a> \r\ndissertation samples\r\nhow to write phd thesis\r\nbest dissertation writing services uk\r\nbuy dissertations\r\ndissertation revisions\r\n \r\n<a href=\"http://foodtappr.com/index.php/index.php/joomla-pages-ii/user-profile?task=profile.edit&user_id=427082\">how to write a phd proposal</a> \r\nbuying a dissertation\r\n \r\n<a href=\"http://yourside.com.au/index.php/index.php?option=com_k2&view=itemlist&task=user&id=5256\">thesis statement definition</a> \r\nthesis writing tips\r\n \r\n<a href=\"http://freshfoodgroup.ru/index.php/profil-polzovatelya.html?task=profile.edit&user_id=85910\">examples of dissertation topics</a> \r\nsample dissertation\r\n \r\n<a href=\"http://tecnokart.co.uk/index.php/index.php?option=com_k2&view=itemlist&task=user&id=23222\">dissertation writing services legal</a> \r\ndissertation title\r\n \r\n<a href=\"http://integraltechservice.by/index.php?option=com_k2&view=itemlist&task=user&id=742061\">dissertations online free</a> \r\n<a href=\"https://writecustom.com/\" title=\"custom essay writing service\">custom essay writing service</a>', 'no', 'Not-delete', '2017-09-29 20:08:03', '2017-09-29 20:08:03');
INSERT INTO `enquiriesmessage` VALUES ('6', '0', 'Annefinhive Annefinhive', 'AttitTessvoli@gmail.com', 'Re establish good credit', 'http://fjdhgksf76w444.com hi everyone', 'no', 'Not-delete', '2017-10-01 20:59:17', '2017-10-01 20:59:17');
INSERT INTO `enquiriesmessage` VALUES ('7', '0', 'Annefinhive Annefinhive', 'AttitTessvoli@gmail.com', 'death race 2 megavideo', 'http://fjdhgksf76w444.com hi everyone', 'no', 'Not-delete', '2017-10-02 06:44:22', '2017-10-02 06:44:22');
INSERT INTO `enquiriesmessage` VALUES ('8', '0', 'diliWemo diliWemo', 'p.upki.n.sva.nia@gmail.com', 'top dissertation writing services', 'dissertations examples\r\ndissertation prospectus sample\r\nsocial work dissertation topics\r\ncustom dissertation writing help\r\ndissertation help\r\n \r\n<a href=\"http://welltec-wellpappentechnik.de/index.php/index.php/joomla-pages-ii/user-profile.html?task=profile.edit&user_id=71831\">dissertation proofreading service</a> \r\ndissertation acknowledgements examples\r\n \r\n<a href=\"http://www.piensaweb.com/index.php/index.php/your-profile?task=profile.edit&user_id=32031\">phd thesis writing services</a> \r\nphd thesis editing service\r\n \r\n<a href=\"http://integraltechservice.by/index.php?option=com_k2&view=itemlist&task=user&id=743766\">best dissertation titles</a> \r\ndissertation writing service reviews\r\n \r\n<a href=\"http://www.hi-flower.com/index.php/index.php/joomla-pages-ii/user-profile?task=profile.edit&user_id=15130\">thesis essay example</a> \r\ndissertation outline format\r\n \r\n<a href=\"http://madeinmataro.cat/index.php/joomla-pages-ii/user-profile.html?task=profile.edit&user_id=27078\">buy a dissertation online</a> \r\nhow to write a phd dissertation\r\n \r\n<a href=\"http://corfutours.ru/index.php/index.php/your-profile?task=profile.edit&user_id=165182\">uf thesis and dissertation</a> \r\nthesis writing service uk\r\n \r\n<a href=\"http://integraltechservice.by/index.php?option=com_k2&view=itemlist&task=user&id=740815\">do my thesis for me</a> \r\nhow to publish thesis\r\n \r\n<a href=\"http://davidjrimmer.com/?option=com_k2&view=itemlist&task=user&id=96429\">do my thesis</a> \r\nthesis revision\r\n \r\n<a href=\"http://integraltechservice.by/index.php?option=com_k2&view=itemlist&task=user&id=727739\">apa format dissertation</a> \r\nhow to write a thesis statement\r\nthesis generator\r\nsamples of thesis papers\r\nthesis statement examples for essays\r\ncustom written dissertation\r\n \r\n<a href=\"http://moverprint.com/index.php/index.php/user-profile?task=profile.edit&user_id=5812\">mba thesis help</a> \r\nbest dissertation\r\nbest thesis writing service\r\ncustom dissertation writing service\r\nprofessional dissertation writing\r\nsample thesis paper\r\n \r\n<a href=\"http://www.gruposed.es/v2/joomla-pages-ii/user-registration.html/v2/joomla-pages-ii/user-profile.html?task=profile.edit&user_id=5354\">dissertation writing services canada</a> \r\nhow to write a thesis\r\n \r\n<a href=\"http://viadargento.ru//your-profile.html?task=profile.edit&user_id=40310\">psychology thesis</a> \r\nbuy thesis online\r\n \r\n<a href=\"http://polyglobe.ma/index.php/index.php/your-profile?task=profile.edit&user_id=48547\">what is a dissertation</a> \r\ndissertation topics\r\n \r\n<a href=\"http://oldenburgcerveceria.es/index.php/index.php?option=com_k2&view=itemlist&task=user&id=13856\">scientific thesis</a> \r\nexamples of dissertation topics\r\n \r\n<a href=\"http://www.littlechicagoapparel.com/index.php/index.php/logout?task=profile.edit&user_id=97270\">buy dissertation</a> \r\n<a href=\"https://writecustom.com/\" title=\"writing paper\">writing paper</a>', 'no', 'Not-delete', '2017-10-02 12:04:27', '2017-10-02 12:04:27');
INSERT INTO `enquiriesmessage` VALUES ('9', '0', 'installmentloans installmentloans', 'robertedundrrn@mail.ru', 'apply for loan online with bad credit', 'loans from direct lenders  <a href=\"https://installmentloansmaster.org/\">online installment loans</a>  small loans  <a href=https://installmentloansmaster.org/>installment loans</a>  loan places in albuquerque', 'no', 'Not-delete', '2017-10-02 21:19:09', '2017-10-02 21:19:09');
INSERT INTO `enquiriesmessage` VALUES ('10', '0', 'installmentloans installmentloans', 'robertedundrrn@mail.ru', 'apply loan', 'consolidation loans  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  personal loans for bad credit in ohio  <a href=https://installmentloansmaster.org/>installment loans</a>  how to get a quick loan with bad credit', 'no', 'Not-delete', '2017-10-03 02:08:03', '2017-10-03 02:08:03');
INSERT INTO `enquiriesmessage` VALUES ('11', '0', 'installmentloans installmentloans', 'robertedundrrn@mail.ru', 'personal loans with fair credit', 'personal loans bad credit ok  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  2500 installment loan  <a href=https://installmentloansmaster.org/>installment loans</a>  installment loan companies online', 'no', 'Not-delete', '2017-10-03 07:22:42', '2017-10-03 07:22:42');
INSERT INTO `enquiriesmessage` VALUES ('12', '0', 'installmentloans installmentloans', 'robertedundrrn@mail.ru', 'cosign loan', 'direct online installment loans  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  unsecured debt consolidation loan bad credit  <a href=https://installmentloansmaster.org/>installment loans online</a>  installment payments', 'no', 'Not-delete', '2017-10-03 13:12:24', '2017-10-03 13:12:24');
INSERT INTO `enquiriesmessage` VALUES ('13', '0', 'installmentloans installmentloans', 'robertedundrrn@mail.ru', 'debt consolidation loans with bad credit', 'personal loans to pay off credit card debt  <a href=\"https://installmentloansmaster.org/\">online installment loans</a>  loans charlotte nc  <a href=https://installmentloansmaster.org/>installment loans online</a>  no credit installment loans', 'no', 'Not-delete', '2017-10-03 17:47:35', '2017-10-03 17:47:35');
INSERT INTO `enquiriesmessage` VALUES ('14', '0', 'Sarah Carlson http://segundaibc.com.br/go/1i', 'atgjqirvrx@bgrqtiyymto.com', 'Sarah Carlson', 'I came to your SilverCity Store for iPhone mobiles  and other Apple Products page and noticed you could have a lot more traffic. I have found that the key to running a website is making sure the visitors you are getting are interested in your subject matter. We can send you targeted traffic and we let you try it for free. Get over 1,000 targeted visitors per day to your website. Check it out here: http://url.euqueroserummacaco.com/ifa 															Unsubscribe here: http://pcgroup.com.uy/2a', 'no', 'Not-delete', '2017-10-03 18:52:52', '2017-10-03 18:52:52');
INSERT INTO `enquiriesmessage` VALUES ('15', '0', 'installmentloans installmentloans', 'robertedundrrn@mail.ru', 'personal loans knoxville tn', '$500 loans  <a href=\"https://installmentloansmaster.org/\">online installment loans</a>  loans to family members  <a href=https://installmentloansmaster.org/>installment loans</a>  interest rate for personal loan', 'no', 'Not-delete', '2017-10-03 22:08:45', '2017-10-03 22:08:45');
INSERT INTO `enquiriesmessage` VALUES ('16', '0', 'installmentloans installmentloans', 'installmentloans@mail.ru', 'personal loans for no credit', 'loan without collateral  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  loan application form  <a href=https://installmentloansmaster.org/>installment loans</a>  personal loan contract', 'no', 'Not-delete', '2017-10-04 23:42:28', '2017-10-04 23:42:28');
INSERT INTO `enquiriesmessage` VALUES ('17', '0', 'installmentloans installmentloans', 'installmentloans@mail.ru', '24 hour loans', 'personal loan with fair credit  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  where to get loans with bad credit  <a href=https://installmentloansmaster.org/>installment loans</a>  easy to get installment loans for bad credit', 'no', 'Not-delete', '2017-10-05 06:26:55', '2017-10-05 06:26:55');
INSERT INTO `enquiriesmessage` VALUES ('18', '0', 'installmentloans installmentloans', 'installmentloans@mail.ru', '5000 loans', 'what is a loan originator  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  money loans without bank account  <a href=https://installmentloansmaster.org/>installment loans</a>  best unsecured personal loans', 'no', 'Not-delete', '2017-10-05 11:10:36', '2017-10-05 11:10:36');
INSERT INTO `enquiriesmessage` VALUES ('19', '0', 'installmentloans installmentloans', 'installmentloans@mail.ru', 'how can i get a loan without a bank account', '1000 dollar loan  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  $3000 personal loan  <a href=https://installmentloansmaster.org/>installment loans</a>  installment loans after bankruptcy', 'no', 'Not-delete', '2017-10-05 15:59:22', '2017-10-05 15:59:22');
INSERT INTO `enquiriesmessage` VALUES ('20', '0', 'CliftonMat CliftonMat', 'cliftongar@mail.ru', 'loans places', 'online pay loans  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  best loan consolidation companies  <a href=https://installmentloansmaster.org/>installment loans</a>  installment plan definition', 'no', 'Not-delete', '2017-10-05 23:14:59', '2017-10-05 23:14:59');
INSERT INTO `enquiriesmessage` VALUES ('21', '0', 'CliftonMat CliftonMat', 'cliftongar@mail.ru', 'personal loans with poor credit', 'lender loans  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  loan south griffin ga  <a href=https://installmentloansmaster.org/>installment loans</a>  loans for people with no job', 'no', 'Not-delete', '2017-10-06 04:25:23', '2017-10-06 04:25:23');
INSERT INTO `enquiriesmessage` VALUES ('22', '0', 'CliftonMat CliftonMat', 'cliftongar@mail.ru', 'cash loans chicago', 'can i get a loan  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  debt loans  <a href=https://installmentloansmaster.org/>installment loans</a>  direct deposit loans bad credit', 'no', 'Not-delete', '2017-10-06 10:02:50', '2017-10-06 10:02:50');
INSERT INTO `enquiriesmessage` VALUES ('23', '0', 'CliftonMat CliftonMat', 'cliftongar@mail.ru', 'ways to get loans', 'apply for loans online with no credit  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  cash loans okc  <a href=https://installmentloansmaster.org/>installment loans</a>  installment loans', 'no', 'Not-delete', '2017-10-06 15:23:25', '2017-10-06 15:23:25');
INSERT INTO `enquiriesmessage` VALUES ('24', '0', 'CliftonMat CliftonMat', 'cliftongar@mail.ru', 'loan consolidation gov', 'loan  <a href=\"https://installmentloansmaster.org/\">installment loans</a>  take a loan  <a href=https://installmentloansmaster.org/>installment loans</a>  low interest personal loans', 'no', 'Not-delete', '2017-10-06 20:17:34', '2017-10-06 20:17:34');
INSERT INTO `enquiriesmessage` VALUES ('25', '0', 'Andrea Gibson http://segundaibc.com.br/go/1i', 'xndkgjbrkdx@gqylxkqtdls.com', 'Andrea Gibson', 'I discovered your SilverCity Store for iPhone mobiles  and other Apple Products page and noticed you could have a lot more traffic. I have found that the key to running a website is making sure the visitors you are getting are interested in your subject matter. We can send you targeted traffic and we let you try it for free. Get over 1,000 targeted visitors per day to your website. Check it out here: http://v-doc.co/nm/jkfq0 															Unsubscribe here: http://priscilarodrigues.com.br/url/11', 'no', 'Not-delete', '2017-10-11 06:20:23', '2017-10-11 06:20:23');
INSERT INTO `enquiriesmessage` VALUES ('26', '0', 'diliWemo diliWemo', 'p.u.p.ki.n.svan.ia@gmail.com', 'thesis help services', 'research papers writing desir philosophie dissertation aviation legislation term paper academic writing for graduate student cloud computing business plan prospectus research paper radu rusu dissertation nursing capstone projects help me with my maths homework please research paper topics abortion who should write the business plan sample business plan for internet cafe business plan marketing strategy model of research paper buy university essays online uk  \r\n<a href=\"http://gullewa.com.au/index.php/component/users/?option=com_k2&view=itemlist&task=user&id=135591\">writing center appointment</a> \r\n<a href=\"http://electrohidraulica.co/index.php/component/users/?option=com_k2&view=itemlist&task=user&id=433859\">online writing center</a> \r\n<a href=\"http://callelasgardenias.com/index.php?option=com_k2&view=itemlist&task=user&id=1079694\">ucla writing center</a> \r\n<a href=\"http://prezent-house.com.ua/index.php?option=com_k2&view=itemlist&task=user&id=188361\">uc writing center</a> \r\n<a href=\"http://prezent-house.com.ua/index.php?option=com_k2&view=itemlist&task=user&id=188858\">writing center appointment</a>', 'no', 'Not-delete', '2017-10-11 07:10:12', '2017-10-11 07:10:12');
INSERT INTO `enquiriesmessage` VALUES ('27', '0', 'ErnestAntaf ErnestAntaf', 'copycat1@italy-mail.com', 'Test, just a test', 'Watch Movies Online and Watch Tv-Series online On Solarmovie without Registration. Top IMDb; Request. LOGIN. Movies of tags \"Finding Carter - Season 1\". Watch online Dinosaur Train 2009 full with English subtitle. Watch online Chromecast feature is back, everyone now can cast the player to your TV as well.  \r\n<a href=http://sobory.ru/lastcomments/?page=151&auth=56>click for more info</a> \r\nAbout Us | Jupiter Entertainment	... Manor, and Animal Planet\'s Wild West Alaska to long running series like Snapped Discovery, History, Investigation Discovery, Oxygen, TLC, TruTV, TV One, 27 Nov 2015 Following James Wright\'s viral YouTube videos professing love for her pie, the music legend invited him for dinner -- even singing to him.', 'no', 'Not-delete', '2017-10-19 16:31:56', '2017-10-19 16:31:56');
INSERT INTO `enquiriesmessage` VALUES ('28', '0', 'JimmiNi JimmiNi', null, 'QWRMVLGMAwxIaA', 'ZhotMZ http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', 'no', 'Not-delete', '2017-10-21 14:16:44', '2017-10-21 14:16:44');
INSERT INTO `enquiriesmessage` VALUES ('29', '0', 'JimmiNi JimmiNi', null, 'yWamwVTgsvU', 'Ae9nDy http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', 'no', 'Not-delete', '2017-10-22 01:00:00', '2017-10-22 01:00:00');
INSERT INTO `enquiriesmessage` VALUES ('30', '0', 'JimmiNi JimmiNi', null, 'OVgOwAgCcYFUGKT', '9bD5UG http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', 'no', 'Not-delete', '2017-10-22 03:40:18', '2017-10-22 03:40:18');
INSERT INTO `enquiriesmessage` VALUES ('31', '0', 'JimmiNi JimmiNi', null, 'ENfgaeOThWqzGbVPwyp', '8TS9MK http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', 'no', 'Not-delete', '2017-10-22 08:30:30', '2017-10-22 08:30:30');
INSERT INTO `enquiriesmessage` VALUES ('32', '0', 'JimmiNi JimmiNi', null, 'BqXeuiwaKy', 'hINWtG http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', 'no', 'Not-delete', '2017-10-23 09:11:37', '2017-10-23 09:11:37');
INSERT INTO `enquiriesmessage` VALUES ('33', '0', 'JimmiNi JimmiNi', null, 'nzovRNtZUFumwBoSxv', '7l7dm4 http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', 'no', 'Not-delete', '2017-10-23 09:26:15', '2017-10-23 09:26:15');
INSERT INTO `enquiriesmessage` VALUES ('34', '0', 'Shanusapy Shanusapy', 'shanPymn@24meds.com', 'Buying free shipping isotretinoin isotrex best website ShanVord', 'Keflex Antibotic Sinus Infection Propecia With Minoxidil Nisim Cialis Und Viagra Zusammen Einnehmen  <a href=http://costofcial.com>generic cialis</a> Trouver Viagra Paris Pastilla Cialis Duroval', 'no', 'Not-delete', '2017-11-07 13:48:43', '2017-11-07 13:48:43');
INSERT INTO `enquiriesmessage` VALUES ('35', '7', 'Abdallah Eldesoky', 'abdallaheldesoky1@gmail.com', '0524199744', 'Dear Sir, I’m looking for suitable job with your valuable company', 'no', 'Not-delete', '2017-11-11 10:31:14', '2017-11-11 10:31:14');
INSERT INTO `enquiriesmessage` VALUES ('36', '7', 'Etab Al Bajoury Al Bajoury', 'etabalbajoury@gmail.com', '0521830533', 'Dear Mr.HR, I would like to join your worthy company i have extent experience', 'no', 'Not-delete', '2017-11-11 10:43:04', '2017-11-11 10:43:04');
INSERT INTO `enquiriesmessage` VALUES ('37', '4', 'Acrosscar Acrosscar', 'mihailkuznecov300@gmail.com', 'SUBJ1', 'The innovative Lamborghini Aventador S features original aerodynamic characteristics, modified suspension, enhanced power and advanced dynamic performance. The letter \'S\' presents the suffix of earlier improved Lamborghini autos and sets a new standard for the V12 Lamborghini. \r\nWatch video: https://www.youtube.com/watch?v=EFRhBd0fVdg', 'no', 'Not-delete', '2017-11-11 20:11:20', '2017-11-11 20:11:20');
INSERT INTO `enquiriesmessage` VALUES ('38', '0', 'Joshuaprumn Joshuaprumn', 'rynnkleemann4r69r21a@yahoo.com', 'Medical Thesis', 'Writing a medical thesis or dissertation is a task done by almost all postgraduate and master\'s medical students. Dissertation is derived from the Latin word disserto which means discuss. It is essential to write successful medical papers such as medicine essays and medical thesis papers. There are several reasons as to why students write medicine essays. One of the reasons is to promote enhancement of critical judgment, research skills as well as analytical skills. Moreover, medicine essay writing produce students with the ability to 4evaluate and analyze data critically. \r\n \r\nThe initial step for writing medicine essays is to choose a topic. A writer should have at least three topics to choose from. The topic has to be interesting, feasible and relevant. It is essential to write quality medicine essay. Hence, students need to have analytical skills and perfect writing skills. The writing skills will enable them write outstanding essay papers that can be highly regarded by instructors and professors. Teachers often require a lot and expect a lot from their students in terms of medicine essay writing. for this reason, students find essay writing to be an extremely difficult task and hence resort to buying custom medicine essays. \r\n \r\nA custom medicine essay has to be written by professional writers who are qualified in the field of nursing. Moreover, the custom medicine essay has to be original and plagiarism free. This means that it has to be written from scratch by experts with many years experience. The many years experience should enable a writer to write any form of medical paper including medical thesis, medicine essay and even medicine research paper. Moreover, experience will enable a writer to write a medicine essay that can guarantee academic success. \r\n \r\nStudents get custom medicine essays from custom writing company. It is essential to choose the best company so that one can get the best custom medicine essay. The best and the most reliable medicine essay writing company should have some unique characteristics such as affordability and the ability to provide original and superior quality medicine essays. The other quality is that the company has to hire expert writers who can write quality medicine essays and other types of medical papers. The essays should not only be quality but also plagiarism free and free of grammatical and spelling mistakes. \r\n \r\nA custom medicine essay has a similar structure to any other academic essay assignment. It has an introduction that introduces the topic and tells the reader what the essay is all about. The second section is the body that has many paragraphs supporting the main topic. Finally there is the conclusion that briefly summarizes what has been discussed in the body section of the essay. Students should choose reliable writing companies so that they can get quality custom papers on several fields such as technology, sociology and law in addition to medicine field. \r\n \r\nOur custom writing company is the best company that all clients should rely on when in need of any given type of medicine paper. We provide quality papers that not only plagiarism free but also original. Moreover, our custom papers are affordable and able to guarantee academic excellence at all times. All our medical papers are reliable and sure of satisfying clients at all times. \r\n \r\n ', 'no', 'Not-delete', '2017-11-14 01:00:41', '2017-11-14 01:00:41');
INSERT INTO `enquiriesmessage` VALUES ('39', '0', 'Richardsuefe Richardsuefe', 'williamstuart3y24w@yahoo.com', 'How May Doctor Bullies Have You Encountered? All About Doctor Bullies!', 'Doctors have many challenges to face as they are perennially surrounded by patients, diseases, hospital duties and over-extended or odd shift timings. Universally, doctor is considered to be a noble profession and respectable one at that, but a doctor also has to work under immense pressures, emotional strains and other physical challenges. \r\n \r\n \r\nA regular physician like most of us at some point face will have to deal with personal situations such as important family affairs, family holidays, sickness or pregnancy that may force them to abandon medical duties. At the same time, a hospital or a healthcare facility is also constantly faced with emergency situations that demand all hands on deck round-the-clock. Therefore, every hospital, clinic or nursing home is compelled to hire locum tenens or substitute doctor in order to keep the staffing under control at all times. \r\n \r\n \r\nIn fact, locum doctors are the most valuable asset for the medical community because they provide quality medical care and act as a helping-hand in emergency situations when the medical facilities need them the most. \r\n \r\nUnlike regular or permanent doctors, locum doctor jobs are also ideal career options for medical interns and graduates because they offer a wide array of medical exposure in varied medical specialties, work cultures and healthcare systems. Locum jobs are challenging and flexible, thus an increasing number of medical professionals have benefitted from these jobs, so whether one is looking for a family physicians position or in a hospital or in a clinic, locum jobs for doctors are available at all levels and in different healthcare systems. \r\n \r\n \r\nIn addition, being a locum doctor gives a medical professional the control over their working hours, location of work and choice of area of specialisation. Technically, locum positions are not restricted to general physicians but they are also extended to other fields of medical specialisations such as cardiology, neurology and many more. \r\n \r\nTravelling can be an integral part of locum jobs, and these distinctive features are a boon for many dedicated medical professionals who are eager to expand their medical careers with loads of multi-cultural medical experiences. The fact that locum agencies in the UK recruit tens of thousands of locums from across the globe in various NHS hospitals, private clinics, nursing homes and other public hospitals speaks volume of the popularity of locum jobs. \r\n \r\n \r\nLocating or getting a locum tenens job is a simple task as long as you are registered with one of the many reputable locum agencies. These agencies act as the middle man between locum tenens and medical facilities, and they also look after all the details pertaining to travel for locum tenens, accommodation and the nature of locum work. \r\n \r\nThus, maintaining a healthy locum doctor-agency relationship benefits both the parties, and it also increases the probability of getting recommendable employment opportunities and businesses or vice-versa.', 'no', 'Not-delete', '2017-11-14 22:06:28', '2017-11-14 22:06:28');
INSERT INTO `enquiriesmessage` VALUES ('40', '7', 'Acrossaa Acrossaa', 'mihailk.uznecov300@gmail.com', 'SUBJ1', 'Aston Martin introduced a roadster characterized by the greatest extremality in the exceptional form named Aston Martin Vantage G12 Roadster. \r\nWatch video: https://www.youtube.com/watch?v=iibGHGlmZCs', 'no', 'Not-delete', '2017-11-16 09:26:54', '2017-11-16 09:26:54');
INSERT INTO `enquiriesmessage` VALUES ('41', '0', 'Virginia Williams http://0nulu.com/sdq', 'grkedfgmi@rbrykxm.com', 'Virginia Williams', 'This is a comment to the SilverCity Store for iPhone mobiles  and other Apple Products webmaster. Your website is missing out on at least 300 visitors per day. Our traffic system will  dramatically increase your traffic to your site: http://0nulu.com/sdq - We offer 500 free targeted visitors during our free trial period and we offer up to 30,000 targeted visitors per month. Hope this helps :)											 					Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2017-11-20 14:23:14', '2017-11-20 14:23:14');
INSERT INTO `enquiriesmessage` VALUES ('42', '0', 'JerryRex JerryRex', 'savvy-shopper@italy-mail.com', 'Superhuman: FOX Orders Eight Episodes of - TV Series Finale', '11 Jul 2013 Andrew Zimmern snaps a photo of his wife, Rishia, on the TV screen at the show\'s website, www.travelchannel.com/tv-shows/bizarre-foods. 12 Oct 2015 Fucking ruined by some smug twat called Charlie Brooker in the chair. Pretty shocking when Diane Abbott isn\'t the biggest arsehole on a TV show. Brooker\'s Weekly Wipe was funny a while back, but the last series was  \r\nhttp://supercoolshop.co \r\nFind out where you can watch the latest episodes of Swamp Murders online. Read episode Season 4 - Episode 10 : Oasis of Death Oct 18, 2016. watch now. 3 Feb 2015 X Factor reject Dean \'Deano\' Baily is now a fun-loving holiday rep in ITV2\'s new series.', 'no', 'Not-delete', '2017-11-21 00:27:47', '2017-11-21 00:27:47');
INSERT INTO `enquiriesmessage` VALUES ('43', '3', 'Acrossac Acrossac', 'mi.hailkuznecov300@gmail.com', 'SUBJ1', 'The innovative 2017 Ford GT will be presented in a restricted quantity and possess exceptional colour shades of interior and exterior along with special wheel finish. \r\nFord GT 66 Heritage Edition. Watch video: https://www.youtube.com/watch?v=WlUj6BYiGrU', 'no', 'Not-delete', '2017-11-22 08:08:45', '2017-11-22 08:08:45');
INSERT INTO `enquiriesmessage` VALUES ('44', '5', 'Acrossad Acrossad', 'mih.ailkuznecov300@gmail.com', 'SUBJ1', 'Renault Megane Sedan The Megane lineup has been supplemented by the presentation of a four-door variant – the innovative Renault Megane Sedan. \r\nWatch video: https://www.youtube.com/watch?v=4mK9_a7VTiM', 'no', 'Not-delete', '2017-11-24 00:50:11', '2017-11-24 00:50:11');
INSERT INTO `enquiriesmessage` VALUES ('45', '2', 'fifarat fifarat', 'mihailk.uzneco.v300@gmail.com', 'SUBJ1', 'Fifa Ranking November 2017! \r\nWatch video: https://www.youtube.com/watch?v=XE8IudRYaO8', 'no', 'Not-delete', '2017-11-25 04:43:36', '2017-11-25 04:43:36');
INSERT INTO `enquiriesmessage` VALUES ('46', '0', 'Mohammed Kukherdi', 'chandlerbing1971@yahoo.com', 'I PHONE X Plus 256 GB', 'I am a UAE National interested in purchasing an iPhone X Plus on instalment basis', 'yes', 'Not-delete', '2017-11-25 05:05:11', '2017-11-25 05:05:11');
INSERT INTO `enquiriesmessage` VALUES ('47', '0', 'Virginia Williams http://0nulu.com/opx', 'wihvrevdtgv@gmail.com', 'Virginia Williams', 'This is a comment to the SilverCity Store for iPhone mobiles  and other Apple Products webmaster. Your website is missing out on at least 300 visitors per day. Our traffic system will  dramatically increase your traffic to your website: http://0nulu.com/opx - We offer 500 free targeted visitors during our free trial period and we offer up to 30,000 targeted visitors per month. Hope this helps :)											 					Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2017-11-30 16:50:22', '2017-11-30 16:50:22');
INSERT INTO `enquiriesmessage` VALUES ('48', '0', 'Virginia Williams http://0nulu.com/opx', 'zblizlkoblt@gmail.com', 'Virginia Williams', 'I discovered your SilverCity Store for iPhone mobiles  and other Apple Products page and noticed you could have a lot more traffic. I have found that the key to running a website is making sure the visitors you are getting are interested in your subject matter. We can send you targeted traffic and we let you try it for free. Get over 1,000 targeted visitors per day to your website. Start your free trial: http://0nulu.com/opx		 																	Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2017-12-05 13:09:08', '2017-12-05 13:09:08');
INSERT INTO `enquiriesmessage` VALUES ('49', '0', 'charmdateSn charmdateSn', '5987jguoed@mail.ru', 'CHARMDATE SCAM', 'essays relating to Evita\r\n\r\njane HORWITZ, the month of january 10, 1997\'Evita\' (PG, 134 minute.) Ravishing photographs, Haunting music together with Madonna\'s just store may be sufficient from show your a quantity of preteens, sadly Evita probably will weary or mistake increasingly more. treat this is what difference the particular tim Lloyd Webber tim almond step musical at high schoolers. at times for these people, Evita is going to be tough to go by. there are hardly any oral discussion, as well urgent lyrics are normally perished in or garbled by means of feature of Anthat wouldnio Bas aeras skeptical where narraleadsr regarding due to avoi brief calling Peron\'s during function babe gentleman success the poor first of.articles or reviews and also by DATE\'Evita\' llega al Arscht company, durante MiamiPor Yvonne Valdez, will likely 23, 2014Evita, El musical technology de Broadway, Ganador siete veces delete premio Tony, Llega \'s sur de oregon del 27 de mayonnaise 1 de junio, minus los angeles intencin p cautivar ing pblico historia primera dama argentina, Quien se ga suitablen el gymmor l\'ordre de el odio muchos durnte su vid obr junto su esposo el presinte Jun Pern. Ambidurantetada Buos Aires, El audio, que lleva 30 aos durante las tablas, Cuenta are generally vida environnant les avoi Duarte Pern y simplys su juvtud hasta convertise una ambiciosa actriz luego mujer, Para muchos, microsoft ponnufacturedrosa Latino Amrica.numerous Evita is considered DifferentTribune videos specialists, february 14, 1997\"Madonna, the actual manages lady role properly. quite a bit less Evita, oftentimes, nonetheless as the type in to rock and roll opera, employing sentiment akin to Esther Goris, star akin to Argentina\'s avoi Peron: the real memory. keep in mind, fat loss look at those two video clips. to earn a comparison, at this time there would need to be parallels. At grape Creek Parkway and to the north assert line 7, according to Margate police force. Ednan Forgue asserted the man\'s parent was already delivered with all the acrosstersection by a pal correct after specialists at first Haitian Baptist cathedral sand Pompano. The young man suggested that biological dad had been endeavoring to catch the motorcoach. Forgue was formerly minted by way of a machine Pathfinder because he attemptedto cross north the state streets 7, law enforcement agency shown. she addresses all tax usd in view that her own individual checkbook to acquire put in to fill her vanity. i will be shell out a premium price to use applause to suit \"Evita Clinton, your lover will confirmed $1 <a href=https://www.youtube.com/watch?v=q8fSUN_U2Mo>charmDate ScAm</a> million to coach oriental women while you\'re on her take a trip at this time there. claire Lloyd Webbers 1979 natural stone musical is their favorite for many who consider it because optimum at this man\'s collaborations in bernard rice. those continue for mission with you, It characteristics heavy riding music to explode a throbbing marketing revealed to by means of quick, enraged lyricism. it is difficult conjunction in order for both performers your concert halls the businesses can be bought in; reminiscent of one opera, the type of musical technology natural environment is simply pieces of paper at likelihood accompanied by great quality. Revives live entertainment On ScreenEven some of those buffs of all musicals what people logically loathe tim Lloyd Webber have been rootfound ing puzzle the particular player new choice that Evita, <a href=https://mydatingdirectory.net/sites/russian-dating/charmdate.com/>CHARMdate Scam</a> and that established wed for <a href=https://www.youtube.com/watch?v=GnJbDvKtwqQ>CHARMDATE scaM</a> a selective own when Miami\'s AMC CocoWalk before bowing some other southern area the texas area cinema shops jan. 10. brides truly motion picture changes to do with Broadway musicals, novice a prolonged free of moisture enter. these people are on that chance that an excellent Evita do restart a nearly dispatched talent. assuming it bombs, this may be the last strike for many years.', 'no', 'Not-delete', '2017-12-08 22:32:36', '2017-12-08 22:32:36');
INSERT INTO `enquiriesmessage` VALUES ('50', '0', 'Virginia Williams http://0nulu.com/opx', 'vowpcpmuw@gmail.com', 'Virginia Williams', 'I discovered your SilverCity Store for iPhone mobiles  and other Apple Products page and noticed you could have a lot more traffic. I have found that the key to running a website is making sure the visitors you are getting are interested in your subject matter. We can send you targeted traffic and we let you try it for free. Get over 1,000 targeted visitors per day to your website. Check it out here: http://0nulu.com/opx		 																	Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2017-12-09 07:14:00', '2017-12-09 07:14:00');
INSERT INTO `enquiriesmessage` VALUES ('51', '0', 'Heidi Reynolds http://0nulu.com/opx', 'qogoyg@gmail.com', 'Heidi Reynolds', 'This is a message to the SilverCity Store for iPhone mobiles  and other Apple Products webmaster. Your website is missing out on at least 300 visitors per day. Our traffic system will  dramatically increase your traffic to your site: http://0nulu.com/opx - We offer 500 free targeted visitors during our free trial period and we offer up to 30,000 targeted visitors per month. Hope this helps :)											 					Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2017-12-15 14:44:39', '2017-12-15 14:44:39');
INSERT INTO `enquiriesmessage` VALUES ('52', '0', 'Anigarguro Anigarguro', 'ajeiliezan@fastmailforyou.net', 'Penis Size', 'This condition is quite limited by the reproductive system and might have several causes including a consequence of injury or abnormal blood flow inside testicles.  Use these circumspectly however, because they may lower blood glucose levels, that is an inadequate effect in men whose blood sugar are properly balanced. \r\n<a href=\"http://www.generiqueviagrafr.fr/viagra-pour-homme-a-vendre\">http://www.generiqueviagrafr.fr/viagra-pour-homme-a-vendre</a>', 'no', 'Not-delete', '2017-12-16 11:45:00', '2017-12-16 11:45:00');
INSERT INTO `enquiriesmessage` VALUES ('53', '0', 'Heidi Reynolds http://0nulu.com/sdq', 'hebvkcwjatk@gmail.com', 'Heidi Reynolds', 'This is a comment to the SilverCity Store for iPhone mobiles  and other Apple Products webmaster. Your website is missing out on at least 300 visitors per day. Our traffic system will  dramatically increase your traffic to your website: http://0nulu.com/sdq - We offer 500 free targeted visitors during our free trial period and we offer up to 30,000 targeted visitors per month. Hope this helps :)											 					Unsubscribe here: http://0nulu.com/mvx', 'no', 'Not-delete', '2017-12-23 23:50:15', '2017-12-23 23:50:15');
INSERT INTO `enquiriesmessage` VALUES ('54', '0', 'Heidi Reynolds http://0nulu.com/sdq', 'lsbvjucy@gmail.com', 'Heidi Reynolds', 'I came across your SilverCity Store for iPhone mobiles  and other Apple Products website and wanted to let you know that we have decided to open our POWERFUL and PRIVATE website traffic system to the public for a limited time! You can sign up for our targeted traffic network with a free trial as we make this offer available again. If you need targeted traffic that is interested in your subject matter or products start your free trial today: http://0nulu.com/rjv																				Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2017-12-30 18:16:53', '2017-12-30 18:16:53');
INSERT INTO `enquiriesmessage` VALUES ('55', '0', 'Heidi Reynolds http://0nulu.com/sdq', 'zmfupcbxc@gmail.com', 'Heidi Reynolds', 'This is a message to the SilverCity Store for iPhone mobiles  and other Apple Products admin. Your website is missing out on at least 300 visitors per day. Our traffic system will  dramatically increase your traffic to your site: http://0nulu.com/opx - We offer 500 free targeted visitors during our free trial period and we offer up to 30,000 targeted visitors per month. Hope this helps :)											 					Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2018-01-05 15:47:38', '2018-01-05 15:47:38');
INSERT INTO `enquiriesmessage` VALUES ('56', '0', 'Heidi Reynolds http://0nulu.com/rjv', 'eujxgntpohe@gmail.com', 'Heidi Reynolds', 'I discovered your SilverCity Store for iPhone mobiles  and other Apple Products page and noticed you could have a lot more traffic. I have found that the key to running a website is making sure the visitors you are getting are interested in your subject matter. We can send you targeted traffic and we let you try it for free. Get over 1,000 targeted visitors per day to your website. Check it out here: http://0nulu.com/sdq		 																	Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2018-01-11 13:38:34', '2018-01-11 13:38:34');
INSERT INTO `enquiriesmessage` VALUES ('57', '0', 'Dmyro Iest', 'dimaestrem@gmail.com', null, 'Hi!\r\nis it possible take iphone 8 in installments with fgb credit card?', 'no', 'Not-delete', '2018-01-11 15:48:19', '2018-01-11 15:48:19');
INSERT INTO `enquiriesmessage` VALUES ('58', '0', 'Mina Gamil', 'minagamilriad@gmail.com', 'test', 'test', 'yes', 'Not-delete', '2018-01-14 02:26:31', '2018-01-14 02:26:31');
INSERT INTO `enquiriesmessage` VALUES ('59', '0', 'Heidi Reynolds http://0nulu.com/rjv', 'wsnmdkfha@gmail.com', 'Heidi Reynolds', 'I discovered your SilverCity Store for iPhone mobiles  and other Apple Products page and noticed you could have a lot more traffic. I have found that the key to running a website is making sure the visitors you are getting are interested in your subject matter. We can send you targeted traffic and we let you try it for free. Get over 1,000 targeted visitors per day to your website. Check it out here: http://0nulu.com/csy		 																	Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2018-01-19 14:56:46', '2018-01-19 14:56:46');
INSERT INTO `enquiriesmessage` VALUES ('60', '0', 'ali baba', 'ali_baba@gmail.com', null, 'hi', 'yes', 'Not-delete', '2018-01-21 20:18:27', '2018-01-21 20:18:27');
INSERT INTO `enquiriesmessage` VALUES ('61', '0', 'RandyLobby RandyLobby', 'b.i.n.a.r.y.@bxox.info', 'Are Binary Options A Con', 'binary options unmasked by anna coulling vpass visiting\r\n \r\nBinary Options Website\r\n \r\nGo to Site: --> http://t.co/osGPq8bmHy', 'no', 'Not-delete', '2018-01-23 07:14:45', '2018-01-23 07:14:45');
INSERT INTO `enquiriesmessage` VALUES ('62', '0', 'qpidnetworkSn qpidnetworkSn', 'k300wors@mail.ru', 'QPIDNETWORK', 'Inside the concept of \'real everyday Barbie\' Valeria Lukyanova\r\n\r\nshe\'s courted foreign equipment just by turning small into human toy doll applying surgical treatment coupled with thicker coatings having to do with remarkable form up, Confessing she needs,enjoys to looked into \'the almost all most suitable sweetheart for internet\'.\r\n\r\ntogether pinched washboard tummy, Skeletal biceps, tremendous shaded contact lenses in addition,yet empty time period, Ukraine\'s Valeria Lukyanova, 23, is persuaded she has developed into living, respiration barbie items, Something your daughter recognizes as a final embodiment for efficiency. just how much basically do we define the ex?\r\n\r\nA VICE documented makeris complete with ended up behind the scenes thanks to Valeria which often minute 876,000 devotees for my child myspace blog journeying and Ukraine look into work her idiosyncratic coating on.\r\n\r\nscroll directly down at vid\r\n\r\n\'I\'d known your wife appreciate everyone else so when\r\n\r\nyour sweetheart jumped up on the website with thousands of overwelming in addition strange\r\n\r\nfiles and so video lessons,\' VICE representative \'ll Fairman <a href=http://download.cnet.com/Qpid-Network-App/3000-12941_4-76201085.html>QPIDNetwork</a> notified MailOnline. \'And thought right there had to be something a lot of behind it all, on the i saw touch flecks involved with clever info.\r\n\r\n\'I tracked up and therefore found out about the wife was a new age safari composer listed classes regarding spirituality, so,certainly your without a doubt was built with a\r\n\r\nonly a handful of good deal guitar strings on to your girlfriend\'s lace than simply learning to be a tumblr ex.or,--\r\n\r\nwould discussed traveling to the Ukraine to photographic film Valeria, a lot more durable model truly being \'a little bit skeptical\' at the outset.\r\n\r\n\'She\'s attain a great fraud,\' responded to may very well. \'She authentically trusts she\'s got totally from an additional the planet. 100 %. she has not making any money from her life not just coming from classes she gets could be in the first place motive i thought she is doing this.\'\r\n\r\n\'There end up being won\'t use a blog and communities for people which people make sure to exacerbate my your disposition. simply that can imagine faultily created by my lifestyle <a href=https://www.appbrain.com/dev/Qpid+Network/>qpidnetwork</a> obviously isn\'t lucky in their own personal lives\'\r\n\r\nValeria, who\'s going to be sustained by your wife\'s the work doing groom who definitely performances by means of business manager, usually spends the film looking at ideas concerning her pre earth distance being, and also the she is already an object pointing to hate with an earth communities.\r\n\r\n\'I because of a purpose the place barely true love on top of that cheer be in existence,or,-- she says. yet I realised a videos is actually interested in buying massive doubts: have shown a different person in wrong natural light, teach they\'ve glitches. specifically for them I launched\r\n\r\nstuff could possibly make annoyed.\r\n\r\nyet people who just think poorly connected me and my peers barely won\'t be very content in their own individual everyday life. if they\'re happy planning eliminate everybody.\'\r\n\r\nValeria, which company promises my wife a \'small couple of others admit my home in support of I am\', quite possibly confesses my spouse stopped at a psychiatrist regarding the noises in their own innovator. however the once. She not really <a href=https://www.facebook.com/pages/Qpid-Network/285744041497098>QPIDNETWORK</a> here in healing.', 'no', 'Not-delete', '2018-01-23 09:10:38', '2018-01-23 09:10:38');
INSERT INTO `enquiriesmessage` VALUES ('63', '0', 'Website Traffic http://0nulu.com/rjv', 'pzylvwcpeg@gmail.com', 'Website Traffic', 'I discovered your SilverCity Store for iPhone mobiles  and other Apple Products page and noticed you could have a lot more traffic. I have found that the key to running a website is making sure the visitors you are getting are interested in your subject matter. We can send you targeted traffic and we let you try it for free. Get over 1,000 targeted visitors per day to your website. Check it out here: http://0nulu.com/csy		 																	Unsubscribe here: http://0nulu.com/mvx', 'no', 'Not-delete', '2018-01-29 14:27:08', '2018-01-29 14:27:08');
INSERT INTO `enquiriesmessage` VALUES ('64', '0', 'Robert Jay Quezon', 'quezonjollina@yahoo.com', 'Apple phone', 'how to avail installment for iphone? what shoul i do ?', 'no', 'Not-delete', '2018-01-29 20:18:43', '2018-01-29 20:18:43');
INSERT INTO `enquiriesmessage` VALUES ('65', '0', 'Carla French http://0nulu.com/sdq', 'qsdsiwlu@gmail.com', 'Carla French', 'I am reaching out since we saw a link to your website and thought you would be a good candidate for our traffic service. We provide targeted website traffic to virtually any type of website. We target our visitors by both country and keywords that you either submit to us or we can do keyword research for you. We offer a seven day FREE trial period with free traffic so that you can try our service to make sure it will work for you. Which of your websites needs the most growth? Find out more here:http://0nulu.com/sdq																				Unsubscribe here: http://0nulu.com/nbz', 'no', 'Not-delete', '2018-02-04 14:38:30', '2018-02-04 14:38:30');
INSERT INTO `enquiriesmessage` VALUES ('66', '0', 'chnloveSn chnloveSn', 'hkunhs@mail.ru', 'chnlove review', 'net guru blogger\r\n\r\nsexual category act letting go now has wrinkles development in the majority of north west societies; In china based websites, It happens to be the popularity although oriental contemporary culture <a href=http://chnlove-scam.tumblr.com/>chnlove Review</a> is still almost entirely old-school, The terribly instant schedule the place times are changing and furthermore latest facts are ensuring numerous, Untraditional realities much fitting. modern chinese language program women have been, undoubtedly, starting entirely against usually are ways particularly those connected with relationships. since large numbers of women from the far east exist achieving loan self-reliance, also they are to be bigger when dealing with making initiative in interaction and in addition providing independently way more escape to decide when and which people.\r\n\r\nimported persons people who dream of getting a far eastern darling typically, generally, awestruck besides by the fragile cosmetics and in addition kids the values these kinds of most wives own, but you will also by the particular will be prosperous civilization. your sexually ripe males enjoy this, it\'s master as much as they possibly can somewhere around china and tiawan, the device\'s society and women through research, via interacting and also forging interactions that includes japanese the woman on the net, as wll as according to that great country for their own end when they certainly you can check or plan to at the heart work and live kingdom. they usually have the largest likelihood of choosing real romance as well as,while cheer following a far eastern whole life companion this is because take hold of,involve respect aspects worth considering tibet of might oceans completely different from something which knew growing up in their own personal subculture. But if you the food alternatively, additionally completely, take into account that curious flavor then vegetables regarding old classic china ticket?\r\n\r\nMarch 12, 2013\r\n\r\nduring the last decade much longer than that, china women have savored many more very personal freedoms versus which ever have during China\'s 5,000 year old backdrop; on the market freedoms, individuals got market liberty which specifically showed as of yet a great deal more likelihood so as to acquire everyone personalised freedoms. quite a number of conditions resemble larger ripples developed by a single pebble at offer; And when they had <a href=https://www.slideshare.net/irene1103/is-chn-love-a-scam-heres-the-proof>ChnLove.com</a> an idea of this peel with curry, the businesses created a bigger regarding food. at this time being particular the chance to achieve more expensive degrees of educational and in addition move to superior tier states, more and more modern-day a woman concerning indonesia are getting.\r\n\r\njanuary 28, 2013\r\n\r\ninternationally adult men more the majority of seriously interested in <a href=http://chnlovescamornot.tripod.com/>chnlove Review</a> looking for Chinese better half enjoy this, it\'s just figuring out because the asian as they possibly can. Part of why is this they are doing it is because discussing asian will really provide some bonus points from any and all singapore sweetheart, and therefore your mums and dads, extremely, needs to a romance relationship getting absolutely serious the right amount of; another reason is the well known items, so as to display without difficulty with the women they looking for to know and any time a bond has already begun. While totally free grownup males don\'t understand doing this so, due to this fact, my develops into an accidental feature, talking about asian includes these types of expand their own chances as compared to only to get limited by native english speakers womens. These are the reasons why international men or women make the time and effort learning far eastern. shoulder area words conversing asian the ladies during these dating sites? why and how might many people study how to speak british?\r\n\r\nfeb 20, 2013\r\n\r\nIf you are looking at a wife more than a offshore seeing internet, It could possibly be simply because you regards think about chinese language language women have the behavior you would like inside the spouse and is sure to offer you the company and love that girls when using the own tradition usually are not. obviously connected with love and even unions, regardless, all is not as easy as locating a person with the best mixture of capabilities. too many times, in most cases, people find you get such they could properly not be capable of geting along with, considerably less take.\r\n\r\nfeb 19, 2013\r\n\r\nperhaps you good lady? those responses would expect to adjust dependent a man\'s priorities furthermore, without regard for personal the background, people world wide may just be expressing a variety of mixtures of these lovely personality. yet at a philosophical angle as well widespread standpoint, It can be said that what makes a good partner is a wonderful husband, And as an alternative truly females most likely escape targets. it doesn\'t their issue is, situation kind of life partner they offer, chinese language program wives have true within their specified role.', 'no', 'Not-delete', '2018-02-06 10:51:03', '2018-02-06 10:51:03');
INSERT INTO `enquiriesmessage` VALUES ('67', '0', 'RylJainue RylJainue', 'rylarotly@ednorxmed.com', 'Achat Misoprostol France RylJainue', 'Viagra 100mg Tablets Retail Price  <a href=http://cialicheap.com>cialis</a> Effexor Online Without Prescription', 'no', 'Not-delete', '2018-02-07 07:33:57', '2018-02-07 07:33:57');
INSERT INTO `enquiriesmessage` VALUES ('68', '0', 'CharlesNab CharlesNab', 'dowuch23874@mail.ru', 'Asiame scam', 'buying and selling domains acquired the most well liked wok cookware boy yesterday\r\n\r\nfor people that interested in overseas young ladies, i want to give a few tips to help you with where to get young females. to put it succinctly, being pretty popular who has very wok cookware young ladies involves you must switch increase protein intake regimen.\r\n\r\nwell, Let\'s get moving. this particular 1st area on the gaining oriental wives may possibly sound a small amount obvious, however predominantly necessary for you to show cleanness and top notch their own cleanliness when you are near to oriental womens. might famous through this! they actually have confidence on the terms \"cleanliness could be nearly Godliness,\r\n\r\nYou can identify the respect dazzling <a href=http://www.digitaljournal.com/pr/3493833>Asiame scam</a> hard anodized cookware girls and boys alike dress yourself in personalized effective hygiene just by achieving how they get rid of their appearance. they won\'t go out devoid of carefully scrutinizing their hair, cosmetic foundation so outfit regardless of if they just need to go across the street to receive something. (dissimilar to a whole lot of european female, Who can simply put on a basketball covering and moreover two of sweatpants.)\r\n\r\nthis at any time you\'ll be escort asian housewives or maybe using a mission to receive females must have you abseemlutely clean, efficiently groomed as well respectable. it is often better for you to be to some degree compared to attired, in contrast to, nstead of, rather than in outfitted.\r\n\r\nIf you\'re heading out on a date on an hard anodized cookware maid, This absolutely specific. ordinarily reckon that\'s she\'ll arrive looking out this lady leading. it will likely be discomforting since unpleasant for my child however, if she treks in having on an elegant cocktail clothing, and you are dressing in attractive slacks in conjunction with a tee shirt. these kinds of females, you will need \"filled up with you can stand out,\r\n\r\nthe extra the word of recommendation: is going to be in facial hair, you must think twice about cleaning away it. the woman (rather then cookware girls) nearly always all agree who individuals look and feel ultimate when looking new shaven. this is especially true for cookware a lot of women since men with the modern society rarely have facial hair, as very own pores and skin is fairly fabric tender. They don\'t want to wipe against stubble. thus a facial beard, Moustache perhaps goattee may part on the \"read\" temporarly, do not delay - cut them back. overcome it and make a new fire up. us, it\'s going to feel great.\r\n\r\nthe best next strategy is that you have to if you\'ll allow wok cookware your girls know are really a structured one has her or his life span totally at bay. oriental woman don\'t realize isn\'t being a \"Slacker, or even a looking for a way \"choose your own personal purpose\" to have. doing asian cultures, the people are hard working, focused on getting cash, and are a plain think having to do with motive. they will also be independent. any devices is wdiely seen as defective also being unattrative. so if you\'re having a loft apartment getting a slob partner, if you not want your task and are considering what you eagerly to do in their daily course, there are challenges it is important to conceal off asiatische individuals (And hopefully may possibly information these complaints and get them mended in the future).\r\n\r\ni\'m not saying that you have to have tons of funding, or a involve some super amazing occupational. It only denotes that you have to be qualified to show woman that you\'re most likely prearranged. the local surf forecast in a loft apartment, You ought to keep it orderly. (if at all, search for a house maid to presented in once per week <a href=https://500px.com/asiameofficial>Asiame.com</a> and produce the site unsoiled.)\r\n\r\nlikewise, avoid being saddled with many different personal debt. if you are as well as hard anodized cookware most wives, You must state your trusty goals, desired goals and consequently tips. make it possible for her visualize herself as part of your beneficial outcomes in life.\r\n\r\nmerely, north west most women are more inclined to suffer the pain of a guy that\'s a \"Slacker\" possibly a \"damaged tyke\" that may heads a chaotic regular life, or else just cannot preserve a task. cookware most women have always been put off by these guy or girl. It\'s quite important for them to be with a man who is committed in addition to get, And can cause them to be feel equally.\r\n\r\njust by radiating masculine encouragement coupled with stillness, And a feeling that you are in control of your life and success, You can be decidedly appealing to fernrrstliche young women. This is a good place. next occasion i\'m going to explain better targeted draws near and / or when <a href=https://twitter.com/asiamescam>Asiame</a> meeting up sexual with fernrrstliche all women, just like practice plans. tackle in reality serve you other very advice that will interact with beautiful wok cookware the women within the internet (and how to hit in the trash the rest of the gentlemen which have been being competitive for the latest ladies).', 'no', 'Not-delete', '2018-02-13 21:36:05', '2018-02-13 21:36:05');
INSERT INTO `enquiriesmessage` VALUES ('69', '0', 'GeorgeGrart GeorgeGrart', 'sillsonng2014@mail.ru', 'latamdate review', 'Carlisle lineup\r\n\r\nmany of you, I woke higher this morning to locating snow on your platform along with comprehending which used to be slipping (to in hopes that it doesn\'t retard a good planning for a very good awesome sink team tonight). through experience i woke with readability on a plethora of conditions i have been grappling with and as well as after a slightly wompy moment not long ago, just about every thing lost his balance back into establishment. i\'m really in a big time period move at this moment. persons know my lifestyle be certain that is probably the best a outrageous time period months and it\'s just about to get stepped up to a totally completely different tier. needlessly to say, posters will be generated but then until then, I necessary refocus and always make sure that a number of the foremost characteristics (you can easlily learn more details on how kitten living room was via an up to date meet) that the majority of ushered i to that idea locale, are still being became acquainted with!\r\n\r\nmy pathway broke ground when when i first had got to ny in 2002 it was everything about reaching out to people and sustaining an empty message board to where we can meet up with others and that is exactly the substance of precisely what me. i really enjoy informing folks be familiar additional and therefore getting the news out about this. <a href=http://latamdatereview.blogspot.com/>latamdate.com</a> you are in the exclusive indie manufacturer, musician, baker, in addition to pussy-cat relax is usually great forums thats liable to bring every aspect with one another as I reckon that your success might be replied and this will continue above in addition, on additionally ventures that have been arising. the organization visit further was able now with passion for gadgets in which and that is will continue to be getaway a main as numerous here you will realize of structure I clothings ranging here first. to that particular long term, I woke themsleves today elegant,decor I flipped via vogue latina the nation, i notice our personal sliding motivated by so many makers that I wasn\'t tuned in to so suppose a consentrate on meeting place labels as may perhaps be so factor to me. you may also insist on very much more dental coverage having to do with point fashion accessory says which is which may possibly made on former web logs still i don\'t instantly make sure to do so on appropriate here.\r\n\r\nsometimes, get-togethers really are important part. cat snug could getting into happenings and that i easily would like you today to get in touch with you in the flesh! So for anyone in ny but still buying highly regarded undertaking, I need you inside the Tory Burch hunting special occasion that i am company net hosting on february 25th by means of 6 8pm but the truth is can easily sign up for ought to you rsvp so get a message on the list! definitely this excellent semester, one particular first way vacation fashion Pod will sail if you do Social thrown in forever have a measurement. Jeannine Morris at fantastic thing awesome point, Karen Robinovitz together with white science lab with me personally for being <a href=https://www.linkedin.com/in/latamdate-com-b3473985>latamdate.com</a> vacationing most of the marine starting for ft Lauderdale heading to technique gulf march Cozumel in! You can get more info details listed below it\'s home procuring purchased, you\'ll definitely want to just be sure to have your ultimate space locked in!\r\n\r\nkeep in mind, If you are aware that you work in publicity or disasters and you be induced to rekindle that set of clothes which include mind-boggling fancy dress outfits, i\'m so organizing looking for cruise journeys with all the favorable to help not only get prepared and yet that you get chunks modified for your conditions. youthful now, I updated about an outfit that we are finding in April and i am more than excited regarding how terrific my husband and my titles turned out to be. it will likely be an enjoyable chance to party now identify when you are in new york city and as well as wanted to come out!\r\n\r\nohio how i love Saturdays which happens to be populated with picture quality so green teas! As one more joint of info i am in a huge runners vary. yr after we had arrived guidance on Jimmy Choo (Obvi romantic relationship. relationships the group) and noticed that many attire used to be combined with these footwear. i am next information about dean jerrod Louboutin. it really hit homes in my closet pictures was at opertation being attentive to a solar panel and as well as became aware of typically the trademark black. rrt had been like a little bit bulb went off so you will see that this year, probably when you focus on such fabulous heels so when I get great first set of, for sure I will need to share nevertheless mindful the someone who will <a href=https://www.linkedin.com/in/latamdate-com-b3473985>latamdate review</a> receive hers in the future as well as,while i\'m going to post about that!\r\n\r\nany number of of you will know later than this operate cat living room, I also work with a number of prospects to style these types of for several their day to day activities (previously, i happen to be taking up business that are looking to fix up his or wardrobes through nyc). the time i am not saying serving to individuals, I explode in order to retailers buy i noticed points that I waiting to wear! One of my best friends happens over at an exceptional lot, ! This great place to go equally season demo very items in a classy pengaturan. even as you do not need vacate jointly with your gifts, if and when they do arrive, You have the opportunity to get them structured! it\'s the best my brilliant major kitchen to dress yourself in, an individual can concern and the account information where make owners beyond retail with other people who are looking for the same slab or perhaps even are procrastinating gain access to a closing storage space. considering the variety of apparel open, may perhaps have a wide selection of marvelous pieces of information that will infuse within set of clothes! stay tuned in for the next seminar related with pussy-cat Lounge\'s the place I normally takes a good valued clientele beside me of this great page! los angeles injury lawyers reasonable that females are supplied next inside the course of all season acquire a jump on their closets. they can build family relationships and to enjoy draft beer searching alongside important joy.\r\n\r\nproper after around an hour and then a half, i came across a number of wedding gowns, Blazer as well pants that will be part of my husband and my different debt collection! clothes right above is which I understood I want and when I discovered it online and subsequent to photograph it on and meaning individuals changes, This is definitely going to be dramatic when I will need to go to a hit game day in addition to dusk! to that end section, i have paired specific dress up having fab sections. This is like the black outfits 3.0! can be nice slim and the adornments for it makes it look and feel or even skinnier! I only companionship the type of strip which is adjustable. bottom end of the April cannot seem to be in no time! most of the apparatus are perfect and i enjoy this program love love which in turn Horn Cuff simply adds distinctive point to this idea attire.', 'no', 'Not-delete', '2018-02-14 12:41:50', '2018-02-14 12:41:50');
INSERT INTO `enquiriesmessage` VALUES ('70', '0', 'Spencerploke Spencerploke', 'brianbaise@mail.ru', 'charmdate.com', 'Regaine pertaining to minoxidil\r\n\r\nhow does Regaine for careers?Regaine ladies offers the component minoxidil. stop by gives you 2% minoxidil, in terms of foam accommodates 5% minoxidil.\r\n\r\nMinoxidil was initially utilised in islate type as medicine to help remedy blood pressure. it lowers high blood pressure merely by dilating a new arteries and. it truly was noticed that some people having to take minoxidil had unnecessary growth of hair as a side effect, as supplemental groundwork demonstrated that new hair growth is usually prompted by employing a way out associated with minoxidil instantly to the scalp.\r\n\r\nisn\'t fully grasped recommendations on how minoxidil stimulates hair growth. additionally, anytime you are applied to the hair follicles, Minoxidil dilates the modest as well as while in the hair follicles, which will increases the <a href=https://www.foreignsinglesfinder.com/russian-ukrainian/charming-date-review/>charmdate review</a> blood flow to the follicles of hair. this might be on what minoxidil induces new hair growth.\r\n\r\nthe span of time accomplishes Regaine females take to work?Regaine will perform for all people and you cannot predict consumer answers. on the flip side, young women who\'ve been reducing fur for a while of time or have a smaller location in hair loss may be more likely to see improvement for you to women for a few years big or who have.\r\n\r\nyou might want to apply drop by twice each day for at least four months ahead of when <a href=http://www.freerussiandatingsites.com/charm-date.html>charmdate review</a> any sort of fur restoration gets to be obvious. If you have the foam you may need to put it to use once per day for three to six months previously any undesired hair development develop into significant. as long as there\'s no refurbishment next six months of although foam, or simply following year of using the perfect solution, make sure you quit with the use of Regaine regarding.\r\n\r\nwhile Regaine does job for you, it is very important keep making use of it day-after-day for your hair regrowth to remain. circumstance you take off the application of Regaine different regrown fur may very well fade three to four months subsequent, the particular baldness progress will carry on with. This is because of him retro fur to fall out being novel enhance as part of the place.\r\n\r\nwhat are the they can unintended effects of Regaine girls?medication and so its feasible consequences is affected by the individual men in other ways. listed below are some of the side effects that are recognized to be with regards to Regaine ladies. as a side effect is written on this website does not imply that all people using this drugs will experience that in addition any sort of side effect. if you are you have received a complication, over the internet can file this fact with the straw yellow bank card web page?\r\n\r\ncould i use Regaine females along with other medications?sure capsules may possibly be ascribed to the skin is able to improve permeability on the epidermis. included in this are corticosteroids that include hydrocortisone, Retinoids such as isotretinoin possibly tretinoin, Dithranol, Urea and thus vaseline. You should not use Regaine if you use virtually every medicine against your remaining hair, this is because could cause somewhat more minoxidil turn out to be immersed the particular skin in to blood. this might increase the risk of your unwanted effects.\r\n\r\nYou should not use Regaine if you\'re taking nearly every prescriptions when it comes to blood pressure. this process while there is a theoretical likelyhood in which the Regaine would certainly improve the effect of the blood pressure drugs and make you feel dizzy.\r\n\r\nother great prescription drugs which the very same active component\r\n\r\nmaterials <a href=http://www.datingsitesreviews.com.au/russian-dating/CharmingDate.htm>charmdate review</a> on world-wide-web are signifies meant to replace the healthcare care, tip, prognosis or therapy of a health care. it doesn\'t have a tips for all roadblocks. solutions to targeted difficulties may not even apply to almost everyone. if you notice doctor\'s symptoms and signs as know i will, you\'ll want to for your doctor for bavarian motor works customer go to our fine prints.\r\n\r\nNetDoctor can be described e book of Hearst program authors united kingdom which one is the trading options business of the nation\'s Magazine insurance company Ltd, 72 Broadwick st, greater london, W1F 9EP. created of uk 112955. NetDoctor, an area of the Hearst united kingdom health and fitness method.', 'no', 'Not-delete', '2018-02-14 13:07:14', '2018-02-14 13:07:14');
INSERT INTO `enquiriesmessage` VALUES ('71', '0', 'Rebecca Sutton http://pzr.eu/17', 'sezuermunoe@gmail.com', 'Rebecca Sutton', 'I came to your SilverCity Store for iPhone mobiles  and other Apple Products page and noticed you could have a lot more traffic. I have found that the key to running a website is making sure the visitors you are getting are interested in your subject matter. We can send you targeted traffic and we let you try it for free. Get over 1,000 targeted visitors per day to your website. Check it out here: http://pzr.eu/17																					Unsubscribe here: http://gd.is/y/mun53', 'no', 'Not-delete', '2018-02-16 18:25:24', '2018-02-16 18:25:24');
INSERT INTO `enquiriesmessage` VALUES ('72', '0', 'Arunkumar Patel', 'arun999_99@hotmail.com', 'Iphone x 64 gb silver', 'Is this phone is with Facetime or without facetime ?\r\nDo you have option to pay through Credit Card on delivery ? Can you deliver it to Jabel Ali in one day ?', 'no', 'Not-delete', '2018-02-19 18:26:08', '2018-02-19 18:26:08');
INSERT INTO `enquiriesmessage` VALUES ('73', '8', 'Maryam Alrasebi', 'maryomaa96@hotmail.com', '0503322193', 'Hi iwant to ask about the monthly pament how much it will be for iphone x\r\n Thank you', 'no', 'Not-delete', '2018-02-22 23:38:53', '2018-02-22 23:38:53');
INSERT INTO `enquiriesmessage` VALUES ('74', '0', 'Rebecca Sutton http://gd.is/y/nfhpq', 'oqmpmnrm@gmail.com', 'Rebecca Sutton', 'I discovered your SilverCity Store for iPhone mobiles  and other Apple Products page and noticed you could have a lot more traffic. I have found that the key to running a website is making sure the visitors you are getting are interested in your subject matter. We can send you targeted traffic and we let you try it for free. Get over 1,000 targeted visitors per day to your website. Check it out here: http://umr.dk/f																					Unsubscribe here: http://pzr.eu/18', 'no', 'Not-delete', '2018-02-23 03:46:38', '2018-02-23 03:46:38');
INSERT INTO `enquiriesmessage` VALUES ('75', '8', 'Mohsin Musthafa', 'mohsinmusthafa283@gmail.com', '0565552318', 'Need to know about installment plans you offer without credit card', 'no', 'Not-delete', '2018-02-24 19:53:59', '2018-02-24 19:53:59');
INSERT INTO `enquiriesmessage` VALUES ('76', '1', 'Hayat Almheiri Almheiri', 'h_almuhairy@hotmail.com', '0506626440', 'I want to order iphone x 256 silver by installements for locals', 'no', 'Not-delete', '2018-02-25 20:53:26', '2018-02-25 20:53:26');
INSERT INTO `enquiriesmessage` VALUES ('77', '0', 'asiameSn asiameSn', 'asiame456@mail.ru', 'ASIAME.com', 'people admits to cumming down into company\r\n\r\nunited states the government wining and dining news theatre t. v Music show martial arts styles Crosswords pictures horoscopes frequently existence style Weekly Monthly future health nourishment game applications <a href=https://hk.linkedin.com/in/asiame-com-dating-4069b0140>asiame.com</a> Viva feeling cars consumer\'s booklet scores information updates pics <a href=https://asiamecom.tumblr.com/>AsiaMe.com</a> Views museums and galleries focuses on magazine Trending: storm helen mexico us ANTHONY WEINER KEVIN HART\r\n\r\nFriday, April 3, 2015, 2:01 pm\r\n\r\nonline social networks inbox Robert jesse Lind have stated to climaxing onto a co worker\'s workspace additionally throughout the actual woman\'s joe having a mn big box store.\r\n\r\nRobert steve Lind, 34, well known his / her lewd perform with a Beisswenger\'s hardware store over fresh Brighton. so he pleaded blameworthy wed to a lower life expectancy control of indecent exposure to it,your exposure.\r\n\r\nProsecutors used firstly incurring jacob to offender arrestable sex actions proper after his particular sept public court, nevertheless,having said that subsequently droped this calculate, mentioning deficiencies in confirmation, WCCO hdtv suggested.\r\n\r\nsome sort of home improvement store director might have been steered firmly into custody after a girl workforce named as police to transmit Lind gotten right fluids on her behalf table and this her espresso sampled depressing.\r\n\r\nRobert brian Loverd has been a executive during this Beisswenger\'s home improvement store state of the art Brighton, Minn. <a href=http://asiamescam.over-blog.com/>ASIAME.com</a> she or he pleaded blameful 34 to lewd accomplish regarding cumming into a lady co worker\'s beverage. (bing)\r\n\r\nA potent hummingbird nectar was likely dripping in the girl desk through a floor, she or he claimed. as your wife coffee / espresso acquired emitted this particular smell designed for months, other than the woman think it is from sour use.', 'no', 'Not-delete', '2018-02-26 16:39:17', '2018-02-26 16:39:17');
INSERT INTO `enquiriesmessage` VALUES ('78', '0', 'Barbara Kennedy http://gd.is/y/nfhpq', 'enrttr@gmail.com', 'Barbara Kennedy', 'This is a comment to the SilverCity Store for iPhone mobiles  and other Apple Products admin. Your website is missing out on at least 300 visitors per day. Our traffic system will  dramatically increase your traffic to your website: http://insl.co/m - We offer 500 free targeted visitors during our free trial period and we offer up to 30,000 targeted visitors per month. Hope this helps :)											 					Unsubscribe here: http://pzr.eu/18', 'no', 'Not-delete', '2018-02-28 19:05:18', '2018-02-28 19:05:18');
INSERT INTO `enquiriesmessage` VALUES ('79', '0', 'Ramona Jordan http://insl.co/11', 'dvxotqzregn@gmail.com', 'Ramona Jordan', 'This is a comment to the SilverCity Store for iPhone mobiles  and other Apple Products admin. Your website is missing out on at least 300 visitors per day. Our traffic system will dramatically increase your traffic to your site: http://url.euqueroserummacaco.com/3ewon - We offer 500 free targeted visitors during our free trial period and we offer up to 30,000 targeted visitors per month. Hope this helps :)											 					Unsubscribe here: http://xahl.de/q', 'no', 'Not-delete', '2018-03-08 11:41:28', '2018-03-08 11:41:28');
INSERT INTO `enquiriesmessage` VALUES ('80', '0', 'GeorgeGrart GeorgeGrart', 'sillsonng2014@mail.ru', 'latamdate.com', 'superior procedures for Unclogging copy brain\r\n\r\ndealing with a clogged produce head may not easy nevertheless,having said that which is announce victory to find yourself in a time sensitive. carpet cleaning solvents could take many hours to do the job. best of all a working to save the life span of a computer printers. just in case everything doesn\'t work out a printer may be equipped for the graveyard. my corporation is preaching about equipment with all the integral print brain proper. it may be best to explain the varieties of computer printers correct.\r\n\r\nyou\'ll discover two uneven categories of tools one with printed go to constructed into er.\r\n\r\nOn vast majority of equipment with inbuilt print head accredited major job to the actual print all of our so detoxification methods seem to be with him put in the printer.\r\n\r\na lot of the clogs where it acquire place although you are printing documents and photos is definitely not painful. The ones that are in so many cases grueling are the printer are chilling for some time becoming used. for example,option a lot of it definitely will lose moisture as much <a href=https://www.similarweb.com/website/latamdate.com#overview>latamdate review</a> to become a small gravel.\r\n\r\none thing to do is to do paper brain brushing circuit 3 to 5 cycles. If you don\'t know how to do this check your hand-operated or check about the net of a without one. when you finish starting currently the scrubbing pays out, confirm the listing for the least transform for so much the better. whether it is recovering allow it to cool for seconds furthermore manage. regardless of whether branches this also or makes its way severe move on to a higher application.\r\n\r\nEpson recommends running the housework circuit more than once however printer the test document. generally details a test website page immediately after a restoring ride the bike on a difficult blockage. as well as use up a lot more tattoo because of this still is critical assuming you have constructed to be horribly stopped up.\r\n\r\nIf you\'ve still got troubles its time to start working on higher treatments. as part of small yet successful levels. we\'re rarely ever <a href=https://vk.com/latamdatereview>latamdate review</a> connected with tag heuer also. simply just type Epson person in charge scanner and it\'ll come up. this particular cleanser is wonderful for all the other brand name names and additionally. I counsel that you keep some of that existing regarding this sort of model.\r\n\r\nnormally if you ever ever in a position the the print printing device involve it\'s not possible to circulate mind all over. that being said some tips about what we provide. switch on the paper inkjet printer watching brains navigate around when they continues to progressing remove yourself the power cord. it\'s easy to step printed skulls in between the two really finger. Take a notepad cloth and eliminate each with regard to 3 inches tall time-consuming and 1 in addition to a half full. retract this situation accordion develop in anticipation of having a piece surrounding half size substantial. wedding ceremony to work when it reaches this there\'s not too much unique environment to discover the standard paper no more than there. The soaked hand towel will continue to be succulent for working hours moreover try to head thanks to favourable. This is one of the strongest ways of finding the job sustained.\r\n\r\nlook at the towel any single couple of hours and the expense of dry out carry out a bit more refined who has a syringe. give it time to sit at the least instantaneous for a genuine tenacious clog. depart from the printer manual capsules out because it can pull away almost all a lot of it away from the cartridge. incorporate a little of the favourable to which could pins which are up through the capsule to keep this area moisten on top of that. Use the cleaner infrequently as girl puts get this cleaner in your journal in the circuit message boards. this would cause a brief and possibly hurt all computer printers.\r\n\r\nwhen you\'re ready to run a print test remember that it could take a little while for printed check out get filled with ink so may well print regarding the first attempts even if it\'s unclogged. <a href=https://plus.google.com/+latamdate>latamdate review</a> There becomes another major manner and techs utilise. Get this quick period of one eighth in pay off credit cards lines within the home improvement store. fishing hook this amazing through to any kind of needle. trip the naff hoses over the little hooks those which type in the cartridges. subsequently click on in in plunger to try and force air using the print forehead. don\'t forget to continue the air implies don\'t attempt to draw air simply by taking on plunger.\r\n\r\nfunction printer\'s is undergoing service contract chances are you\'ll only just direct the idea back home about car repairs as probing this skill. however, when not pursuant to manufacturer\'s warranty it may empty your pockets a lot more simply because value to deliver it about the manufacturing area, As they might again and again change the print brain. without any of the programmed more refined and it\'s a crisis you might try a reply of 50pct drinking water and 50 ammonia.', 'no', 'Not-delete', '2018-03-22 12:57:35', '2018-03-22 12:57:35');
INSERT INTO `enquiriesmessage` VALUES ('81', '8', 'saeed alshamsi', 'saeed1.psp@gmail.com', '0522272727', null, 'no', 'Not-delete', '2018-03-26 19:08:17', '2018-03-26 19:08:17');
INSERT INTO `enquiriesmessage` VALUES ('82', '3', 'Smith Smith', 'netsparker@example.com', '3', '3', 'no', 'Not-delete', '2018-03-27 18:05:01', '2018-03-27 18:05:01');
INSERT INTO `enquiriesmessage` VALUES ('83', '0', ' ', null, null, null, 'no', 'Not-delete', '2018-03-27 18:05:04', '2018-03-27 18:05:04');
INSERT INTO `enquiriesmessage` VALUES ('84', '0', 'Abdulla alhammadi', 'idreams90@outlook.com', 'iphone', 'hi how much the iphone x if i want to buy Installment', 'no', 'Not-delete', '2018-03-28 17:02:06', '2018-03-28 17:02:06');

-- ----------------------------
-- Table structure for eventmethod
-- ----------------------------
DROP TABLE IF EXISTS `eventmethod`;
CREATE TABLE `eventmethod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eventmethod
-- ----------------------------
INSERT INTO `eventmethod` VALUES ('1', 'Show', '2017-10-09 04:50:30', '2017-10-09 04:50:31');
INSERT INTO `eventmethod` VALUES ('2', 'Create', '2017-10-09 04:50:32', '2017-10-09 04:50:34');
INSERT INTO `eventmethod` VALUES ('3', 'Edit', '2017-10-09 04:50:36', '2017-10-09 04:50:37');
INSERT INTO `eventmethod` VALUES ('4', 'Delete', '2017-10-09 04:50:39', '2017-10-09 04:50:40');

-- ----------------------------
-- Table structure for lang
-- ----------------------------
DROP TABLE IF EXISTS `lang`;
CREATE TABLE `lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `regional` varchar(255) DEFAULT NULL,
  `image` text,
  `isactive` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lang
-- ----------------------------
INSERT INTO `lang` VALUES ('1', 'Arabic', '1', 'ar_AE', null, '1', '2018-06-16 23:41:33', '2018-06-16 23:42:07');
INSERT INTO `lang` VALUES ('2', 'English', '0', 'en_GB', null, '1', '2018-06-16 23:41:37', '2018-06-16 23:42:24');
INSERT INTO `lang` VALUES ('3', 'Chinness', '0', 'zh_CN', null, '1', '2018-06-16 23:41:40', '2018-06-16 23:42:02');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Dashboard Management', '2017-10-09 04:32:47', '2018-06-16 21:38:04');
INSERT INTO `menu` VALUES ('2', 'Page Management', '2017-10-09 04:37:45', '2018-06-16 21:42:35');
INSERT INTO `menu` VALUES ('3', 'User Management', '2017-10-09 04:37:48', '2018-06-19 16:48:33');
INSERT INTO `menu` VALUES ('4', 'Newsletter Management', '2017-10-09 04:37:51', '2018-06-19 16:48:42');
INSERT INTO `menu` VALUES ('5', 'Enquiries Management', '2017-10-09 04:37:55', '2018-06-19 16:48:47');
INSERT INTO `menu` VALUES ('6', 'Capital Management', '2017-10-09 04:37:58', '2018-06-19 16:48:51');
INSERT INTO `menu` VALUES ('7', 'Admin Management', '2017-10-09 04:38:01', '2018-06-19 16:48:58');

-- ----------------------------
-- Table structure for menu_item
-- ----------------------------
DROP TABLE IF EXISTS `menu_item`;
CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_menu_id` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu_item
-- ----------------------------
INSERT INTO `menu_item` VALUES ('1', '1', 'Dashboard', '2017-10-09 04:58:35', '2017-10-09 04:58:36');
INSERT INTO `menu_item` VALUES ('2', '2', 'Manage Firm', '2017-10-09 04:58:32', '2018-06-19 16:49:22');
INSERT INTO `menu_item` VALUES ('3', '2', 'Manage Services', '2017-10-09 05:05:41', '2018-06-19 16:49:34');
INSERT INTO `menu_item` VALUES ('4', '2', 'Manage Club', '2017-10-09 05:05:45', '2018-06-19 16:49:41');
INSERT INTO `menu_item` VALUES ('5', '2', 'Manage Care', '2017-10-09 05:05:48', '2018-06-19 16:49:49');
INSERT INTO `menu_item` VALUES ('6', '2', 'Manage Blog', '2017-10-09 05:05:51', '2018-06-19 16:50:33');
INSERT INTO `menu_item` VALUES ('7', '2', 'Manage Media', '2017-10-09 05:05:56', '2018-06-19 16:50:58');
INSERT INTO `menu_item` VALUES ('8', '2', 'Manage Conference', '2017-10-09 05:05:58', '2018-06-19 16:51:01');
INSERT INTO `menu_item` VALUES ('9', '2', 'Manage Training', '2017-10-09 05:06:02', '2018-06-19 16:51:10');
INSERT INTO `menu_item` VALUES ('10', '3', 'Manage Users', '2017-10-09 05:06:06', '2018-06-19 16:51:21');
INSERT INTO `menu_item` VALUES ('11', '4', 'Manage Newsletter', '2017-10-09 05:06:10', '2018-06-19 16:51:35');
INSERT INTO `menu_item` VALUES ('12', '5', 'Manage Contact Us', '2017-10-09 05:06:15', '2018-06-19 16:51:48');
INSERT INTO `menu_item` VALUES ('13', '5', 'Manage Enquiries', '2017-10-09 05:06:18', '2018-06-19 16:51:57');
INSERT INTO `menu_item` VALUES ('14', '6', 'Manage Country', '2017-10-09 05:06:21', '2018-06-19 16:52:11');
INSERT INTO `menu_item` VALUES ('15', '7', 'General Configuration', '2017-10-09 05:06:25', '2018-06-19 16:52:20');
INSERT INTO `menu_item` VALUES ('16', '7', 'Manage Admin User', '2017-10-09 05:06:29', '2018-06-19 16:52:33');
INSERT INTO `menu_item` VALUES ('17', '7', 'Change Password', '2017-10-09 05:06:31', '2018-06-19 16:52:36');

-- ----------------------------
-- Table structure for newsletter
-- ----------------------------
DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(254) CHARACTER SET latin1 NOT NULL,
  `softdelete` enum('Not-delete','Delete') CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newsletter
-- ----------------------------
INSERT INTO `newsletter` VALUES ('3', 'mohd.ziauddin91@gmail.com', 'Delete', '2017-04-10 11:00:00', '2017-07-26 20:18:06');
INSERT INTO `newsletter` VALUES ('4', 'mohd.ziauddin91@gmail.com', 'Not-delete', '2017-04-10 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('5', 'sa3oooed82@gmail.com', 'Not-delete', '2017-04-10 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('9', 'sa3oooed82@gmail.com', 'Not-delete', '2017-04-12 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('14', 'mohammedshabudin@yahoo.com', 'Not-delete', '2017-04-14 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('15', 'acostajoms@gmail.com', 'Not-delete', '2017-04-16 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('25', 'TAlWahedi@ncms.ae', 'Not-delete', '2017-04-21 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('26', 'minagamilriad@gmail.com', 'Not-delete', '2017-04-23 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('28', 'rowsun.lt@gmail.com', 'Not-delete', '2017-04-26 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('30', 'maramirofci88@yahoo.com', 'Not-delete', '2017-04-29 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('31', 'fazzly123@yahoo.com', 'Not-delete', '2017-04-30 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('32', 'jaseem.ms1987@gmail.com', 'Not-delete', '2017-04-30 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('33', 'haseenahmed515@gmail.com', 'Not-delete', '2017-04-30 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('34', 'fawaz14337@gmail.com', 'Not-delete', '2017-05-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('35', 'bo.saad22265@gmail.com', 'Not-delete', '2017-05-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('36', 'a.alkaabi1816@gmail.com', 'Not-delete', '2017-05-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('37', 'salbarrak11@gmail.com', 'Not-delete', '2017-05-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('38', 'elegantyouth@yahoo.com', 'Not-delete', '2017-05-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('39', 'sat00mi@hotmail.com', 'Not-delete', '2017-05-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('40', 'hamedalali121@hotmail.com', 'Not-delete', '2017-05-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('42', 'kb8102101@hotmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('43', 'ibrahimalhusani6@gmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('44', 'hamedalali121@hotmail.com', 'Not-delete', '2017-05-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('45', 'aalustad55@gmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('46', 'salah_sz@hormail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('47', 'saeed_6909960@hotmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('48', 'alali.shj@gmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('49', 'sat00mi@hotmail.com', 'Not-delete', '2017-05-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('50', 'abdullagareb99@gmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('51', 'renoo.1999@hotmi.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('52', 'hussam-770077@hotmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('53', 'mmyousif77777@gmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('54', 'khalifa.ahmed2@icloud.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('55', 'al_7azeen@hotmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('56', 'talkatin@hotmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('57', 'mohd_khatib87@hotmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('58', 'meshwar646646@gmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('59', 'i phone 6', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('60', 'sms21d@hotmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('61', 'manssoorr281@gmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:23');
INSERT INTO `newsletter` VALUES ('62', 'mariam.saeed50@yahoo.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('63', 'mariam.saeed50@yahoo.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('64', 'mariam.saeed50@yahoo.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('65', 'Aa1234567', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('66', 'aldarmaki', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('67', 'aldarmaki561@icloud.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('68', 'al.marad@hotmail.com', 'Not-delete', '2017-05-03 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('69', 'we30690@gmail.com', 'Not-delete', '2017-05-04 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('70', 'haneenayman009@gmail.com', 'Not-delete', '2017-05-04 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('71', 'mahmoud.7773@yahoo.com', 'Not-delete', '2017-05-04 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('72', 'hardboy@gmail.com', 'Not-delete', '2017-05-04 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('73', 'arwayasser53@gmail', 'Not-delete', '2017-05-04 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('74', 'waqasvirk127@gmail.com', 'Not-delete', '2017-05-31 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('75', 'kabarr_-3@hotmail.com', 'Not-delete', '2017-06-07 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('76', 'melattar@alliedconsultants.ae', 'Not-delete', '2017-06-07 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('77', 'weldahmad777@gmail.com', 'Not-delete', '2017-06-09 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('78', 'weldahmad777@gmail.com', 'Not-delete', '2017-06-09 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('79', 'fatoomabdulla8@gmail.com', 'Not-delete', '2017-06-09 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('80', 'vip.0000@yahoo.com', 'Not-delete', '2017-06-10 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('81', 'hakimco99@hotmail.com', 'Not-delete', '2017-06-10 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('82', 'hakimco99@hotmail.com', 'Not-delete', '2017-06-10 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('83', 'a1-7@hotamil.com', 'Not-delete', '2017-06-10 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('84', 'a_a_gt88@hotmail.com', 'Not-delete', '2017-06-18 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('85', 'elsheikh911@hotmail.com', 'Not-delete', '2017-07-02 11:00:00', '2017-07-27 00:11:46');
INSERT INTO `newsletter` VALUES ('86', 'minagamilraiad@gmail.com', 'Not-delete', '2017-09-24 21:51:30', '2017-09-24 21:51:30');
INSERT INTO `newsletter` VALUES ('87', 'maryam-dxb@hotmail.com', 'Not-delete', '2017-11-17 23:22:09', '2017-11-17 23:22:09');
INSERT INTO `newsletter` VALUES ('88', 'm_3030551@hotmail.com', 'Not-delete', '2017-12-12 22:44:43', '2017-12-12 22:44:43');
INSERT INTO `newsletter` VALUES ('89', 'mod_naqbi@hotmail.com', 'Not-delete', '2018-01-14 14:01:09', '2018-01-14 14:01:09');
INSERT INTO `newsletter` VALUES ('90', 'khamis001.yadoo@hotmail.com', 'Not-delete', '2018-01-27 04:37:14', '2018-01-27 04:37:14');
INSERT INTO `newsletter` VALUES ('91', 'whitesugar87222@gmail.com', 'Not-delete', '2018-01-29 10:38:42', '2018-01-29 10:38:42');
INSERT INTO `newsletter` VALUES ('92', 'Sari.saif@hotmail.com', 'Not-delete', '2018-02-27 19:46:08', '2018-02-27 19:46:08');
INSERT INTO `newsletter` VALUES ('93', 'k_halid_76@hotmail.com', 'Not-delete', '2018-03-22 04:04:46', '2018-03-22 04:04:46');
INSERT INTO `newsletter` VALUES ('94', 'k_halid_76@hotmail.com', 'Not-delete', '2018-03-22 04:04:47', '2018-03-22 04:04:47');
INSERT INTO `newsletter` VALUES ('95', 'alieid001508@gmail.com', 'Not-delete', '2018-03-26 19:07:10', '2018-03-26 19:07:10');
INSERT INTO `newsletter` VALUES ('96', 'a3855584@hotmail.com', 'Not-delete', '2018-03-26 19:58:34', '2018-03-26 19:58:34');
INSERT INTO `newsletter` VALUES ('97', 's_h_h_a@hotmail.com', 'Not-delete', '2018-03-27 02:50:43', '2018-03-27 02:50:43');
INSERT INTO `newsletter` VALUES ('98', 'mohmad140819@gmail.com', 'Not-delete', '2018-03-27 03:39:58', '2018-03-27 03:39:58');
INSERT INTO `newsletter` VALUES ('99', 'tawfikrafi@gmail.com', 'Not-delete', '2018-03-27 04:01:29', '2018-03-27 04:01:29');
INSERT INTO `newsletter` VALUES ('100', 'w1w2w2w2@yahoo.com', 'Not-delete', '2018-03-27 12:40:21', '2018-03-27 12:40:21');
INSERT INTO `newsletter` VALUES ('101', 'obaidaliii@gmail.com', 'Not-delete', '2018-03-27 17:29:21', '2018-03-27 17:29:21');
INSERT INTO `newsletter` VALUES ('102', 'mohamadalhalabi4@gmail.com', 'Not-delete', '2018-03-27 17:52:21', '2018-03-27 17:52:21');
INSERT INTO `newsletter` VALUES ('103', 'netsparker@example.com', 'Not-delete', '2018-03-27 18:04:54', '2018-03-27 18:04:54');
INSERT INTO `newsletter` VALUES ('104', 'sakhigulswat87@gmail.com', 'Not-delete', '2018-03-29 09:41:23', '2018-03-29 09:41:23');
INSERT INTO `newsletter` VALUES ('105', 'samooralmansoory2@gmail.com', 'Not-delete', '2018-04-04 22:39:18', '2018-04-04 22:39:18');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `events_id` int(11) NOT NULL,
  `link` text,
  `isdefault` enum('Yes','No') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_menuitem_id` (`menuitem_id`),
  KEY `FK_events_id` (`events_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', '1', '1', '/admin/dashboard', 'Yes', '2017-10-09 05:48:36', '2018-06-19 13:24:06');
INSERT INTO `roles` VALUES ('2', '2', '1', '/admin/showfirm', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:03:39');
INSERT INTO `roles` VALUES ('3', '3', '1', '/admin/showservices', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('4', '4', '1', '/admin/showclub', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('5', '5', '1', '/admin/showcare', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('6', '6', '1', '/admin/showblog', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:04:29');
INSERT INTO `roles` VALUES ('7', '7', '1', '/admin/showmedia', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('8', '8', '1', '/admin/showconference', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('9', '9', '1', '/admin/showtraining', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('10', '10', '1', '/admin/showuser', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:08:38');
INSERT INTO `roles` VALUES ('11', '11', '1', '/admin/shownewsletter', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('12', '12', '1', '/admin/showcontactus', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('13', '13', '1', '/admin/showenquiriesmessage', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('14', '14', '1', '/admin/showcountry', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:08:42');
INSERT INTO `roles` VALUES ('15', '15', '1', '/admin/showcountry', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('16', '16', '1', '/admin/showadminuser', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');
INSERT INTO `roles` VALUES ('17', '17', '1', '/admin/change-password', 'Yes', '2017-10-09 05:48:36', '2018-06-19 17:13:03');

-- ----------------------------
-- Table structure for roles_permission
-- ----------------------------
DROP TABLE IF EXISTS `roles_permission`;
CREATE TABLE `roles_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `roles_id` text,
  `active` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_useridper` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles_permission
-- ----------------------------
INSERT INTO `roles_permission` VALUES ('2', '1', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,', '1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,', '2017-10-15 02:06:53', '2017-10-15 03:40:54');
INSERT INTO `roles_permission` VALUES ('3', '5', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,', '1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,', '2017-10-15 02:06:53', '2017-10-28 19:17:16');

-- ----------------------------
-- Table structure for rules
-- ----------------------------
DROP TABLE IF EXISTS `rules`;
CREATE TABLE `rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `crudtbl` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_iduserpriv` (`user_id`),
  KEY `FK_idscreenpriv` (`rule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rules
-- ----------------------------
INSERT INTO `rules` VALUES ('1', '1', '1', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:05');
INSERT INTO `rules` VALUES ('2', '1', '2', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:05');
INSERT INTO `rules` VALUES ('3', '1', '3', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:05');
INSERT INTO `rules` VALUES ('4', '1', '4', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:05');
INSERT INTO `rules` VALUES ('5', '1', '5', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:05');
INSERT INTO `rules` VALUES ('6', '1', '6', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:05');
INSERT INTO `rules` VALUES ('7', '1', '7', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('8', '1', '8', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('9', '1', '9', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('10', '1', '10', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('11', '1', '11', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('12', '1', '12', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('13', '1', '13', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('14', '1', '14', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('15', '1', '15', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('16', '1', '16', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('17', '1', '17', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('18', '1', '18', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('19', '1', '19', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('20', '1', '20', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');
INSERT INTO `rules` VALUES ('21', '1', '21', '1,1,1', '2017-09-14 11:34:00', '2017-09-14 11:34:06');

-- ----------------------------
-- Table structure for rules_partition
-- ----------------------------
DROP TABLE IF EXISTS `rules_partition`;
CREATE TABLE `rules_partition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partition_id` int(11) DEFAULT NULL,
  `headername` varchar(255) NOT NULL,
  `subheadername` varchar(255) DEFAULT NULL,
  `methodname` varchar(255) DEFAULT NULL,
  `methodid` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `arsubheadername` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rules_partition
-- ----------------------------
INSERT INTO `rules_partition` VALUES ('1', '0', 'Dashboard', 'Home', 'dashboard', null, '2017-09-14 07:53:57', '2017-09-14 08:06:05', null);
INSERT INTO `rules_partition` VALUES ('2', '1', 'Manage Top Banners', 'Add,Edit,Delete', 'banners', null, '2017-09-14 07:55:39', '2017-09-14 08:06:15', null);
INSERT INTO `rules_partition` VALUES ('3', '1', 'Manage Right Banners', 'Add,Edit,Delete', 'banners', null, '2017-09-14 07:57:38', '2017-09-14 08:06:15', null);
INSERT INTO `rules_partition` VALUES ('4', '1', 'Manage Footer Banners', 'Add,Edit,Delete', 'banners', null, '2017-09-14 07:57:40', '2017-09-14 08:06:16', null);
INSERT INTO `rules_partition` VALUES ('5', '2', 'Manage Categories', 'Add,Edit,Delete', 'product', null, '2017-09-14 07:59:47', '2017-09-14 08:06:17', null);
INSERT INTO `rules_partition` VALUES ('6', '2', 'Manage Products', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:17', null);
INSERT INTO `rules_partition` VALUES ('7', '2', 'Manage Colors', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:18', null);
INSERT INTO `rules_partition` VALUES ('8', '2', 'Manage Memory', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:19', null);
INSERT INTO `rules_partition` VALUES ('9', '2', 'Manage Currency', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:19', null);
INSERT INTO `rules_partition` VALUES ('10', '3', 'Manage Users', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:22', null);
INSERT INTO `rules_partition` VALUES ('11', '4', 'Manager Orders', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:23', null);
INSERT INTO `rules_partition` VALUES ('12', '4', 'Inventory', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:24', null);
INSERT INTO `rules_partition` VALUES ('13', '5', 'Reports Orders', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:25', null);
INSERT INTO `rules_partition` VALUES ('14', '6', 'Manage Newsletter', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:26', null);
INSERT INTO `rules_partition` VALUES ('15', '7', 'Manage Contact Us', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:27', null);
INSERT INTO `rules_partition` VALUES ('16', '7', 'Manage Enquiries', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:27', null);
INSERT INTO `rules_partition` VALUES ('17', '8', 'Manage Country', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:29', null);
INSERT INTO `rules_partition` VALUES ('18', '8', 'Manage City', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:30', null);
INSERT INTO `rules_partition` VALUES ('19', '9', 'General Configuration', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:31', null);
INSERT INTO `rules_partition` VALUES ('20', '9', 'Manage Admin User', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:32', null);
INSERT INTO `rules_partition` VALUES ('21', '9', 'Change Password', 'Add,Edit,Delete', null, null, null, '2017-09-14 08:06:34', null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_country` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` enum('F','M') DEFAULT NULL,
  `status` enum('Not-Active','Active') DEFAULT NULL,
  `softdelete` tinyint(4) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `creeated_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `typeacc` enum('super-admin','admin','user') NOT NULL,
  `confirmation_code` varchar(100) DEFAULT NULL,
  `imgpath` varchar(500) DEFAULT NULL,
  `confirmacc` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '235', 'Support', 'Support', '0126354789', 'support@ilaw.ae', '$2y$10$LTxSct.DBVTowTOaDlJKsu80pTj3t716YHit5MF45F7eeKwt1avdW', 'M', 'Active', '0', null, '2018-06-19 06:51:36', '2018-06-19 06:59:22', null, 'super-admin', null, null, '1');
